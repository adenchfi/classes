import requests
import json

Navy_Pier = {"lat": 41.892000, "lng": -87.605217, "ID": 'Navy Pier', "WK_SC": 76, "TR_SC": 73}
Mill_Park = {"lat": 41.882997, "lng": -87.622557, "ID": 'Mill. Park', "WK_SC": 98, "TR_SC": 100}
Greektown_West_Loop = {"lat": 41.881551, "lng": -87.646613, "ID": 'Greektown', "WK_SC": 94, "TR_SC": 100}
IIT = {"lat": 41.831108, "lng": -87.628706, "ID": 'IIT', "WK_SC": 75, "TR_SC": 79}
Adam = {"lat": 41.838064, "lng":-87.644652, "ID": 'Bridgeport', "WK_SC": 90, "TR_SC": 60}
Chinatown = {"lat": 41.852108, "lng": -87.632041, "ID": 'Chinatown', "WK_SC": 96, "TR_SC": 81}
Soldier_Field = {"lat": 41.862887, "lng": -87.618196, "ID": 'Soldier Field', "WK_SC": 42, "TR_SC": 78}
Roose_State = {"lat": 41.867219, "lng": -87.627327, "ID": 'Roosevelt and State', "WK_SC": 95, "TR_SC": 100}
Old_Town = {"lat": 41.907051, "lng": -87.638465, "ID": 'Old Town', "WK_SC": 94, "TR_SC": 84}
UIC = {"lat": 41.871405, "lng": -87.649880, "ID": 'UIC', "WK_SC": 93, "TR_SC": 75}
W_87th_St = {"lat": 41.739491, "lng": -87.630553, "ID": 'W 87th Street', "WK_SC": 65, "TR_SC": 71}

points_list = [Navy_Pier, Mill_Park, Greektown_West_Loop, IIT, Adam, Chinatown, Soldier_Field, Roose_State, Old_Town, UIC, W_87th_St]
reply = {}
scores_dict = {}
MY_API_KEY = "3EeYtwtiHxqOIHWI38gI8GzTkQO7ZWzo"
for point in points_list:
    url = "https://api.spatial.ai/neighborhood/point/social_score?lat={}&lng={}&apikey={}".format(
        point['lat'], point['lng'], MY_API_KEY
    )
    response = requests.get(url)
    reply[point['ID']] = response.json()
    scores_dict[point['ID']] = {'WK_SC': point['WK_SC']}
    scores_dict[point['ID']]['TR_SC'] = point['TR_SC']
    name = point['ID']
    tmp = scores_dict[name]['social_scores'] = reply[name]['social_score']['quarters'][1]['scores']
#    scores_dict[name][]
# now we have the Social Point Scores for a couple different points.
# For information on what the data looks like, look at https://s3.amazonaws.com/spatial-public/docs/SpatialNeighborhoodAPIOverview.pdf
#print(scores_dict)
with open('test.txt', 'w') as outfile:
        json.dump(scores_dict, outfile)
