(TeX-add-style-hook
 "Wackerly_Multivar_Prob_Dists"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "math_pkgs"
    "article"
    "art10"
    "hyperref")
   (LaTeX-add-labels
    "def:multinomial"
    "eq:bivariate_normal"
    "eq:cond_exp"
    "thm:exp_of_expect"
    "thm:cond_var"))
 :latex)

