\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Crystal Structures}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Cubic Lattices}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Hexagonal Lattices}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Crystal Lattice Planes}{2}{subsection.2.3}
\contentsline {paragraph}{Miller Indices}{2}{section*.2}
\contentsline {subsubsection}{\numberline {2.3.1}Diamond}{3}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Zincblende}{3}{subsubsection.2.3.2}
