import matplotlib.pyplot as plt
from numpy import cos
from numpy import arange
from math import pi

def E(E_v, B, g, k, a):
    return E_v - B - 2*g*cos(k*a)

Es = []
a = 4e-10
ks = arange(0, pi/a, (pi/a)/100)

Ev = 0
b = 0
G = 1

for k in ks:
    Es.append(E(Ev, b, G, k, a))

plt.plot(ks, Es)
plt.show()
