\select@language {english}
\contentsline {section}{\numberline {1}Phonons}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Homework Assignment}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Linear chain of N atoms}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Transverse Modes}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Enumeration of Modes}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Compounds}{3}{subsection.1.5}
\contentsline {section}{\numberline {2}Approximation Validation - Generalized Dispersion}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Photon/Phonon Momentum}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Elastic Scattering of Neutron/Photon Beam}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Inelastic Scattering of Neutron/Photon Beam}{5}{subsection.2.3}
