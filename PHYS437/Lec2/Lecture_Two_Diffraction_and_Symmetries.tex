\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Crystal Symmetries and Diffraction \\ Kittel Ch. 2}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}
Recall the basic Bragg law
\begin{equation}
  \label{eq:bragg}
  2d \sin \theta = n\lambda.
\end{equation}
The lattice plane spacing is $d$, and the path differences for rays reflected from adjacent planes is $2d\sin\theta$. The wavelength $\lambda$ can only be $\leq 2d$.

\section{Symmetries}

A crystal has a variety of invariances in terms of transformations. Rotations, inversions, and translations are generally the operations that leave a crystal structure invariant. See Tinkham's Group Theory and QM for more information.

Here we discuss translational symmetries in crystals. In particular, for any translation vector of the form $T = u_1a_1 + u_2a_2 + u_3a_3$, where the $a_i$ are crystal axes, we have invariance of:
\begin{itemize}
\item Charge concentration
\item Electron number density
\item Magnetic moment density
\item Any other local physical property of the crystal. 
\end{itemize}

We deal with the electron \textbf{number density} $n(\mathbf{r})$. From the previous discussion, we can see that
\begin{equation}
  n(\mathbf{T + r}) = n(\mathbf{r}). 
\end{equation}
Such a periodicity property (translational symmetry) creates an ideal situation for Fourier analysis, because we know that sines and cosines are periodic functions. They could be construed as eigenfunctions of the translation operator $T(n(\mathbf{r})) = n(\mathbf{T + r})$.

For a 1D crystal with period $a$, we consider $n(x)$ of the form
\begin{equation}
  n(x) = n_0 + \sum_{p>0} [C_p \cos(2\pi p x/a) + S_p \sin(2\pi p x/a)]
\end{equation}
where the $p$ are positive integers and $C_p, S_p$ are real constants. The factor $2\pi /a$ ensures that $n(x)$ has period $a$.

We then consider the \textbf{reciprocal lattice} or Fourier space of the crystal. The $2\pi p/a$ points are points on this reciprocal lattice, and tell us the allowed terms in the previous Fourier series. Recall $k_p = 2\pi p/a$, the wavenumber. We can even write the previous expressions in the form
\begin{equation}
  n(x) = \sum_p n_p \exp(i2\pi px/a),
\end{equation}
where the sum is over \textbf{all} integers. The coefficients $n_p$ are now complex numbers. To ensure $n(x)$ is a real function, we then require
\[
  n_{-p}^* = n_p,
\]
where the asterisk denotes the complex conjugate.

\subsection{Three Dimensions}

We can extend this analysis to three dimensions. In particular, we wish to find a set of vectors $G$ such that
\begin{equation}
  n(\mathbf{r}) = \sum_G n_G \exp(i\mathbf{G\cdot r})
\end{equation}
is invariant under \textbf{all} crystal translations $\mathbf{T}$ that leave the crystal invariant. It will then be shown that the coefficients $n_G$ determine the x-ray scattering amplitude.

For one dimension, we find that the coefficients $n_p$ are
\begin{equation}
  n_p = a^{-1} \int_0^a n(x) \exp(-2i \pi px/a) dx.
\end{equation}

Similarly, we find for 3D space that the coefficients $n_G$ are
\begin{equation}
  n_G = V_G^{-1} \int_{cell} n(\mathbf{r}) \exp(-i \mathbf{G \cdot r}).
\end{equation}

Here, $V_G$ is the volume of a cell of a crystal. 

\subsection{Reciprocal Lattice Vectors}

It turns out that with a little vector analysis we can find all possible vectors $\mathbf{G}$ that satisfy translational invariance. First we construct the axis vectors $\mathbf{b_i}$ of the \textbf{reciprocal lattice}:
\begin{align}
  \mathbf{b_1} = 2\pi \frac{\mathbf{{a_2} \cross \mathbf{a_3}}}{\mathbf{a_1} \cdot (\mathbf{a_2} \cross \mathbf{a_3})} \\
  \mathbf{b_2} = 2\pi \frac{\mathbf{{a_1} \cross \mathbf{a_3}}}{\mathbf{a_1} \cdot (\mathbf{a_2} \cross \mathbf{a_3})} \\
  \mathbf{b_3} = 2\pi \frac{\mathbf{{a_1} \cross \mathbf{a_2}}}{\mathbf{a_1} \cdot (\mathbf{a_2} \cross \mathbf{a_3})} \\
\end{align}
\begin{remark}
  Note that the denominators are the volume of the cell. Also note the factor $2\pi$ is not used by crystallographers, but are convenient in solid state physics. 
\end{remark}

Each $\mathbf{b_i}$ is orthogonal to two of the axis vectors, per the definition of the cross product. Thus the $\mathbf{b_i}$ have the property
\begin{equation}
  \mathbf{b_i \cdot a_j} = 2\pi \delta_{ij}. 
\end{equation}

Here is where we get out $\mathbf{G}$. It turns out that if we let $\mathbf{G}$ be the vectors that correspond to points in the reciprocal lattice,
\[
  \mathbf{G} = v_1 \mathbf{b}_1 + v_2 \mathbf{b}_2 + v_3 \mathbf{b_3},
\]
where the $v_i$ are integers. This form of $\mathbf{G}$ is called a reciprocal lattice vector. Note that then there is the desired invariance $n\mathbf{(r + T)} = n(\mathbf{r})$. This can be seen by writing
\begin{equation*}
  n(\mathbf{r + T}) = \sum_G n_G \exp(i\mathbf{G \cdot r}) \exp(i\mathbf{G \cdot T}) 
\end{equation*}
and noting $\exp(i\mathbf{G \cdot T}) = \exp(i2\pi (v_1 u_1 + v_2 u_2 + v_3 u_3)) = 1$ because the $u_i, v_j$ are integers, we then have the desired invariance. 

\end{document} % END



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
