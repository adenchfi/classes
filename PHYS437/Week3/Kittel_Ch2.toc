\select@language {english}
\contentsline {section}{\numberline {1}Diffraction Conditions (continued)}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Deriving Bragg's Law}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Brillouin Zones of 2-d Square Lattice}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}FCC Real Space}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Perturbation Theory}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Structure Factor}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}FCC}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Form Factor}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}CHapter 4 - Phonons}{5}{section.4}
