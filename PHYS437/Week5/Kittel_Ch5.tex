\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}



\begin{document}
\title{Thermal Properties of Phonons \\ Kittel Ch. 5}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}
Now we would like to compute the stored energy of a solid, given its phonon modes and number of phonons in each of those modes. Recall there are $10^8$ modes in a centimeter of a solid. Energy of a certain vibrational modes is \[
E= n\hbar \omega_k
\] where $n$ is the number of phonons in the mode. However, if we take a thermal average, \[
E_{av} = <n> \hbar w_k.
\]
Consider $N$ identical modes in thermal equilibrium, \[
\frac{N_{n+1}}{N_n} = e^{-\hbar \omega / \tau}, \quad \tau = k_b T
\] where the exp gives you the ratio of the number of states with $n + 1$ modes in them to the number of states with $n$ modes in them. Occupancy number! Taking a thermal average now,
\begin{align*}
  <n> = \sum_s s P(s) = \frac{\sum_s s e^{-s\hbar \omega/\tau}}{\sum_s e^{-s\hbar \omega/\tau}}.
\end{align*}
The denominator is a geometric series. Also,
\begin{equation}
  \sum_s s x^s = \frac{x}{(1-x)^2}
\end{equation}
which means
\begin{equation}
  <n> = \frac{x}{(1-x)^2}(1-x) = \frac{x}{x-1} = \frac{1}{e^{\hbar \omega/\tau} - 1}
\end{equation}
which is the Planck distribution. As the temperature goes to 0, $<n>$ goes to zero. However, if $T >> \hbar \omega$, then \[
<n> = \frac{k_B T}{\hbar \omega}
\] since we can expand the exponential to first order.
\begin{example}
  If $\hbar \omega = 10meV$, and $\tau = k_B T = 100meV$, then $<n> = 10$. 
\end{example}

If we know the average occupancy, we can get the average energy: \[
<E> = <n> \hbar \omega = \frac{\hbar \omega}{e^{\hbar \omega / k_B T} - 1}
\] and for $\tau >> \hbar \omega$, $<E> = \hbar \omega (k_B T/\hbar \omega)$ and thus \[
<E> = k_B T
\] which gives us the equipartition theorem. \[
<E> = <K> + <U>
\] where each degree of freedom gives $(1/2)k_b T$. If you have $N$ nodes, \[
<E> = Nk_B T = nRT.
\]

\section{Total Vibrational Energy in Lattice}

This is known as the lattice contribution to the specific heat. When we consider metals, we'll also have to consider the electronic contribution. The previous section only covered energy contributions from a particular mode $k$.\[
<E> \equiv U = \sum_k \sum_p <n>_{k,p} \hbar \omega_{k, p}
\] where we refer to $p$ as the possible polarizations (or branches) of our lattice phonons, and each branch has its own $\omega(k)$. 
\subsection{3D}

There are around $10^24$ k=values. We'll replace the acoustic branch with a straight line, and optical branches with a flat line for approximations. We'll turn the sums into integrals. These are called Einstein modes. This will result in the Debye model. Steps:
\begin{enumerate}
\item Replace actual dispersion curve with straight line.
\item Assume differences between branches is small enough that we can instead treat them the same, and multiply the result for those by 3.
\item Take a discrete sum on $k$, and replace it with an integral. When we do this, the units differ; so we need to add in a term that accounts for that. We add in $N_k$:
  \begin{equation}
    \sum_k [] = \int_k N_k [] d^3 k = \int D(\omega) d\omega
  \end{equation} where we are trying to turn the 3D integral into a 1D one integrating over the possible frequencies, with frequency density $D(\omega)$.
  \begin{align*}
    1D: \quad \sum_k \implies \int (N_k)_{1D} dk \implies \int D(\omega)_{1-D} d\omega \\
    2D: \quad \sum_k \implies \int (N_k)_{2D} dk \implies \int D(\omega)_{2-D} d\omega \\
    3D: \quad \sum_k \implies \int (N_k)_{3D} dk \implies \int D(\omega)_{3-D} d\omega.
  \end{align*}
\end{enumerate}
Let's address the 1D case. $N \sim 10^8$, which means there are $10^8 \ k_n$, with 1 k-mode each $2\pi / L$. The density of odes $N_k = \frac{1}{(2\pi / L)} = \frac{L}{2\pi}$.

\[
\sum_k = 2\sum_{k \geq 0} = 2\int_0^{\pi/a} \frac{L}{2\pi} dk = \frac{2\pi}{a}\frac{L}{2\pi} = L/a = N
\] which is what we wanted. Now, on our dispersion curve $w(k) vs k$, we want to turn an integral over $k$ to an integral over $\omega$. We can write
\begin{equation}
  d\omega D(\omega) = 2N_k dk = 2\frac{L}{2\pi} dk 
\end{equation}
and in 1-D, \[
D(\omega) = \frac{L}{\pi} \frac{1}{d\omega/dk}. 
\] There is a singularity at $\pi/a$ (van-hove singularity) because $\pdv{\omega}{\kappa} = 0$ there.

\paragraph{Debye Approximation}

Replace the dispersion curve with a straight line, whose slope is the initial slope of the dispersion curve, $\omega(k) \approx v_s k$. Then $\pdv{w}{k} = v_s$ and $D(\omega) = \frac{L}{\pi v}$.
\begin{remark}
  Why not replace the dispersion curve with the \textbf{average} slope of the line? Dr. Z says to try it out and see what happens. 
\end{remark}
This approximation cannot mis-count the number of modes: \[
\int_0^{\omega_D} D(\omega) d\omega = N. 
\]
With this constraint, we get an approximate Debye frequency $\omega_D$ that is never measured. 
\subsubsection{2-D}

In 2D, there's about $10^{16}$ modes. If we have cubic symmetry, the k-space forms a square lattice. One mode in eahc square of area $(\frac{2\pi}{L})^2$.
\begin{equation*}
  N_k = \frac{\text{1 mode}}{(\frac{2\pi}{L})^2} = \frac{L^2}{4\pi^2}
\end{equation*}
If we assume $\omega_D$ depends only on $\abs{\vec k}$, where $\vec k = k_x \hat x + k_y \hat y$, then we can get a density. $\omega$ is constant and has circular symmetry.
\begin{align*}
  dN = D(\omega) d\omega = \frac{L^2}{4\pi^2}(2\pi k)dk \\
  D(\omega) = \frac{L^2}{2\pi} \frac{k}{\pdv{w}{k}} \tag{Debye Approx: w = vk} \\
  D(\omega) = \frac{L^2}{2\pi} \frac{\omega}{v\cdot v} = \frac{L^2}{2\pi}\frac{\omega}{v^2}
\end{align*}

\subsubsection{3-D}

Assume a cubic crystal of volume $L^3$.  Density of modes in k-space is
\begin{equation}
\frac{L^3}{8\pi^3} = N_k
\end{equation}
and we have a spherical shell of constant $\abs{\vec k}$. Doing what we did before, we say
\begin{align*}
  D(\omega) d\omega = dN = & \frac{L^3}{8\pi^3} 4\pi k^2 dk \\
  = & \frac{L^3 k^2}{2\pi^2 \frac{d\omega}{dk}} \tag{Make Debye approx} \\
  = & D(\omega) = \frac{L^3}{2\pi^2} \frac{\omega^2}{v^2} = \frac{v\omega^2}{2\pi^2 v^3}
\end{align*}
Recall our constraint; our model has to  account for all the modes of the system: \[
\int_0^{\omega_D} D(\omega) d\omega = N 
\] which means
\begin{align*}
  \frac{V}{2\pi^2 v^2} \int_0^{\omega_D} \omega^2 d\omega = \frac{V}{2\pi^2v^2}\frac{\omega_D^3}{3}  = N\\
  \implies \omega_D = \left[(N/V) 6\pi^2 v^2 \right]^{1/3}
\end{align*}
We can get a $k_D$ from this as $k_D = \frac{\omega_D}{v}$, which turns out to be 1.24 times $\pi/a$. 
\subsubsection{Overview}

In 1D, $D(\omega)$ is linear. In 2D, $D(\omega)$ is linear in $\omega$. In 3D, $D(\omega)$ is quadratic in $\omega$. 

\subsection{Stored Thermal Energy (Lattice)}

\begin{remark}
  Volume/length expansion due to heat can be seen as a sum of displacements of atoms from equilibrium, which can be connected to phonons and modes, etc. 
\end{remark}

For a single branch,
\begin{align*}
  U = \int_0^{\infty} <n>\hbar \omega D(\omega) d\omega \\
  U = \int_0^{\omega_D} \frac{V}{2\pi^2 v^2}\omega^2 [\frac{\hbar \omega}{e^{\hbar \omega/\tau} - 1}] d\omega
\end{align*}
If we approximate each branch as being the same, we then write it as 3 x the single branch term. Plugging that in, we get
\begin{equation}
  U = 9Nk_b T (\frac{T}{\theta_D})^3 \int_0^{k_D} \frac{x^3}{e^x - 1} dx
\end{equation}
The heat capacity is $C_V = \pdv{U}{T}\Big|_{V} \sim (\frac{T}{\theta_D})$. $\theta_D = \hbar \omega_D / k_B$. 

\begin{remark}
  This model gives a decent approximation for heat capacities of solids. One approximation wouldn't work if we have a layered anisotropic material. 
\end{remark}

\section{Debye Temperature Review}

Recall $\int_0^{\omega{_d}} D(\omega) d\omega = N$, where $N$ is the number of primitive cells (lattice points). \[
\omega_D = v(N/V)^{1/3} (6\pi^2)^{1/3}
\] and the Debye Temperature is \[
\theta_D = \frac{\hbar \omega_D}{k_B} \propto v \propto \sqrt{\frac{C}{M}}
\] where the last is related to a spring constant $C$ with atoms of mass $M$. Using the Debye temperature, we can say at room temperature that lots of modes in say Pb is occupied.

\[
<n> = \frac{1}{e^{\hbar \omega / k_B T} - 1}
\] and \[
E = <n>\hbar \omega.
\]
At $k_B T >> \hbar \omega$, $E \approx k_B T$.

Recall the lattice thermal energy \[
U = 9Nk_B T (T/\theta_D )^2 \int_0^{x_D} \frac{x^3}{e^x - 1}dx
\] where $x \equiv \frac{\hbar \omega}{k_B T}$.

For low $T$, $x_D \rightarrow \infty$, and we can solve the integral in $U$ above to get $\frac{\pi^4}{15}$.

\begin{remark}
  Heat capacity $C_v = 3Nk_B = \pdv{u}{T}$ for $T >> \theta_D$. The specific heat is gained by dividing by your desired unit of measurement, e.g. specific heat = C / kg, or C/mole, C/Volume, etc. 
\end{remark}
We can say $U = 3NK_b T (T^3 / \theta^3)$ where the last term is a hand-wavey idea of being the fraction of the phonon modes present. 
\subsection{Multiple Atoms in Lattice}

For multiple branches, we can say $C_v = [\cdot] [2T^3 / \theta^3_{TA} + T^3/\theta^3_{LA}] = [\cdot] T^3/\theta^3_{eff}$.
Furthermore, we assume the optical modes have flat dispersion curves - known as the Einstein approximation.

\[
U = N<n>\hbar \omega = \frac{N\hbar \omega}{e^{\hbar \omega / \tau} - 1}.
\]

\begin{remark}[Different Temperatures]
  We have to switch models from a $T^3$ one to the linear one for $c_v$, etc, when we get close to the Debye temperature. 
\end{remark}
\begin{remark}[2D]
  If we're working with lower-dimensional materials, the formulation previously would instead get $T^2/\theta^2$ terms. This would result in a $T^2$ power law instead. On a log-log scale, we'd expect a slope of 2. However, instead we see a power law at low T with data. 
  
\end{remark}
  In 3D, $D(\omega) \propto \omega^2$ because the area of a sphere. 
\end{document} % END

\subsection{General Result for D(w)}

\begin{equation} \label{DOS}
  D(\omega) = \frac{V}{8\pi^3} \int \frac{dS_\omega}{v_G}
\end{equation} where $v_G$ is the group velocity.

If we have an energy vs wave-vector curve (or $\omega vs k$), then we can note that the points where that curve is 0 is where there will be kinks/peaks in the DOS, which is phonon DOS vs phonon frequency.

\subsubsection{How to get DOS from Phonon Dispersion Curve}

Get your 3D phonon curves in your brillouin zone. You'd go along a direction, choose a particular k point, get all the
points at which the energy/omega is constant, get the sum and divide by the group velocity. Like in Eq. \eqref{DOS}. Do that for each k-point you go along,
and do it for different paths on your Brillouin zone. 

\section{Thermal Conductivity}

We see experimentally that the heat flux $j$ obeys \[
j = -\kappa \dv{T}{x}
\]
where \[
\dv{T}{x} = \frac{\Delta T }{L}. 
\]
Since we see a length dependence, that means the phonons must be scattering instead of propagating freely. They can scatter at defects, or even with each other if they're not independent (if we can't make the harmonic approximation).

$<v_x^3> = \frac{1}{3}<v^3>$.
We can say our constant \[
\kappa = \frac{1}{3} c v l
\]
where $c$ is a volume specific heat, $v$ is the speed of propagation/sound in the material, and $l$ is the mean free path. 
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
