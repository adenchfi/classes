\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Total Vibrational Energy in Lattice}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}3D}{3}{subsection.2.1}
\contentsline {paragraph}{Debye Approximation}{4}{section*.2}
\contentsline {subsubsection}{\numberline {2.1.1}2-D}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}3-D}{4}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Overview}{5}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Stored Thermal Energy (Lattice)}{5}{subsection.2.2}
\contentsline {section}{\numberline {3}Debye Temperature}{5}{section.3}
