\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Introduction to Linear Optimization \\ Bertsimas Ch. 1}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}

\subsection{Linear Algebra}

Linear Optimization can be used to solve a variety of problems. As can be inferred from the title, it is related to linear algebra. Things we will be using from linear algebra include:
\begin{itemize}
\item Matrices (systems of linear equations) - Gauss-Jordan Elimination Algorithm
\item Rank, proerties of vector spaces, linear independence
\item Basis, subspaces
\end{itemize}

Real applications might include high-dimensional spaces. Dimensions can refer to degrees of freedom, or choices you have to make. 

\subsection{Optimization}

The point of optimization is to minimize/maximize some function $f(\vec{x})$ such that $\vec{x}$ satisfies certain conditions or constraints, or belongs to a set $S$. This viewpoint allows you to better model certain problems.
\begin{itemize}
\item The $x_{i}$ are known as the \textbf{decision variables}, 
\item $f$ is the \textbf{objective function}. 
\item $S$ is called the \textbf{constraint set}. 
\end{itemize}

\subsection{What is Linear Optimization?}
For linear optimization, $f(\vec{x}) = \vec{c}^{T}\vec{x} = $
\[
  \begin{bmatrix}
    c_{1} & c_{2} & \dotsc & c_{n}
  \end{bmatrix}
  \begin{bmatrix}
    x_{1} \\ x_{2} \\ \vdots \\ x_{n}
  \end{bmatrix}
  = c_{1}x_{1} + c_{2}x_{2} + \dotsc + c_{n}x_{n}
\]
For notation purposes, we will refer to every vector as a column vector. Furthermore, we will start to refer to vectors $\vec{v}$ as $\mathbf{v}$.

For Linear Optimization problems, the set $S$ is defined by $m$ linear constraints:
\[
  S = \{x \in \mathcal{R}^{n} |
  \begin{cases}
    a_{i}^{T} x \leq b_{i}, & i = 1, \dotsc, m_{1} \\
    a_{i}^{T}x = b_{i}, & i= m_{1}+1, \dotsc, m_{2} \\
    a_{i}^{T}x \geq b_{i}, & i=m_{2}+1, \dotsc, m
  \end{cases}
\]
where $a_{i} \in \mathcal{R}^{n}$ and $b$ is a scalar.
\begin{remark}
  For notation purposes, for note-writing purposes, I may omit the vector or bold sign on variables. Context should be clear on whether or not they are vectors. 
\end{remark}
\begin{remark}
  There are infinitely many solutions to these sorts of problems. However, it can be shown that there are only finitely many choices for certain linear optimization problems. The Simplex method was developed for this purpose. 
\end{remark}
You can even approximate nonlinear problems as a collection of linear problems.

\begin{itemize}
\item $\mathbf{x}$ that belongs to $S$ is called a \textbf{feasible solution}
\item Feasible solution $\mathbf{x}$ that optimizes $f(x)$ is called an \textbf{optimal solution}. 
\end{itemize}

\paragraph{Observations}

\begin{enumerate}
\item $a_i^T x = b_i$ is equivalent to $a_i^Tx \leq b_i$ \textbf{and} $a_i^Tx \geq b_i$
\item $a_i^T x < b_i$ can be rewritten as $(-a_i)^T x \geq -b_i$
\item $\max c^Tx$ is equivalent to $\min (-c)^T x$
\end{enumerate}

By using these observations, we can simplify our notation and our minimization problem. We can write any linear optimization problem (LP) as
\begin{equation}
  \label{eq:LP}
  \min c^T x | A\vec{x} \geq \vec{b}
\end{equation}
where $A$ is a matrix of the $a_i$ vectors as row vectors. \[
  A =
  \begin{bmatrix}
    \mathbf{a}_1^T \\
    \mathbf{a}_2^T \\
    \vdots \\
    \mathbf{a}_m^T \\
  \end{bmatrix}
\]
and $\vec{b}$ is an $m\cross 1$ column vector.

\begin{example}
  $\max f(x) = -2x_1 + x_2 - 4x_3$ subject to
  \begin{align*}
    -x_1 - x_2 - x_4 \geq -2 \\
    3x_2 - x_3 = 5 \\
    -x_3 - x_4 \leq -3 \\
    x_1 \geq 0 \\
    x_3 \leq 0.
  \end{align*}
  In this case, we want to make all of the constraints into a $\geq$ problem.
  \begin{itemize}
  \item We rewrite $3x_2 - x_3 = 5$ as $3x_2 - x_3 \leq 5$ \textbf{and} $3x_2 - x_3 \geq 5$. The first becomes $-3x_2 + x_3 \geq -5$
  \item  We also rewrite $x_3 \leq 0$ as $-x_3 \geq 0$.
  \item Now we can create $\mathbf{c}^T$ and $A$ and properly define our minimization problem.
  \end{itemize}
  \[
    \min c^T x =
    \begin{bmatrix}
      2 & -1 & 4 & 0
    \end{bmatrix}
    \begin{bmatrix}
      x_1 \\ x_2 \\ x_3 \\ x_4
    \end{bmatrix}
    \text{such that}
    A =
    \begin{bmatrix}
      -1 & -1 & 0 & -1 \\
      0 & -3 & 1 & 0 \\
      0 & 3 & -1 & 0 \\
      0 & 0 & 1 & 1 \\
      1 & 0 & 0 & 0 \\
      0 & 0 & -1 & 0
    \end{bmatrix}
    \begin{bmatrix}
      x_1 \\ x_2 \\ x_3 \\ x_4
    \end{bmatrix}
    \geq
    \begin{bmatrix}
      -2 \\ -5 \\ 5 \\ 3 \\ 0 \\ 0
    \end{bmatrix}
  \]
\end{example}

\begin{remark}
  Later on, we will turn these problems into linear problems $A\mathbf{x} = \mathbf{b}$ for computational problems. 
\end{remark}

\subsection{Standard Form}

\begin{equation}
\min \mathbf{c}^T\mathbf{x} \text{ such that } A\mathbf{x} = \mathbf{b} \text{ and } x \geq \mathbf{0}
\end{equation}

By definition, $x, c \in \mathcal{R}^n, b \in \mathcal{R}^m$, and $A$ is $m \cross n$.

\subsubsection{Steps for Creating a Linear Optimization Problem}

\begin{enumerate}
\item $x_i \leq 0 \rightarrow -x_i \geq 0$ 
\item Elimination of free variables: If $x_j$ is free, then replace it as $x_j = x_j^+ - x_j^-$ where $x_j^+, x_j^- \geq 0$. That way we keep our variables $x_j \geq 0$  in our problems. 
\item $a_i^T x \leq b_i \iff a_i^T x + s_i = b_i$ where $s_i \geq 0$ is known as a \textbf{slack variable}.
\item Similarly, $a_i^T x \geq b_i \iff a_i^T x - s_i = b_i$ where $s_i \geq 0$ is known as a \textbf{surplus variable}. 
\end{enumerate}

Applying these rules yields the \textbf{Standard Form} mentioned above. 

\section{Summary of Course}

First half of the course is dedicated to understanding the development of the simplex algorithm.

Second half of the course is more along the lines of error, extra restrictions, maybe network optimization.
\begin{remark}
  Field of math/programming focused on problems where values can only take the values 0, 1. Consider the connection to fermions and fermi statistics. 
\end{remark}
\begin{remark}
  Can replace a couple of homeworks with our project if I want. 
\end{remark}

\section{Lecture Two}

Recall the standard form
\begin{align*}
  \min c^T x \\
  s.t. Ax = & b \\
  x \geq & 0
\end{align*}

\begin{example}
  Turn the following into standard form: 
  \begin{align*}
    \min 2x_1 + 4x_2 \\
    s.t. \\
    x_1 + x_2 & \geq 3 \\
    3x_1 + 2x_2 & = 14\\
    x_1 & \geq 0
  \end{align*}
  \begin{solution}
    Since $x_2$ is free, let $x_2 = w_2 - z_2$ where $w_2, z_2 \geq 0$. Also introduce slack variables when needed to turn it into an equality.
    \begin{align*}
      \min 2x_1 + 4w_2 - 4z_2 \\
      \text{such that} \\
      x_1 + w_2 - z_2 - s_1 & = 3 \\
      3x_1 + 2w_2 - 2z_2 &= 14\\
      x_1, w_1, z_1, s_1 & \geq 0
    \end{align*}
  \end{solution}
\end{example}

\begin{example}
  Factory produces chairs and tables. Here are the parameters:
  \begin{itemize}
  \item The profit is \$5/chair and \$10/table.
  \item 1 chair uses 0.01 $m^3$ of wood and $0.5 m^2$ leather, and takes 10 minutes
  \item 1 table uses $.04 m^3$ wood, and $1 m^2$ leather, taking 5 minutes. 
  \item Factory has $10 m^3$ of wood and $50m^2$ of leather, with a 10 hour shift. 
  \end{itemize}
  We want to optimize the profit. We want to allocate resources.

  Let $x_1$ be equal to the number of chairs to be manufactured, and $x_2$ the number of tables to be manufactured. 
  \begin{align*}
    \max 5x_1 + 10x_2 \quad s.t.\\
    0.02x_1 + 0.04x_2 \leq & 10 \\
    0.5x_1 + 1.0x_2 \leq & 50 \\
    10x_1 + 5x_2 \leq & 600 \\
    x_1, x_2 \geq & 0 \\
    x_1, x_2 \in & \mathbb{Z}. 
  \end{align*}
  We would then turn this into standard form. 
\end{example}

\begin{example}[Transportation Problem]
  Given a network (directed graph) $\mathcal{G} = (N, A)$ ($N$ = nodes/vertices, $A$ = axes/edges). Each arc $(i, j)$ has capacity bounds $l_{ij}, u_{ij}$ and cost $c_{ij}$ per good $x_{ij}$. Each node $i$ has a supply/demand $b(i)$ associated with it. If $b(i) > 0$, it is a factory. If $b(i) < 0$, it is a retail store/warehouse. If $b(i) = 0$, it is a distribution center (whatever goes in comes out).

  The drawing is of a directed graph.

  You want to distribute your goods as efficiently as possible. You want to satisfy some constraints and minimize/maximize some objective function.
  How do I represent this mathematically?
  \begin{solution}
    How many goods need to travel on each $(i, j)$ arc? If, for node $i$, $b(i) = 0$, we want the inflow = outflow. For $b(i) > 0$, we want inflow + $b(i)$ = outflow, and vice versa for $b(i) < 0$. 

    Let $x_{ij}$ be the ``flow'' on the arc $(i, j)$ out of all possible arcs $A$. We would like to minimize the total cost $\sum c_{ij}$. Our problem is then
    \begin{align*}
      \min \sum_{(i, j) \in A} c_{ij} x_{ij} \\
      \text{such that outflow - inflow} = b(i) \forall i\in N \\
      \sum_{j: (i, j) \in A} x_{ij} - \sum_{j: (j, i) \in A} x_{ji} = b(i) \forall i\in N\\
      x_{ij} \geq l_{ij} \forall (i, j) \in A\\
      x_{ij} \leq u_{ij} \forall (i, j) \in A \\
    \end{align*}
    This is often referred to as a transportation or distribution problem.
    \begin{remark}[Connection to Physics]
      Can we use this stuff to model flow of electrons? Diffusion on discrete/finite paths? 
    \end{remark}
  \end{solution}
\end{example}

\subsection{Geometric Insight}

\begin{example}
\begin{align*}
  \min -x_1 - x_2 \\
  \text{such that} \\
  x_1 + 2x_2 \leq & 3 \\
  2x_1 + x_2 \leq & 3 \\
  x_1, x_2 \geq & 0
\end{align*}
For each scalar $z$ with $-x_1 - x_2 = z$, the equation of a straight line with normal given by $c = (-1, -1)$. The objective function is a family of straight lines. We need to pick one of those straight lines that gives us the best value and is in our feasible set of solutions (see picture). For 2D problems such as depicted, our set of solutions will be in the family of lines perpendicular to the line/plane defined by the objective function. 

\end{example}

When we change $LP(1)$ to $LP(2)$ (non-standard to standard form), why are the two LPs the ``same''? For example, we often introduce new variables...
\begin{definition}
  Two LPs are said to be \textbf{equivalent} if given a feasible solution to one problem, we can find a feasible solution to the other with the same cost (value of the objective function). 
\end{definition}

\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
