\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Linear Algebra}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Optimization}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}What is Linear Optimization?}{2}{subsection.1.3}
\contentsline {paragraph}{Observations}{3}{section*.2}
\contentsline {subsection}{\numberline {1.4}Standard Form}{4}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Steps for Creating a Linear Optimization Problem}{4}{subsubsection.1.4.1}
\contentsline {section}{\numberline {2}Summary of Course}{4}{section.2}
\contentsline {section}{\numberline {3}Lecture Two}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Geometric Insight}{6}{subsection.3.1}
