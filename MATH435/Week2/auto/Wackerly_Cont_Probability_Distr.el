(TeX-add-style-hook
 "Wackerly_Cont_Probability_Distr"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "math_pkgs"
    "article"
    "art10"
    "hyperref")
   (LaTeX-add-labels
    "eq:prob_interval"
    "eq:normal_dist"
    "eq:gamma_dist"
    "eq:beta_denom"))
 :latex)

