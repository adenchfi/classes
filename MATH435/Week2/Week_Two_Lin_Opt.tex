\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Introduction to Linear Optimization \\ }
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}

Comments on homework and last week: You can change a linear program and so long as the cost/objective function is the same, any solutions you find will also work for the original problem. You can also only focus on the ``optimal'' solution for both problems. You are \textbf{not} losing any information when you convert one linear program to the second form.

LP1($\vec x$) and LP2($\vec y$). 
\begin{enumerate}
\item  You have decision variables $\vec x$ with cost $c_1$ and $\rightarrow$ decision variables $\vec y$ with cost $c_2 \leq c_1$. 
\item $\vec x$ with cost $c_1' \leq c_2' \leftarrow \vec y$ with cost $c_2'$
\end{enumerate}
If you show that the two above are true, you end up with equivalency of the costs.

Recall the family of solutions are all on the same straight line for 2D problems.
\begin{example}
  \begin{align*}
    \min \quad &  c_1x_2 + c_2 x_2 \tag{$c^T = (c_1, c_2)$ = cost vector} \\
    s.t. \quad & -x_1 + x_2 \leq 1 \\
         & x_1 \geq 0 \\
         & x_2 \geq 0
  \end{align*}

  The feasible solution set all belongs under the line $-x_1 + x_2 = 1$, but in the first quadrant. There may be some different scenarios:
  \begin{enumerate}
  \item $c^T = (1, 1)$. $\rightarrow$ $x = (0, 0)$ is the unique optimal solution. 
  \item $c^T = (1, 0)$. We have infinitely many feasible solutions on the $x_2$ axis. Any feasible solution on the $x_2-axis$ minimizes $c^T x$ such that $0 \leq x_2 \leq 1$. So they're infinite but also bounded. 
  \item $c^T = (0, 1)$. Now we have an \textbf{unbounded} infinite set of solutions, since $x_1$ is unbounded. Any solution on the $x_1$ axis is good. 
  \item $c^T = (-1, -1)$. As $x_i$ gets larger, the objective/cost function continually gets smaller. Therefore there are an infinite number of solutions $x_i$. One reason this is the case is because $\vec c$ is parallel to $-x_1 + x_2 = 1$. For any feasible solution, we can always find another one with less cost by increasing the value of $x_1$. $\therefore$ no feasible solution is optimal, since the minimization problem goes to $-\infty$. We call such an LP \textbf{unbounded}. 
  \item Add another constraint to the problem: $-x_1 + x_2 \geq 2$. Directly opposes previous constraint, and the feasible solution set is $\emptyset$. The LP is called \textbf{infeasible}. 
  \end{enumerate}
\end{example}

\subsection{Linear Algebra Review}

A function is called \textbf{linear} $f: \mathbb{R}^n \rightarrow \mathbb{R}^n$ if
\begin{enumerate}
\item $f(\alpha x) = \alpha f(x)$ where $\alpha \in \mathbb{R}, x \in \mathbb{R}^n$
\item $f(x + y) = f(x) + f(y)$
\end{enumerate}
\begin{definition}[Affine Functions]
  A linear function plus a constant term is called an \textbf{affine function} (translation by a fixed vector). For example,
  \begin{equation}
    g_i(x) = a_i^T x - b_i \text{  are affine functions}
  \end{equation}

\end{definition}


\section{General Forms of Linear Problems}
\begin{example}
  Say $f(x)$ = 
  \begin{align*}
    \max_{i = 1, \dotsc, k} (c_i^T x + d_i).
  \end{align*}
  This is called a \textbf{piecewise linear convex} function. This means that, for every value $x$, you have $i$ affine functions defined by $(c_i^T x + d_i)$. At every $x$, choose the \textbf{max} value of all the affine functions. Ends up looking like a convex polygon.
  \begin{remark}
    You could represent any multivariable function as an infinite sum of affine functions, right? Each point has a tangent line which is another affine function. 
  \end{remark}
  How do we turn this into a linear programming problem?
  \begin{align*}
    \min \quad & \max_{i = 1, \dotsc, k} (c_i^T x + d_i) \\
    s.t. \quad & A\vec x \geq \vec b
  \end{align*}
  How do we express this as an LP? Recall $\max_{i = 1, \dotsc, k} (c_i^T x + d_i)$ is equal to the smallest number $z$ that satisfies $z \geq c_i^T + d_i \ \forall i = 1, \dotsc, k$. We can then write
  \begin{align*}
    \min \quad & z \\
    s.t. \quad & z \geq c_i^T x + d_i \quad \forall i = 1, \dotsc, k \tag{Same as $z - c_i^T x \geq d_i$}\\
    & A\vec x \geq \vec b
  \end{align*}
\end{example}
\begin{remark}
  Convex functions can be approximated by piecewise linear convex functions. A second course in this stuff would be a course in convex optimization. 
\end{remark}

\begin{example}
  \begin{align*}
    \min \quad & \sum_{i=1}^{i=n} c_i\abs{x_i} \tag{assume $c_i \geq 0 \ \forall i$} \\
    s.t. \quad & A\vec x \geq \vec b
  \end{align*}
  We can convert this to a piecewise linear convex function. We can do this in two ways. 
  \begin{enumerate}
  \item \begin{solution}
    To do this, we say $\abs{x_i}$ is the smallest $z_i \ s.t. \ x_i \leq z \ \& -x_i \leq z_i$.
    \begin{align*}
      \min \quad & \sum_{i=1}^{i=n} c_i z_i \\
      s.t. \quad & Ax \geq b \\
                 & x_i \leq z_i \quad \forall i = 1, \dotsc, n \\
                 & -x_i \leq z_i 
    \end{align*}
    \begin{remark}
      Why are these two optimization problems equivalent? 
    \end{remark}
  \end{solution}
\item
  \begin{solution}
    Another method:

    Let $x_i = x_i^+ - x_i^-$, where we can assume
    \begin{itemize}
    \item $x_i  x_i^+ $ when $x_i \geq 0$
    \item $x_i = -x_i^-$ when $x_i < 0$
    \end{itemize}
    Then $\abs{x_i} = x_i^+ + x_i^-$.
    We get
    \begin{align*}
      \min \quad & \sum_{i=1}^{i=n} c_1(x_i^+ x_i^-) \\
      s.t. \quad & Ax^+ - Ax^- \geq b \\
                 & x^+, x^- \geq 0 \\
      \text{where } & x^+ = (x_1^+, \dotsc, x_n^+) \\
      & x^- = (x_1^-, \dotsc, x_n^-)
    \end{align*}
    \begin{remark}
      But we need to quantify our assumptions - show that the solution to our new LP by solving it and showing it behaves like the absolute value function. 
    \end{remark}
    \begin{remark}
      This sort of transformation/creation of variables seems like something you can do with \textit{any} even function. And you could do something similar for odd functions. Of course, it wouldn't necessarily be a linear function... 
    \end{remark}
    Assume $c_i > 0 \ \forall i$. At an optimal solution for LP2, either:
    \begin{itemize}
    \item $x_i^+ = 0$, or
    \item $x_i^- = 0$. 
    \item Otherwise, we would reduce both $x_i^+ \ \& \ x_i^-$ by the same amount \& maintain feasibility while reducing objective function cost. e.g. $x_1^+ = 0.5, x_1^- = 0.1$, then $x_1^+ = 0.4, x_1^- = 0$. 
    \end{itemize}
    Therefore an optimal solution to this LP also is an optimal solution to our original LP. 
  \end{solution}
\end{enumerate}
\end{example}

\section{Geometry and Linear Algebra}

\begin{definition}[Polyhedron]
  A polyhedron $\mathbb{P}$ is defined as
  \begin{equation}
    \mathbb{P} = \{x \in \mathbb{R}^n | Ax \geq b\} \tag*{where $A$ is $m\cross n$, $b \in \BB{R}^n$}
  \end{equation}
\end{definition}
\begin{remark}
A \textbf{feasibility set} of an LP is a polyhedron.
\end{remark}

\begin{definition}[Polytope]
  A \textbf{polytope} is a \textbf{bounded} polyhedron. Bounded means that $\exists$ a constant $K$ s.t. $|x_i|\leq K \ \forall i$ for every $x \in \BB{P}$. 
\end{definition}

\begin{remark}
A polytope could also be considered a ``closed'' polyhedron.
\end{remark}

\begin{definition}[Convexity]
  A set $S$ is \textbf{convex} if, for all $x, y \in S$ and any $\lambda \in [0, 1]$,
  \begin{equation*}
    \underbrace{\lambda x + (1-\lambda)y}_{\text{line seg. b.w. x, y}} \in S
  \end{equation*}
  \begin{remark}[Meaning]
    This means convex sets are shapes in space where the line between any two points in the set $S$ is also inside the set $S$. 
  \end{remark}
\end{definition}

\begin{definition}
  Let $x^1, x^2, \dotsc, x^k \in \BB R^n$ (superscripts, not exponents) and $\lambda_1, \dotsc, \lambda_R \in [0, 1]$ such that $\sum_{i=1}^{i=k} \lambda_i = 1$.
  Then
  \begin{enumerate}
  \item $\sum_{i=1}^{i=k} \lambda_i x^i$ is a \textbf{convex combination} of the vectors $\vec x^1, \dotsc, \vec x^k$.
  \item The \textbf{Convex Hull} is the set of all convex combinations of vectors $x^1, \dotsc, x^k$.
    \begin{remark}[Connection to Span]
      Consider that this property is like the \textbf{span} of the vectors, except you only look at the linear combinations with sets of $\lambda_i$ such that $\sum \lambda_i = 1$. 
    \end{remark}
    \begin{example}
      For 2D systems, the convex hull for vectors $x, y$ is the set of all points between $\vec x, \vec y$. Furthermore, you can think of the convex hull of a set of points $\vec x^i$ as the boundary defined by all the hyperplanes connecting the points. Another intuition is the ``smallest'' convex set containing $\vec x^1, \dotsc, \vec x^k$. 
    \end{example}
  \end{enumerate}
\end{definition}

\begin{theorem}
  \begin{enumerate}[label=\alph*)]
  \item If $c_1, c_2$ are convex, then $c_1 \cap c_2$ is convex
  \item Every polyhedron is a convex set
  \item The Convex Hull $CH(\vec x^i, \dotsc, \vec x^k)$ is a convex set.
  \item The convex combination of finitely many elements of a convex set also belong to that set. 
  \end{enumerate}
\end{theorem}
\begin{remark}
  Any/all variables $x, y$ are vectors even if unlabeled. 
\end{remark}
\begin{remark}[Thoughts]
  I wonder if there are any special symmetries in the definition of convex sets. Interchange of points/elements of the set, perhaps? Can you form a convex set from a set of basis vectors with restraints on the transformations/linear combinations thereof that's more fundamental than the current definition? 
\end{remark}

\subsection{Extreme Points}


\begin{definition}
  $x \in \BB P$ is called an \textbf{extreme point} of $\BB P$ if it cannot be expressed as a convex combination of two \textbf{other} points in $\BB P$. Another way of saying it is:
  If $x = \lambda y + (1 - \lambda)z, \lambda \in [0, 1]$ then $x$ is an extreme point \textbf{if} either
  \begin{itemize}
  \item Either $y \not\in \BB P$ or $z \not\in \BB P$, or
  \item $x = y$ or $x = z$
  \end{itemize}

\end{definition}

\begin{definition}[Vertex]
  A point $x \in \BB P$ is a \textbf{vertex} of $\BB P$ if $\exists c \ s.t. \ c^T x < c^T y \forall y \in \BB P \& y \neq x$
  \begin{remark}
    Recall $c^T x$ is the dot product of $\vec c$ and $\vec x$. 
  \end{remark}
  \begin{remark}
    Everything in $\BB P$ is to the one side of the hyperplane defined by $c^T y = c^T x$, with $y$ variable and $c^T x$ fixed (scalar). Also known as a \textbf{separating hyperplane}. 
  \end{remark}
  \begin{remark}
    $\exists c \ s.t.\ x$ is the unique optimal solution of (recalling these are vectors)
    \begin{align*}
      \min \quad & c^T y \\
      s.t. \quad & y \in \BB{P}. 
    \end{align*}
  \end{remark}
  \begin{remark}
    $\exists c$ s.t. $\BB{P}$ is on one side of the hyplerplane $\{ y | c^T y = c^T x\}$ which meets $\BB{P}$ only at $x$. 
  \end{remark}
\end{definition}



\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
