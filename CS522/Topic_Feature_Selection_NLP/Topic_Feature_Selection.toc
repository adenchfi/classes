\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Dimensionality Reduction}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Latent Semantic Analysis}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Subspaces and Projections}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Singular Value Decomposition}{3}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}LSA and SVD in Recommender Systems}{4}{subsubsection.2.1.3}
\contentsline {section}{\numberline {3}Lecture Two - LSA, pLSA, LDA}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Latent Semantic Analysis}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Topic Modelling}{5}{subsection.3.2}
