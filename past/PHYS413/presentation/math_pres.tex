%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer Presentation
% LaTeX Template
% Version 1.0 (10/11/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%   PACKAGES AND THEMES
%----------------------------------------------------------------------------------------

\documentclass{beamer}

\mode<presentation> {

  % The Beamer class comes with a number of default slide themes
  % which change the colors and layouts of slides. Below this is a list
  % of all the themes, uncomment each in turn to see what they look like.

  %\usetheme{default}
  %\usetheme{AnnArbor}
  %\usetheme{Antibes}
  %\usetheme{Bergen}
  %\usetheme{Berkeley}
  %\usetheme{Berlin}
  %\usetheme{Boadilla}
  %\usetheme{CambridgeUS}
  %\usetheme{Copenhagen}
  %\usetheme{Darmstadt}
  %\usetheme{Dresden}
  %\usetheme{Frankfurt}
  %\usetheme{Goettingen}
  %\usetheme{Hannover}
  %\usetheme{Ilmenau}
  %\usetheme{JuanLesPins}
  %\usetheme{Luebeck}
  \usetheme{Madrid}
  %\usetheme{Malmoe}
  %\usetheme{Marburg}
  %\usetheme{Montpellier}
  %\usetheme{PaloAlto}
  %\usetheme{Pittsburgh}
  %\usetheme{Rochester}
  %\usetheme{Singapore}
  %\usetheme{Szeged}
  %\usetheme{Warsaw}

  % As well as themes, the Beamer class has a number of color themes
  % for any slide theme. Uncomment each of these in turn to see how it
  % changes the colors of your current slide theme.

  %\usecolortheme{albatross}
  %\usecolortheme{beaver}
  %\usecolortheme{beetle}
  %\usecolortheme{crane}
  %\usecolortheme{dolphin}
  %\usecolortheme{dove}
  %\usecolortheme{fly}
  %\usecolortheme{lily}
  %\usecolortheme{orchid}
  %\usecolortheme{rose}
  %\usecolortheme{seagull}
  %\usecolortheme{seahorse}
  %\usecolortheme{whale}
  %\usecolortheme{wolverine}

  %\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
  %\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line

  %\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}
\usepackage{ amssymb }
\usepackage{mdwlist}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables

%----------------------------------------------------------------------------------------
%   TITLE PAGE
%----------------------------------------------------------------------------------------

\title[Green's Functions]{An Introduction to Green's Functions} % The short title appears at the bottom of every slide, the full title is only on the title page

\author{Adam Denchfield} % Your name
\institute[IIT] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
          {
            Illinois Institute of Technology \\ % Your institution for the title page
            \medskip
            \textit{adenchfi@hawk.iit.edu} % Your email address
          }
          \date{\today} % Date, can be changed to a custom date

          \begin{document}

          \begin{frame}
            \titlepage % Print the title page as the first slide
          \end{frame}

          \begin{frame}
            \frametitle{Overview} % Table of contents slide, comment this block out to remove it
            \tableofcontents % Throughout your presentation, if you choose to use \section{} and \subsection{} commands, these will automatically be printed on this slide as an overview of your presentation
          \end{frame}

          %----------------------------------------------------------------------------------------
          %   PRESENTATION SLIDES
          %----------------------------------------------------------------------------------------

          %------------------------------------------------
          \section{1D Green's Functions} % Sections can be created in order to organize your presentation into discrete blocks, all sections and subsections are automatically printed in the table of contents as an overview of the talk
          %------------------------------------------------

          \subsection{Motivation} % A subsection can be created just before a set of slides with a common theme to further break down your presentation into chunks

          \begin{frame}
            \frametitle{Motivation}
            Suppose that you have an equation of the form 
            \begin{equation} \label{ODE}
            \mathcal{L}\psi(x) = \frac{d}{dx}\Big( p(x)\frac{d\psi}{dx} \Big) + q(x)\psi = f(x)
            \end{equation}
            
            where $f(x)$ is an inhomogenous term (or source term) and $\mathcal{L}$ is a self-adjoint differential operator. When you solve this equation for a particular source term $f$, you get a homogenous solution and a particular solution. In order to solve this equation for a different $f$, you must solve for a new particular solution. \\~\\

			The theory of integral equations and Green's functions makes finding the solution very little work even if the source term $f$ is changed. It will be shown later that this solution ends up taking the form (in 1-D)
			\begin{equation} \label{Green}
			\psi(x) = \int_a^b G(x, t)f(t)dt
			\end{equation}
          \end{frame}

          %------------------------------------------------

		\subsection{Green's Functions}
			
          \begin{frame}
            \frametitle{Motivation, continued}
            \begin{itemize}
            \item We would like our Green's function $G(x, t)$ in \eqref{Green} to satisfy $\mathcal{L}G(x,t) = \delta(x-t)$. 
            \item To see why, just apply $\mathcal{L}$ to both sides of \eqref{Green}:
            \end{itemize}
            \begin{equation} \label{LGd}
			\mathcal{L}\psi(x) = \int_a^b \mathcal{L} G(x, t)f(t)dt = \int_a^b \delta(x-t) f(t)dt = f(x)
			\end{equation}
			
			\begin{itemize}
			\item This gives us one hint on how to find Green's functions. Next we will discuss its properties.
			\end{itemize}
          \end{frame}
		
          %------------------------------------------------
		\subsection{Properties of 1D Green's functions}
          \begin{frame}
            \frametitle{Properties}
            To begin understanding its properties, consider integrating \eqref{ODE} over a small range of x-values that includes x = t:
            \begin{equation*}
            \int_{t-\epsilon}^{t+\epsilon} \frac{d}{dx}\left[p(x)\frac{dG(x, t)}{dx}\right] dx + \int_{t-\epsilon}^{t+\epsilon} q(x)G(x,t)dx = \int_{t-\epsilon}^{t+\epsilon} \delta(t-x)dx
			\end{equation*}
			which, carrying out the first and last integrations, simplifies to 
			\begin{equation}\label{int}
			p(x)\frac{dG(x,t)}{dx}\Big\rvert_{t-\epsilon}^{t+\epsilon} +  \int_{t-\epsilon}^{t+\epsilon} q(x)G(x, t)dx = 1
			\end{equation}			             
			This cannot be satisfied in the limit of small $\epsilon$ if both G(x,t) and its derivative are continuous at x = t. 
          \end{frame}
          
          
			\begin{frame}
			\frametitle{Properties, continued}
			Eq. \eqref{int} implies that we should accept a discontinuity in dG/dx. Thus, in the limit as $\epsilon$ goes to 0, one of the integrals in \eqref{int} vanishes and we are left with the requirement
			\begin{equation}
                          \label{deriv}
			\lim_{\epsilon -> 0_+} \left[\frac{dG(x,t)}{dx}\Big\rvert_{x=t+\epsilon} - \frac{dG(x,t)}{dx}\Big\rvert_{x=t-\epsilon}\right] = \frac{1}{p(t)}. \footnotemark
			\end{equation}			
			
			Next we discuss how to find one form of G(x,t) as an expansion of eigenfunctions.			
			
			\footnotetext{Look familiar? If you let $\frac{1}{p(t)} = \sigma(t)$, this is exactly the discontinuity requirements for the potential V(r) over a conducting surface with surface charge density $\sigma(t)$.}
			\end{frame}

          %------------------------------------------------

          \begin{frame}
            \frametitle{Finding Green's functions}
            \begin{columns}[c] % The ``c'' option specifies centered vertical alignment while the ``t'' option is used for top vertical alignment

              \column{.55\textwidth} % Left column and width
              \textbf{Green's function expansion}
              \begin{enumerate}
              \item We can expand our Green's function G(x,t) in the eigenfunctions of our operator $\mathcal{L}$, which can be chosen as orthonormal on $(a,b)$.
              \item $\mathcal{L}\psi_n(x) = \lambda_n\psi_n(x),\ \langle\psi_n\vline\psi_m\rangle = \delta_{nm}$.
              \item Expanding both the x and t dependence of G(x, t) in this orthonormal set gives            
              \end{enumerate}
            $$ G(x, t) = \sum_{nm}g_{nm}\psi_n(x)\psi_m^*(t) $$
              \column{.45\textwidth} % Right column and width
              \textbf{Delta function expansion}
              \begin{enumerate}
              \item We can expand the delta function for comparison
              \suspend{enumerate}
              $$\delta(x-t) = \sum_{m}\psi_m(x)\psi_m^*(t) $$
              \resume{enumerate}
              \item We can insert the above and left expressions into $\mathcal{L}G(x,t) = \delta(x-t)$ to obtain
              \end{enumerate}
            \end{columns}
            \begin{equation}
              \label{eq:ortho}
              \mathcal{L}\sum_{nm}g_nm\psi_n(x)\psi_m^*(t) = \sum_m\psi_m(x)\psi^*_m(t)
            \end{equation}
          \end{frame}

          \begin{frame}
            \frametitle{Finding Green's function, continued}
            Applying $\mathcal{L}$ on \eqref{eq:ortho} and recalling it only operates on $\psi_n(x)$, we get
            \begin{equation}
              \sum_{nm}\lambda_n g_{nm}\psi_n(x)\psi_m^*(t) = \sum_m\psi_m(x)\psi(m)^*(t)
            \end{equation}
            Integrating over $(a, b)$ and using the orthonormality conditions yields $g_{nm} =\delta_nm/\lambda_n$. Recalling our original expansion for G(x, t), we have the expansion
            \begin{equation}
              \label{eq:green}
              G(x,t) = \sum_n \frac{\psi_n(x)\psi_n^*(t)}{\footnotemark\lambda_n}
            \end{equation}
            The importance of \eqref{eq:green} isn't due to its direct value as a computational tool, but moreso it reveals the symmetry of $G$: $$ G(x, t) = G(t, x)^* $$
            \footnotetext{This analysis fails for any $\lambda_n = 0$, but I will ignore this special case.}
          \end{frame}

          \begin{frame}
            \frametitle{Finding Green's function, continued}
            Now we can find the Green's function given a Hermitian operator $\mathcal{L}$ and its boundary conditions over $(a,b)$. We assume one homogenous BC on each end.
            \begin{columns}[c] % The ``c'' option specifies centered vertical alignment while the ``t'' option is used for top vertical alignment
              \column{.55\textwidth} % Left column and width
              \center{${a \leq x < t}$}
              \begin{enumerate}
              \item While x < t, G(x, t) must have an x-dependence $y_1(x)$ that solves $\mathcal{L} = 0$ and satisfies the x = a BC.
              \item The most general $G(x,t)$ is then $G(x, t) = y_1(x)h_1(t), \ (x < t)$ with $h_1(t)$ currently unknown.
              \end{enumerate}
              \column{.45\textwidth} % Right column and width
              \center{${b \geq x > t}$}
              \begin{enumerate}
              \item While x > t, G(x, t) must have an x-dependence $y_2(x)$ that solves $\mathcal{L} = 0$ and satisfies the x = b BC.
              \item The most general $G(x,t)$ is then $G(x, t) = y_2(x)h_2(t), \ (x > t)$ with $h_2(t)$ currently unknown.
              \end{enumerate}    
            \end{columns}
          \end{frame}

          \begin{frame}
            \frametitle{Finding Green's function, continued}
            The symmetry condition we established, $G(x, t) = G(t, x)^*$, only allows the previous two equations to be consistent if $h_2^* = Ay_1,\  h_1^* = Ay_2$, with $A$ a constant to be determined. Assuming $y_1, y_2$ can be chosen to be real, we are led to the conclusion that
            \[
              G(x, t) =
              \begin{cases}
                Ay_1(x)y_2(t) & x < t \\
                Ay_2(x)y_1(t) & x > t \\
              \end{cases}
            \]
            where the $y_i$ fulfill the conditions previously stated. Using the derivative discontinuity equation \eqref{deriv}, we can determine A as
            \begin{equation}
              \label{eq:A}
              A = \frac{1}{p(t)} \left(\frac{\partial y_2(t)}{\partial x}y_1(t) - \frac{\partial y_1(t)}{\partial x}y_2(t)\right)^{-1}
             \end{equation}
             Notice that the term on the right involving the $y_i$ is the Wronskian of the two solutions, which have a value proportional to $1/p(t)$, and thus A is a constant. We're done! We have an expression for G(x,t) and A for homogenous Dirichlet BCs. Two and three dimensions have a similar analysis, but no explicit formula for the solution.
          \end{frame}
          % Was going to do this, but it would make my talk way too long
          % \section{2D and 3D Green's Functions}
          
          % \begin{frame}
          % \frametitle{2D and 3D Green's Functions}
          % \end{frame}

          %------------------------------------------------
          \section{Applications and Examples}
          % ------------------------------------------------
          \subsection{Simple Second-Order ODE}
          \begin{frame}
            \frametitle{Simple Second-Order ODE}
            Consider the ODE $-y'' = f(x)$ with BCs $y(0) = y(1) = 0$. The corresponding homogenous equation $-y'' = 0$ has general solution $y_0 = c_0 + c_1 x$. We construct two trial functions: $y_1 = x$ which satisfies $y_1(0) = 0$ and $y_2 = 1 - x$, which satisfies $y_2(1) = 0$. The coefficient $$p(x) = -1, y_1'(x) = 1, y_2'(x) = -1,$$ and the constant A in the Green's function is $$ A = \left[(-1)[(-1)x - (1)(1 - x)]\right]^{-1} = 1.$$
              Therefore our Green's function is
              \[
                G(x, t) =
                \begin{cases}
                  x(1 - t) & 0 \leq x < t \\
                  t(1 - x) & t < x \leq 1 \\
                \end{cases}
              \]
              Note how we can now solve for any particular solution $y(x)$ for \textit{any} function $f(x)$ by substituting in the inhomogenous term: $$ y(x) = \int_0^1 G(x, t) f(t) dt $$
          \end{frame}

          
		\subsection{Coulomb's Law}
          \begin{frame}
            \frametitle{Applications - Coulomb's Law}
            Coulomb's Law can be derived from solving Poisson's equation,
            \begin{equation}
              \label{eq:poisson}
              \nabla^2 \phi = 4\pi\rho
            \end{equation}
            where $\phi$ is our potential (often called V) and $\rho$ is a density function, in this case electric charge density. The differential operator here is $\mathcal{L} = \nabla^2$. We want to find a Green's function $G(r_1,r_2)$ such that
            \begin{equation}
              \label{point}
              \nabla^2G(\vec{r_1},\vec{r_2})= \delta^3(\vec{r_1} - \vec{r_2})
            \end{equation}

          \end{frame}

          %------------------------------------------------
          
		\begin{frame}
                  \frametitle{Coulomb's Law, continued}
                  However, recall (from previous class discussions) that $\nabla^2\left(\frac{1}{\vec{r} - \vec{r'}}\right) = -4\pi\delta^3(\vec{r}-\vec{r'})$. By comparing this with \eqref{point}, we can identify the Green's function as $G(\vec{r}, \vec{r'}) = -\frac{1}{4\pi|\vec{r}-\vec{r'}|}$. This gives us our solution
                  \begin{equation}
                    \label{eq:coul}
                    \phi(\vec{r}) = \int G(\vec{r}, \vec{r'}) [4\pi\rho(\vec{r'})]d^3\vec{r'} = -\int \frac{\rho(\vec{r'})}{|\vec{r}-\vec{r'}|}d^3\vec{r'}
                  \end{equation}
                  As a sanity check, use a point source, $\rho(\vec{r'}) = \frac{q}{4\pi\epsilon_0}\delta^3(\vec{r}-\vec{r'})$. Integrating over a delta function gives the appropriate Coulomb potential $\phi = \frac{q}{4\pi\epsilon_0r}$, where $r$ is the magnitude of the position difference vector.
		\end{frame}		          
          
		\subsection{Quantum Mechanical Scattering}


          %------------------------------------------------

%          \begin{frame}[fragile] % Need to use the fragile option when verbatim is used in the slide
%            \frametitle{Verbatim}
%            \begin{example}[Theorem Slide Code]
%\begin{verbatim}
%  \begin{frame}
%\frametitle{Theorem}
%\begin{theorem}[Mass--energy equivalence]
%$ = mc^2$
%\end{theorem}
%\end{frame}\end{verbatim}
%\end{example}
%\end{frame}

%------------------------------------------------

%------------------------------------------------

\begin{frame}
\Huge{\centerline{The End}}
\end{frame}

%----------------------------------------------------------------------------------------

\end{document}
