(TeX-add-style-hook
 "slides"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    ""
    "amssymb"
    "mdwlist"
    "graphicx"
    "booktabs")
   (LaTeX-add-labels
    "ODE"
    "Green"
    "LGd"
    "int"
    "deriv"
    "eq:ortho"
    "eq:green"
    "eq:A"
    "eq:poisson"
    "point"
    "eq:coul"))
 :latex)

