\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{1D Green's Functions}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Green's Functions}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Properties of 1D Green's functions}{5}{0}{1}
\beamer@sectionintoc {2}{Applications and Examples}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Simple Second-Order ODE}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Coulomb's Law}{12}{0}{2}
