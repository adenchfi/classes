(TeX-add-style-hook
 "myth"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "smalldemyvopaper" "11pt" "twoside" "onecolumn" "openright" "extrafontsizes")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8x") ("fontenc" "T1") ("Alegreya" "osf") ("AlegreyaSans" "osf") ("hyperref" "hidelinks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir11"
    "inputenc"
    "fontenc"
    "Alegreya"
    "AlegreyaSans"
    "microtype"
    "setspace"
    "lettrine"
    "titlesec"
    "lipsum"
    "calc"
    "hologo"
    "hyperref")
   (TeX-add-symbols
    "ISBN"
    "press"
    "halftitlepage"
    "titleM")
   (LaTeX-add-lengths
    "drop"))
 :latex)

