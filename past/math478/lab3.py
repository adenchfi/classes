# program for MATH 478 written by Adam Denchfield
import numpy as np
import matplotlib.pyplot as plt

def f(t, y): # derivative of the function
    return (1+(y-t)**2)
    
def dfdt(t, y):
    return (-2*(y-t))

def dfdy(t, y):
    return (2*(y-t))

#def df2dt2(t, y):  # df2dt2 = 2
#    return 2

#def df2dy2(t, y):
#    return 2

#def df2dydt(t, y):
#return -2

def fEuler(tn, yn, h): # one step of the classic forward Euler method
    temp = yn + h*f(tn, yn)
    return temp

def taylor_3pt(tn, yn, h): # n = 3 Taylor series rule
    return (yn + h*f(tn, yn) + h*h*(dfdt(tn, yn)+dfdy(tn, yn)*f(tn, yn))/2 + h**3 * (2-4*f(tn, yn)+2*(f(tn, yn))**2 + dfdy(tn, yn)*(dfdt(tn, yn)+dfdy(tn, yn)*f(tn, yn))) / 6)

def predictor_corrector_fEuler(tn, yn, h): # implicit trapezoidal rule, except we use Euler method to predict the next derivative step to prevent needing to solve a nonlinear equation
    return (yn + 0.5*h*(f(tn, yn) + f(tn+h, fEuler(tn, yn, h))))

# START

t = [] # t contains the different time values for n = 10, 20, and 40 respectively
t.append( [i for i in np.arange(0, 1, 1/10)])
t.append([i for i in np.arange(0, 1, 1/20)])
t.append([i for i in np.arange(0, 1, 1/40)])

yeuler = [[0.5] for _ in range(0, 3)] # 0.5 is the initial y-value
ytaylor =[[0.5] for _ in range(0, 3)]
ypredictor = [[0.5] for _ in range(0, 3)]
yexact = [[] for _ in range(0, 3)] # don't need to initialize this one since we know the exact value formula

# open the different files I'll send the data to
# 'e' means euler, 't' means 3-point taylor, 'p' means predictor-corrector
f10e = open('f10e.txt', 'w')
f10t = open('f10t.txt', 'w')
f10p = open('f10p.txt', 'w')
f20e = open('f20e.txt', 'w')
f20t = open('f20t.txt', 'w')
f20p = open('f20p.txt', 'w')
f40e = open('f40e.txt', 'w')
f40t = open('f40t.txt', 'w')
f40p = open('f40p.txt', 'w')

# make a 2D array that makes it easier to iterate over; makes it more extensible too
files = [[f10e, f20e, f40e], [f10t, f20t, f40t], [f10p, f20p, f40p]]


for idx, method in enumerate(files):
    for fileidx, fi in enumerate(method):
        # write the header for each file
        files[idx][fileidx].write("h\t\tu_k\t\ty_k-u_k\t\t(y_k-u_k)/h(h**2 for taylor)\t\t\n")

for which, intervals in enumerate(t):
    # since h is fixed for each n, just initialize it at the beginning of each n=10, 20, 40
    h = intervals[1] - intervals[0]        

    # looping through the time steps now!
    for idx, tn in enumerate(intervals):

        temp = fEuler(tn, yeuler[which][idx], h) # was debugging, I don't need the temp variable, can directly have this inside the append
        yeuler[which].append(temp)
        ytaylor[which].append(taylor_3pt(tn, ytaylor[which][idx], h))
        ypredictor[which].append(predictor_corrector_fEuler(tn, ypredictor[which][idx], h))
        yexact[which].append((tn**2 - 2*tn - 1)/(tn-2))

        # I don't need a separate loop to write the data to file, with how I've structured my program.
        files[0][which].write("%f\t%f\t%f\t%f\n" % (h, yeuler[which][idx], yexact[which][idx]-yeuler[which][idx], (yexact[which][idx]-yeuler[which][idx])/h))
        files[1][which].write("%f\t%f\t%f\t%f\n" % (h, ytaylor[which][idx], yexact[which][idx]-ytaylor[which][idx], (yexact[which][idx]-ytaylor[which][idx])/h**2))
        files[2][which].write("%f\t%f\t%f\t%f\n" % (h, ypredictor[which][idx], yexact[which][idx]-ypredictor[which][idx], (yexact[which][idx]-ypredictor[which][idx])/h))
    intervals.append(1)


# in case you want to plot it, uncomment the code below!

#plt.plot(t10, yeuler, t10, ytaylor, t10, ypredictor)
#t10.pop(-1) # need to do this since yexact has one less element than the others
#plt.plot(t10, yexact)
#plt.show()

