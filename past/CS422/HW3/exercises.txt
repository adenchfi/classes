Adam Denchfield, HW3 Problems

Ch. 5
-----
- 7
  Table:
  Record	A	B	C	Class
  1		0	0	0	+
  2		0	0	1	-
  3		0	1	1	-
  4		0	1	1	-
  5		0	0	1	+
  6		1	0	1	+
  7		1	0	1	-
  8		1	0	1	-
  9		1	1	1	+
  10		1	0	1	+

a) Estimate the conditional probabilities for P(A|+), P(B|+), P(C|+), P(A|-), P(B|-), P(C|-)
P(A = 1|+) = P(A = 1)*P(+|A = 1) / P(+)
- P(A = 1) = 5/10 = 1/2
- P(+) = 1/2
- P(+|A = 1) = 3/5
- Therefore P(A = 1|+) = 3/5 = 0.6

P(A = 0|+) = P(A = 0)*P(+|A=0) / P(+)
- P(A = 0) = 1/2
- P(+) = 1/2
- P(+|A=0) = 2/5 = 0.4
- Therefore P(A = 0|+) = 2/5 = 0.4 = 1 - P(A = 1|+)

P(B = 1|+) = P(B = 1)*P(+|B=1) / P(+)
- P(B = 1) = 3/10
- P(+) = 1/2
- P(+|B=1) = 1/3
- Therefore P(B = 1|+) = (3/10)*(1/3) / (1/2) = 2/10 = 1/5 = 0.2

P(B = 0|+) = P(B = 0)*P(+|B=0) / P(+)
- P(B = 0) = 7/10
- P(+) = 1/2
- P(+|B=0) = 4/7
- Therefore P(B = 0|+) = (7/10) * (4/7) / (1/2) = 8/10 = 4/5 = 0.8 = 1 - P(B = 1|+)

P(C = 1|+) = P(C = 1)*P(+|C=1) / P(+)
- P(C=1) = 9/10
- P(+) = 1/2
- P(+|C = 1) = 4/9
- Therefore P(C = 1|+) = 8/10 = 4/5 = 0.8

P(C = 0|+)
- P(C=0) = 1/10
- P(+) = 1/2
- P(+|C=0) = 1/1
- Therefore P(C=0|+) = 1/5 = 0.2 = 1 - P(C = 1|+)

P(A=1|-) = P(A=1)*P(-|A=1) / P(-)
- P(-) = 1/2
- P(A=1) = 1/2
- P(-|A=1) = 2/5
P(A=1|-) = 2/5 = 0.4
P(A=0|-) = 1 - P(A=1|-) = 0.6

P(B=1|-) = P(B=1)*P(-|B=1) / P(-)
- P(-) = 1/2
- P(B=1) = 3/10
- P(-|B=1) = 2/3
- P(B=1|-) = 4/5 = 0.8
P(B=0|-) = 1/5 = 0.2

P(C=1|-) = P(C=1)*P(-|C=1) / P(-)
	 = (9/10) * (5/9) * 2
	 = 1.0
	 
P(C=0|-) = 0.0

b) Use the estimates above to predict the class label for a test sample (A = 0, B = 1, C = 0) using the naive Bayes approach
- Use Bayes' theorem
P(+|A = 0, B = 1, C = 0)
= P(A = 0, B = 1, C = 0|+) x P(+) / P(A = 0, B = 1, C = 0)
- now assume probabilities are independent of each other
= P(A = 0|+)*P(B=1|+)*P(C=0|+) x P(+) / P(A = 0, B = 1, C = 0)
= (2/5)(1/5)(1/5) x (1/2) / P(A=0, B=1, C=0)
= (1/125) / P(A=0, B=1, C=0)
- no cases of (A=0, B=1, C=0), so let's treat them as independent for the denominator too
= (1/125) /( (1/2)(3/10)(1/10) )
= (2/125) * (100/3)
= 200/375
= 8/15 ~ 0.533 ~ 53.3%

- For the -'s, since P(C=0|-) = 0, the probability of it being - is 0 with this approach.
Therefore the class will be +.

- 8
a) Oh come on! I just did this for a whole table, now I'm doing it again? Alright...

P(A = 1|+) = P(A = 1)*P(+| A = 1) / P(+)
      	   = 0.6
P(B = 1|+) = P(B = 1)*B(+| B = 1) / P(+)
      	   = 0.4
P(C = 1|+) = P(C = 1)*P(+| C = 1) / P(+)
      	   = 0.8
P(A = 1|-) = 0.4
P(B = 1|-) = 0.4
P(C = 1|-) = 0.2

b) Since P(+) = P(-), we can just compare P(+| {A=1, B=1, C=1}) and P(-| {A=1, B=1, C=1})
P({A=1, B=1, C=1}|+) = P(A=1|+)*P(B=1|+)*P(C=1|+) = 0.192
P({A=1, B=1, C=1}|-) = P(A=1|-)*P(B=1|-)*P(C=1|+) = 0.032

The record will be assigned the + class.
c)
P(A = 1) = 0.5, P(B = 1) = 0.4, P(A=1, B=1) = 0.2, which also happens to equal P(A=1)*P(B=1). Therefore, A and B are independent.
d)
P(A = 1) = 0.5, P(B = 0) = 0.6, P(A = 1, B = 0) = P(A=1)*P(B=0) = 0.3, so they're still independent.
e)
P(A=1, B=1|+) = 0.2
P(A=1|+) = 0.6
P(B=1|+) = 0.4
P(A=1, B=1|+) does NOT equal the two multiplied. Hence, when we're considering only those given in a certain class, A and B are not independent.
------------------------
Chapter 6 Problems

2)
a)
- {e} shows up in 8 out of 10 transactions. s({e}) = 8/10 = 0.8.
- {b, d} shows up in 2/10 transactions. s({b, d}) = 2/10 = 0.2.
- {b, d, e} shows up in 2/10 transactions. s({b, d, e}) = 2/10 = 0.2.
b) Confidence -> c({x, y} -> {z}) = s({x, y, z}) / s({x, y}).
- c({b, d} -> {e}) = 0.2/0.2 = 1.0.
- c({e} -> {b, d}) = 0.2/0.8 / 0.25.
- Confidence is NOT a symmetric measure.
c) In this case, we have 5 baskets / 'transactions' since we have 5 customers.
- {e} is bought by every customer except 4. s({e}) = 0.8.
- {b, d} appears in 2/5 customers' histories, so s({b, d}) = 0.4.
- {b, d, e} appears in 2/5 customers' histories. s({b, d, e}) = 0.4.
d)
- c({b, d} -> {e}) = 0.4/0.4 = 1.0
- c({e} -> {b, d}) = 0.4 / 0.8 = 0.5.
e) s_2 >= s_1 always. Also c_2 >= c_1, but this is faulty - agglomerating your data could reduce the accuracy the confidence measure provides.

6)
a) 6 items. 3^d - 2^{d+1} + 1, d = 6 -> number of rules = 602.
b) The largest transaction size is 4 items, meaning no frequent itemsets of size > 4 will appear...
c) 6 choose 3 = 20.
d) Since I know the support of 3 and 4-itemsets will be less than or equal to the support of the 2-itemsets, I'll just look at the supports of the 2-itemsets. Simple counting yields {Bread, Butter} as the highest frequency with support = 5.
e) This means find a pair (a, b) such that s({a, b})/s{a} = s({a, b}) / s({b}). Solving just means s({a}) = s({b}), which is easy to find. Bread and Butter always appear together, so I know their supports are the same; (Bread, Butter) is such a pair.
-----------------------
Chapter 8

2) I'll try not to comment on how easy this is. Unfortunately, I can't draw it here, so I'll include the points circled in this file, named "pointshw3".

11)
- If the SSE is low for all clusters for that one variable, then that variable is nearly constant, most likely. 
- If the SSE is relatively low for a single cluster, then this helps define that particular cluster. 
- If the SSE is relatively high for all clusters, then that attribute has no predictive power for the clusters.
- If the SSE of that attribute is high for just one cluster, then perhaps that attribute doesn't actually have predictive power, at least not in that region of the other attribute.
- The per variable SSE could give information on what features aren't important, or that have false correlations in the grand scheme of things and should be removed from the data.ls

