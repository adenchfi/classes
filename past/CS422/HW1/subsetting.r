# CS422 Data Mining
# Vijay K. Gurbani, Ph.D.
# Illinois Institute of Technology

# Understanding the practicum part in Homework 1.

college <- 
  read.csv("/home/vkg/IIT/CS422/lectures/homeworks/homework-1/College.csv", 
           sep=",", header=T)

# Get a subset of 20 rows
ss <- college[1:20, ]
ss

# For these 20 records, see the values of the attribute Top10perc
ss$Top10perc

# Which ones are > 50?
ss$Top10perc > 50

# Get their indices
which(ss$Top10perc > 50)

# Get a subset of only those colleges where Top10perc > 50
sss <- subset(ss, ss$Top10perc > 50)
sss

rm(list=ls())



