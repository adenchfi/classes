# CS422 Data Mining
# Adam Denchfield, totally future Ph.D candidate
# Illinois Institute of Technology

# setwd("/mnt/c/Users/Adam\ Denchfield/Desktop/CS422")
college <-
  read.csv("College.csv",
               sep=",", header=T)

rownames(college) <- college[, 1]
fix(college)

college <- college[ , -1]
fix(college)

ss <- college[1:40, ]

# summary(ss) can be useful if we summarize subsets of the data, so I don't get a big blob of data
#pairs(ss[, 11:18])
#boxplot(college$Outstate~college$Private, col=rainbow(2))

Elite <- rep("No", nrow(college))
Elite[college$Top10perc > 50] <- "Yes"
Elite <- as.factor(Elite)
college <- data.frame(college, Elite)
summary(college)

boxplot(college$Outstate~college$Elite, col=rainbow(2))

par(mfrow=c(3,3))
hist(college$Outstate, breaks = 24)
hist(college$Grad.Rate, breaks = 6)
hist(college$Books, breaks=24)

#summary(college$Books)
# Note: oh my god one place has estimated books costing $2340!!!

# Playing with the data, seeing what ridiculous things I can find
expensive_book_schools = subset(college, college$Books > 1000)
summary(expensive_book_schools)
subset(college, college$PhD < 10)

subset(college, college$Books > 2000)
# Let me get this straight... the one place with less than 10% Ph.Ds in their staff is ALSO the one
# whose estimated books cost is over $2000?! Wow. Simply amazing.

cor(college$PhD, college$Grad.Rate)
# that was one thing I was really interested in. Correlation around 0.3, it's there but a weak correlation.
#

cor(college$Grad.Rate, college$Books)
# pretty much no correlation then!

cor(college$Grad.Rate, college$Top10perc)
cor(college$Grad.Rate, college$Top25perc)

# Interesting. Graduation rate correlation for the two above is around 0.48, 0.49.
# Moderate correlation then. I looked it up; graduation rate isn't necessarily a good data point to use
# Graduation rate measures full-time freshmen who graduate within six years
# students who transfer out count as drop-outs, those who transfer in don't count either
cor(college$Top10perc, college$Apps)

cor(college$Apps, college$PhD)
# moderate correlation of 0.39. If we treat % of Ph.Ds as roughly corresponding to the eliteness,
# then we have a weak correlation between how many people apply and the quality of the institution

cor(college$Grad.Rate, college$S.F.Ratio)
# negative correlation of -0.307... looks like the higher the student-faculty ratio, the worse the grad
# rate is. Makes sense.