# CS422 Data Mining
# Vijay K. Gurbani, Ph.D.
# Illinois Institute of Technology

rm(list=ls())
# Attribute transformation: Standardizing through scaling

m <- matrix(c(0,20,3000,50000,2,0,1,1), ncol=2, nrow=4)
m

std <- scale(m, center=T, scale=T)
std

# To convince yourself:
a1 <- mean(m[,1])  # Get the mean of column 1
s1 <- sd(m[,1])    # Get the sd of column 1
# And manually calculate the transformation
cat(paste("Result", (m[2,1]-a1)/s1)) 

# Same for the second column
a2 <- mean(m[,2])  # Get the mean of column 2
s2 <- sd(m[,2])    # Get the sd of column 2
# And manually calculate the transformation
cat(paste("\nResult", (m[3,2]-a2)/s2, "\n"))

# And finally, notice that the std matrix has column mean of 0 and sd of 1
cat(paste("Column 1 mean of std", mean(std[,1]), "\n"))
cat(paste("Column 1 sd of std", sd(std[,1])))
cat("\n===========\n")

# And same for column 2
cat(paste("Column 2 mean of std", mean(std[,2]), "\n"))
cat(paste("Column 2 sd of std", sd(std[,2])))