# CS422 Data Mining
# Vijay K. Gurbani, Ph.D.
# Illinois Institute of Technology

# Relationship between variance, covariance and correlation

v <- c(1, 2, 3); mean(v); sd(v)
w <- c(10, 20, 30); mean(w); sd(w)
x <- c(100, 200, 300); mean(x); sd(x)

cat("Covariances don't tell us much")
cov(v,w)
cov(v,x)
cov(w,x)

cat("Correlations do!")
cor(v,w)
cor(v,x)
cor(w,x)

# Visualizing correlations
library(corrplot)
data(iris)
head(iris)
res <- cor(iris[, 1:4])
res
corrplot(res)