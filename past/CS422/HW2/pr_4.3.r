# problem 4.3

a1 = c(TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE)
a2 = c(TRUE, TRUE, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, TRUE)
a3 = c(1.0, 6.0, 5.0, 4.0, 7.0, 3.0, 8.0, 7.0, 5.0)
TC = c('+', '+', '-', '+', '-', '-', '-', '+', '-')

dframe = data.frame(a1, a2, a3, TC)

plus_prob = sum(TC == '+') / length(TC)
minus_prob = sum(TC == '-') / length(TC)

# part a; total entropy for pos/neg class
entr_sign = - (plus_prob * log2(plus_prob) + minus_prob * log2(minus_prob))
plus_entr = - (plus_prob * log2(plus_prob))

# part b
a1_t_pr = sum(a1 == TRUE) / length(a1)
a1_f_pr = sum(a1 == FALSE) / length(a1)
a2_t_pr = sum(a2 == TRUE) / length(a2)
a2_f_pr = sum(a2 == FALSE) / length(a2)

a1_entr = - (a1_t_pr * log2(a1_t_pr) + a1_f_pr * log2(a1_f_pr))

info_gain = entr_sign - a1_entr

a2_entr = - (a2_t_pr * log2(a2_t_pr) + a2_f_pr * log2(a2_f_pr))
info_gain_a2 = entr_sign - a1_entr
print(a2_entr)

# ASK FOR HELP ON PR 4.3 -- HOW TO CALCULATE ENTROPY FOR ENTIRE THING????