x1 <- c(1, 1, 1, 1)
y1 <- c(2, 2, 2, 2)
x2 <- c(0, 1, 0, 1)
y2 <- c(1, 0, 1, 0)
x3 <- c(0, -1, 0, 1)
y3 <- c(1, 0, -1, 0)
x4 <- c(1, 1, 0, 1, 0, 1)
y4 <- c(1, 1, 1, 0, 0, 1)
x5 <- c(2, -1, 0, 2, 0, -3)
y5 <- c(-1, 1, -1, 0, 0, -1)

# magnitude of vector
mag <- function(x){sqrt(sum(x*x))}

# cosine distance
cosdist <- function(x, y){
sum(x*y) / (mag(x)*mag(y))
}

# Euclidean distance
eucl <- function(x, y){
sqrt(sum((x-y)^2))
}

# jaccard distance; I'm SURE there's a better way of doing it in R, but here's a hacky way of getting it done
jacc <- function(x, y){
f11 = sum(x[x == y])
f10 = 0
f01 = 0
# just need the sum of f10 and f01
# call it f
f = sum(xor(x, y)) # seems correct
j_sim = f11 / (f + f11)
#print(f11)
#print(f)
j_sim  # return j_sim as the similarity measure
j_dist = 1 - j_sim
#j_dist # return this
}

print("Part a")
cosdist(x1, y1)
cor(x1, y1)
eucl(x1, y1)

print("Part b")
cosdist(x2, y2)
cor(x2, y2)
eucl(x2, y2)
print("Jaccard test")
jacc(x2, y2)

print("Part c")
cosdist(x3, y3)
cor(x3, y3)
eucl(x3, y3)

print("Part d")
cosdist(x4, y4)
cor(x4, y4)
jacc(x4, y4)

print("Part e")
cosdist(x5, y5)
cor(x5, y5)
