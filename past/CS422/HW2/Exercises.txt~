By: Adam Denchfield
Exercises:
Ch. 2: 14, 19
Ch. 4: 2, 3, 5

2.14
----

Well, the elephants' attributes are all continuous numerical attributes. They're not going to be asymmetric, since biological features like that usually aren't - it's not like most elephants don't even have ears. Also, the length/norm of the attributes do matter. So the cosine measure is out, and as a similarity measure the data points are qualitatively different in terms of scale, so correlation shouldn't be used. Euclidean distance should work, since it just compares the same attributes. It may be helpful to mean-center the data and make the standard deviation one by dividing by it. 

2.19
----

Made an R script to do the calculations. It should be in this .zip file, "similarity.r". For the Jaccard, I used the distance, not the similarity.
Here is the output:


[1] "Part a"
[1] 1
[1] NA
Warning message:
In cor(x1, y1) : the standard deviation is zero
[1] 2
[1] "Part b"
[1] 0
[1] -1
[1] 2
[1] "Jaccard test"
[1] "Part c"
[1] 0
[1] 0
[1] 2
[1] "Part d"
[1] 0.75
[1] 0.25
[1] "Part e"
[1] 0.4714045
[1] 0.4714045

4.2
---

Recall Gini(t) = 1 - \sum_{i=0}^{c-1} [p(i|t)]^2
- c = number of classes
- p = fraction of records that belong to one of the i classes

a) For overall tree, C0 count = 10, C1 count = 10, Gini(T) = 1 - (10/20)^2 - (10/20)^2 = 1 - 0.5 = 0.5

b) Customer ID Attribute - Gini would be 0, wouldn't it?
ID = 1, Gini = 1 - (1/1)^2 - (0/1)^2 = 0, etc

c) Gender
      Gender
   Male  Female 
C0 5	 5
C1 3	 7

Male Gini: 1 - (5/8)^2 - (3/8)^2 = 0.46875
Female Gini: 1 - (5/12)^2 - (7/12)^2 = 0.4861111

Weighted Gini index for the attribute: 8/20 * 0.46875 + 12/20 * 0.48611111 = 0.4792
Gender Gini = 0.4792

      Gender
   Male  Female 
C0 6	 4
C1 4	 6

Male Gini: 1 - (6/10)^2 - (4/10)^2 = 1 - 9/25 - 4/25 = 1 - 13/25 ~ 0.5 = 12/25
Female = 12/25

d) Car Type with multiway split
       Car Type
   Luxury  Sports  Family
C0 1	   8	   1
C1 7	   0	   3

Luxury: 1 - ( (1/8)^2 + (7/8)^2 ) = 0.21875
Sports: 0.0
Family: 0.375

Average Car Type Gini Index: 8/20 * 0.21875 + 8/20 * 0.0 + 0.375 * 4/20 = 0.1625

Therefore car type has a pretty low Gini index of 0.1625

e) Shirt Size w/ Multiway split

   	      Shirt Size
   Small   Medium   Large   Extra Large
C0 3	   3	    2	    2
C1 2	   4	    2	    2

Similar to above, but not doing by hand anymore (thanks Python!) (maybe I should've just made it a database in R...)

>>> 1 - sum([(C/5)**2 for C in Sm])
0.48
>>> 1 - sum([(C/7)**2 for C in Med])
0.48979591836734704
>>> 1 - sum([(C/4)**2 for C in Lar])
0.5
>>> 1 - sum([(C/4)**2 for C in ExL])
0.5
>>> 5/20 * 0.48 + 7/20 * 0.4898 + 4/20 * 0.5 + 4/20 * 0.5
0.4914299999999999

Average (weighted) Gini:
0.491423

f) Car Type is the best attribute out of all of them, even using a multiway split instead of binary. I mean, it makes sense - Sports cars only show up with one kind of class label, so it seems sensible to split there first. 

g) Customer ID doesn't actually distinguish between class labels, usually. And for test data, there will not be customer IDs similar to the ones given, so there will be no precedent. 

4.3
---
- Recall Entropy(tree) = -\sum(i = 0)(c-1) p(i|t) log_2 p(i|t)
- Recall log_2 (x) = ln(x) / ln(2) so I don't have to look up log_2 in R

a) 0.76 with respect to the positive class -e.g. info gain from total entropy to positive class child
b) Recall information gain, with entropy, = Entropy(parent) - sum(j = 1 to c) N(c_j)/N * Entropy(c_j).

-- ASK FOR HELP - HOW TO CALCULATE TOTAL ENTROPY????

4.5
---
