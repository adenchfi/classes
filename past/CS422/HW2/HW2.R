# Adam Denchfield made this! 2017

library("psych")
library("rpart")
library("rpart.plot")
library("caret")
library("ROCR")
setwd("C:/Users/Adam Denchfield/Desktop/CS422")
data = read.csv("ILPD.csv")
set.seed(100)

# split the data into 60/40 train/test
index <- sample(1:nrow(data), size=0.4*nrow(data))
test <- data[index, ]
train <- data[-index, ]

pairs.panels(train)

"
a)
i) The strongest correlation is between the tb and db variables - the total Bilirubin and direct Bilirubin.
This seems to make sense, being nearly the same at 0.97. 
ii) There are several very weak correlations. For example, the label and tp (total Proteins) variables have
a correlation of 0.00. The sgoaa (Sgot Aspartate Aminotransferase) and tp (total Proteins) variables also
have a weak correlation, of -0.01. Also, age has 0 correlation with the tb and sgoaa attributes. 
iii) The most negative correlation is class label vs db (direct bilirubin); checking the correlation between
label and tb yields -0.24, which is expected since tb is highly correlated with db.
iv) Gaussian distribution? Age vs tb, Age vs db, and age vs sgpaa. Maybe aap vs sgpaa.

b) Whether or not we normalize/scale our data depends on the classification algorithm we use. Since we 
are using decision trees, normalizing/scaling our data shouldn't be necessary. Where the algorithm decides
to split the trees and on what attributes shouldn't be affected by normalization/scaling. 

c) 
"
# create a model using all predictor variables. 
model <- rpart(label ~ ., method="class", data=train)
pred <- predict(model, test, type="class")
cM <- confusionMatrix(pred, reference=test[, 11])
rpart.plot(model)
"
               Accuracy : 0.6824          
                 95% CI : (0.6184, 0.7417)
No Information Rate : 0.6996          
P-Value [Acc > NIR] : 0.7417          

Kappa : 0.2056          
Mcnemar's Test P-Value : 0.2010          

TPR / Sensitivity : 0.8098          
TNR / Specificity : 0.3857          
PPV/Pos Pred Value : 0.7543          
NPV/Neg Pred Value : 0.4655          
Prevalence : 0.6996          
Detection Rate : 0.5665          
Detection Prevalence : 0.7511          
Balanced Accuracy : 0.5978 
"
# ROC curve
pred.rocr <- predict(model, newdata=test, type="prob")[,2]
f.pred <- prediction(pred.rocr, test$label)
f.perf <- performance(f.pred, "tpr", "fpr")
plot(f.perf, colorize=T, lwd=3)
abline(0,1)
auc <- performance(f.pred, measure = "auc")
auc@y.values[[1]]

# d) Determine where to prune the tree, and prune it, see if it changes accuracy
printcp(model)
plotcp(model)

model.prune1 <- prune(model, cp = 0.025)
pred_prune1 <- predict(model.prune1, test, type="class")
cM_prune1 <- confusionMatrix(pred_prune1, reference=test[, 11])
#cM_prune1
printcp(model.prune1)
plotcp(model.prune1)

pred.rocr2 <- predict(model.prune1, newdata=test, type="prob")[,2]
f.pred2 <- prediction(pred.rocr2, test$label)
f.perf2 <- performance(f.pred2, "tpr", "fpr")
plot(f.perf2, colorize=T, lwd=3)
abline(0,1)
auc2 <- performance(f.pred2, measure = "auc")
auc2@y.values[[1]]
rpart.plot(model.prune1)
"
I tested out cp values from 0.04 to 0.01. All seemed to not be good - there is little to no improvement
in the tree's predictive power. The ROC curve looks a little worse, actually. Printing the models with
rpart.plot (and realizing it's quite hard to see...) I've found that there's a lot of nodes determining
the decision. Some of the variables have slight negative correlations. It may be that we're using too many
attributes for the job. For example, tb and db are highly correlated, and yet they both appear in the 
decision tree. 

Part e) concerns not considering all of the predictors. I'd remove one of tb/db first, since they're
highly correlated. 
"

# create a model using only a couple predictor variables that seem to be highly correlated with class label
# this appears to be better than a lot of the other models using a lot of variables, for this set of
# training/test data. 
model_new <- rpart(label ~ db + sgpaa, method="class", data=train)
pred_new <- predict(model_new, test, type="class")
cM_new <- confusionMatrix(pred_new, reference=test[, 11])
rpart.plot(model_new)
cM_new
# Just look at the accuracy, sensitivity, specificity, etc below. Seem to all be better than using
# all the predictor variables. 
"
               Accuracy : 0.7082          
                 95% CI : (0.6452, 0.7657)
No Information Rate : 0.6996          
P-Value [Acc > NIR] : 0.4187          

Kappa : 0.2637          
Mcnemar's Test P-Value : 0.1149          

Sensitivity : 0.8344          
Specificity : 0.4143          
Pos Pred Value : 0.7684          
Neg Pred Value : 0.5179          
Prevalence : 0.6996          
Detection Rate : 0.5837          
Detection Prevalence : 0.7597          
Balanced Accuracy : 0.6243          

'Positive' Class : 1      
"
# ROC curve
pred_new.rocr <- predict(model_new, newdata=test, type="prob")[,2]
f.pred_new <- prediction(pred_new.rocr, test$label)
f.perf_new <- performance(f.pred_new, "tpr", "fpr")
plot(f.perf_new, colorize=T, lwd=3)
abline(0,1)
auc_new <- performance(f.pred_new, measure = "auc")
auc_new@y.values[[1]]
# Even the auc value is better, and the ROC curve looks better

"
f)
i) Did so above. 
ii) Did so above.
iii) The model I made performs better, because I am an awesome investigator. And I also believe in 
Occam's razor: His principle states that among competing hypotheses, 
the one with the fewest assumptions should be selected. In this case, only choosing a couple predictive
variables was better. It's like overfitting if you try to use too many parameters. 
"

# Problem 2.2
data_orig = read.csv("ILPD_orig.csv")
data_orig[!complete.cases(data_orig),]

data[!complete.cases(data_orig),]
data_orig[208,] # data_orig[209]'s missing value was replaced by its value in row 208
data_orig[241,]
data[241,]
data[242,] # data[242,] corresponds to data_orig[241], where data_orig[241] has ag missing but data[242]
# does not have it missing. 
summary(data$ag)
# aha! In this case, the missing value was substituted with the median of the values.

data[254,] # this one also had her 'ag' parameter substituted with the mean; recall the data has one
# extra row after the first data point, for some reason...
data[313,] # this one also had its value substituted by the median.
