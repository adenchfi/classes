# Program written by Adam Denchfield for his class CS422, Data Mining
# This program computes the Jaccard similarity (well, the cosine, but it's the same thing when all entries are 0's or 1's) for all possible unique permutations of the pairs

from sklearn.metrics import jaccard_similarity_score as jacc
# turns out the above actually computes the 'accuracy' as the simple matching coefficient (SMC), not the actual jaccard similarity
from math import*
import itertools # found a fancy package

def square_rooted(x):
    return round(sqrt(sum([a*a for a in x])),3)

def cosine_similarity(x,y):
    # this is the same as the 
    numerator = sum(a*b for a,b in zip(x,y))
    denominator = square_rooted(x)*square_rooted(y)
    return round(numerator/float(denominator),3)

cos_sim = cosine_similarity
S1 = [1, 0, 0, 1, 0]
S2 = [0, 0, 1, 0, 0]
S3 = [0, 1, 0, 1, 1]
S4 = [1, 0, 1, 1, 0]
rows = (S1, S2, S3, S4)
S1S2 = cos_sim(S1, S2)
S1S3 = cos_sim(S1, S3) # there's probably a decent way to automate over all the row pairs; found it!
for pair in itertools.permutations(rows, 2): # second parameter defines that I'm talking about pairs, not triplets, etc
    p = cos_sim(*pair)
    print(*pair)
    print(p)
