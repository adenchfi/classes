from sympy import *
from numpy import mean
a = Symbol('a')
b = Symbol('b')
sc = [1, a, b]
sc = Matrix(sc)
A = [3.06, 500*a, 6*b]
A = Matrix(A)
B = [2.68, 320*a, 4*b]
B = Matrix(B)
C = [2.92, 640*a, 6*b]
C = Matrix(C)
#A.dot(B) / (norm(A) * norm(B))
#A.dot(B) / (A.norm() * B.norm())
#A.dot(C) / (A.norm() * C.norm())
#B.dot(C) / (B.norm() * C.norm())
a1 = A.dot(B) / (A.norm() * B.norm())
a2 = A.dot(C) / (A.norm() * C.norm())
a3 = B.dot(C) / (B.norm() * C.norm())
from mpmath import *
print(a1.subs([(a, 1), (b, 1)]))
print(a2.subs([(a, 1), (b, 1)]))
print(a3.subs([(a, 1), (b, 1)]))

print(acos(a1.subs([(a, 0.01), (b, 0.5)])))
print(acos(a2.subs([(a, 0.01), (b, 0.5)])))
print(acos(a3.subs([(a, 0.01), (b, 0.5)])))

# want to get the average component and set a and b as the inverses of those, respectively
disk_av = mean([500, 320, 640])
main_av = mean([6, 4, 6])

print(disk_av)
print(main_av)

print(acos(a1.subs([(a, 1/disk_av), (b, 1/main_av)])))
print(acos(a2.subs([(a, 1/disk_av), (b, 1/main_av)])))
print(acos(a3.subs([(a, 1/disk_av), (b, 1/main_av)])))

#import readline
#readline.write_history_file('/home/adenchfi/classes/CS422/history')
