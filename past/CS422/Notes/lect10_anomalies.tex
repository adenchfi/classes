\documentclass{article}

\input{math_pkgs.tex}

\begin{document}

\section{Statistical Approaches}
\label{sec:stat}

\begin{definition}{Probabilistic Definition of an Outlier}
  An outlier is an object that has a low probability w.r.t. a probability distribution model of the data.
\end{definition}
A wide variety of statistical tests based on the above definition have been devised to detect outliers, or \textbf{discordant observations}, as they are often called in the statistical literature. Many of these discordancy tests are highly specialized and assume a high level of statistical knowledge beyond the scope of this document. So only a few examples are discussed.

\subsection{Detecting Outliers in a Univariate Normal Distribution}
\label{sec:univariate_gauss}

Many datasets end up looking like Gaussian Distributions (also called normal distributions), which is given by
\begin{equation}
  \label{eq:gaussian}
  N(x, \mu, \sigma) = \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}}
\end{equation}
where $\mu, \sigma$ are the mean and standard deviations of the distribution, respectively. These can be estimated using the mean and s.t.d. of the data. $N(x, 0, 1) $ gives a normal (Gaussian) distribution with 0 mean and s.t.d. = 1. Some examples of those values are given below. 

  \begin{table}[ht] % ht means 'here, top' - insert here if you can, at the top of next page if not
    \centering
    \begin{tabular}{ll}
      c & $\alpha$ for N(x, 0, 1) \\
      \hline
      1.00 & 0.3173 \\
      1.50 & 0.1336 \\
      2.00 & 0.0455 \\
      2.50 & 0.0124 \\
      3.00 & 0.0027 \\
      3.50 & 0.0005 \\
      4.00 & 0.0001 \\
    \end{tabular}
    \caption{Univariate Gaussian Distribution values}
    \label{tab:examples}
  \end{table}

  \begin{definition}[Outlier for a Single N(x, 0, 1) Gaussian Attribute]
    An object with attribute value x from a Gaussian distribution with mean 0 and s.t.d. 1 is an outlier if
    \begin{equation}
      \label{eq:outlier_uni}
      |x| \geq c,
    \end{equation}
    where $c = \alpha$ is a constant chosen so as to indicate the object's existence as being low probability for this model.
  \end{definition}

  If the distribution of an attribute of interest (for the \textit{normal} objects) has a Gaussian distribution with mean $\mu$ and a standard deviation $\sigma$, i.e. an $N(x, \mu, \sigma)$ distribution, then to use the above definition, we need to transform the attribute x to a new attribute z that has an N(0, 1) distribution. In particular, the common approach is to set $z = (x - \mu)/\sigma$. $z$ is typically called a z-score. However, $\mu, \sigma$ are typically unknown and are estimated using the sample mean $\bar{x}$ and sample s.t.d. $\sigma_x$. In practice, this works well when the number of observations is large. However, the distribution of z is \textit{not actually} N(0, 1). A more sophisticated statistical procedure is Grubbs' test.

\subsection{Outliers in a Multivariate Normal Distribution}
\label{sec:multivar_gaussian}
We would like to apply the same approach to multiple variables/features. However, the different attributes may be correlated, and we need to take that into account. Figure \eqref{covariance_matrix_example} illustrates an example covariance matrix of a 2D multivariate Gaussian distribution with mean of (0, 0). 
% to be CONTINUED, DUN DUN DUNNNN

\begin{gather}\label{covariance_matrix_example}
  \Sigma =
  \begin{bmatrix}
    1.00 & 0.75 \\
    0.75 & 3.00
  \end{bmatrix}
\end{gather}

If we are going to use a distance measure for detecting outliers, the Mahalanobis distance takes the shape of the data distribution into account. Denote the Malahanobis distance between a point x and its mean $\bar{x}$ as $M(x, \bar{x})$.
\begin{equation}
  \label{eq:malahanobis}
M(\vec{x}, \vec{\bar{x}}) = (\vec{x} - \vec{\bar{x}})S^{-1}(\vec{x}- \vec{\bar{x}})^T
\end{equation}
is the equation of the Malahanobis distance with $S$ the covariance matrix of the data. Pretend they're vectors. EDIT: Actually, I now made a macro to make writing vectors easier in emacs.

Anywho, the M distance of a point to the mean of the underlying distribution can be shown to be directly related to the probability of the data point even existing. In particular, the M-distance = the log of the probability density of the point plus a constant. The Tan book gives an example where it inserts two outliers in a data distribution, where the Euclidean distance E(A, $\bar{x}$) is higher than E(B, $\bar{x}$), yet the M distance M(A, $\bar{x}$) is lower because of the shape of the distribution.

\subsubsection{The Mixture Model Approach for Anomaly Detection}

In clustering, the mixture model approach assumes the data comes from a \textit{mixture} of probability distributions and that each cluster can be identified with one of the aforementioned distributions. Similarly, for anomaly detection, the data is modeled as a mixture of two distributions - one for ordinary data, one for outliers. Section 9.2 of Tan goes into more detail about the mixture model and the equations necessary. We can use something simpler than the EM algorithm in Section 9.2.

Start with two sets: normal objects, and anomalous objects. Everything is in the normal objects at first. An iterative procedure transfers objects from the ordinary set to the anomalous set as long as the transfer increases the overall likelihood of the data.

Now assume the data set D contains objects from a \textit{mixture} of two probability distributions: M, the distribution of the majority of normal objects, and A, the distribution of anomalous objects. The \textit{overall} probability distribution of the data can be written as
\begin{equation}
  \label{eq:distrib}
  D(\vec{x}) = (1-\lambda)M(\vec{x}) + \lambda A(\vec{x})
\end{equation}
where x is an object/data point and $\lambda$ is a number/parameter between 0-1 that gives the (expected) fraction of outliers. The data is used to \textbf{estimate} M(x), and A(x) is often taken to be uniform. Let $M_t$ and $A_t$ be the sets at time t. At t = 0, $M_0 = D$ and $A_0$ is empty. At an \textbf{arbitrary} time t, the likelihood of the data, and the log likelihood of the \textit{entire} data set D are given by the following two equations, respectively:
\begin{equation}
  \label{eq:likelihood}
  \begin{split}
  L_t(D) & = \prod_{x_i \in D} P_D(\vec{x}_i) = \left( (1-\lambda)^{|M_t|} \prod_{\vec{x}_i \in D} P_{M_t}(\vec{x}_i)\right) \left( \lambda^{|A_t|} \prod_{\vec{x}_i \in A_t} P_{A_t}(\vec{x}_i)\right) \\
  LL_t(D) & =  |M_t|log(1-\lambda) + \sum_{\vec{x}_i \in M_t} \log P_{M_t}(\vec{x}_i) + |A_t| \log \lambda + \sum_{\vec{x}_i \in A_t} \log P_{A_t} (\vec{x}_i) \\
    \end{split}
  \end{equation}
  where $P_D, P_{M_t}, and P_{A_t}$ are the probability distribution functions for $D, M_t, A_t$ respectively. This can be derived from the general definition of a mixture model given in section 9.2.2. To do so, make the simplifying assumption that the probability is 0 for \textbf{both} of the following situations: (1) an object in A is a normal object, and (2) an object in M is an outlier. The algorithm basically moves an object from M to A and computes the log likelihood and if the change is above a threshold c, then the change is kept, otherwise it's reversed. Could probably write this pretty naturally in a Lisp language, like Clojure. Also, you may have other distributions for the anomalous set A. The \textbf{strengths and weaknesses} of the statistical approach are summarized as such: it has a firm foundation and build on standard statistical techniques, and these tests can be very effective if there is a sufficient knowledge of the data and type of test that should be applied. However, multivariate data have fewer tests, and can perform poorly for high-dimensional data.

\section{Proximity-Based Outlier Detection}
\label{sec:proximity}

Proximity-Based methods are simpler and more general than statistical approaches, since it's easier to determine a meaningful proximity measure than determine its statistical distribution. One method is distance to k-Nearest Neighbor. But these methods take $O(m^2)$ time, sensitive to parameter choice (e.g. k), and these methods can't handle data with different density regions due to the global assumptions/thresholds made.

\section{Density-Based Outlier Detection}
\label{sec:density}

From a density-based viewpoint, outliers are objects that are in regions of low density.
\begin{definition}[Density-Based Outlier]
  The outlier score of an object is the \textit{inverse} of the density around an object. 
\end{definition}
One common approach is the define density as the reciprocal of the \textbf{average distance} to the k nearest neighbors. Another definition of density is the one used by the DBSCAN clustering algorithm. Even another approach is to simply treat the density as the number of objects within a specified distance d of the object. \textbf{However}, all of these notions fail in identifying outliers correctly when the data has regions of differing density. Therefore, we must define a notion of \textbf{relative density}. One method, used by the SNN density-based clustering algorithm, is discussed in Tan, Ch. 9.4.8. Another method is to compute the relative density as the ratio of the \textit{density of a point x} and the \textit{average density of its nearest neighbors y}. Check the book for the equation, eq. 10.7. One method is the Local Outlier Factor (LOF) technique. Algorithm 10.2 describes a simplified version, reproduced in the listing below.

\begin{lstlisting}[caption = {Relative density outlier score algorithm}] 
  Define k as the number of nearest neighbors.

  for every object do:
     Determine N(x, k)
     # the k-nearest neighbors of x
     Determine dens(x, k)
     # the density of x, using its nearest neighbors, for example the objects in N(x, k)
     
  end for
  for every object do:
     Set the outlier score O(x, k) = average relative density(x, k)
  end for
\end{lstlisting}

\subsubsection{Strengths and Weaknesses}
These methods are $O(m^2)$ in time, though for low-dimensional data it can be reduced to $O(m\log m)$ with special data structures. Parameter selection can also be difficult, though the standard LOF algorithm addresses this by looking at a variety of values for k and taking the maximum outlier socres. However, upper and lower bounds still need to be chosen.

\section{Clustering-Based Techniques}
\label{sec:clust}

\begin{definition}[Clustering-Based Outlier]
  An object is a cluster-based outlier if the object does not strongly belong to any cluster.
\end{definition}
This can be evaluated by: (1) Looking at prototype-based clustering methods and evaluating a point as an outlier if it's too far from its prototype center, (2) if the elimination of an object results in a substantial improvement in the objective function (i.e. the SSE for K-means), then it is considered an outlier.

We could do this in a variety of ways:
\begin{itemize}
\item Measure the distance of the object to the cluster prototype and take this as the outlier score of the data point
\item Construct an outlier score that measures the \textit{relative} distance of an object from the cluster prototype w.r.t. the distances of the other objects in the cluster (for regions of differing densities)
\item If the clusters can be modeled in terms of Gaussian distributions, one can try the Mahalanobis distance
\item For clustering techniques with an objective function, we can assign an outlier score to an object proportional to the improvement in the objective function when that object is eliminated. However, this is comp. expensive.
\end{itemize}

One question is: what is the impact of outliers on the \textit{initial} clustering? If outliers are detected via clustering, but we made the clusters and have outliers in them, how valid are our results? No easy answer to this. One simple approach is to cluster objects, remove outliers, and cluster again - do different clusters emerge?

Another question is the number of clusters to use. Objects may or may not be considered an outlier depending on the number of clusters used. Two approaches may be used (or others): (1) repeat the analysis for multiple numbers of clusters, or (2) choose a large number of small clusters. The idea here is smaller clusters tend to be more cohesive, and if an object is \textit{still} an outlier even with a large number of small clusters, it is likely a true outlier. The downside is that \textit{groups} of outliers may form small clusters and thus escape detection.

Some clustering techniques, such as K-means, have linear or near-linear time/space complexities, so outlier/anomaly detection can be highly efficient. However, there are downsides as mentioned in the previous paragraphs.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
