(TeX-add-style-hook
 "lect10_anomalies"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "math_pkgs"
    "article"
    "art10")
   (LaTeX-add-labels
    "sec:stat"
    "sec:univariate_gauss"
    "eq:gaussian"
    "tab:examples"
    "eq:outlier_uni"
    "sec:multivar_gaussian"
    "covariance_matrix_example"
    "eq:malahanobis"
    "eq:distrib"
    "eq:likelihood"
    "sec:proximity"
    "sec:density"
    "sec:clust"))
 :latex)

