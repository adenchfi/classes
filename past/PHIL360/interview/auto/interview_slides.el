(TeX-add-style-hook
 "interview_slides"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("biblatex" "backend=biber" "style=authoryear" "sorting=none")))
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "biblatex"
    "graphicx"
    "booktabs"))
 :latex)

