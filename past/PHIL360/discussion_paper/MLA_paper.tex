
                \documentclass[12pt]{article}

%
%Margin - 1 inch on all sides
%
\usepackage[letterpaper]{geometry}
\usepackage{times}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}

%
%Doublespacing
%
\usepackage{setspace}
\doublespacing

%
%Rotating tables (e.g. sideways when too long)
%
\usepackage{rotating}


%
%Fancy-header package to modify header/page numbering (insert last name)
%
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{} 
\chead{} 
\rhead{Denchfield \thepage} 
\lfoot{} 
\cfoot{} 
\rfoot{} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
%To make sure we actually have header 0.5in away from top edge
%12pt is one-sixth of an inch. Subtract this from 0.5in to get headsep value
\setlength\headsep{0.333in}

%
%Works cited environment
%(to start, use \begin{workscited...}, each entry preceded by \bibent)
% - from Ryan Alcock's MLA style file
%
\newcommand{\bibent}{\noindent \hangindent 40pt}
\newenvironment{workscited}{\newpage \begin{center} Works Cited \end{center}}{\newpage }


%
%Begin document
%
\begin{document}
\begin{flushleft}

%%%%First page name, class, etc
Adam Denchfield\\
Elisabeth Hildt\\
PHIL 360 - Ethics\\
\today \\


%%%%Title
\begin{center}
Uniqueness: An Outdated Concept?
\end{center}


%%%%Changes paragraph indentation to 0.5in
\setlength{\parindent}{0.5in}
%%%%Begin body of paper here

Is there worth in being unique?

We speak of uniqueness highly, as if it is something to strive to. Yet many of us feared being viewed as outcasts when we were young, and wanted to conform and be ‘normal’ relative to our peers. Wouldn’t we want a more unified sense of community with voluntary conformity? The more distinct the people are, the less likely the community can agree, and dissent follows.

Before getting too far into this discussion, we must decide: what \textit{is} uniqueness? Although this paper addresses uniqueness as a whole. To consider someone unique, we consider someone as the sum of their physical self, their personality, and their actions/lifestyle, all of which can be distinct and unique in an individual. We will not speak of the perception of uniqueness, except in the sense of being able to distinguish yourself as distinct from others. 

So, then, what is the worth of being unique?

Is it the Deontological idea that, in fact, we should pride ourselves on our uniqueness? That there is an inherent worth in being unique? Though that can be a stance to hold onto, this is not the one I take. It is too easy to poke holes into this idea. There are certain forms of uniqueness that most people can agree are wrong to have, such as having a tendency to kill people, mistreat their friends, or abuse children. Priding yourself on being unique in any way does not fit, then, with societal culture; those with unique yet ‘criminal’ quirks are punished in society. Yet is priding one’s self on culturally/socially acceptable uniqueness really what we think of when we think of an inherent value to being unique? No, I doubt it.

What of utilitarianism? Is there value and utility in being unique? An initial argument may suggest that a varied population is good, because of the current trend in specialization. Different people have different skills, and they can focus on providing value for their communities and society through that more effectively than some others would in their place. Yet in other ways we wish to reduce uniqueness; we want everyone to be fed, to be clothed, housed, educated. Again, there are some forms of uniqueness that are unacceptable in a utilitarian perspective. I argue, however, that all forms of uniqueness will (eventually) be useless from a utilitarian perspective, in terms of maximizing utility. In particular, as technology progresses, we will have the ability to mass educate people very effectively  perhaps through implants in the brain. We will also have the technology to allow people to be stronger and faster, whether it be through cybernetic or genetic enhancements. We could reasonably, within two or three hundred years, enable a population to have all the skills people can have. We could give people the understanding of all fields and the physical makeup/enhancements to excel at all physical activities we do nowadays. This is the context (the thought experiment, if you will) from which I make my arguments; I will assume that our technology will progress in such a way, giving way to an ‘ideal’ world I have described above. Now, in terms of happiness and suffering, all forms of physical suffering and suffering from ignorance could be minimized, if not eradicated. Meanwhile, uniqueness will be minimized. Whatever one excels at or discovers, the information will be passed on to everyone else; new discoveries in the sciences will be communicated instantly to everyone in the world, and we’ll have the processing power to understand it. We may choose to perform different actions in our day-to-day lives, but the progress made for one person can and may be shared with everyone else. Is this society of healthy, brilliant individuals not a good one? For pure utilitarianism, I would argue that it is. However, pure utilitarianism would say an equally valid approach to a society is to put everyone in sensory-stimulation tanks that keep them extremely happy all the time. Needless to say, if our only goal is to maximize current happiness with the tools available, we may not have chosen the optimal path.

Preference utilitarianism, on the other hand, may provide a case for uniqueness. This comes from the fact that people tend to have different preferences, goals, and interests. If we did end up having the technology to pursue some sort of hive-mind-like existence where we share all knowledge and physical ability, there will still be a preference for more discovery. Discovery of the unknown will be the only real mystery left, and there are many avenues for such discovery. Thus, people can pursue differing means of discovery; some may go explore other planets, while others seek to understand the depths of the ocean. Some may continue to try and understand exactly how the brain others, and others may try to revive extinct species. Though any knowledge they gain would end up being shared by all, their actions would still be different. One person would want to be a medical researcher; even if what they discover is shared with everyone that night, it isn’t having the knowledge that makes the person unique; it is being the one to discover it. Ownership of discovery and progress in the world could end up being the way to identify uniqueness. Is there worth in being unique in that regard? With preference utilitarianism, I would argue yes. Individuals can achieve uniqueness by following their goals/preferences. By achieving uniqueness through those means, individuals then can experience pride and happiness.

Yet that is not the only kind of uniqueness one can have in a post-scarcity society. There is still the possibility of having aesthetic uniqueness; self-expression through your looks. This touches on the 'physical' aspect. I will argue that ‘aesthetic uniqueness’ in terms of physical looks could end up serving a purpose, at least in terms of utilitarianism. Supposing we are to maximize the amount of peoples’ preferences that become fulfilled, there is indeed room for aesthetic uniqueness. Supposing it does not interfere with one’s work and role in society, one can have preferences to look different from others and further gain a sense of distinctness and identity. A caveat is that one may gain a sense of identity from appearing different from others, but as we have seen from history, people looking different is often translated into them being different; and being different is then translated into being inferior. I will dismiss this notion, as I suppose that in this more advanced and knowledgeable society, every person will be able to distinguish the fact that how one looks does not make them inferior inherently.

Virtue ethics is an interesting case. I will argue that virtue ethics actually supports conformity. Suppose we are to work to be virtuous people. In order to learn what a virtuous person is, we refer to people commonly accepted as virtuous. We then emulate their virtuousness in our own lives, which limits our uniqueness. Ideally, virtue ethics wishes for the whole world to be full of virtuous people. Ironically, a case for uniqueness does present itself; how can one be sure they are virtuous without non-virtuous people to compare one’s self to? That interpretation suggests that the ‘uniqueness’ that should be embraced is to not be virtuous, so that virtuous people can better learn how to be virtuous from a counterexample. I will dismiss this notion, however, because it does not seem sensible from a moral standpoint to support people being the opposite of virtuous. Furthermore, one does not necessarily need a counterexample to virtuousness in order to know how to be virtuous.

Evolution dictates uniqueness is a good thing, in terms of genetic uniqueness. Sexual reproduction started as a way to combat the susceptibility of species to viruses and infection. With advances in technology and genetic alterations to fetuses in the public lens, we must make sure to keep a certain amount of genetic uniqueness among our population to combat the aforementioned risk of viruses/infection. This determines there is a minimum amoumt of genetic uniqueness if we are to consider human health and wellbeing as a goal for society. As genetics play a significant role in personality and how one's brain grows to process information, it can be concluded that our eventual society should indeed maintain a minimum standard of uniqueness. 

With the above conflicting views on uniqueness, one must ask if there is a way to consider the possible overlap between the perspectives to achieve a final understanding of the worth behind being unique. 

When considering the health view, if we are to correlate health with happiness, then pure utilitarianism must end up supporting a minimum standard of uniqueness. Deontology supports uniqueness, and preference utilitarianism could support both, though I have argued that it supports uniqueness. Virtue ethics, interestingly enough, may be the only ethical framework left that presents a challenge to the notion of uniqueness being worthwhile. Even that challenge can be disregarded if one again considers the goal of maximizing human health and protection from disease; it is virtuous to support the health and wellbeing of one's community. 

However, this does not take away an upper bound on uniqueness. With protection from disease as a goal, we can seek to increase genetic diversity on only certain parts of our genome; after all, we have identified certain genes as making us susceptible to particular disorders. In our genetic diversity, we nevertheless do not want to include those genes in our community. Since this genetic diversity is to only be taken so far, then, there has been both lower and upper bounds established on genetic uniqueness. Recall that genetics affect appearance and personality, resulting in a unique population, though perhaps much more homogenous than today's society.

With this conclusion, one concern is whether or not this precludes identical twins, since they are genetically identical. This is one case where Kant's categorical imperative is not sensible to apply; if everyone were genetically identical, it would be bad, but the existence of pairs of twins cannot be realistically extended to the entire population. It may be different with the idea of cloning. It may not be feasible that everyone is cloned from one person. However, recall Henrietta Lacks. Her cells have never stopped dividing, and are used in experiments all over the world. If stem cell research advances to the state where they can use Lacks' cells for stem cell production, then it is possible for \textit{many} stem cell-related medical procedures (such as organ repair/replacement) to have the same progenitor: Henrietta Lacks. These organs would have the same genetic susceptibilities that Lacks herself had. It would be an imperfect yet plentiful method of providing organs to those who need it. However, I argue that Lacks' cells will not be used for this purpose once it becomes easy and cheap enough to use a person's own cells to replace organs/tissue with.

I have concluded in this work that there is worth in being unique, but there are limits to the benefits of uniqueness. These questions will increasingly come up as our population continues to homogenize. From this discussion, I encourage people to not be afraid of the increasing homogenization of our global society, as long as we meet a minimum standard on genetic diversity. There will be legitimate risks to this minimum genetic diversity as gene therapy inevitably becomes a popular trend, and to that end, I encourage legislators and medical professionals to enact policies that keep a a minimum genetic diversity in our population. 


\end{flushleft}
\end{document}
\}