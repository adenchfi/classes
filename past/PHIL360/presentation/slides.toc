\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Background}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{Safety Statistics}{6}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Will My Car Choose to Kill Me?}{8}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{The Moral Dilemma of Self-Driving Cars}{11}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{The Problem}{11}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{A Sensible Solution}{12}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Extra - The Future of Self-Driving Vehicles}{13}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Possible Consequences and Remedies}{13}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Advantages of Self-Driving Cars}{14}{0}{3}
