(TeX-add-style-hook
 "lab_report_4"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "margin=0.5in")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "graphicx"
    "amsmath"
    "gensymb")
   (LaTeX-add-labels
    "background"
    "scint"
    "PMTs"
    "eq:Compton"
    "eq:comp_elec"
    "eq:limit"
    "Cs137_gamma"
    "Co60_gamma"
    "Co57_gamma"
    "Cd109_gamma"
    "Na22_gamma"
    "cosum_gamma"
    "nasum_gamma"))
 :latex)

