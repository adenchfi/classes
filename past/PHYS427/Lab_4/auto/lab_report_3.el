(TeX-add-style-hook
 "lab_report_3"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "margin=0.5in")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "graphicx"
    "amsmath"
    "gensymb")
   (LaTeX-add-labels
    "background"
    "scint"
    "PMTs"
    "Cs_Coarse_32"
    "Gain_vs_Res_Table"
    "Co60_6"
    "Co60_12"
    "Co60_18"
    "Mn"
    "Co60"
    "Co57"
    "Na22"
    "Cd109"
    "Cs137"
    "Gamma_vs_Res_Table"
    "linear_fit")
   (LaTeX-add-bibliographies
    "sample"))
 :latex)

