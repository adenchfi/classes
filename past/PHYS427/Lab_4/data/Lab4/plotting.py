#This is a script that fits to a set of data points to any function that you specify
#You fit the data by defining the function wiht some parameters that will be used to fit the data
import numpy as np
import random
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
import math
import sys

csvfile = sys.argv[1]
#x is the x axis of points
#data will be some points that I will make up to look like the once you would get fro experiment
channel, energy, counts = np.loadtxt(csvfile, delimiter=',', unpack=True)

def ten_round(n):
    return (n + 9) // 10 * 10
step = max(energy) / 20

plt.plot(energy, counts, label = csvfile)
plt.xticks(np.arange(0, max(energy), ten_round(int(max(energy))/20)), size = 13)

plt.xlabel("Energy (keV)", size = 24)
plt.ylabel("Counts", size = 24)
plt.title("Gamma Spectrum", size = 24)
plt.tick_params(direction='in', length=6, width=2)

plt.axvline(28, color='r', label="Iodine")
plt.axvline(35, color='g', label = "Int. Conv.")
plt.axvline(75, color='c', label = "Pb")
if (max(energy) > 240):
    plt.axvline(250, color='y', label = "Backscatter limit")
if (max(energy) > 500):
    plt.axvline(511, color='m', label = "Annihilation")
if (max(energy) > 1000):
    plt.axvline(1022, color='k', label = "Pair Production")
plt.legend(loc='top middle', fontsize = 18)

axes = plt.gca() # get current axes
axes.set_xlim(left = 0)
axes.set_ylim([0,4000])
plt.show()




