%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)

% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[margin=0.5in]{article}

%\usepackage{mhchem} % Package for chemical equation typesetting
%\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
%\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements 
\usepackage{gensymb}
%\usepackage{biblatex}
%\addbibresource{sample.bib}
\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Detection of X-rays \\ } % Title

\author{Adam \textsc{Denchfield}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date


\begin{center}
\begin{tabular}{l r}
Date Performed: & October 20th, 2017 \\ % Date the experiment was performed
Partner: & Yonas Gebre \\ % Partner names
Instructor: & Professor Zasadzinski % Instructor/supervisor
\end{tabular}
\end{center}

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Objectives}
\begin{itemize}
\item Using gamma spectrum analysis, study the spectra of a variety of radioactive isotopes using a NaI(TI) gamma ray spectroscopy system
\item Identify the peaks in each of the spectra and what their origins are
\end{itemize}


% If you have more than one objective, uncomment the below:
%\begin{description}
%\item[First Objective] \hfill \\
%To become acquainted with the production, use, and properties of x-rays
%\item[Second Objective] \hfill \\
%Learn dispersive techniques for measuring an x-ray source
%\item[Third Objective] \hfill \\
%To demonstrate the suitability of the Bohr model for discrete x-ray lines
%\item[Fourth Objective] \hfill \\
%Introduction of random statistics and uncertainties in data
%\end{description}

\section{Background \& Experiment Setup}
\label{background}

\subsection{Gamma Ray Detection}
Light comes in a variety of energies. The highest-energy photons are known as gamma rays, and originate both from outside Earth (and are called cosmic rays) and from nuclear processes such as decays. In order to detect these gamma rays, a setup involving scintillators, photomultiplier tubes (PMTs), and a multichannel analyzer is employed. A scintillator is any material that absorbs light, which excites an electron, and re-emits light when the electron de-excites. The electron can have staggered drops in energy levels, yielding multiple photons of different energies. This lab uses an inorganic scintillator. For inorganic scintillators, an incoming photon excites an electron from the valence band to the conduction band or the exciton band, which leaves an associated hole behind in the valence band. Impurities create electronic levels in the forbidden gap (the region between the valence and conduction bands). Electrons captured in impurity centers (thereby dropping to those levels in the forbidden gap) emit light; the impurities are chosen to make sure the light emitted is in the visible range, for the purposes of this lab. This process is illustrated in Figure \ref{scint}. 
\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{scint.png} % Include the image placeholder.png
\caption{Energy band visualization of an inorganic scintillating material. }
\label{scint}
\end{center}
\end{figure}

These visible-light photons are then detected by PMTs. PMTs use the photoelectric effect to create a cascade of electrons. The first electron is created with an energy $E = h\nu - \phi$, where $\phi$ is the activation function (the energy required to actually break the electron free from the system). This electron proceeds to scatter throughout an individual photomultiplier tube, giving other electrons enough kinetic energy to also break free from their bonds. Thus a cascade of electrons follows, which can be fed into an amplifier and a current can be measured. This process is depicted in Figure \ref{PMTs}. 

\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{PMTs.png} % Include the image placeholder.png
\caption{The gamma detection process, involving a scintillator, a PMT, and a multichannel analyzer at the end of it.}
\label{PMTs}
\end{center}
\end{figure}

The Multichannel Analyzer has 1024 channels and a 1024-channel high resolution Analog-to-Digital converter. It also has a built-in linear amplifier and high voltage power supply for the PMT. Ultimately, the incoming gamma ray will produce a voltage pulse with a height (amplitude) proportional to the gamma energy. The energy spectrum of a gamma source (such as radioactive isotopes) is created from signals coming from the PMTs; the signals are processed according to their amplitudes and then stored in corresponding channel memory locations. Channels 1 - 1024 are merely proportional to gamma energy, having been set by the incident gamma energy as well as amplifier gain. Calibration is then needed to convert channel \# to energy.

The scintillation detector is an NaI(TI) scintillation crystal; this means that in the NaI crystal, there are 0.1\% TI impurities that provide electron energy levels, defining the scintillation process and the resulting pulse of light to be in the visible range. The crystal is housed in a thin metal cover, typically Aluminum, which keeps out stray light and also reflect internal light produced by the gamma ray. The detector is typically surrounded by an external lead shield 1.3cm thick to cut down on unwanted cosmic ray background. Finally, detector resolution can be estimated by dividing the Full-Width Half-Maximum (FWHM) between detector channels by the centroid channel. 

\subsection{Gamma Ray Peak Origins}
There are a variety of processes that work to produce/absorb gamma rays. Before we can understand the spectra of our samples, we must know how to go about identifying the peaks we are going to see. The possible origins of gamma rays in the context of this experiment are summarized below:

\subsubsection*{Photoelectric Effect}
This process occurs when an incoming gamma ray, typically less than 250 keV, gets absorbed by a bound electron in an atom. The electron is kicked out with a kinetic energy equal to the gamma energy minus the electron binding energy (or the workfunction of the material). A vacancy results in the atom and an electron from an outer shell falls in, resulting in a characteristic X-Ray for the atom being produced. The X-Rays are easily absorbed again in the crystal, and the escaped electron usually deposits its kinetic energy in the crystal as well.

\subsubsection*{Compton Scattering}
The incident gamma ray, with $h\nu$ usually between 300-5000 keV, collides with a valence electron and inelastic scattering occurs. The incident photon transfers some of its kinetic energy to the electron, and has an energy $E = h\nu'$ whereas the electron has a kinetic energy $T_c$. The following is Compton's formula for the energy of the scattered photon in terms of the initial energy $h\nu$ and the scattering angle $\theta$:
\begin{equation}
  \label{eq:Compton}
  h\nu' = \frac{h\nu}{1 + \frac{h\nu}{m_o c^2} (1-\cos\theta)}  
\end{equation}

where $m_o c^2$ is the rest mass energy of an electron, 511 keV. Using the above, we can deduce that the kinetic energy $T_c$ of the Compton-scattered electron is
\begin{equation}
  \label{eq:comp_elec}
  T_c = h\nu - h\nu'
\end{equation}

\subsubsection*{Pair Production}
Pair production occurs when a photon of energy above 1022 keV gets converted into an electron-positron pair. Furthermore, the probability of this happening increases as the energy increases. One thing to note is that the electron-positron pair has zero net momentum in the center-of-momentum frame of the original photon. This means they travel in opposite directions once created. After the pair is produced, the positron is still in the crystal, and almost always annihilates with another electron in the crystal, forming two 511 keV photons. If these both escape the crystal, a double escape peak appears in the spectrum equal to the total absorption peak minus 1022 keV. However, if only \textit{one} of these photons escapes from the crystal, then a single escape peak will occur at total absorption minus 511 keV. 

\subsubsection*{Pb X-Rays}
If the crystal is shielded with lead, the incident gamma rays can be absorbed via photoelectric effect in the lead. The resulting Pb X-Rays interact with the detector, and produces a peak at 75 keV.

\subsubsection*{Annihilation Peak}
If the gamma source also emits positrons, then annihilation will occur and a peak at 511 keV will occur.

\subsubsection*{Backscatter Peaks}
Gamma rays that escape the system and interact with the floors/walls may experience a 180\degree\ Compton scattering interaction. These photons rebound back to the detector, but their maximum energy is 255 keV. This can be seen if we let $\theta$ = 180\degree\ in Eq. \eqref{eq:Compton}:

\begin{align}
  \label{eq:limit}
  h\nu' = & \frac{h\nu}{1 + 2\frac{h\nu}{m_o c^2}} \\
  & h\nu  \rightarrow \infty \nonumber \\
  & \frac{h\nu}{m_o c^2} >>  1 \nonumber \\
  h\nu' = & \frac{h\nu}{2h\nu / 511} = 511/2 = 255 keV 
\end{align}

This means a peak around 255 keV will appear on gamma ray spectra if we have backscatter.

\subsubsection*{Iodine Peak}

Lower energy gamma rays (100-200 keV) can serve as a catalyst for 28 keV Iodine X-Rays to be produced. This is similar to the Pb X-Ray peak, except it's with the detector's Iodine atoms rather than a possible Pb shield. There will then also be a peak 28 keV below the total absorption peak.

\subsubsection*{Sum Peaks}

When two or more gamma rays are emitted in cascade, the detector can sometimes produce a pulse whose energy appears to be the sum of the two energies of the peaks.

\subsubsection*{Internal Conversion Peaks}

Internal conversion is the interaction between a nucleus excited state and one of the atom's electrons. In the quantum mechanical model of the electron, there is a finite probability of finding the electron within the nucleus. During the internal conversion process, the wavefunction of an inner shell electron (usually an s electron) is said to penetrate the volume of the atomic nucleus. When this happens, the electron may couple to an excited energy state of the nucleus and take the energy of the nuclear transition directly, without an intermediate gamma ray being first produced. The kinetic energy of the emitted electron is equal to the transition energy in the nucleus, minus the binding energy of the electron to the atom. An electron is ejected from the nucleus at high speeds.

Another process that can happen is the emission of a photon when the nucleus decays from an excited state to the ground state without the above internal conversion with an electron. Typical energies appear to be around 32 keV. For example, excited $^{125}$Te has a 35 keV decay energy.

% why is C-M-m making a new item? whatever
% C-M-SPC selects the entire expression inside the delimiter set. Pretty useful imo
% `c L = \mathcal{L} - wow!
% C-cf is now making fractions
% C-cp is now making partial derivatives that are quite amazing
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Experimental Procedures}

After verifying the system was fully connected and the control computer was powered on, we powered on the Multichannel Analyzer. We set the detector voltage to 700V. For each source, we calibrated the software such that the highest peak was flush on the right, and the energies were listed in keV rather than channel number (using a 3-point calibration). The sources were $^{60} Co, ^{22} Na, ^{54}Mn, ^{137}Cs, ^{57}Co$ and $^{109}Cd$. High quality calibrated spectra were recorded. 

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Data Analysis}

\subsection{Measured Spectra}
The spectrum for $^{137}$Cs is shown in Figure \ref{Cs137_gamma}. One can observe the internal conversion peak at 32 keV, the Pb X-Ray peak at 75 keV, and two more peaks at 190 keV and 662 keV. 662 keV is a characteristic X-Ray of the material, but the 190 keV peak doesn't appear to be in $^{137}$Cs's decay scheme. This means it is likely a backscatter peak of the 662 keV photon. \\

\begin{figure}
\begin{center}
\includegraphics[width=1.00\linewidth]{Cs137_gamma.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{137}Cs$. }
\label{Cs137_gamma}
\end{center}
\end{figure} 

The spectrum for $^{60}$Co is now shown in Figure \ref{Co60_gamma}. It has two nuclear decay peaks at 1332 keV and 1173 keV. The lower peaks do not exactly match up with the effects we expect from Iodine and Pb peaks. From the data, it is likely that much of the sample is excited and is decaying from nuclear excited states. This releases 59 keV gamma rays. The peak at 225 keV is likely backscatter. \\

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Co60_gamma.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{60}Co$. }
\label{Co60_gamma}
%\end{center}
\end{figure} 

Figure \ref{Co57_gamma} displays the decay spectrum of $^{57}$Co. Unlike its isotope $^{60}$Co, there do not appear to be excited states decaying (at about 57 keV). There are decays at 5-7 keV that may be its $K\alpha$ modes, but more likely are the photons emitted from the Auger effect. \\

\begin{figure}
  % \begin{center}
  \includegraphics[width=1.0\linewidth]{Co57_gamma.png} % Include the image placeholder.png
  \caption{Gamma ray spectrum of the isotope $^{57}Co$. }
  \label{Co57_gamma}
  % \end{center}
\end{figure}

Figure \ref{Cd109_gamma} shows the spectrum of $^{109}$Cd. There is the easily identifiable decay mode at 88 keV, which is from electron capture/internal conversion, turning it into $^{109}$Ag. The small peak appears to be electrons dropping from the L-shell, and the 22 keV peak is the $K\alpha$ peaks. \\

\begin{figure}
  % \begin{center}
  \includegraphics[width=1.0\linewidth]{Cd109_gamma.png} % Include the image placeholder.png
  \caption{Gamma ray spectrum of the isotope $^{109}Cd$. }
  \label{Cd109_gamma}
  % \end{center}
\end{figure} 

Figure \ref{Na22_gamma} illustrates some of the other effects that were described. The annihilation peak is easily observed. The Iodine peak is accounted for. There is a peak around 160 keV that is not accounted for in the decay scheme of $^{22}$Na, which could also be causing the Iodine peak since that is caused by absorption of gamma rays between 100-200 keV. This peak is safely below the backscatter limit of 255 keV, so it could be backscatter. Finally, the peak at 1274 keV is the internal conversion to Neon.  \\

\begin{figure}[h]
  % \begin{center}
  \includegraphics[width=1.0\linewidth]{Na22_gamma.png} % Include the image placeholder.png
  \caption{Gamma ray spectrum of the isotope $^{22}Na$. }
  \label{Na22_gamma}
  % \end{center}
\end{figure} 

We also observed sum peaks for $^{22}$Na and $^{60}$Co in Figures \ref{cosum_gamma} and \ref{nasum_gamma}.

\begin{figure}
  % \begin{center}
  \includegraphics[width=1.0\linewidth]{Co60sum.png} % Include the image placeholder.png
  \caption{Gamma ray spectrum of the isotope $^{60}Co$, observing a sum peak.}
  \label{cosum_gamma}
  % \end{center}
\end{figure} 

\begin{figure}
  % \begin{center}
  \includegraphics[width=1.0\linewidth]{Na22sum.png} % Include the image placeholder.png
  \caption{Gamma ray spectrum of the isotope $^{22}Na$, observing a sum peak. }
  \label{nasum_gamma}
  % \end{center}
\end{figure} 

%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Results and Conclusions}

Calibration was successful, and the gamma spectra of a variety of sources have been successfully identified. The lab led to learning how to search for the decay schemes of sources, and about the variety of effects that lead to non-obvious observed peaks. 

%
%\begin{enumerate}
%\begin{item}
%%The \emph{atomic weight of an element} is the relative weight of one of its atoms compared to C-12 with a weight of 12.0000000$\ldots$, hydrogen with a weight of 1.008, to oxygen with a weight of 16.00. Atomic weight is also the average weight of all the atoms of that element as they occur in nature.
%\end{item}
%\begin{item}
%The \emph{units of atomic weight} are two-fold, with an identical numerical value. They are g/mole of atoms (or just g/mol) or amu/atom.
%\end{item}
%\begin{item}
%\emph{Percentage discrepancy} between an accepted (literature) value and an experimental value is
%\begin{equation*}
%\frac{\mathrm{experimental\;result} - \mathrm{accepted\;result}}{\mathrm{accepted\;result}}
%\end{equation*}
%\end{item}
%\end{enumerate}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

%\bibliographystyle{unsrt}

%\bibliography{sample}


%----------------------------------------------------------------------------------------


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
