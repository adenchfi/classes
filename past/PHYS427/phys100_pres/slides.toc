\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Getting Started}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{It's Never Too Early!}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Why Research?}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{My Research}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Density Functional Theory Modeling}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Finite Element Method Analysis \& Computation}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Faculty}{7}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{How do I start?}{8}{0}{3}
