import matplotlib.pyplot as plt
import numpy as np
import sys
filename = sys.argv[1]
# delimiter = , means csv
# adjust number of variables as different column numbers are used
x, y = np.loadtxt(filename, delimiter=',', unpack=True)
plt.plot(x,y, label='Mystery Element Peaks')

plt.xticks(np.arange(min(x), max(x), max(x)/10.0), size='x-large')
plt.yticks(np.arange(0, max(y), max(y)/10.0), size='x-large')
plt.xlabel('2$\\theta$', size=24)
plt.ylabel('Intensity', size=24)
#plt.title('', size='x-large')
plt.legend(fontsize='x-large')
plt.show()
