%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[margin=0.5in]{article}

%\usepackage{mhchem} % Package for chemical equation typesetting
%\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
%\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements 
\usepackage{gensymb}
%\usepackage{biblatex}
%\addbibresource{sample.bib}
\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Detection of X-rays \\ } % Title

\author{Adam \textsc{Denchfield}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Date Performed: & September 22nd, 2017 \\ % Date the experiment was performed
Partner: & Yonas Gebre \\ % Partner names
Instructor: & Professor Zasadzinski % Instructor/supervisor
\end{tabular}
\end{center}

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Objectives}
\begin{itemize}
\item Highlight the significance of using X-Ray Diffraction to determine crystal structures of materials. 
\item XRD will enable us to perform material characterization
\item Using powders of materials for what we call 'powder diffraction' will allow us to better characterize these materials
\item Understand the differences between crystal planes and how they affect diffraction peaks
\end{itemize}


% If you have more than one objective, uncomment the below:
%\begin{description}
%\item[First Objective] \hfill \\
%To become acquainted with the production, use, and properties of x-rays
%\item[Second Objective] \hfill \\
%Learn dispersive techniques for measuring an x-ray source
%\item[Third Objective] \hfill \\
%To demonstrate the suitability of the Bohr model for discrete x-ray lines
%\item[Fourth Objective] \hfill \\
%Introduction of random statistics and uncertainties in data
%\end{description}

\section{Background \& Theory}
\label{background}

\subsection{Crystal Structures}
A crystal structure is defined by two things. The first is a lattice, which is a three-dimensional set of points. The second is a basis, which essentially assigns elements to points within the lattice. They do not have to be on lattice points, so long as they are within the volume defined by the lattice. The union of these two sets defines a crystal structure. Many types of lattices exist, but for the purposes of this lab, we will be dealing with simple cubic (SC), body-centered cubic (BCC), and face-centered cubic (FCC) structures. As one might imagine, these are all cubic structures. These structures are depicted below in Figure \ref{cubic}. 

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.80\textwidth]{cubic.png} % Include the image
\caption{The three primary cubic lattice types. From left to right, SC has 1 lattice point per unit cell, BCC has 2 lattice points, and FCC has 4 lattice points per unit cell.}
\label{cubic}
\end{center}
\end{figure}

\subsection{X-ray Diffraction}
As x-rays are incident upon the surface of a crystalline structure, Bragg diffraction will create an interference pattern that one can measure. The maxima of the pattern can be defined using Bragg's law:
\begin{equation}\label{br_law}
n\lambda = 2d\sin\theta
\end{equation}
The intensity of the x-rays measured at a single point also depends on the crystal plane. Each angle with intensity above background/noise will correspond to peaks of different diffraction planes, such as (1 0 0), (1 1 0), (3 1 1), and more. Figure \ref{planes} illustrates some examples visually. 
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.85\textwidth]{planes.jpg} % Include the image placeholder.png
\caption{An illustration of different crystal planes for the simple cubic lattice structure. }
\label{planes}
\end{center}
\end{figure}
By shifting around Eq. \eqref{br_law}, we note:
\begin{equation}\label{br_law_planes}
n\frac{\lambda}{d} = 2\sin\theta
\end{equation}
For a cubic lattice, $d$ can be represented as 
\begin{equation}\label{d_def}
d = \frac{a}{\sqrt{h^2 + k^2 + l^2}}
\end{equation}
, where $(h k l)$ are the crystal planes mentioned above and $a$ is the lattice spacing. By measuring $\theta$, and knowing $\lambda$, we can identify $d$. By observing the order of the peaks, we can infer which crystal plane is diffracting the x-rays. An illustration of this peak-ordering is shown in Figure \ref{ordered_peaks}. With $d$ and $(h k l)$ identified, we can identify $a$ and compare that to measured values. 
 
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.85\textwidth]{ordered_peaks.png} % Include the image placeholder.png
\caption{A figure pulled from Google Images. This displays the order of the peaks and how they correspond to different crystal planes. For BCC and FCC structures, the available crystal planes are different; some of these planes are eliminated as diffraction candidates.}
\label{ordered_peaks}
\end{center}
\end{figure} 
 

 
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Experimental Procedures}

In order to observe many different diffraction planes $(h k l)$, we will use powder diffraction. This is a technique wherein one grinds the material in question into a powder, where the variety of smaller crystals will be randomly oriented so as to allow the incoming x-rays to interact with the crystals through different planes. A depiction of the process is shown in Figure \ref{powder}. \\

\begin{figure}[h]
\begin{center}
\includegraphics[width=1.00\linewidth]{powder.png} % Include the image placeholder.png
\caption{Incoming x-rays can be assumed to be collinear. They diffract differently with differing crystal planes, yielding different angles $\theta$ that yield intensity maxima for the interference pattern.}
\label{powder}
\end{center}
\end{figure} 

Measurements of the light intensity were done from $2\theta$ = $10\degree$ to $120\degree$. We did powder diffraction on both KCl and Si powders. In addition, an unknown alkali-halide was measured; our goal is to identify this compound by finding its lattice constant. 

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Data Analysis}

One can easily identify the peaks visually. Shown in Figures \ref{Si_powder} - \ref{element_powder} are the x-ray spectra for Si and KCl powders and two unknown powders. 

\begin{figure}[h]
\begin{center}
\includegraphics[width=1.00\linewidth]{Si_powder_spectrum.png} % Include the image placeholder.png
\caption{X-ray spectrum of a Si powder. }
\label{Si_powder}
\end{center}
\end{figure} 

\begin{figure}[h]
%\begin{center}
\includegraphics[width=1.0\linewidth]{KCl_powder_spectrum.png} % Include the image placeholder.png
\caption{X-ray spectrum of a KCl powder. }
\label{KCl_powder}
%\end{center}
\end{figure} 

\begin{figure}[h]
%\begin{center}
\includegraphics[width=1.0\linewidth]{unknown_powder_spectrum.png} % Include the image placeholder.png
\caption{X-ray spectrum of an unknown compound powder. }
\label{unknown_powder}
%\end{center}
\end{figure} 

\begin{figure}[h]
%\begin{center}
\includegraphics[width=1.00\linewidth]{element_powder_spectrum.png} % Include the image placeholder.png
\caption{X-ray spectrum of an unknown elemental powder. }
\label{element_powder}
%\end{center}
\end{figure} 

Table \ref{Peaks_Table} shows some of the first peak angles, associated $(h k l)$ planes, and calculated lattice constant $a$ for each sample. 

\begin{table}\label{Peaks_Table}
\caption{Powder Diffraction Spectra}
\begin{center}
\begin{tabular}{lllll}
Substance & $2\Theta\degree$ & (h k l) & Measured $a$ (nm) & Accepted Lattice Constant (nm) \\ 
\hline
KCl & 28.6$\degree$ & (2 0 0) & 0.623 & 0.63 \\
KCl & 40.7$\degree$ & (2 2 0) & 0.626 & 0.63 \\
Si & 28.4$\degree$ & (1 1 1) & 0.544 & 0.543 \\
Si & 47.2$\degree$ & (2 2 0) & 0.540 & 0.543 \\
Si & 56.0$\degree$ & (3 1 1) & 0.543 & 0.543 \\
Unknown Compound & 38.9$\degree$ & (2 0 0) & 0.4625 & N/A \\
Unknown Compound & 56.2$\degree$ & (2 2 0) & 0.4624 & N/A \\
Unknown Element & 44.55$\degree$ & (1 1 1) & 0.3518 & N/A \\
Unknown Element & 51.8$\degree$ & (2 0 0) & 0.3526 & N/A \\
\end{tabular}
\end{center}
\end{table}


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Results and Conclusions}

Given the unknown was an alkali-halide, that meant that it had an FCC crystal structure. With that in mind, it was not difficult to compare the spectrum to our KCl sample and observe the similar (2 0 0) and (2 2 0) peaks. Using those values, a lattice constant of 0.462 nm was observed. This is consistent with the lattice spacing of NaF, sodium fluoride, whose lattice constant is 0.462 nm. Furthermore, the unknown element has a lattice constant of 0.352 nm, which corresponds with that of Nickel. Finally, to answer the last lab question, the reason we don't do powder diffraction on the tabletop systems is that not only do we have a more limited angle range and mobility, but our sample holder is not designed for holding small powder samples. We also could not get nearly as precise measurements. 

To conclude, X-ray diffraction is a powerful tool. This lab has hammered in the ability to identify lattice structures and lattice constants just from powder diffraction data, an amazing feat in of itself. The techniques learned in this lab should be intimately familiar to any researcher in condensed matter physics, no matter if they are an experimentalist or not.


%
%\begin{enumerate}
%\begin{item}
%%The \emph{atomic weight of an element} is the relative weight of one of its atoms compared to C-12 with a weight of 12.0000000$\ldots$, hydrogen with a weight of 1.008, to oxygen with a weight of 16.00. Atomic weight is also the average weight of all the atoms of that element as they occur in nature.
%\end{item}
%\begin{item}
%The \emph{units of atomic weight} are two-fold, with an identical numerical value. They are g/mole of atoms (or just g/mol) or amu/atom.
%\end{item}
%\begin{item}
%\emph{Percentage discrepancy} between an accepted (literature) value and an experimental value is
%\begin{equation*}
%\frac{\mathrm{experimental\;result} - \mathrm{accepted\;result}}{\mathrm{accepted\;result}}
%\end{equation*}
%\end{item}
%\end{enumerate}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}

\bibliography{sample}


%----------------------------------------------------------------------------------------


\end{document}
