import pandas as pd
import sys
xlsx_file = sys.argv[1]
data_xls = pd.read_excel(xlsx_file, index_col=None)
data_xls.to_csv('element.csv', encoding='utf-8', index=False)
