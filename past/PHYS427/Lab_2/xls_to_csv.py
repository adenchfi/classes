import pandas as pd
import sys
xls_file = sys.argv[1]

data_xls = pd.read_excel(xls_file, 'Sheet1', index_col=None)
data_xls.to_csv('output.csv', encoding='utf-8')
