(TeX-add-style-hook
 "math_pkgs"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "english")))
   (TeX-run-style-hooks
    "amsmath"
    "graphicx"
    "inputenc"
    "babel"
    "amsthm")
   (LaTeX-add-amsthm-newtheorems
    "definition"
    "remark"))
 :latex)

