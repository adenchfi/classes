%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

%\usepackage{mhchem} % Package for chemical equation typesetting
%\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements 
\usepackage{gensymb}
%\usepackage{biblatex}
%\addbibresource{sample.bib}
\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Detection of X-rays \\ } % Title

\author{Adam \textsc{Denchfield}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

\begin{center}
\begin{tabular}{l r}
Date Performed: & September 8th, 2017 \\ % Date the experiment was performed
Partner: & Yonas Gebre \\ % Partner names
Instructor: & Professor Zasadzinski % Instructor/supervisor
\end{tabular}
\end{center}

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Objectives}
\begin{itemize}
\item To become acquainted with the production, use, and properties of x-rays
\item Learn dispersive techniques for measuring an x-ray source
\item To demonstrate the suitability of the Bohr model for discrete x-ray lines
\item Learn to deal with random statistics and uncertainties in data, learn to fit data with Gaussians
\end{itemize}


% If you have more than one objective, uncomment the below:
%\begin{description}
%\item[First Objective] \hfill \\
%To become acquainted with the production, use, and properties of x-rays
%\item[Second Objective] \hfill \\
%Learn dispersive techniques for measuring an x-ray source
%\item[Third Objective] \hfill \\
%To demonstrate the suitability of the Bohr model for discrete x-ray lines
%\item[Fourth Objective] \hfill \\
%Introduction of random statistics and uncertainties in data
%\end{description}

\section{Background \& Theory}
\label{background}

\subsection{X-ray Production}
Light (and thus x-rays) can be produced in a variety of ways. For example, Bremsstrahlung radiation is caused by the \textit{continuous} acceleration of an electron as it approaches and curves around a nucleus. This isn't a specific phenomena to do with nuclei; any electric charge creates light when it accelerates. This is how Argonne National Laboratory produces its x-rays, by accelerating electrons around a large ring using superconducting electromagnets. \\

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.65\textwidth]{Bremsstrahlung.png} % Include the image placeholder.png
\caption{An example of an electron curving around a nucleus. It gives off Bremsstrahlung light as it accelerates around the center. Note that it spends more time near the center while it curves.}
\label{Brem}
\end{center}
\end{figure}

Yet another way to produce light is by a scattering process; an incoming electron knocks an inner-shell electron out of the core, and the two electrons scatter off. The vacancy in the electron shell is quickly filled by a higher-level electron dropping into the lower energy level. When this happens, the energy difference is released as an ejected photon. If the atom is large enough, the first shell electron that dropped to the first is also a vacancy for a third level electron to drop down to, and so on, creating secondary emissions. It is possible that incoming electrons knock away electrons other than those in the K-shell, but those are most probably. One intuitive way to observe where the electron spends most of its time while interacting with an atom. Its high kinetic energy allows it to not be repelled by the initial negative charge on the surface, but it does slow somewhat because of this negative repulsion as it enters the atomic cloud. It is also curving around the nucleus, as in Figure \ref{Brem}. The electron spends most of its time nearer the center, which means the probability of it interacting with the inner core K-shell electrons is the highest. 

\subsection{X-ray Detection}
Detecting light is also a tricky business. In some cases, one can use photomultiplier tubes (PMTs) that are designed to detect certain wavelengths of light by having a photon cause a cascade of electrons at the spot it hits, which we can then use current detectors for. For the purposes of this lab, a Geiger tube was used, which has high energy light ionize inert particles so that they create a current, as depicted in Figure \ref{geig}. \\

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.75\textwidth]{geiger.png} % Include the image placeholder.png
\caption{A geiger counter. The ionized particles generally go to the edge of the tube, as the electrons go to the conducting wire and head down it, causing a measure-able current.}
\label{geig}
\end{center}
\end{figure}

The above methods do not, however, give an indication on the \textit{wavelength} or \textit{energy} of the photons; in fact, light of many wavelengths are being produced, and we cannot distinguish between them. One hint on how to do this can be found in Bragg's law:

\begin{equation}\label{br_law}
n\lambda = 2d\sin\theta
\end{equation}
 
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.65\textwidth]{bragg.png} % Include the image placeholder.png
\caption{An illustration of Bragg diffraction.}
\label{bragg}
\end{center}
\end{figure} 
 
Bragg's law relates the interplanar distance $d$ of a lattice to the wavelength $\lambda$ of the light, for a fixed $\theta$. $n$ is a positive integer. We can modulate $theta$ and observe different wavelengths from the diffraction process. For an illustration of the process, see Figure \ref{bragg}. For light produced by the Bremsstrahlung process, there is a continuous spectrum of light produced in the course of the electron accelerating around the nucleus. For the second process, where electrons are knocked out of an atom and the reminaing electrons drop to lower energy levels, characteristic wavelengths of light are produced for each atom. We will take advantage of Bragg's law in order to distinguish between different wavelengths of light, as outlined in our experimental procedures.
 
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Experimental Procedures}

A depiction of the production of x-rays is given below.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.65\textwidth]{roentgen.png} % Include the image placeholder.png
\caption{Producing x-rays. The electrons are produced by heating up a filament, which then gives off electrons via thermionic emission. A rectified and filtered DC voltage between the cathode and anode is applied, accelerating the electrons to 20-30 keV. The electrons then hit the target and give off light via the two methods referenced in Section \ref{background}.}
\label{roentgen}
\end{center}
\end{figure} 
In this experiment, the target is copper, not tungsten. The choice of target is important, as different targets will produce different characteristic x-rays. The produced x-rays are then diffracted through a crystal. Some wavelengths are reflected, depending on the angle of incidence; this is where Equation \eqref{br_law} comes in. In our case, the crystal is Lithium Fluoride (LiF), which has a rock-salt crystal structure with lattice constant 4.03 \AA. A particular set-up is used to allow for modulation of $\theta$ in Equation \eqref{br_law}, shown below. \\

\begin{figure}[h]
\begin{center}
\includegraphics[width=1.00\linewidth]{setup.png} % Include the image placeholder.png
\caption{Experimental setup.}
\label{exp_setup}
\end{center}
\end{figure} 

A two-to-one gear ratio rotates the crystal holder by half the amount the Geiger counter moves, which ensures the angle of incidence and reflectance are the same. The latter angle can be read by a series of markings on the perimeter of the setup; it is important to note that this measured angle is twice that of the angle of incidence, and so measurements of the angle taken must be divided by two when using Equation \eqref{br_law}. 

Measurements of the light intensity were done from $2\theta$ = $12\degree$ to $110\degree$ for an applied voltage of 30 kV. For a basic statistical analysis of the data, we took 20 measurements at $2\theta$ = $60\degree$ and $44.833\degree$. The former was background (Bremsstrahlung), and the latter was a peak. Finally, we attempted to observe the cutoff wavelength for when Bremsstrahlung radiation was no longer being produced; this should correspond to the maximum energy of electrons being produced, e.g. 30 keV. For comparison, we attempted to observe the cutoff with the applied voltage at 20 kV as well. 

%\begin{tabular}{ll}
%Mass of empty crucible & {7.28}\\
%Mass of crucible and magnesium before heating & {8.59}\\
%Mass of crucible and magnesium oxide after heating & \\
%Balance used & \#4\\
%Magnesium from sample bottle & \#1
%\end{tabular}

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Data Analysis}

Our data spectrum, alongside fitted Gaussians, is presented in Figure \ref{data_fit}.

\begin{figure}
%\begin{center}
%\makebox[1.2\linewidth]{\includegraphics{data_fits.png}}
\includegraphics[width=\linewidth]{data_fits.png} % Include the image placeholder.png
\caption{The experimental data, with the number of counts (intensity of the light) plotted against $2\theta$. The third peak wasn't fit when trying to fit all four peaks at once, but it was also successfully fit on its own. Fitting was done using the \texttt{scipy.optimize} package in Python.}
%\end{center}
\label{data_fit}
\end{figure}

One can easily identify the peaks, but to list the (fitted) peaks, they are: 39.88$\degree$, 44.40$\degree$, 86.44$\degree$, and 99.15$\degree$. Dividing all these by two, one gets the values $\theta$ for which the peaks occur. Using equation \eqref{br_law}, we can get the associated energies these values indicate. One caveat is that the $d$ used must be adjusted according to the equation below:
\begin{equation}
\label{spacing}
d = \frac{a}{\sqrt{h^2 + k^2 + l^2}}
\end{equation}
where $a$ is the lattice spacing (for LiF it is 4.03), and $h$, $k$, and $l$ are the lattice's Miller indices. We do not know how the LiF crystal used for diffraction was cut; however, for now I will assume it was along the {1, 1, 1} plane. This gives $d = \frac{a}{\sqrt{3}}$. The energies obtained are shown below, compared with the established experimental values taken from the NIST X-Ray Transition Database. 

\begin{table}
\caption{Measured v. NIST}
\begin{center}
\begin{tabular}{llll}
Peak & $\Theta\degree$ & Measured E (eV) & Established E (eV) \\ 
\hline
$K\beta_1$ & 19.94$\degree$ & 7812.6 & 8904.0 \\
$K\alpha_1$ & 22.2$\degree$ & 7051.6 & 8027.8 \\
$K\beta_2$ & 43.22$\degree$ & 7781.5 & 8903.1 \\
$K\alpha_2$ & 49.58$\degree$ & 6999.95 & 8027.99 \\
\end{tabular}
\end{center}
\end{table}

It appears that the peaks are off by around 1 keV. The lattice spacing could be different, considering it is unknown what plane the crystal was cut along. It is also possible that there is a constant degree offset due to not placing the crystal quite straight in the crystal holder, which seems more sensible, as using different combinations of Miller indices produces wildly different energies. \\

Conducting an analysis of the fluctuations of data was also done, at $2\theta = 60\degree$ and $2\theta = 48.33\degree$. For the data taken at the near near $48.33\degree$, the mean was 18977.2, and the standard deviation was 103.58. For $60\degree$, the mean was 490.7, and the standard deviation was 18.33. As for evaluating the cut-off wavelength for bremsstrahlung radiation at 20 and 30 kV, the results were inconclusive.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Results and Conclusions}

This experiment's purpose was to get used to detecting x-rays, and the methods available in order to do that. For observing the characteristic lines of Cu, the experiment appears successful with the caveat that there is an unexplained 1 keV difference between the measured and expected values. However, the difference between the $K_\alpha$ and $K_\beta$ energies appears correct. In order to verify that the third and fourth peaks observed were indeed higher-order Bragg peaks, we can manipulate equation \eqref{br_law}:
\begin{equation*}
\begin{split}
\lambda = & 2d\sin\theta_1 \\
2\lambda = & 2d\sin\theta_2 \text{  Dividing the two,}\\
\frac{1}{2} = & \frac{\sin\theta_1}{\sin\theta_2} \\
\sin\theta_2 = & 2\sin\theta_1 \\
\theta_2 = & \arcsin(2\sin\theta_1) \\
\end{split}
\end{equation*}

Using the above relation, we find that the secondary peaks match the predicted angle within less than a degree. It can be concluded that these are indeed secondary Bragg peaks. In addition, the calculated standard deviations for the measurements at $60\degree$ and $48.33\degree$ appear to be on the order of the square root of the value, if a factor of two less. Much learning was accomplished in the course of performing the experiment and writing up this report. Expected energy values taken from \cite{holzer1997k}.


%
%\begin{enumerate}
%\begin{item}
%%The \emph{atomic weight of an element} is the relative weight of one of its atoms compared to C-12 with a weight of 12.0000000$\ldots$, hydrogen with a weight of 1.008, to oxygen with a weight of 16.00. Atomic weight is also the average weight of all the atoms of that element as they occur in nature.
%\end{item}
%\begin{item}
%The \emph{units of atomic weight} are two-fold, with an identical numerical value. They are g/mole of atoms (or just g/mol) or amu/atom.
%\end{item}
%\begin{item}
%\emph{Percentage discrepancy} between an accepted (literature) value and an experimental value is
%\begin{equation*}
%\frac{\mathrm{experimental\;result} - \mathrm{accepted\;result}}{\mathrm{accepted\;result}}
%\end{equation*}
%\end{item}
%\end{enumerate}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}

\bibliography{sample}


%----------------------------------------------------------------------------------------


\end{document}