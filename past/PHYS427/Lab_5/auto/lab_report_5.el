(TeX-add-style-hook
 "lab_report_5"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "margin=0.5in")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "graphicx"
    "amsmath"
    "gensymb")
   (LaTeX-add-labels
    "background"
    "eq:decay"
    "attenuation"
    "eq:logdecay"
    "Nobun"
    "Alum"
    "Lead"
    "Beef"
    "Water"
    "tab:nist")
   (LaTeX-add-bibliographies
    "sample"))
 :latex)

