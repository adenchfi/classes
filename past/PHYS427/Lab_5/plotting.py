#This is a script that fits to a set of data points to any function that you specify
#You fit the data by defining the function wiht some parameters that will be used to fit the data
import numpy as np
import random
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
import math
import sys

csvfile = sys.argv[1]
#x is the x axis of points
#data will be some points that I will make up to look like the once you would get fro experiment
thickness, counts = np.loadtxt(csvfile, delimiter=',', unpack=True)
print(thickness)
def ten_round(n):
    return (n + 9) // 10 * 10
step = max(thickness) / 20

counts = np.log(counts / max(counts))
print(counts)

fit = np.polyfit(thickness, counts, 1) # 1 for linear
slope = fit[0]
fit_fn = np.poly1d(fit)
print(fit)
label1, = plt.plot(thickness, counts, 'yo', label = csvfile)
label2, = plt.plot(thickness, fit_fn(thickness), '--k', label = "Fit with slope {0:.2f}".format(slope))

plt.legend(handles = [label1, label2], loc='best', fontsize = 18)

plt.xticks(np.arange(0, max(thickness), 0.1), size = 14)

plt.xlabel("Thickness (cm)", size = 24)
plt.ylabel(r"ln ($\frac{I}{I_o})$", size = 24)
#plt.title("Gamma Spectrum", size = 24)
plt.tick_params(direction='in', length=6, width=2)

# plt.axvline(28, color='r', label="Iodine")
# plt.axvline(35, color='g', label = "Int. Conv.")
# plt.axvline(75, color='c', label = "Pb")
# if (max(thickness) > 240):
#     plt.axvline(250, color='y', label = "Backscatter limit")
# if (max(thickness) > 500):
#     plt.axvline(511, color='m', label = "Annihilation")
# if (max(thickness) > 1000):
#     plt.axvline(1022, color='k', label = "Pair Production")


axes = plt.gca() # get current axes
axes.set_xlim(left = -0.05)
#axes.set_ylim([0,max(counts)])
plt.show()




