from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import sys

x1,y1=np.genfromtxt(sys.argv[1],dtype=float,unpack=True, delimiter='\t', usecols=(0, 1), comments='#')

# try adding zeros
#for i in range(100,700, 20):
#    if (i!=682):
#        x1 = np.append(x1,i)
#        y1 = np.append(y1, 50)
#print(x1)
#print(y1) #to test

mean = np.mean(y1)
#y1 = y1 - mean
#y1 /= max1
def func(x, *params):
    y = np.zeros_like(x)
    for i in range(0, len(params), 3):
        ctr = params[i]
        amp = params[i+1]
        wid = params[i+2]
        y = y + amp * np.exp( -((x - ctr)/wid)**2)
    return y
print(y1)
#guesses
am1 = 6000
am2 = 10000 # amplitude of guess peak
am3 = 1000
am4 = 4000
pk1 = 38
pk2 = 43
pk3 = 86
pk4 = 100
wd = 0.70 # width of guess peak, also known as the standard deviation
guess = [pk3, am3, wd] 
#guess = [135, 0.4, wd, 179, 0.4, wd, 221, am, wd, 261, am, wd, 311, am, wd, 401, am, wd,440, 0, wd, 461, 0.9, wd, 530, 0, wd, 601, am, wd, 701, am, wd, 749, am, wd]
#for i in range(12):
#    guess += [, 1, 25]
# guess width 25 instead of 50 worked
popt, pcov = curve_fit(func, x1, y1, p0=guess, maxfev=50000)
print (popt)
fit = func(x1, *popt)

plt.plot(x1, y1)
plt.plot(x1, fit, 'r-')
plt.xticks(np.arange(0, 140, 10))
plt.show()
