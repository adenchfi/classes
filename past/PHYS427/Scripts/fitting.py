#This is a script that fits to a set of data points to any function that you specify
#You fit the data by defining the function wiht some parameters that will be used to fit the data
import numpy as np
import random
import matplotlib.pyplot as plt
from lmfit import  Model
import math

#x is the x axis of points
x = range(10)
#data will be some points that I will make up to look like the once you would get fro experiment
data = np.zeros(10)

#Here is where I specify the function thta will be fitted in this case a parabloa
#of the form y = ax^2 + b
#par1 and par2 are the parameters that will be fitted
def Fit_function(x, par1, par2):
    y = par1 * (x**2)   + par2
    return (y)
    
#This for loop is making the fake data that I want to fit, I am making a parabloa but
#multiplying each data point with a random number to make it look more like an exeprimental result
for i in range(0,10):
    data[i] = 4 * math.pow(x[i],2) + 6
    data[i] = data[i] + data[i] * (random.uniform(0, 1))

#This specifies the function that you want to fit
gmodel = Model(Fit_function)

#Here you pass the data that will be fitted and the x axis and a guess for you parmeters
#dont worry about having a good guess just fill in any numbere
result = gmodel.fit(data, x=x, par1=2, par2=4)

#This prints out the values for te parameters that will give the best fit to a liner function
print(result.fit_report())

#This plots your data, your inital guess and the actual fitted line
plt.plot(x, data,         'bo')
plt.plot(x, result.init_fit, 'k--')
plt.plot(x, result.best_fit, 'r-')
plt.show()




