%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki 
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[margin=0.5in]{article}

%\usepackage{mhchem} % Package for chemical equation typesetting
%\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
\usepackage{graphicx} % Required for the inclusion of images
%\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements 
\usepackage{gensymb}
%\usepackage{biblatex}
%\addbibresource{sample.bib}
\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Detection of X-rays \\ } % Title

\author{Adam \textsc{Denchfield}} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date


\begin{center}
\begin{tabular}{l r}
Date Performed: & October 6th, 2017 \\ % Date the experiment was performed
Partner: & Yonas Gebre \\ % Partner names
Instructor: & Professor Zasadzinski % Instructor/supervisor
\end{tabular}
\end{center}

% If you wish to include an abstract, uncomment the lines below
% \begin{abstract}
% Abstract text
% \end{abstract}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Objectives}
\begin{itemize}
\item To familiarize oneself with the equipment for gamma ray detection including the scintillator detector, photomultiplier tubes, and the multichannel analyzer
\item To determine the resolution of a NaI(TI) detector and examine the resolution's dependence on factors such as amplifier gain, photomultiplier voltage, and incident gamma energy
\item To demonstrate the use of amplifier gain to position a total absorption peak (photopeak) in the desired channel
\item To demonstrate that the photomultiplier pulse height is linearly proportional to the incident gamma ray energy, and thus a linear calibration curve is possible
\end{itemize}


% If you have more than one objective, uncomment the below:
%\begin{description}
%\item[First Objective] \hfill \\
%To become acquainted with the production, use, and properties of x-rays
%\item[Second Objective] \hfill \\
%Learn dispersive techniques for measuring an x-ray source
%\item[Third Objective] \hfill \\
%To demonstrate the suitability of the Bohr model for discrete x-ray lines
%\item[Fourth Objective] \hfill \\
%Introduction of random statistics and uncertainties in data
%\end{description}

\section{Background \& Experiment Setup}
\label{background}

\subsection{Gamma Ray Detection}
Light comes in a variety of energies. The highest-energy photons are known as gamma rays, and originate both from outside Earth (and are called cosmic rays) and from nuclear processes such as decays. In order to detect these gamma rays, a setup involving scintillators, photomultiplier tubes (PMTs), and a multichannel analyzer is employed. A scintillator is any material that absorbs light, which excites an electron, and re-emits light when the electron de-excites. The electron can have staggered drops in energy levels, yielding multiple photons of different energies. This lab uses an inorganic scintillator. For inorganic scintillators, an incoming photon excites an electron from the valence band to the conduction band or the exciton band, which leaves an associated hole behind in the valence band. Impurities create electronic levels in the forbidden gap (the region between the valence and conduction bands). Electrons captured in impurity centers (thereby dropping to those levels in the forbidden gap) emit light; the impurities are chosen to make sure the light emitted is in the visible range, for the purposes of this lab. This process is illustrated in Figure \ref{scint}. 
\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{scint.png} % Include the image placeholder.png
\caption{Energy band visualization of an inorganic scintillating material. }
\label{scint}
\end{center}
\end{figure}

These visible-light photons are then detected by PMTs. PMTs use the photoelectric effect to create a cascade of electrons. The first electron is created with an energy $E = h\nu - \phi$, where $\phi$ is the activation function (the energy required to actually break the electron free from the system). This electron proceeds to scatter throughout an individual photomultiplier tube, giving other electrons enough kinetic energy to also break free from their bonds. Thus a cascade of electrons follows, which can be fed into an amplifier and a current can be measured. This process is depicted in Figure \ref{PMTs}. 
\begin{figure}
\begin{center}
\includegraphics[width=1.0\textwidth]{PMTs.png} % Include the image placeholder.png
\caption{The gamma detection process, involving a scintillator, a PMT, and a multichannel analyzer at the end of it.}
\label{PMTs}
\end{center}
\end{figure}

The Multichannel Analyzer has 1024 channels and a 1024-channel high resolution Analog-to-Digital converter. It also has a built-in linear amplifier and high voltage power supply for the PMT. Ultimately, the incoming gamma ray will produce a voltage pulse with a height (amplitude) proportional to the gamma energy. The energy spectrum of a gamma source (such as radioactive isotopes) is created from signals coming from the PMTs; the signals are processed according to their amplitudes and then stored in corresponding channel memory locations. Channels 1 - 1024 are merely proportional to gamma energy, having been set by the incident gamma energy as well as amplifier gain. Calibration is then needed to convert channel \# to energy.

The scintillation detector is an NaI(TI) scintillation crystal; this means that in the NaI crystal, there are 0.1\% TI impurities that provide electron energy levels, defining the scintillation process and the resulting pulse of light to be in the visible range. The crystal is housed in a thin metal cover, typically Aluminum, which keeps out stray light and also reflect internal light produced by the gamma ray. The detector is typically surrounded by an external lead shield 1.3cm thick to cut down on unwanted cosmic ray background. Finally, detector resolution can be estimated by dividing the Full-Width Half-Maximum (FWHM) between detector channels by the centroid channel. 

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Experimental Procedures}

After verifying the system was fully connected and the control computer was powered on, we powered on the Multichannel Analyzer. The multichannel analyzer software on the computer was started, and we set the High Voltage (HV) to 'on' with a setting of 700V. We begin with $^{137} Cs$ because of its single gamma energy peak at 662 keV. We adjust the gain settings (Coarse and Fine) such that the main photopeak is on near channel 1000. We press Acquire and observe the photopeaks. The FWHM and centroid and marked for the main photopeak. This is repeated with different Coarse Gain settings.

Next we use a variety of sources to observe how the resolution depends on the incident gamma energy. We use sources $^{60} Co, ^{22} Na, ^{54}Mn, ^{137}Cs, ^{57}Co$ and $^{109}Cd$. We adjust the gain features such that the main photopeak of $^{60}Co$ is located between channels 900-1000, and acquire spectra for all the specimens using the same settings. 

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Data Analysis}

\subsection{Effect of Gain on Resolution}
The spectrum for $^{137}Cs$ is shown in Figure \ref{Cs_Coarse_32}. 

\begin{figure}
\begin{center}
\includegraphics[width=1.00\linewidth]{700V_Cs_Res.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{137}Cs$. }
\label{Cs_Coarse_32}
\end{center}
\end{figure} 

The spectrum for $^{60}$Co at differing voltage gains (6, 12, and 18 Volts) is shown in Figures \ref{Co60_6} to \ref{Co60_18}. Table \ref{Gain_vs_Res_Table} displays the channel number of the two main photopeaks, as well as their FWHM, centroid, and resolution. 

\begin{table}\label{Gain_vs_Res_Table}
\caption{$^{60}Co$'s two main photopeaks at differing gains}
\begin{center}
\begin{tabular}{llll}
Voltage Gain & FWHM & Centroid & Resolution (\%) \\
\hline
6 & 17 & 272 & 6.25 \\
6 & 19 & 311 & 6.11 \\
12 & 29 & 505 & 5.74 \\
12 & 33 & 574 & 5.75 \\
18 & 42 & 768 & 5.54 \\
18 & 48 & 872 & 5.50 \\
\end{tabular}
\end{center}
\end{table}

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{6VoltageGain.jpg} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{60}Co$ at a voltage gain of 6. }
\label{Co60_6}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{12VoltageGain.jpg} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{60}Co$ at a voltage gain of 12. }
\label{Co60_12}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{18VoltageGain.jpg} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{60}Co$ at a voltage gain of 18. }
\label{Co60_18}
%\end{center}
\end{figure} 


\subsection{Effect of Incident Gamma Energy on Resolution}

% TODO: add the plots of the different sources, make a table of their resolutions, look at the appendix in lab 3 to know what you're doing.

A variety of sources were used to produce incident gammas of different energies. These spectra are shown in % ADD

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Mn54_Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{54}Mn$. }
\label{Mn}
%\end{center}
\end{figure} 


\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Co60_Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{60}Co$. }
\label{Co60}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Co57Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{57}Co$. }
\label{Co57}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Na22_Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{22}Na$. }
\label{Na22}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Cd109_Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{109}Cd$. }
\label{Cd109}
%\end{center}
\end{figure} 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{Cs137_Calibration.png} % Include the image placeholder.png
\caption{Gamma ray spectrum of the isotope $^{137}Cs$. }
\label{Cs137}
%\end{center}
\end{figure} 

\begin{table}\label{Gamma_vs_Res_Table}
\caption{Gamma Peaks vs Resolution}
\begin{center}
\begin{tabular}{llll}
Peak Energy (keV) & FWHM & Centroid & Resolution (\%) \\
\hline
835 & 39 & 574.3 & 6.79 \\
1173 & 45 & 802.7 & 5.606 \\
136 & 12 & 61 & 19.67 \\
511 & 30 & 358.1 & 8.38 \\
88 & 9 & 62.4 & 14.42 \\
662 & 33 & 460.1 & 7.17 \\
\end{tabular}
\end{center}
\end{table}

%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Results and Conclusions}

It turns out that the channel number has a linear relationship with the energy. This is illustrated by the linear fit presented in Figure \ref{linear_fit}. The fit is almost perfect, implying that it will be possible to, at fixed voltage levels, calibrate the detector to display the x-axis as a function of energy. Even if the voltage level varies, the resolution difference with amplifier gain is not very high; a linear relationship would exist between channel number and voltage gain, allowing one set of data points to calibrate the whole slew of data. 

\begin{figure}
%\begin{center}
\includegraphics[width=1.0\linewidth]{linear_fit.png} % Include the image placeholder.png
\caption{A fit of the data points presented in Table \ref{Gamma_vs_Res_Table}. }
\label{linear_fit}
%\end{center}
\end{figure} 

The linear fit above also lets us infer that for these gain settings, a channel number of 450 corresponds to an energy of roughly 600 keV. Not shown here is how to high voltage (HV) applied affects the resolution; the effect is small, only changing the resolution by 0.3\%. By far the highest influence on resolution is the gamma energy; low-energy gammas seem to result in high resolutions up to 17\%.. Amplifier gain settings do not seem to affect resolution much; a gain of 6 and a gain of 18 only have their resolutions differ by 0.7\%. Likely what occurs is that low-energy gammas are more likely to have less byproducts in the form of visible light photons; because of this, there is a smaller signal-to-noise ratio in the PMTs. On the other hand, the other factors wouldn't have resolution be affected so long as the amplifier, for example, did not have much noise. This whole system is useful for detecting the gamma spectrum of materials; when combined with labs 1 and 2, we can not only now identify and characterize materials by their electronic spectra, but also now detect nuclear processes. 

%
%\begin{enumerate}
%\begin{item}
%%The \emph{atomic weight of an element} is the relative weight of one of its atoms compared to C-12 with a weight of 12.0000000$\ldots$, hydrogen with a weight of 1.008, to oxygen with a weight of 16.00. Atomic weight is also the average weight of all the atoms of that element as they occur in nature.
%\end{item}
%\begin{item}
%The \emph{units of atomic weight} are two-fold, with an identical numerical value. They are g/mole of atoms (or just g/mol) or amu/atom.
%\end{item}
%\begin{item}
%\emph{Percentage discrepancy} between an accepted (literature) value and an experimental value is
%\begin{equation*}
%\frac{\mathrm{experimental\;result} - \mathrm{accepted\;result}}{\mathrm{accepted\;result}}
%\end{equation*}
%\end{item}
%\end{enumerate}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}

\bibliography{sample}


%----------------------------------------------------------------------------------------


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
