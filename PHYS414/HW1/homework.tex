\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}

%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{1} % workaround to make problem numbers start with 1
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#1}
\author{Adam Denchfield}
\maketitle

\begin{exercise}
  A long solenoid of radius $R$ has $n$ turns of wire per unit length and carries a time-carrying current that varies sinusoidally as $I = I_o \cos(\omega t)$.
  \begin{enumerate}[label=\alph*]
  \item Determine $\norm{E_max}$ outside the solenoid at distance $r > R$ from its central long axis. 
  \item Determine $\norm{E_max}$ inside the solenoid at distance $r < R$ from its central long axis. 
  \item Describe an analogous problem that would require the Maxwell-Ampere Law instead of Faraday's Law. 
  \end{enumerate}
\end{exercise}

\begin{solution}
As hinted in the problem statement, Faraday's law is to be used for this problem. Restating it,
\begin{equation}
  \mathcal{E} = - \frac{d\Phi_B}{dt}
\end{equation}
where $\mathcal{E}$ is the electromotive ``force'' and $\Phi_B$ is the magnetic flux defined as
\begin{equation}
  \label{eq:flux}
  \Phi_B = \iint \vec{B}\cdot d\vec{A}.
\end{equation}
Note that from the Maxwell-Faraday equation
\begin{equation}\label{eq:MF}
  \oint_{\partial \Sigma} \vec{E}\cdot d\vec{l} = -\frac{d\Phi_B}{dt}
\end{equation}
that we can now begin our solution.

Using \eqref{eq:flux} and \eqref{eq:MF}, we can write for a solenoid that, outside $r = R$, 
\begin{align*}
  \norm{E}(2\pi r) = \pi R^2 \frac{dB}{dt}
\end{align*}
Noting that $B = \mu_0 n I(t)$, we then have
\begin{align*}
  \norm{E} = &\frac{\mu_0 nR^2}{2r} \frac{dI}{dt} \\
  \norm{E} = & -\frac{\mu_0 nR^2}{2r} \omega I_0 \sin(\omega t) \\
  \norm{E_{max}} = & \frac{\mu_0 nR^2}{2r} \omega I_0 \quad \text{because it must be positive and maximized when $\omega t = n\pi$}\\
\end{align*}

Similarly, for $r < R$, 
\begin{align*}
  \norm{E}(2\pi r) = \pi R^2 \frac{dB}{dt} \\
  \norm{E} = & \frac{\mu_0 n r}{2} \frac{dI}{dt} \\
  \norm{E} = & -\frac{\mu_0 nr}{2} \omega I_0 \sin(\omega t) \\
  \norm{E_{max}} = & \frac{\mu_0 nr}{2} \omega I_0 \\
\end{align*}

Finally for part \textbf{c}, consider a straight wire rather than a circularly wrapped one. Maxwell-Ampere's law would be used in, say, a problem where the current through a \textbf{straight} wire is time-dependent. A depiction of such a problem is shown below.

\begin{figure}[h]
  \centering
  \includegraphics[scale=1.0]{current.png}
  \caption{A wire with a time-dependent current running through it.}
  \label{fig:current}
\end{figure}
\end{solution}

\begin{exercise}[Griffiths 7.62]
  Essentially described is a parallel-plate capacitor with distance between them $h$, width $w$, and length $l$. Current $I$ travels down one plate (strip) and goes the opposite direction on the other one.
  \begin{enumerate}[label=\alph*]
  \item Find the capacitance per unit length, $C$. 
  \item Find the inductance per unit length, $L$. 
  \item What is the product $LC$? Compare this with the speed a pulse propagates down the line, $v = 1/\sqrt{LC}$. 
  \item If the strips are insulated from one another by a nonconducting material of permittivity $\epsilon$ and permeability $\mu$, what is then the product $LC$? What is the propagation speed now? 
  \end{enumerate}
\end{exercise}

\begin{solution}
  Recall the following:
  \begin{itemize}
  \item $E = \frac{\sigma}{\epsilon_0}$
  \item $V = Eh = \frac{1}{\epsilon_0} \frac{Q}{wl}h$
  \item $C = \frac{Q}{V} = \frac{\epsilon_0 wl}{h}$
  \end{itemize}
  Therefore we have, for part \textbf{a}, $C = \frac{\epsilon_0 w}{h} l$. The capacitance \textbf{per unit length} is then $\mathcal{C} = \frac{\epsilon_0 w}{h}$.

  Now let $K = I/w$ be the surface current per unit width. We know the magnetic field produced by two planar surfaces is $B = \mu_0 K = \mu_0 \frac{I}{w}$. From \eqref{eq:flux} we can show that $\Phi_B = Bhl = \frac{\mu_0 I}{w}hl$. Now recall one definition of inductance,
  \[
    L = \frac{\Phi_B}{I}.
  \]
  We can now find the inductance of the system to be \[
    L = \frac{\mu_0 hl}{w}.
  \]
  The inductance \textbf{per unit length} is then $\mathcal{L} = \frac{\mu_0 h}{w}$.
  For part \textbf{c}, we have $\mathcal{C} \mathcal{L} = \epsilon_0 \mu_0 = 1.11265006 10^{-17} s^2 / m^2.$ Therefore the pulse velocity is $v = c$.

  For part \textbf{d}, all you need to do is replace $\epsilon_0$ with $\epsilon$ and $\mu_0$ with $\mu$. Therefore $\mathcal{L}\mathcal{C} = \epsilon \mu$ and $v = 1/\sqrt{\epsilon \mu}$.
\end{solution}

\begin{exercise}[LRC Circuit]
  A capacitor C is charged up to a potential $V_0$. At $t = 0$ the switch $S$ is closed so that the capacitor is in series with an inductor $L$ and a resistor $R$.
  \begin{enumerate}[label=\alph*]
  \item Find the current in the circuit as a function of time by assuming a solution $I = Ae^{st}$ to the differential equation for the current.
    \begin{solution}
      Recall the differential equation for an LRC circuit in terms of the charge $q$ is
      \begin{equation}
        L\frac{d^2 q}{dt^2} + R\frac{dq}{dt} + \frac{q}{C} = 0. 
      \end{equation} 
      Assuming a solution $I = Ae^{st}$ and therefore $q(t) = \frac{A}{s} e^{st}$, we note that $I(t=0) = 0$ because current does not immediately begin flowing. Therefore
      \begin{align*}
        LAs + RA + \frac{A}{sC} = 0 \\
        s^2 + Rs/L + \frac{1}{LC} = 0 \\
        s_{1,2} = \frac{-R \pm \sqrt{(R)^2 - \frac{4}{LC}}}{2L}
      \end{align*}
      The solution is then $I = A_1 e^{s_1 t} + A_2 e^{s_2 t}$, by simple differentiation of $q(t)$. 
    \end{solution}
  \item What are the effects of having the resistor in the circuit?
    \begin{solution}
      Well, it prevents the coefficients $s$ from being imaginary so long as $R^2 > \frac{4}{LC}$. This means the charge will decay rather than oscillate (as they would if the coefficients $s$ \textbf{were} imaginary).
    \end{solution}
  \item What are the initial conditions for the charge on the capacitor and the current flowing in the circuit?
    \begin{solution}
      The capacitor is charged to voltage $V_0$, meaning the initial charge on the capacitor is $Q = CV_0$. The current can't immediately begin flowing, so $I(t = 0) = 0$. 
    \end{solution}
  \item Instead of using the initial conditions to solve for $A_1, A_2$ in part (a), assume a solution of the form $I(t) = e^{-\alpha t}[C_1 \sin(\omega' t) + C_2 \cos(\omega' t)]$ and solve for the coefficients $C_1, C_2$.
    \begin{solution}
      Well, this works. Since $I = dq/dt$, and we want to apply two initial conditions, we need to get $q(t)$ again:
      \[
        q(t) = \frac{1}{-\alpha} e^{-\alpha t}[\frac{C_2}{\omega'} \sin(\omega' t) - \frac{C_1}{\omega'} \cos(\omega' t)]
      \]
      through some simple integration. I suppose I should have applied the initial condition on $I$ first to make the integration a little simpler. $I(t=0) = 0$ implies that $C_2 = 0$, and $q(t=0) = CV_0$ implies \[
        C_1 \cos(\omega') = \alpha \omega' CV_0. \] and therefore that $C_1 = \frac{\alpha \omega' CV_0}{\cos(\omega')}$.

      This doesn't make much intuitive sense, but oh well. 
    \end{solution}
  \end{enumerate}
\end{exercise}



\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
