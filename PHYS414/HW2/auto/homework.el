(TeX-add-style-hook
 "homework"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "margin=1in")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "math_pkgs"
    "article"
    "art12"
    "geometry"
    "amsmath"
    "amsthm"
    "amssymb"
    "amsfonts")
   (TeX-add-symbols
    "N"
    "Z")
   (LaTeX-add-labels
    "eq:dustbunny"
    "eq:mst"
    "eq:force_st_tens"
    "fig:dipole"))
 :latex)

