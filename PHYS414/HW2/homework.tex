\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}

%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{2} % workaround to make problem numbers start with 2 for HW 2
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#2}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzrous}
\date{\today{}}
\maketitle

\begin{exercise}
  Calculate the power (energy per unit time) transported down the cables of Ex. 7.13 and Prob. 7.62, assuming the two conductors are held at a potential difference $V$ and carry current $I$ (down one and back up the other).
  \begin{solution}
    Example 7.13: 
    Recall the magnetic field between the cylinders is $\vec B = \frac{\mu_o I}{2\pi s} \hat{\phi}$ and 0 elsewhere. Also recall that magnetic energy goes as $\frac{1}{2\mu_0} B^2$ and thus the magnetic energy per unit volume is \[
      \frac{1}{2\mu_0} \left(\frac{\mu_0 I}{2\pi s} \right)^2 = \frac{\mu_0 I^2}{8\pi^2 s^2}
    \]
    Consider that the total energy in fields, per unit volume, is \[
      u = \frac{1}{2} \left(\epsilon_0 E^2 + \frac{1}{\mu_0 B^2} \right).
    \]
    Since we want to know $u$, and we already have the second term, we just need to know the electric field $E$ of the problem. A second way to do this would be to determine $E$ and $B$, recall that $\frac{\partial u}{\partial t} = -\vec \nabla \cdot \vec S$, and integrate over the surface. That is what I will likely do, since that sort of example immediately precedes the question.

    We already have an expression for $\vec B$, and now we just need one for $\vec E$. Since the wires can be approximated as infinite wires, the electric field produced by them is equal to $\vec E = \frac{\lambda}{2\pi \epsilon_0} \frac{1}{s}\hat{s}$, where $s$ is again the distance between the wires. This leads us to
    \begin{align*}
      \vec S =& \frac{1}{\mu_0} \vec E \cross \vec B\\
      = & \frac{\lambda I}{4\pi^2 \epsilon_0} \frac{1}{s^2} \hat{z} \tag*{Since $E$ and $B$ are perpendicular}\\
      P = & \int \vec S \cdot d\vec a \\
      = & \int_a^b S 2 \pi s ds \\
      = & \frac{\lambda I}{2\pi \epsilon_0} \int_a^b \frac{1}{s^2} ds\\
      = & \frac{\lambda I}{2\pi \epsilon_0} \ln(b/a) 
    \end{align*}
    Recalling that $V = \int_a^b \vec E \cdot d\vec l = \frac{\lambda}{2\pi \epsilon_0} \ln(b/a)$, we have $P = VI$.

    For the system of problem 7.62, we know the electric field produced by planes is \[
      E = \frac{\sigma}{\epsilon_0} \hat{z}.
    \]
    Furthermore, using Ampere's Law around the two planes and taking the limit of the line integral path to zero, we know that the magnetic field is \[
      B = \mu_0 K \hat{x} = \frac{\mu_0 I}{w} \hat{x}
    \]
    where $w$ is the width of the metal ribbons. Using these we can construct the Poynting vector $\vec S$.
    \begin{align*}
      \vec S = & \frac{1}{\mu_0} \vec E \cross \vec B \\
      = & \frac{\sigma I }{\epsilon_0 w} \hat{y} \tag*{Since $E, B$ are perpendicular}\\
      P =& \int \vec S \cdot d\vec a \\
      = & Swh = \frac{\sigma I h}{\epsilon_0} \\
      V =& \int \vec E \cdot d\vec l \\
      =& \frac{\sigma}{\epsilon_0} h \\        
      P =& VI 
    \end{align*}
    It's good to see verification of the established power law. 
  \end{solution}

\end{exercise}

\begin{exercise}
  Consider the charging capacitor in Problem 7.34 of Griffiths E\&M. 
  \begin{enumerate}[label=(\alph*)]
  \item Find the electric and magnetic fields in the gap, as functions of the distance $s$ from the axis and time $t$ (assume charge is 0 at $t = 0$).
    \begin{solution}
      We can treat each end of the wire as a plane, with charge steadily building up. In this case, $E = \frac{\sigma}{\epsilon_0} \hat{z}$, with the surface charge density being $\sigma = \frac{Q}{\pi a^2}$ and built-up charge $Q(t) = It$. This yields \[
        E = \frac{It}{\pi \epsilon_0 a^2} \hat{z}.
      \]
      Now, let us recall the Maxwell equation
      \begin{equation}
        \nabla \cross \vec B = \mu_0 \vec J + \mu_0 \epsilon_0 \frac{\partial E}{\partial t}.
      \end{equation}
      Integrating both sides over an area of a circle with radius $s$, using Stokes' theorem (the curl theorem) and noting that no current flows between the two ends of the wires (so $J = 0$), we have
      \begin{align*}
        B2\pi s = & \mu_0 \epsilon_0 \frac{\partial E}{\partial t } \pi s^2 \\
        = & \mu_0 \epsilon_0 \frac{I \pi s^2}{\pi \epsilon_0 a^2} \\
        \rightarrow \vec B(s, t) = & \frac{\mu_0 I}{2\pi a^2} \hat{\phi}
      \end{align*}
    \end{solution}
  \item Find the energy density $u_{em}$ and the Poynting vector $\vec S$ in the gap. Note especially the \textit{direction} of $\vec S$. Check Eq. 8.12 is satisfied.
    \begin{solution}
      Recall the expression for $u_{em}$ we had previously.
      \begin{align*}
        u_{em} = &\frac{1}{2} \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right) \\
        = & \frac{1}{2} \left[ \epsilon_0 \left(\frac{It}{\pi \epsilon_0 a^2}\right)^2 + \frac{1}{\mu_0} \left(\frac{\mu_0 I s}{2\pi a^2} \right)^2 \right] \\
        = & \frac{1}{2} \left[\frac{4\mu_0 I^2 t^2}{4\pi^2 \mu_0 \epsilon_0 a^4} + \frac{\mu_0 I^2 s^2}{4\pi^2 a^4} \right] \tag*{Creating a common denominator} \\
        = & \frac{1}{2} \left[\frac{4\mu_0 I^2 c^2 t^2 + \mu_0 I^2 s^2}{4\pi^2 a^4} \right]  \tag*{Since $c^2 = \frac{1}{\epsilon_0 \mu_u}$}\\
        u_{em}=& \frac{\mu_0 I^2}{2\pi^2 a^4} \left[(ct)^2 + \left(\frac{s}{2}\right)^2 \right] \tag*{Simple factoring}.
      \end{align*}
      Another way to get the energy density would be to integrate the Poynting vector $\vec S$ over the surface of interest. Let's calculate the Poynting vector, first.
      \begin{align*}
        \vec S =& \frac{1}{\mu_0} \vec E \cross \vec B \\
        = & \frac{1}{\mu_0} \frac{It}{\pi\epsilon_0 a^2 } \frac{\mu_0 Is}{2\pi a^2} (-\hat{s}) \tag{Since the two are perpendicular} \\
        \vec S= & - \frac{I^2 ts}{2\pi^2 \epsilon_0 a^4} \hat{s}.
      \end{align*}
      Recall Eq. 8.12,
      \begin{equation}
        \frac{\partial u}{\partial t } = -\vec \nabla \cdot \vec S.
      \end{equation}
      We can check both terms.
      \begin{align*}
        \frac{\partial u}{\partial t} =& \frac{I^2 t}{\pi^2 \epsilon_0 a^4} \tag{Simple derivative and cancellation} \\
        -\vec \nabla \cdot \vec S = & \frac{I^2 t}{2\pi^2 \epsilon_0 a^4} \vec \nabla \cdot (s \hat{s}) \\
        =& \frac{I^2 t}{\pi^2 \epsilon_0 a^4}  \tag*{$\checkmark$}
      \end{align*}
      That's it! 
    \end{solution}

  \item Determine the total energy in the gap, as a function of time. Calculate the total
    power flowing into the gap, by integrating the Poynting vector over the appropriate
    surface. Check that the power input is equal to the rate of increase of
    energy in the gap (Eq. 8.9—in this case $W = 0$, because there is no charge in
    the gap). [If you’re worried about the fringing fields, do it for a volume of radius
    $b < a$ well inside the gap.]
    \begin{solution}
      Since we have an energy density $u_{em}$, all that we need to do is integrate that density over the available volume. In this case, that volume is the cylinder formed between the two open ends of the wire, with volume $\pi b^2 w$, with $w$ the total separation and $b$ the radius of the cylinder faces. We can get the volume from integrating over cylinders of length $w$ with increasing radii $s$:
      \begin{align*}
        U_{em} = & \int u_{em} 2\pi w s ds = 2\pi w \frac{\mu_0 I^2}{2\pi^2 a^4 }\int_0^b [(ct)^2 + (s/2)^2]s ds \\
        = & \frac{\mu_0 w I^2}{\pi a^4} \left[(ct)^2 \frac{s^2}{4} + \frac{s^4}{16} \right]\Big|_{0}^b \\
        = & \frac{\mu_0 w I^2 b^2}{2\pi a^4} \left[(ct)^2 + \frac{b^2}{16} \right].
      \end{align*}
      We can also look at how much power flows into the gap, by taking the power that crosses the circular boundary on the wire ends. Over the circular surface of radius $b$, we get
      \begin{align*}
        P_{in} =& -\int \vec S \cdot d\vec a \\
        = & \frac{I^2 t}{2\pi^2 \epsilon_0 a^4 }[b\hat{s} \cdot (2\pi b w \hat{s})] \tag{Substitute $b$ for $s$ in $\vec S$} \\
        =& \frac{I^2 wt b^2}{\pi \epsilon_0 a^4}.
      \end{align*}
      Now we want to check that the energy change in the gap is equal to the power coming in.
      \begin{align*}
        \frac{dU_{em}}{dt} =& \frac{\mu_0 w I^2 b^2}{2\pi a^4} 2c^2 t \tag{Take a derivative}\\ 
        = & \frac{I^2 wtb^2}{\pi\epsilon_0 a^4} \tag{Simplify.}\\
        = & P_{in}
      \end{align*}
      We're done!
    \end{solution}
  \end{enumerate}
\end{exercise}

\begin{exercise}[Dustbunny Conservation]
  Suppose that dustbunnies, $D$, is a locally conserved quantity. Express this with an equation, using the symbols $\mathcal{D}$ and $G$. What are their units? Note $D$ is a scalar.

  \begin{solution}
    If $D$ is a locally conserved quantity, then that means any change in the number of dustbunnies in a volume $\Omega$ must be equal to the amount of dustbunnies entering and exiting the domain $\Omega$, which occurs on the boundary $\delta \Omega$. This latter quantity is known as the dustbunny flux, and is
    \begin{equation}
      \label{eq:dustbunny}
      G = \iint_{\delta \Omega} \nabla D \cdot \hat{n} dS  
    \end{equation}
    The quantity $\frac{dD}{dt} = \frac{dD_{in}}{dt} - \frac{dD_{out}}{dt}$ measures the change in dustbunnies inside our domain. The quantity above, however, is a term that we can say is proportional to the total dustbunny ``flow'' or flux $G$. In other words, \[
\frac{dD}{dt} = k^2 G
\]
    where $k$ is a constant. As for units, $D$ is an amount of dustbunnies, $G$ should be dustbunnies per unit time, and thus $k^2$ has a units of time. 
  \end{solution}
\end{exercise}

\begin{exercise}[Units problem]
  Consider the relation $\vec P_{em} = \mu_0 \epsilon_0 \vec S$, where $\vec S$ is the Poynting vector and $\vec P_{em}$ is the density of momentum in the electromagnetic fields. Show that the units match up.
  \begin{solution}
    We know the left is a momentum density, meaning it has units of $\frac{[kg m/t]}{m^3} = \frac{kg}{m^2 t}$. On the right, we have $\mu_0 \epsilon_0$ as an inverse velocity squared, and the Poynting vector has units $[W/m^2]$ where $W = [kg m^2 /t^3]$. This means the right side has units
    \begin{align*}
      & \frac{t^2}{m^2} \frac{kg m^2}{t^3 m^2} \\
      = & \frac{kg}{m^2 t} \\
      & \checkmark 
    \end{align*}
  \end{solution}
\end{exercise}

Preemptively, I will write down the definition of the Maxwell stress tensor.
\begin{equation}
  \label{eq:mst}
  T_{ij} \equiv \epsilon_0 \left( E_i E_j - \frac{1}{2} \delta_{ij} E^2 \right) + \frac{1}{\mu_0}\left(B_i B_j - \frac{1}{2}\delta_{ij} B^2 \right).
\end{equation}

\begin{exercise}[Griffiths 8.4]
  Here we consider some applications of the Maxwell stress tensor.

  
  \begin{enumerate}[label=(\alph*)]
  \item Consider two \textbf{equal} point charges $q$, separated by a distance $2a$. Construct the plane equidistant from the two charges. By integrating Maxwell's stress tensor over this plane, determine the force of one charge over the other.
    \begin{solution}
      We will denote tensors by bold capital letters. For a static case, we know
      \begin{equation}
        \label{eq:force_st_tens}
        \vec{F} = \oint_S \mathbf{T} \cdot d\vec a.
      \end{equation}
      Furthermore, if we are integrating over a plane in between the two charges, then the plane always has area direction $\hat{a} = \hat{z}$, if we define the two charges to be lying on the $z$ axis. Furthermore, a differential of area $d\vec a = dxdy \hat{z} = -r dr d\phi $. Now all that's left is to construct each $T_{ij}$, knowing that for point charges, $\vec E = \frac{q}{4\pi \epsilon_0 \rho^2}2\cos\theta \hat{r}$. Since we defined our charges to be on the $z$ axis, and the two are equal in charge, $T_{ij}$ will have a finite contribution in the $x, y$ directions; however, the area vector $\vec a$ has 0 for its $x, y$ components. Thus the integral is zero for those components. 
      Note that $E_z = 0$, $\cos \theta = \frac{r}{\rho}$ where $\rho$ is the full distance and $r$ is the projection of $\rho$ onto the plane, and $E^2 = \left(\frac{q}{2\pi\epsilon_0 }\right)^2 \frac{r^2}{(r^2 + a^2)^3}$, where $a$ is the distance to the plane. Thus we have
      \begin{align*}
        F_z = & \frac{\epsilon_0}{2} \left(\frac{q}{2\pi \epsilon_0}\right)^2 2\pi \int_0^\infty \frac{r^3 dr}{(r^2 + a^2)^3} \\
        = & \frac{q^2}{4 \epsilon \pi} \frac{1}{2} \int_0^\infty \frac{u du}{(u + a^2)^3} \tag{Letting $u \equiv r^2$.} \\
        = & \frac{q^2}{4 \epsilon \pi} \frac{1}{2} \left[-\frac{1}{u+a^2} + \frac{a^2}{2(u+a^2)^3} \right]\Big|_{0}^\infty \tag{Simple substition}\\
        = & \frac{q^2}{4 \epsilon \pi} \frac{1}{2} \left[0 + \frac{1}{a^2} - \frac{a^2}{2a^4} \right] \\
        = & \frac{q^2}{4 \epsilon \pi} \frac{1}{2} \frac{1}{(2a)^2} \tag*{\checkmark}
      \end{align*}
      We have the correct expression for the force one particle exerts on another! I wonder how this extends to other systems. It's not obvious to me why we integrated over the plane equidistant to the particles. 
    \end{solution}
  \item Do the same for charges opposite in sign.
    \begin{solution}
      In this case, the charges now have canceling contributions to the $x$-axis and $y$-axis, and we now have $\vec E = \frac{2q}{4\pi \epsilon_0 \rho^2} \sin\phi \hat{z}$, where $\sin\phi = \frac{a}{\rho}$. This means we have
      \begin{align*}
        E^2 = E_z^2 = & \left(\frac{qa}{2\pi \epsilon_0} \right)^2 \frac{1}{(r^2 + a^2)^3} \\
        F_z = & \frac{-\epsilon_0}{2} \left(\frac{qa}{2\pi \epsilon_0} \right)^2 2\pi \int_0^\infty \frac{r dr}{(r^2 + a^2)^3} \tag{Using the expression for $T_{ij}$} \\
        = & \frac{-q^2a^2}{4\pi \epsilon_0} \left[\frac{-1}{4} \frac{1}{(r^2 + a^2)^2}\right]\Big|_{0}^\infty \\
        = & - \frac{q}{4\pi \epsilon_0} \frac{1}{(2a)^2} \tag*{\checkmark}
      \end{align*}
    \end{solution}
  \end{enumerate}
\end{exercise}

\begin{exercise}[Dipole Magnets]
  A dipole magnet has a constant, uniform magnetic field of magnitude $B_0$ between the poles. Let the $\hat{z}$ direction be parallel to the field lines between them. Specify all the components of the Maxwell stress tensor in the region between the poles. Display your answer as a 3x3 matrix.

  \begin{solution}
    Using the definition of the Maxwell stress tensor \eqref{eq:mst}, all we have to do is note that for permanent magnets that aren't moving, there is no electric field generated. Therefore $E_i = 0$ and we will likely end up with a symmetric matrix only involving $B_0$ terms. In fact, let's look at the directions a little more. 
    \begin{figure}[h]
      \centering
      \includegraphics[scale=1.0]{dipole_mag}
      \caption{Magnetic dipole with field $B_0$ only in the $z$ direction}
      \label{fig:dipole}
    \end{figure}
    
    The diagonal terms $T_{ij}$ are only nonzero for $j = z$, because $B_0$ is only in the $z$-direction. Similarly, $T_{i\neq j}$ is zero everywhere, because $B_i B_j$ is only nonzero when $i = j =z$, which was covered previously. Therefore
    \[
T_{ij} =
\begin{bmatrix}
  -\frac{B_0}{2\mu} & 0 & 0\\
  0 & -\frac{B_0}{2\mu} & 0\\
  0 & 0 & \frac{B_0}{2\mu}
\end{bmatrix}
\]
This makes sense. Anything in between opposite poles of a magnetic dipole that feels magnetic fields should only be drawn in one direction. 
  \end{solution}
\end{exercise}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
