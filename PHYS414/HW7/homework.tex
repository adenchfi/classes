\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations 
}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\usepackage{gensymb}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{7} % workaround to make problem numbers start with 5 for HW 5
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#7}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzouris}
\date{\today{}}
\maketitle

\begin{exercise}[Griffiths 9.35]
  Suppose we have
  \begin{equation}
    \vec E(r, \theta, \phi, t) = A\frac{\sin \theta}{r}[\cos(kr - \omega t) - (1/kr)\sin(kr - \omega t)]\hat \phi \tag*{with $\omega/k = c$}
  \end{equation}
This is, incidentally, the simplest possible \textbf{spherical wave}. For notational convience, let $(kr - \omega t) \equiv u$.
\begin{enumerate}[label=(\alph*)]
\item Find the associated magnetic field in vacuum.
  \begin{solution}
    \textbf{In a sentence:} Use the Maxwell equations in vacuum to identify a relation between $\pdv{B}{t}$ and $E$, and integrate with respect to time to get $B$.
    
    Recall the Maxwell equations
    \begin{align}
      \nabla \cross \vec E = -\pdv{\vec B}{t} \\
      \nabla \cross \vec B = \mu_0 \left(\vec J + \epsilon_0 \pdv{\vec E}{t} \right)
    \end{align}
    In free space, there is no current. We can solve this problem by using the first equation above to find $-\pdv{\vec B}{t}$ and then integrate with respect to time. Assuming there's no constants that were independent of time, we get our answer for $\vec B$. We could also use the second equation above to get a relation between various components of $\vec B$.
    Recall the curl in spherical coordinates is defined as
    \begin{align*}
      \nabla \cross \vec E = & \frac{1}{r \sin\theta} \left(\pdv{}{\theta} (E_\phi \sin \theta) - \pdv{E_\theta}{\phi} \right) \hat r \\
                             & + \frac{1}{r} \left(\frac{1}{\sin \theta} \pdv{E_r}{\phi} - \pdv{}{r} (r E_\phi) \right) \hat \theta \\
                             & + \frac{1}{r} \left(\pdv{}{r} (r E_\theta) - \pdv{E_r}{\theta} \right) \hat \phi.
    \end{align*}
    Since $\vec E$ only has a $\phi$ component, things are simplified. 
    Taking the curl of $\vec E$ yields
    \begin{align*}
      \nabla \cross \vec E = & \frac{1}{r \sin \theta}\left(\pdv{}{\theta} (E_\phi \sin\theta) \right) \hat r + \frac{1}{r}\left(- \pdv{}{r} (r E_\phi) \right)\hat \theta \\
      = & \frac{1}{r \sin \theta} \left(\pdv{}{\theta} (A \sin^2 \theta \frac{1}{r}[\cos(u) - (1/kr)\sin(u)]) \right) \hat r \\
                             & - \frac{1}{r} \left(\pdv{}{r} (A\sin \theta [\cos(u) - (1/kr)\sin(u)]) \right) \hat \theta \\
      = & \frac{1}{r \sin \theta} \left(2A \sin \theta \cos \theta [\cos(u) - (1/kr)\sin(u)] \right) \hat r \\
                             & - \frac{1}{r} \left( -Ak\sin \theta \sin(u) + A(1/kr^2)\sin \theta \sin u - A(1/r)\sin \theta \cos u \right) \hat \theta \tag{Chain rule} \\
      \nabla \cross \vec E = & \frac{2A \cos \theta}{r^2} \left[\cos(u) - (1/kr)\sin(u)\right]\hat r - \frac{1}{r}[A\sin\theta \sin u (\frac{1}{kr^2} - k) - (A/r)\sin \theta \cos u] \hat \theta
    \end{align*}
    Recall we still need to find the magnetic field. Noting $\pdv{\vec B}{t} = - \nabla \cross \vec E$, we have, after integrating,
    \begin{align*} 
      \vec B(t) = & \frac{2A\cos \theta}{\omega r^2}\left(\sin u + \frac{1}{kr}\cos u \right)\hat r + \frac{A\sin \theta}{\omega r} \left(-k \cos u + \frac{1}{kr^2}\cos u + \frac{1}{r}\sin u \right)\hat \theta.
    \end{align*}
    That gives us our magnetic field $\vec B(r, \theta, \phi, t)$ so long as we assume there were no time-independent constants. 
  \end{solution}
\item Calculate the Poynting vector. Average $\vec S$ over a full cycle to get the intensity vector $\vec I$. Does it point in the expected direction? Does it fall off as $r^{-2}$ as expected?
  \begin{solution}
    Recall $\vec S =  \frac{1}{\mu_0} \vec E \cross \vec B$. With our definitions of $\vec E, \vec B$ above, we can calculate the Poynting vector to be (letting $\beta = [\cos u + (1/kr) \sin u]$)
    \begin{align*}
      \vec S = & \frac{1}{\mu_0} \vec E \cross \vec B \\
      = & \frac{1}{\mu_0} \mdet{\hat r & \hat \theta & \hat \phi \\ 0 & 0 & E_\phi \\
      B_r & B_\theta & 0} \\
      = & \frac{A \sin \theta}{\mu_0 r}\left(\cos u - \frac{1}{kr}\sin u\right)\Big[\frac{2A \cos \theta}{\omega r^2} \beta \hat \theta
       + \frac{A \sin \theta}{\omega r}\left(-k \cos u + \frac{1}{r}\beta \right) (-\hat r) \Big] \\
      \vec S = & \frac{A^2 \sin \theta}{\mu_0 \omega r^2}\Big\{ \frac{2\cos \theta}{r}\Big[\left(1 - \frac{1}{k^2r^2}\right)\sin u \cos u + \frac{1}{kr}(\cos^2 u - \sin^2 u) \Big] \hat \theta \\
      + & \sin \theta\left[\left(-\frac{2}{r} + \frac{1}{k^2r^3} \right) \sin u \cos u + k\cos^2u + \frac{1}{kr^2}(\sin^2 u - \cos^2 u) \right] \hat r  \Big\}
    \end{align*}
    With the Poynting vector, we can find the time-averaged Poynting vector to get the average intensity vector \[
\vec I = \ev{\vec S} = \frac{1}{T} \int_0^T \vec S(t) dt 
\] where $T \equiv \frac{2\pi}{\omega}$.

We expect that for the $r^{-2}$ dependence to be true, that the $r^{-3}$ and $r^{-4}$ terms go away during the time averaging procedure. Let's see if it happens! We know $\ev{\sin u \cos u} = 0$ and $\ev{\sin^2 u} = \ev{\cos^u} = 1/2$. With these, we can see all terms disappear except $k\cos^2u = \frac{k}{2}$ inside the above parentheses for $\vec S$ when we integrate it out. This leaves us with
\begin{align*}
  \vec I = & \ev{\vec S} = \frac{1}{T} \int_0^T \vec S(t) dt \\
  = & \frac{A^2 \sin \theta}{\mu_0 \omega r^2}\left(\frac{k}{2}\sin \theta \right) \hat r \\
  \vec I = & \frac{A^2 \sin^2\theta}{2\mu_0 cr^2} \hat r
\end{align*} where we've used $k/\omega \equiv c$.

  \end{solution}

\item Integrate $\vec I \cdot d\vec a$ over a spherical surface to determine the total power radiated. 
  \begin{solution}
    Considering $da = r^2 \sin \theta dr d\theta d\phi$ for spherical coordinates, in the $\hat r$ direction, we can actually find this integral without too much of a hassle. Let's do it!
    \begin{align*}
      P = &\int_0^R \int_0^{\pi} \int_0^{2\pi} \frac{A^2 \sin^2\theta}{2\mu_0 cr^2} r^2 \sin \theta dr d\theta d\phi \tag{Integrate out $\phi$}\\
      = & 2\pi \frac{A^2}{2\mu_0 c}\int_0^R \int_0^\pi \sin^3(\theta) dr d\theta \tag{If we consider a unit sphere, $R \equiv 1$, that integral is 1} \\
      = & 2\pi \frac{A^2}{2\mu_0 c} \int_0^\pi \sin^3\theta d\theta \tag{Consult an integral table, or use identity $\sin^3 A = \frac{3}{4}\sin A - \frac{1}{4}\sin (3A)$} \\
      P = & 4\pi \frac{A^2}{3\mu_0 c}
    \end{align*}

  \end{solution}

\end{enumerate}

\end{exercise}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
