\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}
\usepackage{gensymb}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{10} % workaround to make problem numbers start with 5 for HW 5
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#10}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzouris}
\date{\today{}}
\maketitle

\begin{exercise}[Griffiths 11.1]
  Check that the retarded potentials of an oscillating dipole (Eqs. 11.12 and 11.17) satisfy the Lorenz gauge condition. Do \textit{not} use approximation 3.
  
  \begin{solution}
    If we recall the Lorenz gauge condition $\nabla \cdot \vec A = - \frac{1}{c^2} \pdv{V}{t}$, we can simply check if this is fulfilled by using the expressions for $V, \vec A$ derived in the text for an oscillating dipole. 
    Recall the two equations:
    \begin{equation}
      V(r, \theta, t) = \frac{p_0 \cos \theta}{4\pi \epsilon_0 r} \left[-\frac{\omega}{c}\sin[\omega(t - r/c)] + \frac{1}{r}\cos[\omega(t - r/c)] \right]
    \end{equation}
    and
    \begin{equation}
      \vec A(r, \theta, t) = - \frac{\mu_0 p_0 \omega}{4\pi r} \sin[\omega(t - r/c)] \hat z
    \end{equation}
    where $r$ is the distance from the dipole of the observer, $\theta$ is the angle from the line perpendicular to the direction of the dipole, and $t$ is, of course, time. 
    
    If we take the divergence of $\vec A$, recall that since it's only in the $\hat z$ direction, we only need the last component. We'll have to worry about chain rule stuff for the $r$ components, but that's fine. Recall that $\pdv{r}{z} = \frac{z}{r}$, $\pdv{1/r}{z} = -\frac{z}{r^2}$, and take the divergence of $\vec A$:
    \begin{align*}
      \nabla \cdot \vec A & = \frac{z\mu_0 p_0 \omega}{4\pi r^3} \sin[\omega(t - r/c)] + \frac{\mu_0 p_0 \omega}{4\pi r}\frac{\omega}{c}\cos[\omega(t - r/c)]\frac{z}{r} \\
                          & = \frac{z\mu_0 p_0 \omega}{4\pi r^2} \left[\frac{1}{r}\sin[\omega(t - r/c)] + \frac{w}{c}\cos[\omega(t - r/c)] \right] \\
      & = \mu_0 \frac{z p_0 \omega}{4\pi r^2} \left[\frac{1}{r}\sin[\omega(t - r/c)] + \frac{w}{c}\cos[\omega(t - r/c)] \right] \\
    \end{align*}
    If we now take the first time derivative of $V$, we get
    \begin{align*}
      \pdv{V}{t} & = \frac{p_0 \cos \theta}{4\pi r}\left[ -\frac{\omega^2}{c}\cos[\omega(t - r/c)] - \frac{\omega}{r}\sin[\omega(t - r/c)]  \right] \\
                 & = - \frac{1}{\epsilon_0} \frac{\cos \theta p_0 \omega }{4\pi r^2}\left[\sin[\omega(t-r/c)] + r\frac{\omega}{c} \cos[\omega(t-r/c)] \right] \tag{Recall $z \equiv r\cos \theta$} \\
                 & = - \frac{1}{\epsilon_0} \frac{z p_0 \omega }{4\pi r^2}\left[\frac{1}{r}\sin[\omega(t-r/c)] + \frac{\omega}{c} \cos[\omega(t-r/c)] \right].
    \end{align*}
    If you compare both expressions expressions, you see $-\mu_0 \epsilon_0 \pdv{V}{t} = \nabla \cdot \vec A$, or $\frac{1}{c^2}\pdv{V}{t} = \nabla \cdot \vec A$, as desired. 
    
  \end{solution}

\end{exercise}

\begin{exercise}[Griffiths 11.3]
  Find the \textbf{radiation resistance} of the wire joining the two ends of the dipole. Show that $R = 790 (d/\lambda)^2 \Omega$, where $\lambda$ is the wavelength of the radiation. For the wires in an ordinary radio (say, $d = 5$cm), should you worry about the radiative resistance contributing to the total resistance? 
  \begin{solution}
    The radiative resistance comes from equating the power as $P = I^2 R$. If we take the text's derivation for average power emitted from a dipole, we have
    \begin{equation}
      \ev{P} = \frac{\mu_0 p_0^2 \omega^4}{12\pi c}.
    \end{equation}
    We can also note that the current, defined as $\pdv{q}{t}$, can be easily found for an oscillating dipole as
    \begin{equation*}
      I = q_0 \omega \sin(\omega t)
    \end{equation*}
    which means the instantaneous radiative power would be
    \begin{align*}
      P = q_0^2 \omega^2 \sin^2(\omega t) R, \\
      \ev{P} = \frac{1}{2}q_0^2 \omega^2 R \tag{Averaging over the sin}.
    \end{align*}
    We can equate the above expression for $\ev{P}$ with our original one and solve for $R$, yielding
    \begin{align*}
      R & = \frac{\mu_0 d^2 \omega^2 }{6\pi c} \tag{Recall $\omega = 2\pi c/\lambda$} \\
      R & = \frac{2}{3}\pi c \mu_0 \left(\frac{d}{\lambda} \right)^2 \tag{Now plug in SI units} \\
      R & = 789.6 \left(\frac{d}{\lambda}\right)^2 \Omega.
    \end{align*}
    With $d = 5 \cross 10^{-2}$ and the average radio wave wavelength of around $\lambda = 10^3 m$ being considered, we can plug these in to the above expression to obtain a radiative resistance term $R = 2 \cross 10^{-6} \Omega$. This is negligible, considering that we regularly consider resistances of Ohms, kilo-Ohms, or higher. 
    
  \end{solution}

\end{exercise}


\begin{exercise}[Griffiths 11.10]
  An insulating circular ring of radius $b$ lies in the $xy$ plane, centered at the origin. It carries a linear charge density $\lambda = \lambda_0 \sin \phi$, where $\lambda_0$ is a constant and $\phi$ is the usual azimuthal angle. The ring is now set spinning at a constant angular velocity $\omega$ about the $z $ axis. Calculate the power radiated.
  \begin{solution}
    Summary: We can then calculate the dipole moment using $\vec p = \int \lambda \pvec r' dl$, and use the expressions derived in the appropriate section (Eq. 60) to get the approximate power radiated from the system: $P_{rad}(t_0) \approx \frac{\mu_0}{6\pi c}[\ddot{p}(t_0)]^2$.
    
    Full Solution: Recall that the dipole moment is defined as
    \begin{equation}
      \int \pvec r' \rho(\pvec r', t_0)d\tau' = \int \lambda \pvec r' dl.
      \end{equation}
      For this system, we can then write, for $t = 0$,
      \begin{align*}
        \vec p_0 = & \int (\lambda_0 \sin\phi)(b\sin\phi \hat y + b \cos \phi \hat x) b d\phi \\
        = & \lambda_0 b^2 \left(\hat y \int_0^{2\pi} \sin^2\phi d\phi + \hat x\int_0^{2\pi} \sin\phi \cos\phi d\phi   \right) \\
        = & \lambda b^2 (\pi \hat y + 0 \hat x) \\
        = & \pi b^2 \lambda_0 \hat y.
      \end{align*}
      As the system rotates, $\vec p(t) = p_0 [\cos(\omega t) \hat y - \sin(\omega t)\hat x]$, which means the second derivative needed for the power expression is $\ddot{\vec p} = -\omega^2 \vec p$, which means that $(\ddot{\vec p})^2 = \omega^4 p_0^2$. By Eq. 60, we have
      \begin{align*}
        P = \frac{\mu_0}{6\pi c}\omega^4 (\pi b^2 \lambda_0)^2 = \frac{\pi \mu_0 \omega^4 b^4 \lambda_0^2}{6c}. 
      \end{align*}
      If we consider the fact that the Advanced Photon Source itself is basically a ring of oscillating charge, we can directly see the intensity of x-rays we'd emit from our system. I wonder how we get the frequency of waves emitted... 
    \end{solution}
    
  \end{exercise}

  \begin{exercise}[Summarize Chapter 10 Topics]
    This exercise, as the name implies, asks us to summarize the topics covered in Chapter 10.
    \begin{solution}
      In Chapter 10, we began using the full brunt of Maxwell's equations for time-dependent fields and sources. We expressed the electric and magnetic fields in terms of electric and vector potentials, and then explored the kinds of transformations we could allow on those potentials that would leave $\vec E, \vec B$ invariant. In particular, so long as $\vec A$ and $V$ fulfilled their respective inhomogeneous wave equations, we were good. Furthermore, we made the point that information cannot travel faster than the speed of light, and therefore the fields at a point $r$ away from the source of the potential would only experience that potential after a time delay. 

      Afterwards, we managed to illustrate the solutions for spherical wave equations of $V$ and $\vec A$, if our potential had spherical symmetry. We then find expressions for $\vec E$ and $\vec B$ in terms of the charge densities and currents (and their time derivatives). An example of a moving point charge was performed, where the scalar potential was put in terms of the absolute time versus the relative time. Finally, the electric and magnetic fields that come from an accelerating point charge were derived. 
    \end{solution}

  \end{exercise}

  \begin{exercise}[Summarize Chapter 11 Topics]
    This exercise, as the name implies, asks us to summarize the topics covered in Chapter 11.
    \begin{solution}
      We defined radiated power as the energy/time transported to infinity ($r \rightarrow \infty$). This means that when calculating the power, the Poynting vector must decay as $1/r^2$, such that the spherical integration (which introduces an $r^2$ factor) yields a constant. Any system with a Poynting vector $\vec S$ that does not obey that condition doesn't have far-field radiation.

      For any arbitrary charge distribution (a dipole is a simple example), use the methods developed previously to find the retarded potentials, and find the electric and magnetic fields from that. Again, you make the far-field approximation ($r >> r'$) and assume that the measurer distance is much larger than the wavelength of the radiation ($r >> \lambda$). Using these approximations allow a more amenable derivation of the scalar and vector potentials of the system. In particular, a Taylor expansion of the charge distribution is possible under these approximations, and same with the current.

      Since we have an exact expression for the potential $V$ in terms of the charge distribution $\rho$, the first few terms of the Taylor expansion of $\rho$ will allow for a pretty good estimation of $V$. Furthermore, we know higher-order terms will contribute less and less to the potential. Since we know the Poynting vector must be of a certain form, this allowed us to drop some terms in the calculated electric and magnetic fields.

      Finally, we specialized our discussion to the power radiated by relativistic point particles. We considered both when the acceleration and velocity were parallel, and then when they were perpendicular. It turned out that perpendicular acceleration gives off a lot more power. 
    \end{solution}
  \end{exercise}
  
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
