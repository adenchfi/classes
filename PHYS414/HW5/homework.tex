\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations 
}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\usepackage{gensymb}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{5} % workaround to make problem numbers start with 5 for HW 5
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#5}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzouris}
\date{\today{}}
\maketitle

\begin{exercise}[Griffiths 9.15]
  In writing Eqs. 9.76 and 9.77, the assumption was made that the reflected and transmitted waves have the \textbf{same polarization} as the incident wave - along the x-direction. Prove that this must be so.
  \begin{proof}
    Let's assume without loss of generality that the polarization vectors of the transmitted and reflected waves are
    \begin{align}
      \hat n_T = \cos \theta_T \hat x + \sin \theta_T \hat y \\
      \hat n_R = \cos \theta_R \hat x + \sin\theta_R \hat y.
    \end{align}
    Can we use the boundary conditions to show that the reflected and transmitted waves are the same polarization as the incoming waves? Note that the boundary conditions are
    \begin{align}
      \epsilon_1 E_1^\perp = \epsilon_2 E_2^\perp, \quad E_1^\parallel = E_2^\parallel  \\
      B_1^\perp = B_2^\perp, \quad \frac{1}{\mu_1}B_1^\parallel = \frac{1}{\mu_2}B_2^\parallel,
    \end{align}
    recalling that the $E, B$ above are vectors.

    Say we have an incoming wave traveling in the z-direction, polarized in the x-direction,
    \begin{align}
      \mathbf{E}_I (z, t) = E_I e^{i(k_1 z - \omega t)} \hat x, \\
      \mathbf B_I (z, t) = \frac{1}{v_1}E_I e^{i(k_1 z - \omega t)}\hat y.
    \end{align}
    After interacting with the boundary, this wave gives rise to reflected and transmitted waves
    \begin{align}
      \mathbf E_R (z, t) = E_I e^{i(-k_1 z - \omega t)} \hat n_R, \\
      \mathbf B_R (z, t) = \frac{1}{v_1}E_I e^{i(-k_1 z - \omega t)}\hat n_R, \\
      \mathbf E_T (z, t) = E_I e^{i(k_2 z - \omega t)} \hat n_T, \\
      \mathbf B_T (z, t) = \frac{1}{v_2}E_I e^{i(k_2 z - \omega t)}\hat n_T.
    \end{align}
    We know that the left and right waves must fulfill the boundary conditions outlined above. 
    \begin{remark}
      I can only accept this for time $t = 0$. For $t > 0$, the incoming wave no longer exists, and instead the amplitudes of the reflected and transmitted waves should add to the amplitude of the incoming wave. 
    \end{remark}
    Since our polarization of our reflected and transmitted waves are not necessarily in $\hat x$, we will address the parallel-component boundary conditions above, and (hopefully) see that they require the polarization to be in the $\hat x$ direction. Since our polarization vectors are still parallel to the interface (xy plane), those conditions are satisfied automatically.
    We note that $E_1^\parallel = E_2^\parallel$ implies
    \begin{align}
      E_I \hat x + E_R \hat n_R = E_T \hat n_T \tag{Or, expanding,}\\
      E_I \begin{bmatrix} E_I \\ 0 \\ 0 \end{bmatrix} + E_R \begin{bmatrix} \cos \theta_R \\ \sin \theta_R \\ 0 \end{bmatrix} = E_T
      \begin{bmatrix}
        \cos \theta_T \\ \sin \theta_T \\ 0
      \end{bmatrix}
    \end{align}
    which gives rise to $E_R \sin \theta_R = E_T \sin \theta_T$. This must be true always, and the only way for that to be the case is for $\theta_R = \theta_T = 0$; the waves must be polarized in the $\hat x$ direction as well.
    
  \end{proof}
\end{exercise}

\begin{exercise}[First Part of Griffiths 9.17]
  Analyze the case of polarization \textit{perpendicular} to the plane of incidence (i.e., electric fields in the y-direction). Impose the boundary conditions 9.101, and obtain the Fresnel equations for $E_{0_R}, E_{0_T}$.
  \begin{solution}
    Recall the boundary conditions found for Eq. 9.101,
    \begin{align}
      \epsilon_1 (\mathbf{E}_I + \mathbf{B}_R)_z = \epsilon_2 (\mathbf{E}_T)_z \\
      (\mathbf{B}_I + \mathbf{B}_R)_z = (\mathbf{B}_T)_z \\
      (\mathbf{E}_I + \mathbf{E}_R)_{x, y} = (\mathbf{E}_T)_{x, y} \\
      \frac{1}{\mu_1}(\mathbf{B}_I + \mathbf{B}_R)_{x, y} = \frac{1}{\mu_2} (\mathbf{B}_T)_{x, y}
    \end{align}
    where $\mathbf{B} = (1/v) \hat k \cross \mathbf{E}$ in each
    case. We know $E$ is polarized in the $\hat y$ direction, and therefore $B$ is polarized in the $(-cos \theta_1 \hat x + \sin \theta_1 \hat z)$.

    The boundary conditions yield,
    \begin{itemize}
    \item 0 = 0 (from BC 1; trivial since there is no z-component)
    \item (BC 2) $\frac{1}{v_1} E_I \sin \theta_1 + (1/v_1) E_R \sin \theta_1 = \frac{1}{v_2}E_T \sin \theta_2$ which implies $E_I + E_R = (\frac{v_1 \sin \theta_2}{v_2 \sin \theta_1})E_T$. Using the law of refraction $\frac{\sin \theta_2}{\sin \theta_1} = \frac{v_2}{v_1}$, we see \[
E_I + E_R = E_T
\] which is the same as what you would get using the third boundary condition. 
\item (BC 4)
  \begin{align*}
    \frac{1}{\mu_1}\left[ \frac{1}{v_1}E_I (-\cos \theta_1) + \frac{1}{v_1} E_R \cos\theta_1 \right] = \frac{1}{\mu_2 v_2} E_T (-\cos\theta_2) \\
    \implies E_I - E_R = \frac{\mu_1 v_1 \cos \theta_2}{\mu_2 v_2 \cos \theta_1} E_T \tag{Now let $\alpha \equiv \frac{\cos \theta_2}{\cos \theta_1}, \beta \equiv \frac{\mu_1 v_1}{\mu_2 v_2}$} \\
    \implies E_I - E_R = \alpha \beta E_T.
  \end{align*}
\end{itemize}
We now have two equations and three unknowns. Let's solve for $E_R$ and $E_T$ in terms of $E_I$. First, add our two equations to get one relation, and subtract them to get another. 
\begin{align*}
  2E_I = (1 + \alpha \beta) E_T \implies E_T = (\frac{2}{1 + \alpha \beta}) E_I \\
  2E_R = (1 - \alpha \beta) E_T \tag{Now we substitute our term for $E_T$} \\
  E_R = \frac{1 - \alpha \beta}{1 + \alpha \beta} E_I
\end{align*}
We now have our Fresnel equations! Keep in mind the above are still complex amplitudes. 
  \end{solution}

\end{exercise}

\begin{exercise}[Griffiths 9.20]
  This problem is about skin depth and exploring the relationships between the components of our solutions.
  \begin{enumerate}[label=(\alph*)]
  \item Show the skin depth in a poor conductor ($\sigma << \omega \epsilon$) is $(2/\sigma) \sqrt{\epsilon / \mu}$, independent of frequency. Find the skin depth (in meters) for (pure) water.
    \begin{solution}
      Recall the definition of skin depth,
      \begin{equation}
        d \equiv \frac{1}{\kappa}, \\
        \kappa = \omega \sqrt{\frac{\epsilon \mu}{2}} \left[\sqrt{1 + (\frac{\sigma}{\epsilon \omega})^2} - 1 \right]^{1/2}.
      \end{equation}
      For $\sigma << \omega \epsilon$, we notice that we can make the binomial expansion of the square-root term $\sqrt{1 + (\sigma / (\epsilon \omega))^2} \approx 1 + (1/2) (\sigma / (\epsilon \omega))^2$ since $\sigma/(\epsilon \omega) << 1$. Thus we get, after canceling the 1's and taking the square root, \[
        \kappa = \omega \sqrt{\frac{\epsilon \mu}{2}} \sqrt{\frac{1}{2}} \frac{\sigma}{\epsilon \omega}
      \]
      which we can further simplify to
      \begin{equation} \label{eq:poor_cond}
        \kappa \approx \frac{\sigma}{2}\sqrt{\frac{\mu}{\epsilon}}
      \end{equation}
      and therefore
      \begin{equation}
        \label{eq:depth_poor_cond}
        d \approx \frac{2}{\sigma}\sqrt{\frac{\epsilon}{\mu}}.
      \end{equation}
      Now, pure water has a poor conductivity - $\sigma = 5.5 \cdot 10^{-6} S/m$, where $S$ means Siemens and is the SI unit for conductivity. Of course, the table in the book lists it as $\sigma = 1/(2.5 \cdot 10^5)$. We have $\epsilon = \epsilon_r \epsilon_0 = 80.1 \epsilon_0$ and $\mu = \mu_0 (1 + \chi_m) = \mu_0 (1- 9.0\cdot 10^{-6}) approx \mu_0$. Plugging in numbers yields \[
d \approx 1.19 \cross 10^4 m.
\]
    \end{solution}    
  \item Show that the skin depth for a good conductor $(\sigma >> \omega \epsilon)$ is $\lambda / 2\pi$, where $\lambda$ is the wavelength \textit{inside} the conductor. Find the skin depth (in nanometers) for a typical metal ($\sigma \approx 10^7 (\Omega m)^{-1}$) in the visible range ($\omega \approx 10^15 / s$), assuming $\epsilon \approx \epsilon_0, \mu \approx \mu_0$. Why are metals opaque?
    \begin{solution}
      With $\sigma >> \omega \epsilon$, that term in $\kappa$ dominates; in fact, it is almost as much as $\Re{k}$. We get \[
\kappa \approx \sqrt{\omega^2 \epsilon \mu / 2} \sqrt{\frac{\sigma}{\epsilon \omega}} = \sqrt{\frac{\omega \sigma \mu}{2}}
\]
and we can plug in numbers to get $\kappa \approx 8 \cross 10^7$. Since $d = \frac{1}{\kappa}$, we have \[
d \approx \frac{1}{8 \cross 10^7} = 13 nm. 
\] Note that because $\kappa \approx k$, $d \approx (1/k) = \lambda/ 2\pi$.

Metals, then, are opaque because light (electromagnetic waves) cannot penetrate through them very far. However, this does mean thin films of metals (say, 10nm) will have a significant amount of light passing through.
    \end{solution}
  \item Show that in a good conductor, the magnetic field lags the electric field by a whole 45\degree. Find the ratio of their amplitudes.
    \begin{solution}
      Recall that
      \begin{equation}
        \phi = \tan^{-1}(\kappa / k)
      \end{equation}
      and furthermore, in good conductors, $\kappa \approx k$. The inverse tangent of 1 is 45\degree, so $\phi = 45\degree$. Meanwhile, also recall that
      \begin{equation}
        \frac{B_0}{E_0} = \frac{K}{\omega} = \sqrt{\epsilon \mu \sqrt{1 + (\sigma / \epsilon \omega)^2}}.
      \end{equation}
      Since we are dealing with a good conductor, we know the inner square root is approximately $\sigma / \epsilon \omega$, and so \[
\frac{B_0}{E_0} \approx \sqrt{\frac{\sigma \mu}{\mu}}.
\] Plugging in numbers for a typical metal, we get $\frac{B_0}{E_0} \approx 10^{-7} S/m$. 
    \end{solution}

  \end{enumerate}

\end{exercise}

\begin{exercise}[Griffiths 9.22]
  Calculate the reflection coefficient for light at an air-to-silver interface at optical frequencies. This means $\mu_1 = \mu_2 = \mu_0, \epsilon_1 = \epsilon_0, \sigma = 6 \cross 10^7 (\Omega \cdot m)^{-1}, \omega = 4 \cross 10^15 / s$.
  \begin{solution}
    We have \[
      R= \abs{E_R / E_I}^2 = \abs{\frac{1 - \beta^*}{1 + \beta^*}}, \quad \beta = \frac{u_1 v_1}{\mu_2 \omega}(k_2 + i\kappa_2).
    \]
    Recall that for a good conductor (like silver!), $\kappa_2 \approx k_2$, so \[
\beta  = \mu_1 v_1 \sqrt{\frac{\sigma}{2\mu_2 \omega}} (1 + i).
\]Plugging in numbers, we get 0.93 - 93\% of the light is reflected. 
  \end{solution}

\end{exercise}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
