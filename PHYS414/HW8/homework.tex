\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations 
}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\usepackage{gensymb}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{8} % workaround to make problem numbers start with 5 for HW 5
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#8}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzouris}
\date{\today{}}
\maketitle

\begin{exercise}[Griffiths 10.2]
  Equations 14, 17, 18, 19, and 21 can be expressed in ``coordinate-free'' form by writing $p_0 \cos \theta = \vec p_0 \cdot \hat r$. Do so.
  \begin{solution}
    For equation 14, we can achieve the desired result via direct substitution. For the other equations, we will have to use other forms, such as
    \begin{equation}
      p_0 = \frac{\vec p_0 \cdot \hat r}{\cos \theta}
    \end{equation}
    in order to achieve coordinate-free forms.
      Recall equation 14:
      \begin{equation}
        V(r, \theta, t) = - \frac{p_0 \omega}{4\pi \epsilon_0 c}\left(\frac{\cos \theta}{r} \right) \sin[\omega(t - r/c)]
      \end{equation}
      By the original proposed substitution, we get
      \begin{align*}
        V(r, \theta, t) = & -\frac{\omega}{4\pi \epsilon_0 c} \left(\frac{\vec p_0 \cdot \hat r}{r} \right) \sin[\omega (t - r/c)] \\
      \end{align*}
      Recall the form of equation 17,
      \begin{equation}
        \vec A(r, \theta, t) = -\frac{\mu_0 p_0 \omega}{4\pi r} \sin[\omega(t - r/c)]\hat z
      \end{equation}
      Consider the case of $\hat r = \hat z$. We then have $p_0 \cos \theta = \vec p_0 \cdot \hat z$. We have, upon assuming that $\vec p_0$ is in the $\hat z$ direction, 
      \begin{align*}
        \vec A(r, \theta, t) - \frac{\mu_0 \omega}{4\pi} \frac{\vec p_0}{r}\sin[\omega(t - r/c)]
      \end{align*}
      For the next two equations, we need to expand our idea of a substitution. If we are considering $\vec p_0$ to be in the $\hat z$ direction, then $\vec p_0 \cross \hat r = p_0 \sin \theta hat \phi$ and $\hat r \cross (\vec p_0 \cross \hat r) = -p_0 \sin \theta \hat \theta$. This latter identity can be substituted into equation 18 to yield
      \begin{align*}
        \vec E(\vec r, t) = \frac{\mu_0 \omega^2}{4\pi} \frac{\hat r \cross (\vec p_0 \cross \hat r)}{r} \cos[\omega(t - r/c)].
      \end{align*}
      The former identity can be used to transform both equations 19 and 21 as such:
      \begin{align*}
        \vec B (\vec r, t) = & - \frac{\mu_0 \omega^2}{4\pi c^2}\frac{\vec p_0 \cross \hat r}{r}\cos[\omega(t - r/c)] \\
        \ev{\vec S} = & \frac{\mu_0 \omega^4}{32 \pi^2 c} \frac{(\vec p_0 \cross \hat r)^2}{r^2} \hat r
      \end{align*}
    \end{solution}
  \end{exercise}

  \begin{exercise}[Gauge Transformation]
    Let $\phi, \vec A$ denote scalar and vector potentials, respectively. Consider the transformation
    \begin{align}
      \phi \rightarrow & \phi' = \phi + \alpha \pdv{\Gamma}{t} \\
      \vec A \rightarrow & \vec A' = \vec A - \alpha \nabla \Gamma
    \end{align}
    where $\Gamma(t, \vec x)$ is the gauge function and $\alpha(t, \vec x)$ is a differentiable function. What restriction must be put on $\alpha$ in order that the two previous equations constitute a gauge transformation?
    \begin{solution}
      We must recall that any gauge transformation leaves the observables, such as $\vec E, \vec B$ unchanged. Recall we can define the magnetic and electric fields as such:
      \begin{align}
        \vec B = \nabla \cross \vec A \\
        \vec E = -\nabla \phi - \pdv{\vec A}{t}
      \end{align}

      This implies that $\nabla \phi + \pdv{\vec A}{t}  = \nabla \phi' + \pdv{\vec A'}{t}$ and $\nabla \cross \vec A = \nabla \cross \vec A'$. Following the consequences of this invariance yields our solution.

      Plugging in our transformations to the constraints above, we obtain
      \begin{align*}
        \nabla \phi + \pdv{\vec A}{t} = \nabla \phi + \pdv{\vec A}{t} + \nabla(\alpha \pdv{\Gamma}{t}) - \pdv{(\alpha \nabla \Gamma)}{t} \\
        \nabla \cross \vec A = \nabla \cross \vec A - \nabla \cross (\alpha \nabla \Gamma)
      \end{align*}
      which, after canceling equivalent terms and expanding out the product rules, yields
      \begin{align*}
        \nabla \alpha \pdv{\Gamma}{t} + \alpha \pdv{(\nabla \Gamma)}{t} - \pdv{\alpha}{t} \nabla \Gamma - \alpha \pdv{(\nabla \Gamma)}{t} = 0\tag{Can cancel more terms} \\
        \alpha (\nabla \cross (\nabla \Gamma)) + (\nabla \alpha) \cross \nabla \Gamma = 0 \tag{Curl of gradient is zero} \\
        \implies \\
        \nabla \alpha \pdv{\Gamma}{t} = \pdv{\alpha}{t}\nabla \Gamma \\
        \nabla \alpha \cross \nabla \Gamma = 0
      \end{align*}
      If we solve for $\nabla \alpha$ in the second to last line above, and consider the chain rule, we can obtain
      \begin{align*}
        \nabla \alpha = \pdv{\alpha}{\Gamma} \nabla \Gamma.
      \end{align*}
      We can then substitute this result into the cross product above, yielding
      \begin{align*}
        (\pdv{\alpha}{\Gamma} \nabla \Gamma) \cross \nabla \Gamma = \pdv{\alpha}{\Gamma} (\nabla \Gamma \cross \nabla \Gamma) = 0
      \end{align*}
      This implies that one solution is that $\alpha$ and $\Gamma$ are independent. If that is the case, then
      \begin{align*}
        \nabla \alpha \pdv{\Gamma}{t} = \pdv{\alpha}{t}\nabla \Gamma 
      \end{align*}
      implies that, if the two are independent functions, that the two sides of the following equation must be equal and therefore constant:
      \begin{align*}
        \nabla \alpha \pdv{t}{\alpha} = C = \pdv{t}{\Gamma} \nabla \Gamma
      \end{align*}
      This gives us two separate equations:
      \begin{align*}
        \nabla \alpha = C \pdv{\alpha}{t} \\
        \nabla \Gamma = C \pdv{\Gamma}{t}
      \end{align*}
      This is the constraint that $\alpha$ and $\Gamma$ must obey. In particular, one can observe that from these we obtain the wave equation for both functions, by careful differentiation. 
    \end{solution}

  \end{exercise}

  \begin{exercise}[Griffiths 10.10]
    An insulating circular ring of radius $b$ lies in the $xy$ plane, centered at the origin. It carries a linear charge density $\lambda = \lambda_0 \sin \phi$, where $\lambda_0$ is a constant and $\phi$ is the usual azimuthal angle. The ring is now set spinning at a constant angular velocity $\omega$ about the $z $ axis. Calculate the power radiated.
    \begin{solution}
      Summary: We can then calculate the dipole moment using $\vec p = \int \lambda \vec r' dl$, and use the expressions derived in the appropriate section (Eq. 60) to get the approximate power radiated from the system: $P_{rad}(t_0) \approx \frac{\mu_0}{6\pi c}[\ddot{p}(t_0)]^2$.

      Full Solution: Recall that the dipole moment is defined as
      \begin{equation}
        \int \vec r' \rho(\vec r', t_0)d\tau' = \int \lambda \vec r' dl.
      \end{equation}
      For this system, we can then write, for $t = 0$,
      \begin{align*}
        \vec p_0 = & \int (\lambda_0 \sin\phi)(b\sin\phi \hat y + b \cos \phi \hat x) b d\phi \\
        = & \lambda_0 b^2 \left(\hat y \int_0^{2\pi} \sin^2\phi d\phi + \hat x\int_0^{2\pi} \sin\phi \cos\phi d\phi   \right) \\
        = & \lambda b^2 (\pi \hat y + 0 \hat x) \\
        = & \pi b^2 \lambda_0 \hat y.
      \end{align*}
      As the system rotates, $\vec p(t) = p_0 [\cos(\omega t) \hat y - \sin(\omega t)\hat x]$, which means the second derivative needed for the power expression is $\ddot{\vec p} = -\omega^2 \vec p$, which means that $(\ddot{\vec p})^2 = \omega^4 p_0^2$. By Eq. 60, we have
      \begin{align*}
        P = \frac{\mu_0}{6\pi c}\omega^4 (\pi b^2 \lambda_0)^2 = \frac{\pi \mu_0 \omega^4 b^4 \lambda_0^2}{6c}. 
      \end{align*}
      If we consider the fact that the Advanced Photon Source itself is basically a ring of oscillating charge, we can directly see the intensity of x-rays we'd emit from our system. I wonder how we get the frequency of waves emitted... 
    \end{solution}

  \end{exercise}
  \begin{exercise}[Griffiths 10.11]
    A current $I(t)$ flows around the circular ring in Fig. 8. Derive the general formula for the power radiated (analogous to Eq. 60), expressing your answer in terms of the magnetic dipole moment, $m(t)$, of the loop.
    \begin{solution}
      We can treat the spinning ring as a stationary ring with spinning charge, as the book does for a dipole of oscillating charge rather than of oscillating separation distance. This ``spinning charge'' is analogous to a current flow. Since current $I(t) = \rho(t)v(t)$, where $\rho(t)$ is the charge density, we can write the current as $I(t) = \lambda_0 \sin\phi \cos (\omega t) \omega R$, where $\omega$ is the angular frequency of the system, $R$ is the radius of the ring.

      Not exactly sure how to proceed, honestly...
    \end{solution}

  \end{exercise}

  \begin{exercise}[Griffiths 10.14]
    In Bohr's theory of hydrogen, the electron in its ground state was supposed to travel in a circle of radius $5 \cross 10^{-11}$m, held in orbit by the Coulomb attraction of the proton. According to classical electrodynamics, this electron should radiate, and therefore spiral in to the nucleus. Show that $v << c$ for most of the trip, allowing for the Larmor formula to be used, and calculate the lifespan of the Bohr atom. Note: This also was the motivation behind developing a quantum-mechanical description of this stuff!
    \begin{solution}
      If we equate the Coulomb force with mass times the angular acceleration formula, we can solve for the average velocity and show that we're guaranteed the velocity is much less than $c$ for most of the trip.
      \begin{align*}
        F = \frac{1}{4\pi \epsilon_0}\frac{q^2}{r^2} = ma = m\frac{v^2}{r} \implies v = \sqrt{\frac{q^2}{4\pi \epsilon_0 mr}}
      \end{align*}
      and since when the electron begins rotating, we have $r_0 = 0.5$ Angstroms,
      \begin{align*}
        \frac{v}{c} = 0.0075
      \end{align*}
      when we plug in all the numbers. Even when the radius is one-hundredth of this, due to being in a square root, the velocity is only ten times larger. Then we can apply the Larmor formula,
      \begin{align*}
        P = \frac{\mu_0 q^2}{6\pi c}(\frac{v^2}{r})^2 = \frac{\mu_0 q^2}{6\pi c}\left(\frac{q^2}{4\pi \epsilon_0 mr^2} \right)^2 \tag{since $a \equiv v^2/r$}.
      \end{align*}
      Since we can also write $P = -\dv{U}{t}$, where $U$ is the (total) energy of the electron, we can use this to easily find $\dv{r}{t}$ by equating the naive expression of $-\dv{U}{t}$ with our Larmor expression for power $P$.
      \begin{align*}
        -\dv{U}{t} = -\frac{q^2}{8\pi \epsilon_0 r^2}\dv{r}{t} = P = \frac{q^2}{6\pi \epsilon_0 c^3} \left(\frac{q^2}{4\epsilon_0 mr^2} \right)^2 \\
        \implies \dv{r}{t} = -\frac{1}{3c} \left(\frac{q^2}{2\pi \epsilon_0 mc} \right)^2 \frac{1}{r^2} \\
        dt = -3c\left(\frac{2\pi \epsilon_0 mc}{q^2}\right)^2 r^2 dr \implies t = -3c\left(\frac{2\pi e_0 mc}{q^2} \right)^2 \int_{r_0}^0 r^2 dr \\
        t = c\left(\frac{2\pi \epsilon_0 mc}{q^2} \right)^2 r_0^3 \tag{Plug in numbers!}\\
        t = 1.3 \cross 10^{-11}s \tag{Not long at all!}
      \end{align*}

    \end{solution}

  \end{exercise}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
