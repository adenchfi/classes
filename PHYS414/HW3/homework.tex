\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}

%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{3} % workaround to make problem numbers start with 2 for HW 2
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#3}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzrous}
\date{\today{}}
\maketitle

\begin{exercise}[Complex number manipulation]
  What is the magnitude and phase of the complex amplitude $C = \frac{1}{a-ib}$, in terms of $a$ and $b$?
  \begin{solution}
    First, multiply the top and bottom by the complex conjugate of the bottom. We get
    \begin{align*}
      C = & \frac{a+ib}{a^2-b^2} \\
      = & \frac{a}{a^2 - b^2} + \frac{ib}{a^2 - b^2} \\
      \implies \abs{C} = & \frac{1}{a^2 - b^2}\sqrt{a^2 + b^2} \tag{Since, for $z = x + iy$, $\abs{z} = \sqrt{x^2 + y^2}$}
    \end{align*}
    That gets us our magnitude. If both $a > 0$, then our phase can be determined by $\phi = \arg{C} = \arctan{(\frac{b}{a})}$ after canceling the common denominators. 
  \end{solution}
\end{exercise}

\begin{exercise}[Trig Identities]
  Show that
  \begin{equation*}
    \sin(\alpha + \beta) = \cos(\alpha) \sin(\beta) + \cos(\beta) \sin(\alpha).
  \end{equation*}
  You can use the Euler identities/formula.
  \begin{solution}
    It is almost trivial when using considering the fact that $\sin(\alpha + \beta) = \Im{e^{i\alpha + i\beta}}$.
    \begin{align*}
      \sin(\alpha + \beta) = & \Im{e^{i(\alpha + \beta)}}\\
      = & \Im{e^{i\alpha} e^{i\beta}} \\
      = & \Im{(\cos \alpha + i\sin \beta) (\cos \beta + i\sin\beta)} \tag{Euler's identity!}\\
      = & \cos\alpha \sin\beta + \cos\beta \sin \alpha \tag{After simplification}
    \end{align*}
    The desired property was shown.
    \begin{remark}
      These sort of techniques can yield tricky ways to find definite integrals, particularly of trigonometric origin. 
    \end{remark}

  \end{solution}

\end{exercise}


\begin{exercise}[Griffiths 9.2]
  Show that the \textbf{standing wave} $f(z, t) = A\sin(kz) \cos(kvt)$ satisfies the wave equation, and express it as a sum of a wave traveling to the left and a wave traveling to the right.
  \begin{solution}
    Recall the wave equation
    \begin{equation}
      \label{eq:wave}
      \pdv[2]{f}{z} = \frac{1}{v^2} \pdv[2]{f}{t}.
    \end{equation}
    Because $f$ is separated into functions depending on $z$ and $t$ alone, the derivatives are easy to compute.
    \begin{align}
      \pdv[2]{f}{z} = &-Ak^2 \sin(kz) \cos(kvt) \\
      \pdv[2]{f}{t} = & -Ak^2v^2 \sin(kz) \cos(kvt) \tag{Notice that the two expressions look like $f$}\\
      \implies \pdv[2]{f}{z} =& \frac{1}{v^2} \pdv[2]{f}{t} \tag{Pattern recognition}.\\
    \end{align}
    Now recall the trig identity
    \begin{equation}
      \sin\alpha \cos \beta = \frac{1}{2} [\sin(\alpha + \beta) + \sin(\alpha - \beta)]
    \end{equation} to write our solution $f$ as
    \begin{equation}
      f = \frac{A}{2} \left[\sin(k(z + vt)) + \sin(k(z - vt)) \right]
    \end{equation}
    which can be seen to be a sum of two waves traveling to the left and to the right. 
  \end{solution}
\end{exercise}

\begin{exercise}[Griffiths 9.4]
  Use separation of variables on the wave equation to obtain Eq. 20,
  \begin{equation}
    \Re{f(z, t)} = \int_{-\infty}^\infty \Re{A(k)} e^{i(kz-\omega t)}.
  \end{equation}
  I have dropped the tilde's indicating quantities are taking the real part; that is not standard notation and, if I'm being honest here, is annoying to type and read.  
  \begin{solution}
    Recall the wave equation \eqref{eq:wave}. If we suppose our solution is of the form $f(z, t) = Z(z)T(t)$, then we can obtain a set of differential equations:
    \begin{align*}
      \frac{\dv[2]{Z}{z}}{Z(z)} = & \frac{1}{v^2} \frac{\dv[2]{T}{t} }{T(t)} \\
      \implies \frac{\dv[2]{Z}{z}}{Z(z)} = & -\lambda^2  \tag{Now assume a solution of the form $Ae^{w z}$}\\
      w^2 = & -k^2  \\
      w = & \pm \sqrt{-k^2} \tag{Recall BCs. If a traveling wave, it isn't square integrable.} \\
    \end{align*}
    We will apply the boundary conditions later. Let us pay attention to the time dependent part now. 
    \begin{align*}
      \frac{1}{v^2} \frac{\dv[2]{T}{t} }{T(t)} = & -k^2 \tag{Assume an exponential form $Ae^{\alpha t}$}\\
      \implies \alpha^2 / v^2 = & -k^2 \\
      \alpha = \pm v \sqrt{-k^2} 
    \end{align*}
    Combining the solutions $f(z, t) = Z(z)T(t)$, and restricting $k$ to be real, we get
    \begin{equation}
      f(z, t) = (A_1e^{ikz} + A_2 e^{-ik z}) (A_3 e^{i\omega t } + A_4 e^{-i\omega t})
    \end{equation}
    where we let $\omega = kv$. The solution depends on our boundary conditions. For example, if we had it be 0 at two ends of a finite interval, we would get an infinite number of discrete sinusoidal waves. If we want it to be a traveling wave, that means the total magnitude must not decay - implying it is not square integrable. I believe the method for solving this on infinite intervals can involve Fourier transforms. Without boundary conditions, we can assume $k$ is a continuous parameter, and there is nothing to restrict our solution from being an infinite sum of these terms (proof necessary, but can be supplied with a method analogous to deriving Fourier Transforms):

    \begin{equation}
      f(z, t) = \int_{-\infty}^\infty \left[A(k) e^{i(kz + \omega t)} + B(k) e^{i(kz - \omega t)}     \right] dk
    \end{equation}
    where the terms were multiplied out, and noting $k = -k$ for each $k$, which allows us to combine terms. Finally, to get to Eq. 9.20 in Griffiths, we note that you can take the real part of both sides to get that solution. 
    
  \end{solution}
\end{exercise}

\begin{exercise}[Griffiths 9.6]
  \begin{enumerate}[label=(\alph*)]
  \item Formulate an appropriate boundary condition, to replace Eq. 27 ( $\pdv{f}{z}\Big|_{0^-} = \pdv{f}{z}\Big|_{0^+}$) for the case of two strings under tension $T$ joined by a knot of finite mass $m$.
    \begin{solution}
      Recall that the sum of forces $\sum F = ma$. In the textbook example, $m = 0$, and so $a = \pdv[2]{f}{t}$ did not have to be considered. So now we have
      \begin{align}
        T\sin(\theta_+) - T\sin(\theta_-) = & ma \nonumber \\
        \implies T\left(\pdv{f}{z}\Big|_{0^+} - \pdv{f}{z}\Big|_{0^-} \right) = & m \pdv[2]{f}{t}\Big|_{0}
      \end{align}
      as the new boundary condition. 
    \end{solution}
  \item Find the amplitude and phase of the reflected and transmitted waves for the case where the knot has mass $m$ and the second string is \textbf{massless}.
    \begin{solution}
      We know that at $z = 0$, the two sides of the interaction/barrier must be equal. Since these are exponentials, $e^0 = 1$, and we get
      \begin{align}
        \Re{A_I} + \Re{A_R} = & \Re{A_T} \\
        T(ik_2 \Re{A_T} - ik_1 (\Re{A_I} - \Re{A_R})) = & m(-\omega^2 \Re{A_T}) \tag{B.C. from first part; now factor out $k_1$} \nonumber \\
        k_1 T(i\frac{k_2}{k_1} - i(\Re{A_I} - \Re{A_R})) = & -\omega^2 m \Re{A_T} \tag{$v_2 = \sqrt{T/\mu_2} = \infty \implies k_2/k_1 = 0$} \nonumber  \\
        ik_1 T (\Re{A_R} - \Re{A_I}) = & -\omega^2 m \Re{A_T}. 
      \end{align}
      We now have two equations with three unknowns. 
    \end{solution}

  \end{enumerate}
\end{exercise}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
