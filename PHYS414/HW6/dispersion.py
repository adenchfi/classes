import matplotlib.pyplot as plt
from scipy.constants import *
from numpy import arange
from numpy import sqrt

w_p = 5e8 # arbitrary frequency; doesn't change shape of graph

def mode(k):
    return sqrt((k*c)**2 + w_p**2)
    
k = arange(0, 5, 0.05)
w = [mode(k_n) for k_n in k]

plt.plot(k, w, label="$\omega(k)$")
plt.xticks(arange(0, 5, 0.5), fontsize = 22)
plt.yticks(fontsize=22)
plt.ylabel("Mode", fontsize=22)
plt.title("Dispersion Relation for a Plasma", fontsize=24)
plt.xlabel("Wavenumber k", fontsize=22)
plt.axhline(w_p, linestyle='dashed', label="$\omega_p$")
plt.legend(loc='upper left', fontsize=24)
plt.show()
