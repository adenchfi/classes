import matplotlib.pyplot as plt
from scipy.constants import *
from numpy import arange
from numpy import sqrt


def skin_depth(w, eps, mew, sig):
    kap = w*sqrt(eps*mew/2) * sqrt(sqrt(1 + (sig/(eps*w))**2) - 1)
    return (1/kap)

sig_Cu = 57e6
mew_Cu = mu_0

sig_Ir = 1e7
mew_Ir = 1000*mu_0

sig_Sea = 4
mew_Sea = mu_0

power_line_freq = 60 # Hertz
pow_line_w = power_line_freq * 2*pi

high_freq = 1e9 # Hertz
high_w = high_freq * 2*pi

Cu_pow = skin_depth(pow_line_w, epsilon_0, mew_Cu, sig_Cu)
Cu_high = skin_depth(high_w, epsilon_0, mew_Cu, sig_Cu)

Ir_pow = skin_depth(pow_line_w, epsilon_0, mew_Ir, sig_Ir)
Ir_high = skin_depth(high_w, epsilon_0, mew_Ir, sig_Ir)

Sea_pow = skin_depth(pow_line_w, epsilon_0, mew_Sea, sig_Sea)
Sea_high = skin_depth(high_w, epsilon_0, mew_Sea, sig_Sea)

print("Cu Power Line Skin Depth \t\t Cu High-Freq Skin Depth")
print(Cu_pow,"\t\t\t", Cu_high)
print("Iron Power Line Skin Depth \t\t Ir High-Freq Skin Depth")
print(Ir_pow, "\t\t\t", Ir_high)
print("Sea Water Power Line Freq. Skin Depth \t High_Freq Skin Depth")
print(Sea_pow, "\t\t\t\t", Sea_high)
