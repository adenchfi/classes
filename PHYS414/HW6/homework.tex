\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations 
}
\input{math_pkgs.tex}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\usepackage{gensymb}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{6} % workaround to make problem numbers start with 5 for HW 5
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#6}
\author{Adam Denchfield \and Instructor: Dr. Linda Spentzouris}
\date{\today{}}
\maketitle

\begin{exercise}[Skin Depths]

  Determine the skin depth for copper , iron , and seawater at 60 Hz (power-line frequency) and 1 GHz. The
  conductivity of iron is only about six times less than that of copper, and is much cheaper than
  copper. Why not use iron wires to carry electric power to our homes?
  
  \begin{solution}
          Recall the definition of skin depth,
      \begin{equation}
        d \equiv \frac{1}{\kappa}, \\
        \kappa = \omega \sqrt{\frac{\epsilon \mu}{2}} \left[\sqrt{1 + (\frac{\sigma}{\epsilon \omega})^2} - 1 \right]^{1/2}.
      \end{equation}
      With this, I can put these formulae into a computer program - like python! I attached the code in an appendix below. Here is the output it produces. 

\begin{verbatim}
adenchfi@Denchfield:~/classes/PHYS414/HW6$ python3 skin_depth.py
Cu Power Line Skin Depth                 Cu High-Freq Skin Depth
0.00860611368278                         2.10805872015e-06
Iron Power Line Skin Depth               Ir High-Freq Skin Depth
0.000649747334361                        1.59154943535e-07
Sea Water Power Line Freq. Skin Depth    High_Freq Skin Depth
32.4873667316                            0.00801327689318
\end{verbatim}
      Recall that the skin depth is how far waves can penetrate the material before losing $1/e$ of its intensity. Considering that power lines operate, among other things, by constant internal reflection, we want our material carrying our power to not be absorbing a lot of the current in the process. Copper absorbs a lot less (and therefore reflects more), whereas iron will absorb our power in less than ten times the distance. 
      
    \end{solution}
  
\end{exercise}

\begin{exercise}[Griffiths 9.24]
  If you take the model in the book Ex. 4.1 at face value, what natural frequency do you get? Put in the actual numbers. Assuming the radius of the atom is 0.5\AA, where does this lie in the electromagnetic spectrum? Find the coefficients of refraction and dispersion, and compare them with the measured values of hydrogen at 0\degree C and atmospheric pressure: $A = 1.36\cross 10^{-4}$, $B = 7.7\cross 10^{-15}m^2$.
  \begin{solution}
    Recall that example 4.1 is about an atomic model such that we have a point nucleus surrounded by a uniformly charged spherical cloud of radius $a$. If we use the electric field of this system to get an expression for the force, equate this to the harmonic oscillator as an approximation, we can extract the natural frequency defined in that approximation.
    In it, the electric field discussed is \[
E(d) = \frac{1}{4\pi\epsilon_0} \frac{qd}{a^3}
\]
where $a$ is the atomic radius, $q$ is the charge, and $d$ is the distance from the center. This implies that
\begin{align*}
  F(x)= -qE = & -\frac{1}{4\pi\epsilon_0}\frac{q^2}{a^3} x = -k_{spring}x \\
  = & -m\omega_0^2 x \tag{Eq. 9.151} \\
  \implies \omega_0 = &\sqrt{\frac{q^2}{4\pi\epsilon_0 ma^3}}
\end{align*}
Since we want a regular frequency, we divide by $2\pi$, and plug in numbers. $q = 1.602 \cross 10^{-19}$, $\epsilon_0 = 8.85 \cross 10^{-12}$, $m = 9.11 \cross 10^{-31}$, and $a = 0.5 \times 10^{-10}$  in SI units. Plugging in values, we get \[
\nu_0 = w_0/2\pi = 7.16 \times 10^{15} Hz 
\] which is in the ultraviolet part of the spectrum. 
  \end{solution}
\end{exercise}

\begin{exercise}[Dispersion Problem]
  The wave number  in a plasma is given by $k = \frac{1}{c} \sqrt{\omega^2 - \omega_p^2}$.
  \begin{enumerate}[label=(\alph*)]
  \item Plot the dispersion relation for this plasma.
    \begin{solution}
      Solving for $\omega(k)$ yields
      \begin{equation}
        \omega(k) = \sqrt{\omega_p^2 + (ck)^2}
      \end{equation} and is plotted in Figure \ref{fig:disp} using Python. 
      \begin{figure}[h]
        \centering
        \includegraphics[width=\linewidth]{plasma_dispersion.png}
        \caption{The code necessary to produce this graph was included in another appendix below. I'm surprised the dispersion relation for a plasma is this simple!}
        \label{fig:disp}
      \end{figure}
      
    \end{solution}
  \item Using the relation given, write an expression for the group velocity.
    \begin{solution}
      All we need to do is take a derivative of $\omega$ with respect to $k$ in order to get the group velocity.
      \begin{align}
        v_g = \pdv{\omega}{k} = & \pdv{}{k} \sqrt{\omega_p^2 + (ck)^2} \nonumber \\
        = & \frac{1}{2} \frac{1}{\sqrt{\omega_p^2 + (ck)^2}} 2 c^2 k \\
        = & \frac{kc^2}{\sqrt{\omega_p^2 + (ck)^2}}
      \end{align}

    \end{solution}

  \end{enumerate}

\end{exercise}

\begin{exercise}[Waveguide]
  The longitudinal electric field $E_z$ is zero for transverse-electric (TE) modes in an evacuated waveguide. The longitudinal magnetic field has the form $B_z = B(x,y) e^{i(kz - \omega t)}$. The wave equation can be solved using the separation of variables method, assuming a solution of the form $B_z(x, y, z) = X(x)Y(y)e^{i(kz-\omega t)}$.
  \begin{enumerate}[label=(\alph*)]
  \item Substitute $B_z$ given above into the wave equation
    \begin{equation}
      \label{eq:wave}
      \left[\pdv[2]{}{x} + \pdv[2]{}{y} + \pdv[2]{}{z} - \frac{1}{c^2}\pdv[2]{}{t} \right] B_z = 0
    \end{equation}
    and perform the initial steps of separation of variables.
    \begin{solution}
      The problem is self-explanatory. This is doing the first steps of the method of separation of variables, with us assuming the form of the $z$ dependence. When we substitute the expression above, we get, after dividing out the $e^{i(kz - \omega t)}$ terms still present,
      \begin{align*}
        X''(x)Y(y) + X(x) Y''(y) - k^2 X(x)Y(y) + \frac{\omega^2}{c^2}X(x)Y(y) = & 0 \tag{Now divide by $X(x)Y(y)$}\\
        \frac{X''(x)}{X(x)} + \frac{Y''(y)}{Y(y)} - k^2 + \frac{\omega^2}{c^2} = & 0
      \end{align*}
      Looking at the last line, we note that the first two terms must individually equal constants in order for this equation to be true for all $x, y$. This allows us to write
      \begin{align*}
        \frac{X''(x)}{X(x)} = -k_x^2 \\
        \frac{Y''(y)}{Y(y)} = -k_y^2 
      \end{align*}
      and now we have a system of linear ordinary differential equations. 
    \end{solution}
  \item Write the general solutions for $X(x), Y(y)$ for the above equations.
    \begin{solution}
      If we assume that $k_x, k_y$ are real, then these ODEs have periodic solutions which can be written as either complex exponentials or sines/cosines. I will go with the former for now.
      \begin{align}
        X(x) = Ae^{ik_x x} + Be^{-ik_x x} \\
        Y(y) = Ce^{ik_y y} + De^{-ik_y y} \\
      \end{align}
    \end{solution}    
  \item The domain has a cross section with width $\Delta x = a$ and height $\Delta y = b$. The left and bottom are defined by $x = 0, y = 0$, respectively. According to Maxwell's equations, we have further relations between the components of the magnetic field:
    \begin{align*}
      B_x = \frac{ik}{(\frac{\omega}{c})^2 - k^2} \pdv{B_z}{x} \\
      B_y = \frac{ik}{(\frac{\omega}{c})^2 - k^2} \pdv{B_z}{y}
    \end{align*}
    The boundary conditions are such that the perpendicular components of $B$ must be zero at the waveguide walls. This means we have
    \begin{align*}
      B_y(x, 0) = 0 \quad B_y(x, b) = 0 \\
      B_x(0, y) = 0 \quad B_x(a, y) = 0
    \end{align*}
    Use these conditions to find allowed values for $k_x, k_y$.
    \begin{solution}
      We must use the Maxwell-Equation relation to find $B_x, B_y$ and then apply the boundary conditions to $B_x, B_y$ to obtain our restriction for $k_x, k_y$. Further, we'll simplify the notation where we can. 
      \begin{align}
        B_x = \frac{ik}{(\frac{\omega}{c})^2 - k^2} (Aik_x - Bik_x) B_z \\
        B_y = \frac{ik}{(\frac{\omega}{c})^2 - k^2} (Cik_y - Dik_y) B_z \\ 
      \end{align}
      Now we apply the boundary conditions, re-expressing the exponentials as sums of sines and cosines.
      \begin{align*}
        B_x(x, 0) = 0 \implies & \frac{-k k_x}{(\omega / c)^2 - k^2} (\ov A \cos{k_x x} + \ov B \sin{k_x x})(\ov C \cos(k_y y) + \ov D \sin(0)) e^{ik z} e^{-i \omega t} = 0  \tag{Therefore }\\
        \implies & \ov C = 0 \\
        B_y(0, y) = 0 \implies & \frac{-k k_y}{(\frac{\omega}{c})^2 - k^2} (\ov A \cos (0)) (\ov D \sin(y)) e^{ikz} e^{-i \omega t} = 0 \\
        \implies & \ov A = 0 \\
        B_x(x, b) = 0 \implies &  \frac{-k k_x}{(\omega / c)^2 - k^2} B\sin{k_x x} D\sin{k_y b} e^{ikz} e^{-i \omega t} = 0 \\
        \implies & k_y b = n\pi \implies k_y = \frac{n_y\pi}{b} \\
        B_y(a, y) = 0 \implies & \frac{-k k_x}{(\omega / c)^2 - k^2} B\sin(k_x a) D\sin{\frac{n\pi}{b}y} e^{ikz} e^{-i \omega t} = 0 \\
        \implies & k_x a = n\pi \implies k_x = \frac{n_x\pi}{a}
      \end{align*}
      which gives us our desired expressions for $k_x, k_y$. 
    \end{solution}
  \item Show that \[
      k = \sqrt{(\frac{\omega}{c})^2 - k_x^2 - k_y^2}.
    \]
    This is the dispersion relation for the waveguide.
    \begin{solution}
      All we need to do is plug our solution from part (b) into the wave equation, notice that we can divide all the differentiated terms by our original solution, and we are left with an equation relating the constants.
      \begin{align*}
        -k_x^2 - k_y^2 - k^2 + (\frac{\omega}{c})^2 = 0 \\
        \implies k = \sqrt{(\frac{\omega}{c})^2 - k_x^2 - k_y^2}
      \end{align*}
    \end{solution}    
  \item If the dimensions of the waveguide are $a = 3cm$ and $b = 1cm$, find the range of drive frequencies for which only one transverse-electric (TE) mode will propagate down the guide.
    \begin{solution}
      This seems like I should just be able to plug the numbers in for $k_x, k_y$ and the answer should just pop out. In particular, if we want just \textbf{one} mode, and we know $k$ is continuous (dependent on waveguide dimensions), we can find the minimum $n_x, n_y$. In particular, $n_x = 1, n_y = 0$ gets the minimum. We solve for the drive frequency and get
      \begin{equation}
        \omega^2 = k^2 c^2 + k_x^2 + k_y^2 \tag{Recall $\omega_{nm} = k_x^2 + k_y^2$ is the cutoff frequency} \\
        \omega^2_{one} = k^2 c^2 + \frac{\pi^2}{9}
      \end{equation}
      Our expression for $\omega^2_{one}$ is the expression for how high the drive frequency (squared) can be while only stimulating one TE mode. In particular, if $k \rightarrow 0$ then it becomes exactly $\omega_{nm}^2$, which is possible, because $k$ is continuous down to 0. 
    \end{solution}

  \end{enumerate}

\end{exercise}


\newpage
\appendix

\section{Skin Depth Code}

\lstinputlisting[language=Python]{skin_depth.py}


\section{Dispersion Relation Code}
\lstinputlisting[language=Python]{dispersion.py}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
