\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\input{math_pkgs.tex}

\newcommand{\F}{\mathcal{F}}
%\newenvironment{problem}[2][Problem]{\begin{trivlist}
%  \item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
%If you want to title your bold things something different just make another thing exactly like this but replace ``problem'' with the name of the thing you want, like theorem or lemma or whatever
\setcounter{section}{2} % workaround to make problem numbers start with 2 for HW 2
\begin{document}

%\renewcommand{\qedsymbol}{\filledbox}
%Good resources for looking up how to do stuff:
%Binary operators: http://www.access2science.com/latex/Binary.html
%General help: http://en.wikibooks.org/wiki/LaTeX/Mathematics
%Or just google stuff

\title{Homework Assignment \#1}
\author{Adam Denchfield \and Class: MATH 481}
\date{\today{}}
\maketitle

\begin{exercise}
  Let $(\Omega, \mathcal{F}, \BB P)$ be a probability space. Prove the following properties of $\BB P$:
  \begin{enumerate}[label=(\alph*)]
  \item For any $A \in \F, \P(A) \leq 1$
    \begin{proof}
      Recall the properties of the probability $\P$ on $(\Omega, \F)$:
      \begin{enumerate}
      \item $P(\Omega) = 1$
      \item (Countable additivity) If $\{A_n\}$ is a sequence of pairwise disjoint events in $\F$, then
        \begin{equation*}
          \P\left(\bigcup_{n} A_n\right) = \sum_n \P(A_n).
        \end{equation*}
      \end{enumerate}
      We can then write $\Omega$ as the union of all elements of a partition of $\Omega$;
      \begin{equation*}
        \Omega = \{\bigcup_n A_n | \{A_n\} \text{ forms a partition of $\Omega$}\}.
      \end{equation*}
      We can then write
      \begin{align*}
        \P(\Omega) = & \P(\bigcup_n A_n) \\
        = & \sum_n \P(A_n) = 1 \tag{Due to defn. of $\P(\Omega)$}
      \end{align*}
      It is then trivial to see that any event $A \in \F$ which is a subset of $\Omega$ has $\P(A) \leq 1$. In particular,
      \begin{align*}
        \P(A_j) & = \P(\Omega \setminus A_{i \neq j}) = \sum_{n = j} \P(A_n) \\
                & = 1 - \sum_{n \neq j} \P(A_{n}) \tag{Due to the properties of sums and $\P(\Omega) = 1$}\\
                & \leq 1.
      \end{align*}
    \end{proof}
  \item $\P(\emptyset) = 0$
    \begin{proof}
      Given the above formulation, we can write $\emptyset = \Omega \setminus \Omega$ and the result immediately follows, noting $A_j = \Omega = \bigcup_n A_n$. 
    \end{proof}
  \item For any $A \in \F, \P(A^c) = 1 - \P(A)$.
    \begin{proof}
      Again with the formulation for part (a), the result is trivial. We can write $\P(A^c) = \P(\Omega \setminus A) $. Suppose that we denote $\Omega \setminus A$ as $\{\tilde{A}_k\}$. We then have
      \begin{align*}
        \P(\bigcup_n \tilde{A}_n) & = \sum_n \P(\tilde{A}_n) \\
                                  & = 1 - \sum_{A_n} A_n \tag{Since $A_n \in A$ and $(A^c)^c = A$} \\
                                  & = 1 - \P(A) \tag{Countable additivity}
      \end{align*}
    \end{proof}
  \item For any $A, B \in \F$, $\P(A \cup B) = \P(A) + \P(B) - \P(A \cap B)$.
    \begin{proof}
      Recall the property of countable additivity. If each of $A$ and $B$ are disjoint with each other, we could normally write $\P(A \cup B) = \P(A) + \P(B)$. However, they are not necessarily disjoint. A geometric consideration can help us. If $A$ and $B$ share part of our domain $\Omega$, then writing $\P(A \cup B) = \P(A) + \P(B)$ maps the part of the domain $A$ and $B$ share \textbf{twice}. We can exactly counter this double-counting by simply removing one instance of the overlap, yielding $\P(A \cup B) = \P(A) + \P(B) - \P(A \cap B)$. 
    \end{proof}
  \item For any $A, B \in \F $ with $A \subseteq B$, $\P(A) \leq \P(B)$.
    \begin{proof}
      We can use the result above. If $A \subseteq B$, then we can write $B = A \cup B$. Therefore we have $\P(A \cup B) = \P(A) + \P(B) - \P(A \cap B) = \P(B)$ since $\P(A) = \P(A \cap B)$ by definition. Since $\P(A \cup B) \geq \P(B)$ for general $A, B$, we can infer that $\P(A) \geq \P(A \cap B)$ for general $A, B$. We note that these inequalities are equalities in this case, and since $\P(A \cup B) \geq \P(A \cap B)$, we easily see that $\P(A) \leq \P(B)$.
    \end{proof}
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Show that if $A_1, A_2, \dotsc$ is an \textit{expanding} sequence of events, then
  \begin{equation*}
    P(A_1 \cup A_2 \dotsc) =\lim_{n\rightarrow \infty} P(A_n). 
  \end{equation*}
  Furthermore, if $A_1, \dotsc,$ is a \textit{contracting} sequence of events, then \[
    P(A_1 \cap A_2 \dotsc ) = \lim_{n \rightarrow \infty} P(A_n). 
  \]
  \begin{solution}
    Recall that if $A_1 \subset A_2$, then $A_1 \cup A_2 = A_2$. Applying this strategy $n$ times for an expanding sequence of events, we find $A_1 \cup A_2 \dotsc A_n \cup \dotsc = A_n \cup \dotsc$. This means eventually it resolves as $\lim_{n\rightarrow \infty} A_n$. Taking the probability of this yields our result.

    For the contracting sequence of events $\{A_k\}$, we can use similar arguments to find the proof of the statement above. 
  \end{solution}
\end{exercise}

\begin{exercise}
  Consider a probability space $\Omega = \{a, b, c, d\}$. The $\sigma$-algebra $\F$ is the collection of all subsets of $\Omega$. Define a random variable $X$ such that $X(a) = 1, X(b) = 1, X(c) = -1, X(d) = 0$. List all sets in $\sigma(X)$.
  \begin{solution}
    Note that the possible values of the random variable are $-1, 0, 1$. We want to consider all combinations of inputs such that we get those values. \[
      \sigma(X) = \{\emptyset, \{a\}, \{b\}, \{c\}, \{d\}, \{a, d\}, \{b, d\}, \{b, c\}, \{a, b, c, d\}\}      
\]
  \end{solution}
\end{exercise}

\begin{exercise}
  Let $\Omega = \{1, 2, 3, 4, 5, 6\}$. Check if the following collections $\F, \mathcal{G}$ are $\sigma$-algebras: (redacted since it's reproduced online).
  \begin{solution}
    $\F$ is a $\sigma-$algebra. It meets all the properties of one. It has the empty set, $\Omega$, and for every $A$ in it there also exists $A^c$. All possible unions of the $A_i$ are also contained in $\F$. $\mathcal{G}$, however, is not a $\sigma$-algebra. It fails on the second property; the complement to $\{1, 2, 3, 4\}$ does not exist. 
  \end{solution}
\end{exercise}

\begin{exercise}
  Show that the distribution function $F_\xi$ is non-decreasing, right-continuous, and
  \begin{align*}
    \lim_{x\rightarrow -\infty} F_\xi (x) = 0 \\
    \lim_{x \rightarrow +\infty} F_\xi (x) = 1
  \end{align*}
  \begin{solution}
    Consider the definition of the distribution function, $F_\xi (x) = \P(\xi \leq x)$. We can consider the $\sigma$-field generated by $\xi \leq x, \sigma(\xi \leq x)$. The number of available subsets of that $\sigma$-field decreases as we take $\xi \rightarrow -\infty$. In particular, if we write any interval $\xi - \epsilon \leq x$ as an intersection of contracting subintervals $A_{-1}, A_{-2}, \dotsc$ as $\epsilon \rightarrow \infty$ (or, equivalently, $x\rightarrow -\infty $), then results from a previous problem show it resolves to $\lim_{n \rightarrow -\infty} \P(A_n)$, which converges to $0$ because $A_n$ in this case converges to the empty set.

    Similarly, we can write $\xi \leq x$ as a series of expanding sets/intervals as $x \rightarrow \infty$. The sets expand to $\Omega$, and $\P(\Omega) = 1$. The result has been achieved. 
  \end{solution}
\end{exercise}

\begin{exercise}
  Show that if $\xi$ has discrete distribution with values $x_1, x_2, \dotsc, $ then $F_\xi$ is constant on each interval $(s, t]$ not containing any of the $x_i$'s and has jumps of size $\P\{\xi = x_i\}$ at each $x_i$.
  \begin{solution}
    This seems obvious from the definition of $F_\xi$ and $\P$. $F_\xi (x) = \P(\xi \leq x)$, so let's make an interval $(s, t]$. If $x_i \not \in (s, t]$ for any $i$, then we immediately get from the definition of $F_\xi$ that \[
F_\xi (t) = F\xi (s) = P\{\xi \leq t\} - P\{\xi \leq s\} = \P\{\xi \in (s, t]\} = 0
\], meaning the distribution function $F_\xi$ at both points are the same. Since it's non-decreasing, it must be constant on that interval. It's rather easy to show that there are jumps of size $\P\{\xi = x_i\}$ at each $x_i$; the left-side and right-side limits on each $x_i$ differ by the value $\P\{\xi = x_i\}.
$
\end{solution}
\end{exercise}

\begin{exercise}
  Let $f$ be the function \[
f(x) = cx(2-x), \quad x \in (0, 2). 
\]
\begin{enumerate}[label=(\alph*)]
\item Find $c$ such that $f$ is a probability density function.
  \begin{solution}
    This means $\int_0^2 cx(2-x) dx = cx^2 - cx^3/3 \Big|_{0}^2 = 1 \implies c = 3/4$. 
  \end{solution}
\item If $X$ is a random variable with the above density function, compute $\P(0.5 < X < 1.5)$.
  \begin{solution}
    Using the definition of probability density function, \[
\int_{0.5}^{1.5} \frac{3}{4} x(2-x) dx = 0.6875.
\]
  \end{solution}
\item Find the cumulative distribution function for $f$.
  \begin{solution}
    This is just the indefinite integral of $f$. $\int \frac{3}{4}x(2-x) = (3/4)(x^2 - x^3/3)$
  \end{solution}
\item Find $\BB E(X)$.
  \begin{solution}
    \begin{align*}
      \BB E(X) = & \int_0^2 x \frac{3}{4}x(2-x) \\
      = & \frac{x^3}{2} - \frac{3x^4}{16} \Big|_{0}^2 \\
      = & 4 - 3 = 1.0.
    \end{align*}
  \end{solution}
\end{enumerate}
\end{exercise}

\begin{exercise}[Textbook 1.12]
  
  \begin{solution}
    Recall that the $\sigma$-fields $\sigma(\xi), \sigma(\eta)$ are composed of all possible subsets and complements of events of the form $\{\xi \in A\}, \{\eta \in B\}$, where $A, B$ are Borel sets in $\BB R$. From our definition of independence of $\sigma$-fields, the two $\sigma$-fields are independent only if all events $\{\xi \in A\}$ and $\{\eta \in B\}$ are independent for any Borel sets $A, B$.

    If you consider that this is true for all $\xi, \eta$ satisfying the above conditions, then it is only natural to say that $\xi \indep \eta$. 
\end{solution}

\end{exercise}

\begin{exercise}
  \begin{enumerate}[label=(\alph*)]
  \item Find the marginal density functions $f_X, f_Y$ for $X, Y$ respectively.
    \begin{solution}
      A marginal density function $f_Y$ would be $\int_{x_1}^{x_2} f(x, y) dx$. In this case we have\[
        f_Y = \int_{y^2}^1 \frac{3}{2} dx = \frac{3}{2} - \frac{3y^2}{2} = \frac{3}{2}(1-y^2). 
      \]
      We can find $f_X$ the same way;
      \[
        f_X = \int_0^1 \frac{3}{2} dy = \frac{3}{2}. 
      \]
    \end{solution}
  \item Are $X, Y$ independent? No. We cannot write $f(x, y)$  as $f_X(x) f_Y(y)$ for all $x, y$. 
  \item Since $f_Y$ does not contain $x$ terms, $f_{Y|X} (y|x) = \frac{3}{2}(1-y^2).$
  \item Find the conditional expectation $\BB E(Y|X = 1)$.
    \begin{solution}
      \begin{align*}
        \BB E(Y|X=1) = & \int_0^1 y\frac{3}{2}(1-y^2)\\
        = & \frac{3}{4}y^2 - \frac{3}{8}y^4 \Big|_{0}^1 \\
        = & \frac{3}{8}. 
      \end{align*}
    \end{solution}
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Let $X$ be a random variable with the cumulative probability distribution function \[
F(x) =
\begin{cases}
  0, &  x < 0 \\
  1/3, & x \in [0, 2) \\
  3/4, & x \in [2, 4) \\
  1, & x > 4
\end{cases}
\].
\begin{enumerate}[label=(\alph*)]
\item Find the moment generating function of $X$.
  \begin{solution}
    The easiest way I see of doing this is to turn the CDF into a PMF. In that case, we have \[
p(x) =
\begin{cases}
  1/3, & x = 0 \\
  5/12, & x = 2 \\
  1/4, & x = 4 \\
  0, & \text{elsewhere}
\end{cases}
\]
Then the moment-generating function is
\begin{align}
  M_X(t) = & \sum_{i=1}^{i=\infty} e^{tx_i} p(x_i) \\
  = & e^{0} \cross \frac{1}{3} + e^{2t} \times \frac{5}{12} + e^{4t} \times 1/4 \nonumber \\
  = & \frac{1}{3} + \frac{3}{4}e^{2t} + e^{4t}.
\end{align}
\end{solution}
\item Calculate the mean and variance of $X$. 
  \begin{solution}
    Although we could compute the mean and variance the 'easy' way, I'll do it using moment-generating functions. $\BB E(X) = \mu_X = M_X'(0) = 0 + (2)\frac{5}{12} + 1 = 11/6.$ Furthermore, $var(X) = \BB E(X^2) - (\BB E(X))^2 = M_X''(0) - (11/6)^2 = 5/3 + 4 - (11/6)^2 = \frac{415}{180}.$
    \begin{remark}
      Getting a negative variance is wrong, so I assume I went wrong in my arithmetic somehow. 
    \end{remark}
  \end{solution}

\end{enumerate}
\end{exercise}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
