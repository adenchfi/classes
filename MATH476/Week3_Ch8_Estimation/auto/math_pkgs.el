(TeX-add-style-hook
 "math_pkgs"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "english") ("cleveref" "capitalize")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "amsmath"
    "graphicx"
    "amssymb"
    "physics"
    "inputenc"
    "babel"
    "amsthm"
    "cleveref"
    "enumitem"
    "listings"
    "caption")
   (TeX-add-symbols
    '("ov" 1)
    '("EV" 3)
    '("inner" 2)
    '("mean" 1)
    "FT"
    "Ang"
    "indep")
   (LaTeX-add-amsthm-newtheorems
    "theorem"
    "corollary"
    "lemma"
    "definition"
    "example"
    "exercise"
    "problem"
    "remark"
    "solution")
   (LaTeX-add-amsthm-newtheoremstyles
    "solution")
   (LaTeX-add-caption-DeclareCaptions
    '("\\DeclareCaptionFormat{mylst}" "Format" "mylst")))
 :latex)

