\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Error Estimation \\ Wackerly Ch. 8}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Mean-squared Error}

\begin{definition}
  $MSE(\hat{\theta}) = E[(\hat{\theta}-\theta)^2]$.
\end{definition}
\begin{theorem}
  $MSE(\hat{\theta}) = Var(\hat{\theta}) + Bias(\hat{\theta})^2$.
  \begin{proof}
    Let $\ov \theta = E[\hat{\theta}]$. Expand the quadratic, rewrite, add/subtract some stuff.
    \begin{align*}
      & E[(\hat{\theta} - \ov \theta + \ov \theta - \theta)^2] \\
      = & \underbrace{E[(\hat{\theta} - \ov \theta)^2]}_{Var} + \underbrace{2E[(\hat{\theta} - \ov \theta)]}_{= 0} + \underbrace{(\ov\theta  - \theta)^2}_{Bias^2} \\
    \end{align*}
  \end{proof}
  \begin{remark}
    Often, goal is to have small MSE, so we know we need a small variance and small bias$^2$. \textbf{However}, as you make one smaller, the other gets larger. This is known as ``bias-variance tradeoff''. 
  \end{remark}
\end{theorem}

\begin{definition}[Error of Estimation]
  Let $\varepsilon = \abs{\hat{\theta} - \theta} $ be the \textbf{error of estimation}.
\end{definition}

\begin{example}
  We sample $n = 1000$ voters. $y = 560$ are in favor of candidate Jose.
  \begin{enumerate}
  \item Estimate $p = \theta$, true fraction of voters in population who vote for Jose.
    \begin{solution}
      $\hat{p} = \frac{y}{n} = \frac{560}{1000} = 0.56$.
    \end{solution}
  \item Place a 2-standard-error bound on the $\varepsilon = \abs{\hat{p} - p}$. Meaning, what is $P(\varepsilon < 2\sigma_{\hat{p}}) < ?$
    \begin{solution}
      $Y$ is a binomial random variable. The distribution of $\hat{p}$ is well-approximated by the normal distribution since $n = 1000 >> 0$. $y/n \sim N(p, \sigma^2_{\hat{p}})$.
      \begin{align*}
%        P(\varepsilon < 2\sigma_{\hat{p}}) = P(2\sigma_\hat{p}) < \hat{p} - p < 2\sigma_{\hat{p}} \\
%        P(\varepsilon < 2\sigma_{\hat{p}}) = P(2\sigma_\hat{p} < \hat{p} - p < 2\sigma_{\hat{p}}) \approx 0.95 \tag{for normal dists} 
      \end{align*}
      Use the fact that $\sigma_{\hat{p}} = \sqrt{\frac{p(1-p)}{n}}$ to get:
      \[
2\sigma_{\hat{p}} \approx 0.031
\]
meaning $P(\varepsilon < 0.03) = 0.95$, i.e. reasonably confident that 0.56 is within 0.03 of the true population/probability. 
    \end{solution}
  \end{enumerate}
\end{example}

What about if you don't know the distribution of $\hat{p}$? Use Tchebysheff's Theorem. We often use $k = 2$, and then the probability of it appearing in 2 std deviations in 0.75.

Empirically, we often find that it's $\geq 0.95$ even for other distributions. 

$P(\varepsilon < b)$ = the fraction of times, in repeated sampling, that $\hat{\theta}$ falls within $b$ of $\theta$.

\subsection{Random Intervals}
\label{sec:8.5}

We may not always use \textbf{point estimators}. Instead, we may use \textbf{interval estimations}. In particular, \textbf{confidence intervals}.

Why? It's a convenient summary of variability of $\hat{\theta}$.

\begin{example}
  May want to know $\hat{\theta} and its variance.$
\end{example}

\begin{example}[Confidence Interval]
  May want to know the \textbf{confidence interval} for $\theta$, an interval which we believe to have large probability (under sampling distribution of $\hat{\theta}$) of containing the true $\theta$. 
\end{example}

\begin{definition}
  Let $X = (X_1, \dotsc, X_n)$ iid sample from distribution with PDF/PMF $f_\theta (x)$, with $\theta$ unknown.
  Let $\alpha \in (0, 1)$ be any (small) number (e.g. $\alpha = 0.05$). We call $\alpha$ the \textbf{significance level. }

  A $100(1 - \alpha)$\% confidence interval for $\theta$ is an interval such that \[
P_\theta(interval contains \theta) = 1-\alpha,
\]
for \textbf{any} $\theta$ in the parameter space $\theta \in \Theta$.
\begin{remark}[Book Notation]
  $p(\hat{\theta}_L \leq \theta \theta \hat{\theta}_u) = \underbrace{1 - \alpha}_{conf. coeff.}$ with L = lower significance limit, U = upper sig. limit.
  Confidence intervals can also be one-sided, e.g. \[
P(\theta \leq \hat{\theta}_u) = 1-\alpha
\]
\end{remark}
\end{definition}

\begin{example}[Computing Confidence Intervals]
  \textbf{The Pivotal Method}.
  \begin{enumerate}
  \item Find a pivotal quantity, a function of a sample, whose probability doesn't depend on $\theta$.
  \end{enumerate}
  Suppose $n = 1$, sampled from $Unif[0, \theta]$. Find a 95\% lower confidence interval bound for $\theta$.

  We know $Y \sim Unif[0, \theta]$. To get rid of the $\theta$-dependence, we define $U = \frac{Y}{\theta} \sim Unif(0, 1)$.

  So $f_u (u) \
  \begin{cases}
    1 & 0 \leq u \leq 1 \\
    0 & \text{otherwise}
  \end{cases}
  $
  $u$ is a function of the sample (and $\theta$), but $f_u$ doesn't depend on $\theta$. Therefore, $u$ is known as a \textbf{pivotal quantity}.

  Use $u$, and look for a number $a$ such that $P(u \leq a) = 0.95$.
  \begin{enumerate}[resume]
  \item Use $f_u$ to get probability/confidence interval; transform back to quantity with $\theta$. 
  \end{enumerate}
\end{example}

\begin{example}
  Single observation $Y$ from exp. distribution with mean $\theta$. Use $Y$ to find 90\% confidence interval (C.I.) for $\theta$. For $f_Y(y)$, we use $y$ as a placeholder for the values of $Y$. 
  \begin{equation*}
    f_Y (y) =
    \begin{cases}
      \frac{1}{\theta} e^{y/\theta} & y \geq 0 \\
      0 & \text{otherwise}
    \end{cases}
  \end{equation*}
Let $U = \frac{Y}{\theta}$, a function of $Y$ and $\theta$. If we identify the probability density function (PDF), when we integrate to make sure it's one, we have $du = \pdv{u}{y} \pdv{y}{\theta} d\theta$.  We get
\begin{equation}
  f_U(u) =
  \begin{cases}
    e^{-u}, & u \geq 0\\
    0, & \text{elsewhere.}
  \end{cases}
\end{equation}
Note that $f_U(u)$ doesn't depend on $\theta$, so it can be a pivot. Find $a, b$ such that $P(a \leq U \leq b) = 0.90$. You can compute the integrals and get $1 - e^{-a} = 0.05 \implies a = 0.051$ and we can get $b = 2.996$.

\begin{equation*}
  P(0.051 \leq U \leq 2.996) = 0.9
\end{equation*}

Since $P(Y > 0) = 1$ (or $Y$ is always positive), we can divide by it to get
\begin{equation*}
  0.9 = P(\frac{0.051}{Y} \leq \frac{1}{\theta} \leq \frac{2.996}{Y})
\end{equation*}
Flipping the fractions, we get
\begin{equation*}
  P(\frac{Y}{2.996} \leq \theta \leq \frac{Y}{0.051})
\end{equation*}

\begin{remark}
  The confidence interval contains the true $\theta$ with probability 0.90. 
\end{remark}
\end{example}

\subsection{Another Method: Large-Sample Confidence Intervals}

Recall table 8.1 with point estimators $\hat{\theta} $ for
\begin{equation*}
  \theta =
  \begin{cases}
    \mu \\p \\ \mu_1 - m_2 \\ p_2 - p_1
  \end{cases}
\iff \hat{\theta} =
\begin{cases}
  \ov x \\ x/n  \\ \ov x - \ov y \\ \hat{p}_1 - \hat p_2
\end{cases}
\end{equation*}
In these cases, $Z = \frac{\hat{\theta} - \theta}{\sigma_{\hat \theta}}$. This approaches the standard normal for $n >> 0$. Note that $\sigma_{\hat \theta} = \frac{\sigma_x}{\sqrt{n}}$.

Z is a function of sample (through $\hat{\theta}$) and a function of $\theta$. AND its $f_Z(z)$ doesn't depend on $\theta$. This means $Z$ is (approximately) a pivotal quantity.
\begin{example}
  Let $\hat{\theta} = $  statistic $\sim N(\theta, \sigma_{\hat \theta})$. Find $100(1-\alpha)$ \% confidence interval for $\theta$.

  Since $Z:0 \frac{\hat{\theta} - \theta}{\sigma_{\hat \theta}} \sim N(0, 1)$ and we just need to get the values for the tails.
  \begin{align}
    P(-z_{\alpha / 2} \leq z \leq z_{\alpha / 2}) = 1 - \alpha \\
    = P(... \leq \frac{\hat{\theta} - \theta}{\sigma_{\hat \theta}}) \nonumber \\
    = P(-z_{\alpha / 2} \sigma_{\hat \theta} - \hat \theta \leq -\theta \leq z_{\alpha / 2}) \sigma_{\hat \theta} - \hat \theta \nonumber \\
    = P(\hat \theta - z_{\alpha / 2} \sigma_{\hat \theta} \leq \theta \leq \hat \theta + z_{\alpha/2} \sigma_{\hat \theta}) 
  \end{align}
\end{example}

\begin{example}
  $n = 64$ randomly selected customers, record shopping times. It turns out the average shopping time is 33 minutes, with variance $256$ minutes squared.

  We would like to estimate $\mu = $ average shop time per customer for the whole population in consideration. We'd like to estimate this with confidence $1 - \alpha = 0.90$.

  In this example, $\theta = \mu$. We have $\hat{\theta} = \ov X = 33$ and $S^2 = 256$ ($S^2$ is our sample variance), $n = 64$. Conf. Interval for $\theta$ is $\hat{\theta} \pm z_{\alpha/2} \sigma_{\hat \theta}$, so just plug in the values - we have enough samples that we assume the Central Limit Theorem applies. So $C.I.$ is
  \[
\ov X \pm z_{\alpha/2} \frac{\sigma}{\sqrt{n}} \approx z_{\alpha /2} \frac{\Sigma}{\sqrt{n}}
\]
and thus we get $P(29.71 \leq \mu \leq 36.29) = 0.9$.
Conclusion: This interval contains the true $\mu$ with probability $0.9$. You could get a bunch of intervals with many different samplings,
\begin{align*}
  \ov X_1 = 33 \rightarrow (29.7, 36.2) \\
  \ov X_2 = 40 \rightarrow (3?, 4?) \\
  \vdots 
\end{align*}
and all the above intervals obtained with contain $\mu$ with probability 0.90. 
\end{example}

\begin{remark}
  If you don't have a sample mean $\hat{\theta}$ yet, you can ask an expert what they think the sample mean or probability will be. 
\end{remark}

\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
