\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Error Estimation \\ Wackerly Ch. 8}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Minimum Variance Estimation}

We want to focus on unbiased estimators and find the ones with the smallest variance (which leads to the smallest MSE). These estimators are MVUE (minimum variance unbiased estimator), sometimes called UMVUE (U for uniformly).
\begin{definition}
  An estimator $\hat \theta$ is the \textup{minimum variance unbiased estimator (MVUE)} if, for all $\theta \in \Theta$, \[
E_\theta (\hat \theta) = \theta
\] and \[
Var(\tilde \theta) \geq Var(\hat \theta)
\] for \textbf{any} other unbiased $\tilde \theta$. 
\end{definition}
\begin{remark}
  We said ``the'' estimator; we want to check uniqueness. Uniqueness proofs in math are great. Almost always assume they're not unique, show they must be equal.
  \begin{proof}
    Let $\hat \theta_1, \hat \theta_2$ be 2 minimum variance unbiased estimators (MVUE)s. Define a new estimator \[
\hat \theta_3 \equiv \frac{\hat \theta_1 + \hat \theta_2}{2}
\] which is also unbiased. What is the variance of $\hat \theta_3$?
\begin{align*}
  Var(\hat \theta_3) = \frac{1}{4} Var(\hat \theta_1) + \frac{1}{4}Var(\hat \theta_2) + 2 \frac{1}{4}Cov(\hat \theta_1, \hat \theta_2) \\
  = \frac{1}{2}V + \frac{1}{2}Cov(\hat \theta_1, \hat \theta_2) \\
  \geq \tag{Because $\hat \theta_{1, 2}$ are both MVUE} \\
  \implies Cov(\hat \theta_1, \hat \theta_2) \geq V
\end{align*}
Now consider
\begin{align*}
  V(\hat \theta_1 - \hat \theta_2) \\
  = V(\hat \theta_1) + V(\hat \theta_2) - 2Cov(\hat \theta_1, \hat \theta_2) \\
  \leq 2v - 2v = 0 \\
  \implies \hat \theta_1 - \hat \theta_2 = C \tag{Constant!}
\end{align*}
 And if their expectations are the same, they must be the same. The expectations are the same because if they weren't, they wouldn't be unbiased estimators. 
  \end{proof}
\end{remark}
\begin{remark}
  What IS the minimum variance?

  Look up the Cramer-Rao lower bound on variance. 
\end{remark}
\begin{problem}
  How do we find/identify these MVUEs?
  \begin{solution}
    Suppose $T$ is a sufficient statistic that ``best'' summarizes your data, and some function $h(T)$ can be found such that $E[h(T)] = \theta$, then $h(T)$ \textbf{is} the minimum variance unbiased estimator MVUE of $\theta$. 
  \end{solution}
\end{problem}
\begin{problem}[Problem With Above Approach]
  This ``best'' T can be complicated. We also have no clue how to find this function $h(T)$ in general. We need new methods for finding estimators. 
\end{problem}
We will look at the method of moments, and maximum likelihood estimators (MLEs).
\begin{example}
  Let $Y_1, \dotsc, Y_n$ be a random sample from Weibull distribution. We have \[
f_\theta (y) = f(y | \theta) =
\begin{cases}
  2y/\theta e^{-y^2 / \theta} & y > 0 \\
  0 & y \leq 0
\end{cases}
\]
We want to find MVUE for $\theta$. Look at the likelihood function of $Y_1, \dotsc, Y_n$:
\begin{align*}
  L(y_1, \dotsc, y_n | \theta)  = f(y_1, \dotsc, y_n | \theta) \\
  = \frac{2^n}{\theta^n} y_1 \dotsc y_n e^{\frac{-1}{\theta}\sum_{i=1}^n y_i^2} \tag{Identify our $g, h$} \\
  = \underbrace{\frac{2^n}{\theta^n}e^{-\frac{1}{\theta}\sum y_i^2}}_{g(y_i, \theta)} \cdot \underbrace{y_1 \cdots y_n}_{h(y_1, \dotsc, y_n)}
\end{align*}
Our sufficient statistic for $g$ is the sum of our $y_i$'s $\sum_{i=1}^{i=n} y_i^2 = T$.
Now we need a function of $\sum_{i=1}^{i=n}y_i^2$ that is unbiased for $\theta$.

Let $W = Y_i^2$ - working with just one of the random variables. We have the density, using the chain rule, \[
f_w(w) = f(\sqrt{w}) \dv{\sqrt{w}}{w}
\] and we know those values, so \[
f_w(w) = \frac{2}{\theta}\sqrt{w} e^{-w/\theta}\frac{1}{2\sqrt{w}} = \frac{1}{\theta}e^{-w/\theta}
\] which is the exponential distribution, with $w > 0$. Therefore, \[
Y_i^2 \sim Exp(\theta)
\] and then $E[Y_i^2] - E[w] = \theta$, so $E[\sum_{i=1}^{i=n} Y_i^2] = n\theta$ and so our unbiased estimator is \[
  \hat j = \frac{1}{n} \sum_{i=1}^{i=n} Y_i^2
\] and then $\hat \theta = $ is the MVUE of $\theta$. 
\end{example}

\section{Methods of Estimation}

Moments, MLEs, min-max, least squares, Bayes, and more. We will focus on the first two. 

\subsection{Method of Moments}

This is the simplest method, and driven by unbiasedness.

Idea: Start with \textbf{any} statistic $T$. Calculate $E[T] \equiv h(\theta)$. Set $T = h(\theta)$ and use the solution as $\hat \theta$.
\begin{example}
  Suppose we have $X_1, \dotsc, X_n \sim_{iid} N(\theta, ?)$. Our goal is to estimate $\theta^2$. Since we often use $\ov X$ to estimate $\theta$, it's reasonable to use $T = \ov X^2$. Compute $E[T] = \dotsc = \frac{\theta^2 + 1}{n}$ after some algebra (expand out $\ov X$).

  Now set \[
\ov X^2 = \frac{\theta^2 + 1}{n}
\] and solve for $\theta^2$:
\[
\theta^2 = n\ov X^2 - 1
\]
\end{example}

\section{General Method of Moments}

Suppose we wish to estimate $s$ parameters. Choose as estimators those values that solve the following $s$ equations: \[
\mu_k' = m_k' \quad \forall k = 1, \dotsc, s
\] where $m_k'$ is the $k$-th sample moment and $\mu_k'$ is the $k$-th population (true) moment, which is also equal to $E[Y^k]$. We can write this then as \[
E[Y^2] = \frac{1}{n}\sum_{i=1}^{i=n}Y_i^k.
\]
\begin{example}
  Random sample from the gamma distribution, $Y_1, \dotsc, Y_n$, $Gamma(\alpha, \beta)$. Find method of moment (MoM) estimators $\hat \alpha, \hat \beta$. Recall:
  \begin{itemize}
  \item $E[Y_i] = \mu = \alpha \cdot \beta$ for this distribution.
  \item $E[Y_i^2] = \mu^2 + \sigma^2 = \alpha^2\beta^2 + \alpha \beta^2$
  \end{itemize}
  So we have two equations;
  \begin{align}
    \alpha \beta  \frac{1}{n}\sum_{i=1}^{i=n}Y_i = \ov Y\\
    \alpha^2 \beta^2 + \alpha \beta^2 = \frac{1}{n}\sum_{i=1}^{i=n}Y_i^2
  \end{align}
  Solve for one and substitute:
  \begin{align*}
    \beta = \frac{1}{\alpha}\frac{1}{n}\sum Y_i \tag{Substitute into 2nd}\\
    \alpha^2 \frac{1}{\alpha^2} \frac{1}{n^2}(\sum Y_i)^2 + \alpha \frac{1}{\alpha^2}\frac{1}{n^2} (\sum Y_i)^2 = \frac{1}{n}Y_i^2 \\
    \implies \frac{1}{\alpha} = \frac{(1/n) \sum Y_i^2 - (1/n^2) \ov Y^2}{(1/n^2)\ov Y^2} \tag{Recall we should have written $\hat \alpha, \hat \beta$ from start}\\
    \implies \hat \alpha = \frac{\ov Y^2}{\frac{1}{n}\sum Y_i^2 - \ov Y^2} \tag{Sub. into $\beta$}\\
    \implies \hat \beta = \frac{(1/n)\sum Y_i^2 - \ov Y^2}{\ov Y}
  \end{align*}
  Are these estimators any good? Turns out they are \textbf{consistent} (converges in probability to the true parameters).


\end{example}
\textbf{But} they aren't going to be the best estimators. Using the factorization criterion, one can show $\sum Y_i$ and $\Pi Y_i$ are \textbf{sufficient} statistics for $Gamma(\alpha, \beta)$. This means if we take our estimators $\hat \alpha, \hat \beta$ and \textbf{condition} on these sufficient statistics, then we improve our estimators - because right now they are currently \textbf{not} functions of the above sufficient statistics.

Method of moment (MoM) estimators are
\begin{itemize}
\item Easy to compute 
\item Consistent
\item Not necessarily very efficient
\end{itemize}

\section{The Method of Maximum Likelihood}

Can be used for any problem where we know the joint pdf/PMF of data. Used all the time. Lots of guarantees for these estimators.

Let $X_1,\dotsc, X_n$ has a specified distribution (model assumption - know shape, but not parameters). This model assumption is huge.
There exists $\theta \in \Theta$ such that $X_1, \dotsc, X_n \sim f_\theta (x)$. 
Our goal is to estimate $\theta$.
\begin{remark}
  Hypothesis testing will be something we can do to check if our model assumption is decent. 
\end{remark}

\begin{remark}[Recall]
  Recall the Likelihood function\[
L(x_1, \dotsc, x_n | \theta) \equiv f_\theta (x_1, \dotsc, x_n) = \prod_{i=1}^n f_\theta (x_i) \tag{Function of both the data and the parameter}
\]
But we're going to do a fixed data set, and only vary the parameter $\theta$ such that we treat $L = L(\theta)$.
\end{remark}
If $L(\theta_1) > L(\theta_2)$, then $\theta_1$ is a better fitting parameter - more \textbf{likely} to have been responsible for producing the observed $x_1, \dotsc, x_n$.

\subsection{Method}

The Maximum Likelihood Estimator (MLE) $\hat \theta$ of $\theta$ is defined as \[
\hat \theta = argmax_\theta L(\theta) 
\] which can be obtained by taking a derivative and setting it to 0.

If we have $\theta_1, \dotsc, \theta_k$ then
\begin{equation}
  \pdv{}{\theta_i} L(\theta_1, \dotsc, \theta_k) = 0 \forall i = 1, \dotsc, k
\end{equation}
which are known as the ``likelihood equations''. May have to use numerical solvers for this. 
\begin{definition}
  We defin the \textbf{log-likelihood} 
  \begin{equation}
    \label{eq:log-likelihood}
    l(\theta) \equiv \log L(\theta) 
  \end{equation}
\end{definition}
which we often do because densities often have exponentials.

\begin{example}
  $Y_1, \dotsc, Y_n \sim N(\mu = \theta_1, \sigma^2 = \theta_2)$. We have
  \begin{align*}
    L(\mu, \sigma^2) = f(y_1 | \mu, \sigma^2) \cdots f(y_n | \mu, \sigma^2) \\
    = \frac{1}{\sigma \sqrt{2\pi}} \exp(-(y_1 - \mu)^2 / 2\sigma^2) \cdots \frac{1}{\sigma \sqrt{2\pi}} \exp(-(y_n - \mu)^2 / 2\sigma^2) \\
    = \frac{1}{\sigma^n (2\pi)^{n/2}} e^{\frac{-1}{2\sigma^2} \sum (y_i - \mu)^2}
  \end{align*}
  If we take the natural log,
  \begin{align*}
    l(\mu, \sigma^2) = -\frac{n}{2} \ln \sigma^2 - \frac{n}{2}\ln 2\pi - \frac{1}{2\sigma^2} \sum_{i=1}^{i=n} (y_i - \mu)^2 \\
    \pdv{}{\sigma} l(\mu, \sigma^2) = -\frac{1}{n}\frac{1}{\sigma} + \frac{1}{\sigma^3}\sum_{i=1}^{i=n} (y_i - \mu)^2\tag{But we want w.r.t. $\sigma^2$}\\
    \pdv{}{\mu} l(\mu, \sigma^2) \frac{1}{\sigma^2} \sum_{i=i}^{i=n} (y_i - \mu) \\
    \pdv{}{\sigma^2} l(\mu, \sigma^2) = -\frac{n}{2}\frac{1}{\sigma^2} + \frac{1}{\sigma^4}\sum_{i=1}^{i=n}(y_i - \mu)^2
  \end{align*}
  Set the last two to zero and get two equations.
  \begin{align*}
    \sum_{i=1}^{i=n} (y_i - \hat \mu) = 0 \\
    n\hat\mu = \sum y_i \implies \hat \mu = \frac{1}{n}\sum y_i \tag{$\ov Y$ is MLE of $\mu$; sub into 2nd eq.}\\
    - \frac{n}{\hat\sigma^2} + \frac{1}{\hat\sigma^4}\sum_{i=1}^{i=n}(y_i - \ov Y)^2 = 0 \\
    \implies \hat \sigma^2 = \frac{1}{n}\sum_{i=1}^{i=n} (y_i - \ov y)^2
  \end{align*}
\end{example}

\subsection{Properties of MLE}

Under certain conditions (model regularity - smooth functions, and $\theta$ is not at the endpoints), the MLE $\hat \theta$ has really good properties:
\begin{enumerate}
\item invariance under transformations
\item consistency
\item asymptotic optimality (efficient)
\item asymptotic normality (large n, dist. of MLE is normal)
\end{enumerate}


\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
