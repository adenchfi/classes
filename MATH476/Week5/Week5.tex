\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Error Estimation \\ Wackerly Ch. 8}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Consistency}

Notation: $\hat \theta = \hat \theta_n$ to indicate dependency on sample size.
\begin{definition}
  An estimator $\hat \theta_n$ is \textbf{consistent} if $\hat \theta_n \rightarrow \theta$ in probability. 
\end{definition}
Recall: Let $T_1, T_2, \dotsc, T_n$ be random variables (in same sample space). Then $T_n$ converges in probability to $T$ if for all $\epsilon > 0$, \[
\lim_{n \rightarrow \infty} P(\abs{T_n - T} > \epsilon) = 0.
\]
\begin{example}[Tossing Coins]
  We toss a coin repeatedly to estimate $p$ in Bernoulli random variable. Check book example.
\end{example}

\begin{theorem}
  Suppose $\hat \theta_n$ is an unbiased estimator of $\theta$.

  Then $\hat \theta_n$ is consistent if $\lim_{n \rightarrow \infty} Var(\hat \theta_n) = 0$.
  \begin{proof}
    Fix $n$, fix $\epsilon > 0$. We can write \[
P(\abs{\hat \theta_n - \theta} > \epsilon) 
\]\[
= P(\abs{\hat \theta_n - \theta} > \frac{\epsilon}{\sigma_{\hat \theta_n} \sigma_{\hat \theta_n}})
\] where we use the Tchebyshev inequality, written as \[
P(\hat{\hat \theta_n - \theta} > k \sigma_{\hat \theta_n}) \leq \frac{1}{k^2}.
\]
If we write $k = \frac{\epsilon}{\hat \theta_n}$, then we get the limit easily. 
  \end{proof}

\end{theorem}
\begin{remark}
  The converse is not true. 
\end{remark}

\begin{example}
  Let $Y_1, \dotsc, Y_n$ be random sample from a distribution with mean $\mu$ and variance $\sigma^2 < \infty$. Show $\ov Y_n \equiv \frac{1}{n} \sum Y_i$ is a consistent estimator of $\mu$.
  \begin{solution}
    We know $E[\ov Y_n] = \mu$ (unbiased), so we use the theorem. We have the variance \[
Var(\ov Y_n) = \frac{1}{n^2} \sum_{i=1}^{i=n}Var(Y_i) + \underbrace{2\sum_{i=1}^{i=n}\sum_{j=1}^{j=n} \frac{1}{n^2} Cov(Y_i, Y_j)}_{= 0 \text{ b/c indep.}} 
\]
Which then equals \[
\frac{1}{n^2}n\sigma^2 = \frac{\sigma^2}{n}.
\]
We take the limit \[
\lim_{n\rightarrow\infty} = \lim_{n \rightarrow \infty} \frac{\sigma^2}{n} = 0
\]
\end{solution}
\begin{remark}
  The fact that $\ov Y_n \rightarrow \mu$ in probability is the law of large numbers. 
\end{remark}
\end{example}


\begin{theorem}
  Suppose $\hat \theta_n \rightarrow \theta$ in probability, and $\hat \gamma_n \rightarrow \gamma$ in probability.
  \begin{enumerate}[label=(\alph*)]
  \item $\hat \theta_n + \hat \gamma_n \rightarrow \theta + \gamma$ in probability. 
  \item As above, but multiplication.
  \item As above, but division so long as $\gamma \neq 0$. 
  \item if $g(\cdot) $ is $\BB R$-valued function continuous at $\theta$, then $g(\hat \theta_n) \rightarrow g(\theta)$ in probability. 
  \end{enumerate}

\end{theorem}

\begin{example}
  $Y_1, \dotsc, Y_n$ a random sample from an unknown population. Let's define three of the moments:
  \begin{enumerate}
  \item $E[Y_i] = \mu$
  \item $E[Y_i^2] = \mu_2'$
  \item $E[Y_i^4] = \mu_4'$
  \end{enumerate}
  where all are finite.

  We \textbf{claim} that \[
S_n^2 = \frac{1}{n-1} \sum_{i=1}^{i=n} (Y_i - \ov Y_n)^2
\] is a consistent estimator of $\sigma^2 = Var(Y_i)$.
\begin{proof}
  We expand the quadratic, note that $\ov Y_n$ is a constant, and also note the middle term will become 0 once you write out the terms.
  \begin{align*}
    S_n^2 = & \frac{1}{n-1} \left(\sum_{i=1}^{i=n} Y_i^2 - n \ov Y_n^2 \right) \\
    = & \frac{n}{n-1}\left(\frac{1}{n}\sum_{i=1}^{i=n} Y_i^2 - \ov Y_n^2 \right) \tag{After factoring out the n}
  \end{align*}
  We note that the first term inside the parentheses is the sample mean of $n$ random variables, $iid$, with $E[Y_i^2] = \mu_2'$ and $Var[Y_i^2] = \mu_n' < \infty$.

  We can use the law of large numbers, and say \[
\frac{1}{n}\sum_{i=1}^{i=n} Y_i^2 \rightarrow \mu_2'
\] in probability, and recall $()^2$ is a continuous function, $g(y) = Y^2$. Using our theorem, \[
\ov Y_n^2 \rightarrow \mu^2.
\]
So now \[
\frac{1}{n} \sum Y_i^2 - \ov Y_n^2 \rightarrow \mu_2' - \mu^2
\] in probability - and it's the definition of the variance $\sigma^2$, $Var(Y_i)$.

Finally, look at the constant outside - $n/(n-1)$. Since that sequence approaches 1 in the limit of large $n$, that also converges to 1.

Therefore, \[
S_n^2 \rightarrow 1\cdot \sigma^2
\] by part b) of theorem about limits; i.e., the sample variance is a consistent estimator of population variance. 
\end{proof}
\end{example}
We also used $S$ for $\sigma$ if $n >> 0$ when $\sigma$ unknown. Theoretical justification:
\begin{theorem}[Slutsky's Theorem]
  Suppose $u_n \rightarrow N(0, 1)$ in distribution. If $W_n \rightarrow 1$, then
  \begin{equation}
    \frac{u_n}{w_n} \rightarrow N(0, 1)
  \end{equation}
  in distribution.
  \begin{remark}
    Could be useful in physics! 
  \end{remark}
\end{theorem}

She did another example which shows why we can use $S$ instead of $\sigma$ in the central limit theorem. Also, $t_n \rightarrow N(0, 1)$ as $n \rightarrow \infty$. 

\section{Second Lecture - Sufficiency}

\paragraph{Intuition}

Let $X_1, \dotsc, X_n$ sample. But you lose it!
Question: Is there a way to produce an ``equivalent'' dataset without repeating the experiment, if you have saved some bit of information from your sample? Is there a characteristic of the sample $X_1, \dotsc, X_n$ that would allow us to simulate another sample $\tilde X_1, \dotsc, \tilde X_n$ with the same properties?
\begin{remark}
  Kind of like the streaming data thing. If we're counting the number of 1's in a streamed data set, but we can't access the data after it's streamed, how do we estimate the amount of information conveyed in the last $N$ values? Say we want to check for irregularities in a data stream - we can compare that and see differences. 
\end{remark}
We want both samples to contain the same amount of info about $\theta$.
\begin{example}
  Binomial experiment, with $n$ outcomes $X_1, \dotsc, X_n$ where $X_i$ can be 1 with probability $p$, and 0 with probability $1 - p$.

  Let $T \equiv \sum_{i=1}^{i=n} x_i$ be the number of successes in a sample. Given $T$, compute the conditional probability \[
P(X_1 = x_1, \dotsc, X_n = x_n | T = t) = \frac{P(X_1 = x_1, \dotsc, T = k)}{P(T = t)} 
\] which, since if $x_i = 0$ the probability is 0, we get the numerator is $p^t(1-p)^{n-t}$, and the denominator is the number of ways we can order $t$ 1's in $n$ places, so it's $\binom{n}{t} p^t (1-p)^{n-t} = \frac{1}{\binom{n}{k}}$ if $\sum x_i = t$.

Note that $p$ goes away when we condition on the statistic $T = t$; the statistic $T$ is \textbf{sufficient} for $p$ (unknown parameter). $T$ carries all the information about $p$; when we conditioned on it (set it to be a fixed value), we don't have $p$ in our probability.

The basic idea: remember $T = t$, can get another sample from the conditional distribution $X_1, X_2, \dotsc, X_n | T$ yields the same information about $p$. 
\end{example}

\begin{definition}
  Suppose you have $Y_1, \dotsc, Y_n \sim f_\theta (y)$, iid, $\theta $ unknown. Then a statistic $T \equiv T(Y_1, \dotsc, Y_n)$ is said to be \textbf{sufficient} for $\theta$ \textbf{if} the conditional distribution of $Y_1, \dotsc, Y_n$ given $T$ does \textbf{not} depend on $\theta$. 
\end{definition}
\begin{remark}
  Perhaps we make observations of a physical system, observe its momenta, energy levels, etc - we can use that data to simulate data or make a distribution that has the same properties as what we wanted. We'd find a statistic $T$ that can ensure the probability of a certain sample/observation set does not depend on the parameters of the original distribution. 
\end{remark}

How to find sufficient statistics?
You can find $P(X_1 = x_1, \dotsc, T = k)$, the joint distribution/density, and see if it factors into two probability distributions, one that depends on T, and one that doesn't.
\begin{definition}
  Let $y_1, \dotsc, y_n$ be sample observations (values of the RVs $Y_1, \dotsc, Y_n \sim f_\theta (y)$). The \textbf{likelihood} of the sample is (just the joint density)
  \begin{equation}
    L(y_1, \dotsc, y_n | \theta) = f_\theta (y_1) \cdot \dotsc \cdot f_\theta (y_n) \equiv f(y_1 | \theta) \cdot \dotsc \cdot f(y_n | \theta)
  \end{equation}

\end{definition}
\begin{theorem}[Neyman-Fisher]
  Let $Y_1, \dotsc, Y_n \sim f_\theta (y)$ and are iid.
  A statistic $T = T(y_1, \dotsc, y_n)$ is \textbf{sufficient} for $\theta$ if and only if the likelihood can be factored as:
  \begin{align}
    L(y_1, \dotsc, y_n | \theta) = g(T, \theta) \cdot h(y_1, \dotsc, y_n)
  \end{align}
  where we see there is a part that depends on $\theta$ and a part that doesn't. In the proof, $g(T, \theta)$ should be the PMF/PDF of $T$. Recall $T$ is a statistic of the sample. 
\end{theorem}

\begin{example}
  Have $y_1, \dotsc, y_n$ is a sample from \[
f(y_i | \theta) =
\begin{cases}
  \frac{1}{\theta} e^{-y_i / \theta} & 0 \leq y_i \leq \infty \\
  0 & \text{otherwise}.
\end{cases}
\]
where $\theta > 0, i = 1, \dotsc, n$. Show that $\ov Y$ is sufficient for $\theta$.
\begin{solution}
  \[
L(y_1, \dotsc, y_n | \theta) = f(y_1, \dotsc, y_n | \theta) 
\] which, because they're independent, \[
= \Pi f(y_i | \theta) = \frac{e^{-y_1/\theta}}{\theta} \cdots \frac{e^{-y_n / \theta}}{\theta} = e^{-n \ov Y / \theta} / (\theta^n) \cdot 1.
\]
So our function $h(y_1, \cdots, y_n) = 1$, and our $g$ is $g(\ov Y, \theta)$. However, we could have used the functions from before we converted to $\ov Y$ to get something sufficient for $\theta$ - $\sum y_i$. 
\end{solution}
\end{example}

One possible goal is to reduce the amount of data as much as possible, while having it remain sufficient (minimal sufficiency). 

\subsection{Rao-Blackwell Theorem}

This theorem tells us more about sufficient statistics. If you have an unbiased estimator, how do you improve the variance (lower the variance) of an unbiased estimator (since that's good) and remain unbiased?
\begin{theorem}[Rao-Blackwell Theorem]
  Let $T$ be a sufficient statistic and $\hat \theta$ be an unbiased estimator of $\theta$. Define a new estimator $\hat \theta^* \equiv E[\hat \theta | T = t]$.

  Then $\hat \theta^*$ is an unbiased estimator of $\theta$ and the $Var(\hat \theta^*) \leq Var(\hat \theta)$ for all $\theta$! In fact, you only get equality if and only if $\hat \theta = \hat \theta^*$.
  \begin{proof}
    Since $T$ is sufficient for $\theta$, the conditional distribution of any sample (or sample statistic) given $T$ doesn't depend on $\theta$. This means that $\hat \theta^*$ \textbf{is} a statistic. Since it's a statistic, we can use it as an estimator. Use Ch. 5,
    \begin{align*}
      E[\hat \theta^*] = E[E[\hat | T]] = E[\hat] = \theta \tag{B.c. it's unbiased} \\
      Var(\hat \theta) \\
      = Var(E[\hat | T]) + \underbrace{E[Var(\hat \theta | T)]}_{\geq 0} \\
      = Var(\hat \theta^*) + E[Var(\hat \theta | T)] \\
      \implies Var(\hat \theta) \geq Var(\hat \theta^*)
    \end{align*}

  \end{proof}

\end{theorem}
\begin{remark}
  This is good because we can find an unbiased estimator easily, even if it has a high variance, and then so long as we can find a sufficient statistic, we can improve on our initial guess. 
\end{remark}

\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
