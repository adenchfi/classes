\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Statistics and sampling distributions \\ Wackerly Ch. 7.3-7.4}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{}
\begin{theorem}[Theorem 7.3]
  If $X_1, \dotsc, X_n \sim N(\mu, \sigma^2)$,
  then
  \begin{equation*}
    \frac{n-1}{\sigma^2} S^2 \sim \chi^2_{n-1\ d.o.f.} 
  \end{equation*}
  and $\ov{x} \indep S^2$ - the two are independent random variables
\end{theorem}

$S^2$ is the sample variance
\begin{equation}
  S^2 = \frac{1}{n-1} \sum_{i=1}^{i=n} (X_i - \ov X)^2
\end{equation}
\begin{remark}
  Recall the definition of independent random variables. $P(X=i, Y=i) = P(X=i)P(Y=j|X=i) = P(X=i)P(Y=j)$
\end{remark}
\begin{example}
  Suppose we select $n=10$ bottles randomly and measure amount of fill. We use 10 observations to measure $S^2$, sample variance. Do you think $S^2$ will be close to $\sigma^2 = 1$? Recall we don't know the mean, but we do know the variance. 

  We don't know the total population $p$ of bottles produced. However, in statistics, 10 observations is often ``close enough'', weirdly.

  Real question: How to measure how close $S^2$ is to $\sigma^2$?
  \begin{solution}
    We use probabilities and compute, for example, $P(a \leq S^2 \leq b) = 0.9$ where $a, b$ are some numbers. We know the distribution of $S^2$, a random variable, and we can use the previous theorem.

    \begin{align*}
      P(a \leq S^2 \leq b) \\
      = & P(\frac{n-1}{\sigma^2}a \leq \frac{n-1}{\sigma^2 S^2} \leq \frac{n-1}{\sigma^2} b)
    \end{align*}
    We can then look up the numbers for $P(9a/1 \leq \chi^2 \leq 9b/1)$. $9a = 3.3, 9b = 16.9 \implies a = 0.369, b = 1.88$.

    This means $S^2$ has a 90\% chance of appearing in that interval. Consider $\sigma^2 = 1.0$. Since 1.0 is in that interval, we're good. 
  \end{solution}
\end{example}

\begin{example}
  Suppose you know the population mean $\mu$ and $X_1, \dotsc, X_n \sim N(\mu, \sigma^2)$. We define some r.v./statistic $T_n$ as
  \begin{equation}
    T_n = \sqrt{n} \frac{\ov X - \mu}{S} \quad S = \sqrt{S^2} = \text{sample std dv}
  \end{equation}
  What is the sampling distribution of $T_n$?
  We know $z := \sqrt{n} \frac{\ov X - \mu}{\sigma} \sim N(0, 1)$. Replace $\sigma$ with $S$ in this formula. We also know
  \begin{equation}
    \frac{n-1}{\sigma^2}S^2 \sim X^2_{n-1}
  \end{equation}
  and also $S^2 \indep \ov X$. 
\end{example}

\subsection{How to get the T-dist?}

When this problem arose, nobody knew the distribution of $T_n$!
\begin{solution}
  Setup: We have
  \begin{align*}
    W = (n-1)\frac{S^2}{\sigma^2} \sim \chi^2_{n-1} \\
    Z \indep W
  \end{align*}
  We can do a tricky division:
  \begin{equation}
    \sqrt{n}\frac{\ov X - \mu}{S} = \frac{Z}{\sqrt{W/\sigma}}
  \end{equation}
  Any such random variable as the so-called $t$-distribution with $\nu$ degrees of freedom. 
\end{solution}

\begin{problem}
  Sampling distribution of statistics $T_n$ will not be known, except special cases.
  In such cases, we rely on approximations.
  \begin{enumerate}
  \item Numerical approximations. Get a lot of samples, compute a lot of $T_n$'s, look at the frequency histogram and infer what it looks like. She will give us some code for this. 
  \item Asymptotic ($n >> 0$); if $n$ is large enough, we look at the limiting distribution. 
  \end{enumerate}
\end{problem}

\section{Central Limit Theorem}


\begin{theorem}[Central Limit Theorem]
  Have an iid sample $X_1, \dotsc, X_n$ from \textbf{some} distribution with mean $\mu$ and variance $\sigma^2 < \infty$. For $\ov X_n$ (the n-th sample mean of $n$ samples), let
  \begin{equation}
    Z_n := \sqrt{n} \frac{\ov X_n - \mu}{\sigma}
  \end{equation}
  Then $Z_n$ converges in distribution to $N(0, 1)$ as $n \rightarrow \infty$. 
\end{theorem}
\begin{remark}
  The CLT says if $n >> 0$, the distribution of $\ov X_n$ is approximately $N(\mu, \sigma^2/n)$. This matches up with the special case of when the $X_i$ are already $X_i \sim N(\mu, \sigma)$, except here it is now exact even if $n \sim 0$.

  One can make an argument that CLT implies $\ov X_n \rightarrow \mu$ in probability.
\end{remark}

\begin{remark}
  You can transform your data in any way and the Central Limit Theorem still applies. 
\end{remark}

\section{Estimation}

\begin{remark}[Reminder]
  We assume we have a ``good sample'' as a given in this course. 
\end{remark}

\subsubsection{Steps}

\begin{enumerate}
\item Exploratory data analysis to determine the statistical model - the functional form of $f(x)$ (the probability distribution we assume)
\item Given an $f(x)$ from above, and given the sample, we try to \textit{infer} the unknown quantity (e.g. mean/expected value). We can make this inference by simply \textbf{estimating} this quantity (mean).
  \begin{definition}[Estimator]
    An \textbf{estimator} for an unknown quantity $\theta$ is simply a rule (formula) that tells us how to calculate the value of the estimate $\hat{\theta}$ based on a sample. 
  \end{definition}
  \begin{example}
    \nonumber
    Sample mean $\ov X = \frac{1}{n} \sum X_i$ should estimate (true but unknown) mean $\mu$. So if $\theta = \mu$, then $\hat{\theta} = \ov X$. We can also write, for the mean, $\hat{\mu} = \ov X$. $\hat{\mu}$ is an estimator of $\mu$. It's a Random Variable, it depends on the sample.
    \begin{remark}[Difference Between Statistic and Estimator]
      Every statistic can be used as an estimator. An estimator \textbf{has} to take values in the \textbf{parameter space} $\Theta$, so that it makes sense to compare $\theta$ and $\hat{\theta}$. There is no such restriction on a statistic. 
    \end{remark}
  \end{example}
\end{enumerate}

\subsection{Properties of Estimators}

Any measure of goodness of $\hat{\theta}$ can be derived from its sampling distribution. E.g. apply your estimator to a known example. If the sampling distribution is not available, perhaps focus on simpler properties.
\begin{enumerate}
\item We want $\hat{\theta}$ to be equal to $\theta$. However, $\hat{\theta}$ is a RV and $\theta$ is a fixed value. So:
  \begin{definition}[Unbiasedness]
    $E[\hat{\theta{}}] = \theta$ is true if the estimator is unbiased.
    \begin{remark}[Bias]
      Bias$(\hat{\theta}) = E[\hat{\theta}] - \theta$. 
    \end{remark}
    \begin{remark}
      Your estimator could have $E[\hat{\theta}] = \theta$ but never actually have $\hat{\theta} = \theta$. The variance of our estimator could be huge.
    \end{remark}
  \end{definition}
  \begin{definition}[Standard Error]
    We can measure the standard deviation of $\hat{\theta}$ as
    \begin{equation}
      \sigma_{\hat{\theta}} = \sqrt{\sigma_{\hat{\theta}}^2}
    \end{equation}
    being known as the ``STD ERROR'' of $\hat{\theta}$. 
  \end{definition}
  \begin{example}
    Say $X \sim Pois(\theta)$. Say we want to estimate $\eta = e^{-2\theta}$ and not $\theta$ itself.

    We know $\hat{\theta} = X$ is unbiased for $\theta$. However, the natural estimator to use $e^{-2X}$ is \textbf{not} unbiased for $e^{-2\theta}$.

    Instead, $\hat{\eta} = (-1)^X$ \textbf{is} unbiased.
    \begin{proof}
      $E[(-1)^X] = \underbrace{\sum_{n=0}^{\infty}}_{x's Pois can take} e^{-\theta} \frac{(-1)^X \theta^X}{x!} = e^{-2\theta}$.
    \end{proof}
    \begin{remark}
      In fact, there is a theorem that shows this estimator is the \textbf{best} possible estimator for $e^{-2\theta}$. \textbf{However}, it only takes values $\{-1, 1\}$ and may \textbf{never} be close to the true $e^{-2\theta}$. 
    \end{remark}
  \end{example}


\paragraph{To Do}
Look at table 8.1 of std errors of various estimators. Bookmark it.
\begin{example}
  Let $\theta = \mu$. Then $\hat{\theta} = \ov X$ is unbiased. $E[\ov X] = \mu$, therefore $\sigma_{\ov X}^2 = Var(\ov X) = \frac{\sigma^2}{n}$. 
\end{example}
\begin{example}
  If we have 2 populations, we would like to see if the population means are different. Therefore we have a value $\theta = \mu_1 - \mu_2$ which is a difference of means. How do we make a good estimator?
  \begin{solution}
    $\hat{\theta} = \ov X - \ov Y$ for populations 1 and 2 respectively. Therefore $E[\hat{\theta}] = E[\ov X] - E[\ov Y] = \mu_1 - \mu_2$ is unbiased. We also have $Var(\hat{\theta}) = Var(\ov X) + Var(\ov Y) = \frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}$.

    Table 8.1 will tell us that $\sigma_{\ov X - \ov Y} = \sqrt{\frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}}$ which is the standard error of $\hat{\theta} = \ov X - \ov Y$. 
  \end{solution}
\end{example}
\item Consistency. Another reasonable property of an estimator is that, for an estimator of sample size $n$, $\hat{\theta} = \hat{\theta}_n$
  \begin{equation}
    \lim_{n \rightarrow \infty} \hat{\theta}_n = \theta. 
  \end{equation}
  \begin{definition}[MSE]
    If we want a criterion of an estimator to not depend on our sample size $n$, we define the \textbf{mean-square error [MSE]}. This measures the closeness of $\hat{\theta}$ to $\theta$ in a way that doesn't depend on sample size $n$.
    \begin{equation}
      MSE(\hat{\theta}) = E[(\hat{\theta} - \theta)^2]. 
    \end{equation}
    Proposition: $MSE(\hat{\theta}) = Var(\hat{\theta}) + Bias(\hat{\theta})^2$.
  \end{definition}
\end{enumerate}



\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
