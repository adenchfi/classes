\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Statistics and sampling distributions \\ Wackerly Ch. 7.1-7.4}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{General Introduction}
Her lectures will not always be like the book's content. Can use python or R to do stuff. The final will not be cumulative. Check Piazza for all course details, apparently. 

Statistics and Probability are very related. However, they have entirely different goals.
\begin{itemize}
\item Probability: assume something about a distribution of a random variable (RV). Objective: derive the properties of that RV. For example, the probability of the RV taking a certain value, or the expected value of the EV, etc. 
\item Statistics: Sample from a population (get your data). Objective: Learn something about that population. 
\end{itemize}

\paragraph{Goal of course:}

Develop a mathematical theory of statistics using calculus and probability. 

\section{Statistics}

\textbf{Notation}. Let $X_1, \dotsc, X_n$ be a random sample that's identically distributed and independent (iid) from a distribution (cumulative distribution function, CDF) $F(X)$. The CDF admits two cases:
\begin{itemize}
\item \textbf{PMF}: discrete case, probability mass function
\item \textbf{PDF}: continuous case, probability density function
\end{itemize}

For the purpose of this course, both will be indicated by $f(x)$. This $f(x)$ characterizes some population where $\{X_k\}$ was sampled from. Typically, there is something unknown about the population that we want to estimate. 

\begin{example}[Voters]
  A typical example includes voting. Our population to draw samples from is the registered voters in Cook county. Our \textbf{unknown} is the proportion of voters that will vote Republican in the next election. We can \textbf{estimate} this proportion from a random sample.

  The population/distribution is not completely known. Mathematically, we introduce a quantity $\theta \in \Theta \in \mathcal{R}^d, d \geq 1$. The distribution we want has PMF/PDF of form $f_\theta (x)$ for some $\theta \in \Theta$.

In Cook county, denote $D = 1$   and $R = 0$. Sample the population randomly. Then the \# of voters who will vote $R$ is a RV with a binomial distribution. $x \sim Bin(n, \theta)$ where $\theta \in [0, 1]$. 
\end{example}

A statistician uses $X_1, \dotsc, X_n$ to learn/estimate the parameter $\theta$. We say the probability distribution function $f_\theta (x)$ is the ``shape'' of the distribution, but the parameter $\theta$ varies; our goal is to try and pick the ``best'' value of $\theta$ that best fits our sample. 

\paragraph{Steps}

\begin{enumerate}
\item Choose functional form (or ``shape'') of $f_\theta (x)$ (we will skip this for the most part). This is known as ``model specification''.
\item Produce some \textbf{summary} of the information in the data about the unknown parameter. These ``summaries'' are called \textbf{statistics}. (Another approach is to do Bayesian statistics)
\item Learn/make an inference about $\theta$. Two main statistics inference problems:
  \begin{enumerate}
  \item Point estimation. 
  \item Hypothesis testing.
  \end{enumerate}
\end{enumerate}

\section{Sampling Statistics}

\subsection{Point Estimation}

Suppose $X_1, \dotsc, X_n$ are iid (identical, ind. distributions) with probability density function $f_\theta (x)$. What is, then, the point estimation problem?

Find a quantity $\hat{\theta}$, called an estimator, depending on $X_1, \dotsc, X_n$, which is a ``good guess'' for $\theta$. Obviously, choice of $\hat{\theta}$ depends on
\begin{enumerate}
\item Your data
\item Assumed model (form of $f_\theta (x)$)
\item Definition of ``good'' - close to $\theta$. However, $\theta$ is unknown. 
\end{enumerate}
We expect that $\hat{\theta}$ will miss $\theta$ by some positive amount. We would like to then minimize $\norm{\hat{\theta} - \theta}$.
We can summarize $\hat{\theta}$ with the standard error, the variance, etc.

\subsection{Hypothesis Testing}

Answer questions such as:
\begin{itemize}
\item is $\theta = \theta_0?$
\item is $\theta \leq \theta_0?$
\end{itemize}
General setup:
\begin{enumerate}
\item Construct a decision rule, depending on $X_1, \dotsc, X_n$, to decide if $\theta = \theta_0$ or not
\item Find $\hat{\theta}$ and ask $\hat{\theta} = \theta_0?$ How far from it are you? 
\end{enumerate}
To do this, we introduce quantities called \textbf{statistics}. 

\begin{definition}[Statistics]
  Let $X_1, \dotsc, X_n$ be a sample whose distribution may or may not depend on the unknown $\theta$. Then any function $T = T(X_1, \dotsc, X_n)$ that does \textbf{not depend on $\theta$} is called a \textbf{statistic}. 
\end{definition}

Examples of $T$:
\begin{itemize}
\item $T = \bar{X}$
\item $T = S^2 = \frac{1}{n-1} \sum_{i=1}^{i=n} (x_i - \bar{x})^2$
\item $T = 7$ - bad statistic, but not illegal
\item $T = I_{(\infty, y]} (x_1)$ - indicator function.
  \begin{remark}
\[
  I_{(\infty, y]} (x_1) =
  \begin{cases}
    1 & if x_1 \leq 7 \\
    0 & \text{otherwise}
  \end{cases}
\]
  \end{remark}
\end{itemize}

Choice of $T$ depends on the problem you want to solve. The distribution of $T$ may (and we hope does) depend on $\theta$. That way we can try and find the distribution of $\theta$. 

\begin{definition}[Order Statistics]
  The \textbf{order statistics} of a sample are the sorted values denoted by \[
X_{(1)} \leq X_{(2)} \leq \dotsc \leq X_{(n)}
\]. We order our random variables in increasing order. 
\end{definition}
\begin{example}
  For example, if $n$ is odd, then $X_{((n+1)/2)}$ is the sample median. 
\end{example}
\begin{remark}
  Even if the distribution of $X_1, \dotsc, X_n$ depends on $\theta$, sorting them does \textbf{not} depend on $\theta$. 
\end{remark}
\begin{theorem}
  Fix $k \leq n$. Suppose the sample $X_1, \dotsc, X_n$ has CDF $F(X) = P(X_i \leq x)$. Then the CDF of the $k$-th order statistic can be derived as follows:
  
  \begin{align*}
    G_k(x) = P(X_{(k)} < x) = P(\text{at least $k$ of $X_1, \dotsc, X_n$ are $\leq x$})\\
    = & \sum_{j=k}^{j=n} (j \text{ of the $x$'s are $\leq x$}) \\
    = & \sum_{j=}^{j=n} \binom{n}{j} [F(x)]^n [1 - F(x)]^{n-j}
  \end{align*}
  Then you can obtain the PDF of the $x_{(k)}$ by differentiating $G_k(x)$:
  \begin{equation}
    g_k(x) = k \binom{n}{k} f(x) [F(x)]^{k-1} [1-F(x)]^{n-k}
  \end{equation}
\end{theorem}

\begin{example}
  If $X_i \sim Unif(0, 1)$, then we know $F(x)$ and $f(x)$, and
  \[
    g_k(x) = k\binom{n}{k} x^{k-1} (1-x)^{n-k}
  \]
  which is the PDF of $beta(k, n-k+1)$
  \begin{remark}
    If you specify $k = n$, then you get a much easier/intuitive answer. 
  \end{remark}
\end{example}

\begin{remark}
  We can then find the distribution of $T$, one of our statistics, and then using the process before, we can get the CDF of the k-th order statistic. 
\end{remark}

\subsection{Wackerly Sampling Dists Intro}

A \textit{statistic} is a function of the observable random variables in a sample and known constants. Because a statistic is a function of the random variables observed in a random sample, the statistic itself is a random variable. Consequently, using the methods of Chapter 6, we will derive its probability distribution, which we will call the \textit{sampling distribution} of the statistic. 

\subsection{Sampling Distributions Related to the Normal Distribution}

Get samples $X_1, \dotsc, X_n$ and build $T^{(1)} = T(X_1, \dotsc, X_n)$ from it.
Get samples $Y_1, \dotsc, Y_n$ and build $T^{(2)} = T(Y_1, \dotsc, Y_n)$ from it.
Continue the process and get our $T^{(n)}$ statistics from $n$ samples. The sampling distribution of $T$ can be approximated by the histogram of $\{T^{(k)}\}$.

\begin{theorem}
  Let $Y_1, \dotsc, Y_n$ be a random sample of size $n$ from a normal distribution with mean $\mu$ and variance $\sigma^2$. Then
  \[
    T_n = \bar{Y} - \frac{1}{n} \sum_{i=1}^{i=n} Y_i
  \]
  is also normally distributed, with mean $\mu_{\bar{Y}} = \mu$ and variance $\sigma^2_{\bar{Y}} = \sigma^2/n$.
  \begin{proof}
    The proof follows from properties of functions of independent random variables, and Theorem 6.3. 
  \end{proof}
\end{theorem}

\begin{example}
  In the text. Answer you can do
\begin{verbatim}
1 - 2*(1-pnorm(1.2))
\end{verbatim}
in R. 
\end{example}

\begin{remark}
  It also follows that
  \[
    Z = \frac{\bar{Y} - \mu_{\bar{Y}}}{\sigma_{\bar{Y}}} = \frac{\bar{Y} - \mu}{\sigma/\sqrt{n}} = \sqrt{n} \left(\frac{\bar{Y} - \mu}{\sigma} \right)
  \]
  has a standard normal distribution. 
\end{remark}

\begin{theorem}[Sampling Distribution of Sum of Squares of Normal Variables]
  Let $Y_1, \dotsc, Y_n$ be defined as previously. Then $Z_i = (Y_i - \mu)/\sigma$ are independent, standard normal random variables, and \[
    \sum_{i=1}^{i=n} Z_i^2 
  \]
  has a $\chi^2$ distribution with $n$ degrees of freedom. 
\end{theorem}

\subsection{Central Limit Theorem}

\begin{theorem}[Central Limit Theorem]
  Let $Y_1, \dotsc, Y_n$ be independent and identically distributed random variables with $E(Y_i) = \mu_i$ and $V(Y_i) = \sigma^2 < \infty$. Define
  \[
    U_n = \sqrt{n}\left(\frac{\bar{Y} - \mu}{\sigma} \right)
  \]
  where $\bar{Y} = \frac{1}{n} \sum_{i=1}^{i=n} Y_i$.

  Then the distribution function of $U_n$ converges to a standard normal distribution function as $n \rightarrow \infty$. 
\end{theorem}
\begin{proof}[Central Limit Theorem]
  The proof is fascinating and found in Wackerly Ch. 7.4. It takes a limit of a moment-generating function representation to prove the result. 
\end{proof}


\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
