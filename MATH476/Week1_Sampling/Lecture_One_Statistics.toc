\select@language {english}
\contentsline {section}{\numberline {1}General Introduction}{1}{section.1}
\contentsline {paragraph}{Goal of course:}{1}{section*.2}
\contentsline {section}{\numberline {2}Statistics}{2}{section.2}
\contentsline {paragraph}{Steps}{2}{section*.3}
\contentsline {section}{\numberline {3}Sampling Statistics}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Point Estimation}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Hypothesis Testing}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Wackerly Sampling Dists Intro}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Sampling Distributions Related to the Normal Distribution}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Central Limit Theorem}{5}{subsection.3.5}
