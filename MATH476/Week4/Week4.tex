\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Error Estimation \\ Wackerly Ch. 8}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Conf. Intervals, cont'd}

She finished the example from last time, and found $n = 406$. Probably similar to a book problem.
\begin{remark}
  What if we have no idea about $p$, or the guess for $p$ is a ``wild guess'' like 0.6?
  We decide to find $\hat p$ that has maximum variance over all possible $\hat p$'s. 
\end{remark}
For this example, with a binomial distribution, we want to find a value $\hat p$ that maximizes $Var(\hat p) = Var(y/n)$.
\begin{equation*}
  Var(y/n) = \frac{1}{n^2}Var(y) = \frac{1}{n^2} np(1-p) 
\end{equation*}
We want to maximize this quantity, so
\begin{align*}
  \pdv{}{p} \left(\frac{1}{n}p(1-p) \right) = 0 \\
  \implies \frac{1}n{}(1 - 2p) = 0\\
  \implies p = \frac{1}{2} = 0.5
\end{align*}
If we take the second derivative, we confirm it is a maximum. So max of $var(y/n)$ occurs at $p = 0.5$, and using $p = 0.5$ in conf. interval computation gives $n = 423$. For large samples, they seem to always converge to a normal distribution.

\subsection{Small-Sample Confidence Intervals}

For $\mu,\ \mu_1 - \mu_2$. 

If your data looks approximately like a normal curve (we assume $Y \sim Normal$ becausse we want intervals to be okay for ANY n), we can use these techniques.

\paragraph{Setup}

$Y_1, \dotsc, Y_n \sim N(\mu, \sigma^2)$.
\paragraph{Goal}

Conf. Int. for $\mu$ when $V(Y) = \sigma^2$ is unknown, and $n$ too small for previous section techniques. Recall Thm 7.1, 7.2, def 7.3. 

\begin{example}
  Let $T = \frac{\ov Y - \mu}{s/\sqrt{n}}$ has t-distribution with $n - 1$ degrees of freedom. It's like a normal distribution, but it has heavy tails. We can use $T$ as a pivot to get confidence interval for $\mu$!
\end{example}

So find values such that
\begin{align*}
  \BB P(-t_{\alpha /2} \leq T \leq t_{\alpha /2}) = 1 - \alpha \\
  = \BB P(-t_{\alpha/2} \leq \frac{\ov Y - \mu}{s/\sqrt{n}} \leq t_{\alpha/2}) = 1 - \alpha\\
\end{align*}
All you need to do is solve for $\mu$ and then use tables, considering $\mu$ is the only unknown once you know your data.

\begin{example}[Book Example With Guns]
  You get 8 data points, and they say to find the 95\% confidence interval for the true average velocity $\mu$ for shells of that particular type of bullet. 8 is a small number.
  \begin{solution}
    Assume velocities $Y_i \sim N(\mu, \sigma^2)$. [Is this reasonable? Depends on the manufacturing process. If a robot is doing it, it's fine; if a human is doing it, maybe not good].
    \begin{remark}
      This assumption \textbf{really} says that the manufacturing process is ``good'' with a ``normal error'' $\epsilon_i \sim Normal$; there is another error, measurement error $\epsilon_j \sim Normal$ (we assume measurement error is normal). 
    \end{remark}
    We can use small sample interval for $\mu$: \[
\ov Y \pm t_{\alpha/2} \frac{s}{\sqrt{n}} = \underbrace{2959}_{avg of 8} \pm t_{0.025, n-1 = 7} \frac{39.1}{\sqrt{8}}
\]
where we assume $s$ is the sample variance, and $t_{0.025, dof=7} = 2.365$. We conclude \[
P[(2959-32.7, 2959+32.7) \text{contains true avg velocity}] = 0.95
\]
  \end{solution}
\end{example}

\begin{example}
  We have two normal populations, $X \sim N(\mu_x, \sigma^2)$, $Y \sim N(\mu_y, \sigma^2)$, assuming the variances are the same.

  What is the confidence interval for $\mu_x - \mu_y$?
  What is the pivotal quantity?
  Answer: \[
Z = \frac{\ov X - \ov Y- (\mu_x - \mu_y)}{\sqrt{\frac{\sigma_x^2}{n_x} + \frac{\sigma_y^2}{n_y}}}
\]
We claim that since both $X, Y$ are normal, then $Z \sim (0, 1)$ - there's some theorem or exercise that shows we can do this.

We don't know $\sigma$, but the only thing we can do is plug in $S$, the std of the sample. Since we have two samples, we do a \textbf{pooled variance}, $S^2_p$ is defined as a pooled estimator for $\sigma^2$ from 2 samples.
\begin{align*}
  S_p^2 = \text{ a weighted average of } S_x^2, S_y^2 \\
  = \frac{(n_x - 1)S_x^2 + (n_y - 1)S_y^2}{n_x + n_y - 2} \tag{Produces an unbiased estimator} \\
  = \frac{\sum_{i=1}^{i=m} (x_i - \ov x)^2 + \sum_{i=1}^{i=m} (y_i - \ov y)^2}{n_x + n_y - 2} \tag{Squares of normals are $\chi^2$}
\end{align*}
Write $w = \frac{(n_x + n_y - 2)S_p^2}{\sigma^2} \sim \chi^2_{n_x + n_y - 2}$
and $w \indep Z$. Therefore, \[
T = \frac{Z}{\sqrt{\frac{w}{\nu}}} \sim t_{n_x + n_y - 2 d.o.f.}
\] where $\nu$ is d.o.f. = $n_x + n_y - 2$ here. Plugging in, we get
\begin{equation}
  T = \frac{\ov X - \ov Y - (\mu_x - \mu_y)}{S_p \sqrt{\frac{1}{n_x} + \frac{1}{n_y}}}
\end{equation}
This variable can be a pivot, because the only unknowns are $\mu_x, \mu_y$. The confidence interval for $\mu_x - \mu_y$ is \[
(\ov X - \ov Y) \pm t_{\alpha/2} S_p \sqrt{\frac{1}{n_x} + \frac{1}{n_y}}
\] where $t_{\alpha/2}$ has $n_x + n_y - 2$ degrees of freedom. 
\end{example}

\begin{remark}[Book Example]
  There's a book example on how to use this stuff, just read that and go through it. Ex. 8.12. 
\end{remark}

\subsection{How good is S as an estimator for $\sigma$?}
Recall that $\sigma^2$ quantifies variability in the population.

Confidence Intervals for $\sigma^2$: 
We proved \[
S^2 = \frac{1}{n-1} \sum_{i=1}^{i=n} (x_i - \ov x)^2
\] is \textbf{unbiased} estimator of $\sigma^2$. We use that a lot.

As usual, for the C.I., we want to find the pivotal quantity. We need to make assumptions about our population. Assume $X_1, \dotsc, X_n \sim N(\mu, \sigma^2)$ where both are unknown.

By Theorem 7.3, we have $\frac{(n-1)S^2}{\sigma^2} = \frac{\sum_{i=1}^{i=n} (x_i - \ov x)^2}{\sigma^2} \sim \chi^2_{n-1}$.

So now we find $a, b$ such that \[
P(a \leq \frac{(n-1)S^2}{\sigma} \leq b) = 1 - \alpha
\] and this can be done because we know the middle quantity is a $\chi^2$ distribution, so we can find $a, b$. After that, we transform it to an equivalent form, \[
P(\frac{(n-1)S^2}{b} \leq \sigma^2 \leq \frac{(n-1)S^2}{a}) = 1 - \alpha
\]
and we get a confidence interval for $\sigma^2$.
\begin{example}[Sand]
  Measurements of sand volume are 4.1, 5.2, 10.2. Our goal is to estimate the variability of these measurements with confidence coefficient 0.90, or $\alpha = 0.10$.
  \begin{solution}
    We assume normality of the recorded measurements. We don't have the tools (so far) to do anything else.
    We then find $S^2$ with the above formula, yielding $S^2 = 10.57$. We know $\alpha/2 = 0.05$, and we have $n-1$ DOF. Look up: $\chi^2_{0.95} = 0.103 \& \chi^2_{0.05} = 5.991$ which are $a, b$ respectively. We get an interval for $\sigma^2$ as $(3.53, 205.24)$.

    \begin{remark}
      If you don't assume normality, but assume another distribution whose square you know the properties of, and can do the same. Alternatively, use a simulation. 
    \end{remark}
    \begin{remark}
      Recall the conditions for something to be a pivot:
      \begin{enumerate}
      \item Have to be computed only from the data,
      \item Only unknown can be the variable you're looking to estimate,
      \item Should be a quantity you know the distribution of
      \end{enumerate}

    \end{remark}
  \end{solution}

\end{example}

\section{Properties of Point Estimators}

We already discussed being unbiased, but there are other properties.
\begin{definition}[Relative Efficiency]
  If you have more than 1 estimator for the same $\theta$, all unbiased, then which one is preferred? The one with the smaller variance. Let $\hat \theta_1, \hat \theta_2$ be 2 unbiased estimators of $\theta$. We say $\hat \theta_n$ is \textbf{relatively more efficient} if $Var(\hat \theta_2) > Var(\hat \theta_1)$.
  \textbf{Efficiency} of $\hat \theta_1$ relative to $\hat \theta_2$ is
  \begin{equation}
    Eff(\hat \theta_1, \hat \theta_2) = \frac{Var(\hat \theta_2)}{Var(\hat \theta_1)}. 
  \end{equation}
  If it's greater than 1, then $\hat \theta_1$ is better. 
\end{definition}

She goes through an example very similar to the HW, except also computing the variances of the estimators and computing their relative efficiencies.

\subsection{Cramer-Rao Inequality}

There exists a concept of \textbf{efficiency} evaluated by a special case of Cramer-Rao inequality (ex-eq. 9.8).
\begin{enumerate}
\item gives a lower bound on the variance of \textbf{any} unbiased $\hat \theta$. 
\item The quantity $n \cdot I(\theta)$ = usually called Fisher information matrix. \# MLE \# sufficiency
\end{enumerate}
As you get more and more data, you want your estimator to be closer to the truth. -> Consistency. 

\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
