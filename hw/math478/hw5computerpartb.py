import numpy as np
import matplotlib.pyplot as plt
plt.figure()

xlist = np.linspace(-6.0, 6.0, 600)
ylist = np.linspace(-6.0, 6.0, 600)
levels = np.linspace(-1, 1, 3)
def Circle(x, y):
    return x*x + y*y

X, Y = np.meshgrid(xlist, ylist)

Z = X + 1j* Y

# Euler/RK1
#phi = 1 + Z

# RK2
#phi = 1 + Z + 0.5*np.power(Z, 2)

# RK3
#phi = 1 + Z + 0.5*np.power(Z, 2) + (1/6)*np.power(Z, 3)

# RK4
#phi = 1 + Z + 0.5*np.power(Z, 2) + np.power(Z, 3)/6 + np.power(Z, 4) / 24
#rg = abs(phi)
# endRK4

# implicit/backward Euler
#phi = 1/(1+Z)

# implicit trapezoid rule
#phi = (1+Z/2)/(1-Z/2) 

# AB2 multistep
#phi = 1/2 + (3/4)*Z + np.sqrt(1 + Z + (9/4)*np.power(Z, 2))

# BDF2
phi = 1/np.sqrt(3 - 2*Z)
rg = abs(phi)

cp = plt.contourf(X, Y, rg, levels = levels)

plt.colorbar(cp)
plt.title('BDF2 Stability')
plt.xlabel('Real')
plt.ylabel('Imag')
plt.show()
