from math import cos, sin

def f(y, t):
    # dy/dt
    # takes the current y-value needed
    dydt = -100*(y-cos(t))-sin(t)
    return dydt

def BDF2(y, stepsize, step):
    h = stepsize
    n = step
    denom = 1 + (2/3.)*h*100
    numer = (4/3.)*y[n-1] - (1/3.)*y[n-2] + (2/3.)*h*(-100*cos(h*n) + sin(h*n))
    return (y)
def Euler(y, f_arr, stepsize, step):
    y.append(y[step] + stepsize*f_arr[step])
    return y
# Adams-Bashford 2 method
def AB2(y, f_arr, stepsize, step):
    # y, f are arrays currently size (step+1) and (step) respectively
    # f is the derivative of u w.r.t. t
    h = stepsize
    n = step
    f_arr.append(f(y[n], h*n))
    y.append(y[n] + (h/2.0)*(3.0*f_arr[n] - f_arr[n-1]))
    return (y, f_arr)

#n_total = 200 # number of steps to take between t = 0 and 1

target = open("478HW5Computer.txt", 'w')

total_steps = [5, 10, 20, 50, 100, 200, 500]

target.write('h\t\t\tAB2\t\t\tBDF2\n')
for n in total_steps:
    h = 1.0/n
    yAB2 = yBDF2 = [1] # y(0) = 1
    dydtAB2 = dydtBDF2 = [f(yAB2[0], 0)] # initial dy/dt value
    
    # do one Euler iteration to get a second value to start the AB2 method and BDF2 method
    yAB2 = Euler(yAB2, dydtAB2, h, 0)
    yBDF2 = Euler(yBDF2, dydtBDF2, h, 0)

    for i in range(1, n):

        (yAB2, dydt) = AB2(yAB2, dydtAB2, h, i)
        yBDF2 = BDF2(yBDF2, h, i)
    # write to file; -1 index means last element
    target.write('%f\t\t%f\t\t%f\n' % (h, yAB2[-1], yBDF2[-1]))
#    print(yBDF2)

