from sympy import *

x = Symbol('x')
ci = [1/4.0, 1/2.0, 3/4.0]
lagrange = [1, 1, 1]

for j in range(3):

    for k in range(3):
        if (k != j):
            lagrange[j] *= (x - ci[k])/(ci[j]-ci[k])


b = [0, 0, 0]
a = [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 0]]
for j in range(3):
    b[j] = integrate(lagrange[j], (x, 0, 1))
    for i in range(3):
        a[j][i] = integrate(lagrange[i], (x, 0, ci[j]))
print lagrange
print b
print a

h = (x - 1/4.0)*(x-1/2.0)*(x-3./4)
print(integrate(h, (x, 0, 1)))
print(integrate(h*x, (x, 0, 1)))
