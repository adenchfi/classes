from sympy import *

init_printing(use_unicode=False, wrap_line=False, no_global=True)

z = symbols('z')
th = symbols('th')
bT = Matrix([th, 1-th])
one = Matrix([1, 1])
M = Matrix(( [1, 0], [-z*th, 1-z*(1-th)] ))

Minv = M.inv()
temp = Matrix(Minv.dot(bT))
#print(temp)
r = 1 + z*(temp).dot(one)
#print(simplify(r))

for i in range(-10, 0, 1):
    if (r.subs(z, i).subs(th, 0.6) > 1):
        print(i, r.subs(z, i).subs(th, 0.5))
