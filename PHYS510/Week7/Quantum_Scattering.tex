\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Scattering Theory\\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Quantum Scattering}

We have an initial incoming wave, $\ket{in}$, with the form \[
\ket{in} = e^{ikz}
\] and then it scatters. After scattering, it has the form (in spherical coordinates) \[
\ket{out} = f(\theta) \frac{e^{ikr}}{r} + e^{ikz} = \psi
\]

The incoming plane wave/beam had some particles transmit through the scattering site (hence, the $e^{ikz}$ term) and some scatter and become a spherically outgoing wave.

How do we find the scattering cross section $d\sigma$? Recall $d\sigma$ is
\begin{align*}
  d\sigma = & \frac{\abs{probability,+\infty}}{prob, -\infty} \\
  = & \frac{\abs{\psi - e^{ikz}}^2}{\abs{e^{ikz}}^2} r^2 d\Omega \tag{Prob. of Scat. = Pr(scat)/Pr(incom)}\\
  = & \frac{\abs{f(\theta)e^{ikr}/r}^2}{\abs{e^{ikz}}^2}r^2d\Omega \\
  = & \abs{f(\theta)}^2 d\Omega
\end{align*}

We decide to represent the incoming wave as a superposition of spherical waves. For a free particle, we have
\begin{align*}
  \frac{1}{r^2}\dv[2]{r^2 R_{kl}}{r} + [k^2 - \frac{l(l+1)}{r^2}]R_{kl} \\
  R_{kl} = 2k J_l(kr) \tag{Bessel functions}
\end{align*}
If we have a central potential, we have \[
  \frac{1}{r^2}\dv[2]{r^2 R_{kl}}{r} + [k^2 - \frac{l(l+1)}{r^2}\frac{2mU}{\hbar^2}]R_{kl} 
\]
Asymptotically, as $r\rightarrow \infty$, we have $rU(r) \rightarrow 0$. We have, from the Bessel function definition, 
\begin{align*}
  R_{kl} \approx \frac{2}{r}\sin(kr - nl/2 + \delta_l)
\end{align*}
where the $\delta_l$ comes from the fact that we can only change the phase of our solutions, and keep the probability the same.

If we have a central potential, and we take the asymptotic form, we have
\begin{equation}
  \frac{1}{r}(rR_{kl})'' + k^2 R_{kl} = 0
\end{equation}

\begin{remark}
  It's enough to know the asymptotic form of our wavefunction, and therefore Schrodinger equation. This means we don't have to find an exact solution for all time; often the asymptotic form is good enough. 
\end{remark}

We choose the axis $z$ such that we can represent the incoming plane wave as
\begin{align*}
  e^{ikz} = & e^{ikr\cos\theta} \\
  = & \sum_{l=0}^{\infty} (-i)^l (2l+1)P_l(\cos\theta) \frac{r^l}{k^l}(\frac{1}{r}\dv{}{r})^l \frac{\sin{kr}}{kr}
\end{align*}
Again, we can choose the asymptotic form of this to make life easier. We'd also like the lowest possible exponential on the $r$ on the denominator. So let's ignore higher order $1/r$ terms as $r \rightarrow \infty$, so we do the $l$ derivatives and end up with
\begin{align*}
  e^{ikz} \sim \frac{1}{kr} \sum i^ (2l+1)P_l(\cos\theta) \sin(kr - \frac{\pi l}{2}) \tag{As $r \rightarrow \infty$}
\end{align*}

For a central potential, the spherical Schrodinger equation has wavefunctions of the form
\begin{align*}
  \psi = \sum_{l=0}^\infty A_l P_l(\cos \theta) R_{kl}(r)
\end{align*}
Recall as $r \rightarrow \infty$, our function has the form $e^{ikz} + f(\theta)e^{ikr}/r$, but the second term goes to 0 as $r \rightarrow \infty$ for, say, Coulomb law. We now claim
\begin{align*}
  A_l = \frac{1}{k}(2l+1)i^l e^{i\delta_l}
\end{align*}
\begin{proof}
  \begin{align*}
    R_{kl} \approx & \frac{2}{r}\sin(kr = \pi l/2 + \delta_l) \\
    = & \frac{2}{r} \frac{1}{2i}[e^{i(kr - \frac{\pi l}{2} + \delta_l)}] \tag{$e^{i\pi/2} = -i$} \\
    = & \frac{1}{ir} [(-i)^l e^{i(kr + \delta_l)} - i^l e^{-i(kr + \delta_l)}] \tag{Sub. in $A_l$ from claim}\\
    \psi \approx & \frac{1}{2ikr} \sum_{l=0}^\infty (2l+1)P_l(\cos \theta) \left[e^{ikr}e^{2i\delta_l} + (-1)^{l+1}e^{-ikr}   \right] \\
    e^{ikz} \approx & \frac{1}{2ikr}\sum_{l=0}^\infty (2l+1)P_l(\cos \theta) \left[e^{ikr} + (-1)^{l+1} e^{-ikr} \right] \tag{Above exp. with $\delta_l = 0$}
  \end{align*}
  Now subtract one from the other, since $\psi = e^{ikz} + f(\theta) e^{ikr}/r$. We end up subtracting the incoming spherical wave terms ($e^{-ikr}$). Subtract, and we should get
  \begin{align*}
    f(\theta) e^{ikr}/r \approx & \frac{1}{2ikr} \sum_{l=0}^\infty (2l+1)P_l(\cos \theta)e^{ikr} (e^{2i\delta_l} - 1)
  \end{align*}
  Note we can pull out the $e^{ikr}$, and we get $\frac{e^{ikr}}{r}$ out in front, with \[
f(\theta) = \frac{1}{2ik} \sum_{l=0}^\infty (2l+1)P_l(\cos \theta) (S_l - 1)
\] where $S_l = e^{i2\delta_l}$.
\end{proof}
Thus we have \[
d\sigma = \abs{f(\theta)}^2 d\Omega
\]

\section{Unitary Limit}

Let us calculate a total cross section for $\sigma$.
\begin{align*}
  \sigma = & \int d\sigma = \int \abs{f(\theta)}^2 d\Omega\\
  d\Omega = & \sin\theta d\theta d\phi \\
  \sigma = & 2\pi \int_0^\pi \abs{f(\theta)}^2 \sin \theta d\theta \\
  & \text{We can factor $S_l - 1 = e^{i2\delta_l} - 1$ as $e^{i\delta_l}(e^{i\delta_l - e^{-i\delta_l}}) = 2ie^{i\delta_l}\sin \delta_l$} \\
  = & \frac{2\pi}{4k^2} \int_0^\pi \sum_{l=0}^\infty (2l+1)^2 P_l^2(\cos\theta) 4\sin^2(\delta_l) \sin\theta d\theta \\
  = & \frac{2\pi}{k^2} \sum_{l=0}^\infty (2l+1)^2 \sin^2(\delta_l) \int_{-1}^{1} P^2(x)dx \tag{Recall orthogonality of Legen. poly's} \\
  \sigma = & \frac{4\pi}{k^2}\sum_{l=0}^\infty (2l+1)\sin^2\delta_l
\end{align*}
Recall that the orthogonality of the Legendre polynomials have $\int P^2(x) = 2/(2l+1)$
For every mode $l$, we have \[
\sigma_l = \frac{4\pi}{k^2}(2l+1)\sin^2(\delta_l)
\]
and thus the maximum possible value for each mode is \[
  \sigma_{l, max} = \frac{4\pi}{k^2}(2l+1)
\]
This is known as the \textbf{unitary limit}.

\subsection{Optical Theorem}

\begin{align*}
  f(\theta) = &  \frac{1}{2ik} \sum_{l=0{^\infty (2l+1)P_l(\cos \theta) (e^{2i\delta_l} - 1)}} \tag{Do the same factoring trick as before}\\
  = & \frac{1}{k} \sum_{l=0}^\infty (2l+1)P_l(\cos \theta) e^{i\delta_l} \sin \delta_l
\end{align*}
We now calculate \[
\Im{f(\theta)} = f(\theta) - f^* (\theta)  \frac{1}{k} \sum (2l+1)P_l(\cos \theta) \underbrace{(e^{i\delta_l} - e^{-i\delta_l})}_{= \sin(\delta_l)} \sin \delta_l
\]
which resolves into
\begin{align}
  \frac{1}{k} \sum_{l=0}^\infty (2l+1) P_l(\cos \theta) \sin^2(\delta_l) \tag{Set $\theta = 0; \cos 0 = 1; P_l(1) = 1$} \\
  \Im{f(0)} = \frac{1}{k} \sum_{l=0}^\infty (2l+1)\sin^2(\delta_l) \tag{Looks very much like our $\sigma$} \\
  \sigma = \frac{4\pi}{k} \Im f(0) 
\end{align}
which is the optical theorem. The \textbf{optical theorem} says if you can find $f(\theta)$, then we can find the total cross section $\sigma$. 
% Note to self: Make a shortcut for writing ^\dagger (C-c d maybe)

We will see scattering on a rigid sphere in Quantum Mechanics.

\section{Review}

Recall the second quantization formalism. When we have waves, we can quantize them via the second quantization formalism.

For scattering theory, asymptotically we want the equations for free particles. When we have waves, we can know the form of the particles. However, when the particles scatter (have some strong interaction), the very \textbf{definition} of the particle becomes fuzzy, since we cannot further quantize the system or fully know their form during these strong interactions.
\begin{remark}
  Waves <=> particles. No waves <=> no particles. After scattering, waves <=> particles. 
\end{remark}

\section{Perfectly Rigid Sphere Scattering}

We have the potential
\begin{equation}
  U =
  \begin{cases}
    0 & r > a \\
    \infty & r < a
  \end{cases}
\end{equation}
This is a good approximation if we have neutrons of keV energy, and electrons of MeV energy. Inside, $psi = 0$, because the wavefunction cannot penetrate. Not even tunnel, because the potential is infinite.
\begin{itemize}
\item Inside (r < a): $\psi = 0$
\item Outside (r > a): $R_{kl} (r) = A_l j_l(kr) + B_l n_l(kr)$ (spherical Bessel functions of first and second kind)
\end{itemize}
We can apply our boundary conditions, $\psi(a - \epsilon) = \psi(a + \epsilon) = 0$, yielding
\begin{align*}
  R_{kl} (a) = 0 \\
  A_lj_l(ka) + B_l n_l (ka) = 0 \\
  \implies \frac{B_l}{A_l} = - \frac{j_l(ka)}{n_l(ka)}\\
\end{align*}
When we look at asymptotic behavior ($x \rightarrow \infty$),
\begin{align*}
  j_l(x) \sim \frac{1}{x} \sin(x - \frac{l\pi}{2}) \\
  n_l(x) \sim -\frac{1}{x}cos(x - \frac{l\pi}{2}) \tag{Apply this for $r \rightarrow \infty$}\\
  R_{kl})(r) \sim A_l \frac{\sin(kr - l\pi/2)}{kr} - B_l \frac{\cos(kr - l\pi/2)}{kr} \\
  = \frac{\sqrt{A_l^2 + B_l^2}}{kr}\left[\underbrace{\frac{A_l}{\sqrt{A_l^2 + B_l^2}}}_{\equiv \cos \delta_l} \sin(kr - l\pi/2) + \underbrace{\frac{-B_l}{\sqrt{A_l^2 + B_l^2}}}_{\equiv \sin \delta_l} \cos(kr - l\pi/2)\right] \\
  = \frac{\sqrt{A_l^2 + B_l^2}}{kr} \sin(kr - l\pi/2 + \delta_l) \\
  \tan d_l = \frac{-B_l}{A_l} = \frac{j_l(ka)}{n_l(ka)} \tag{From our boundary conditions}
\end{align*}

So what does this tell us about the partial cross-section $\sigma_l$?
\begin{align*}
  \sigma_l = \frac{4\pi}{k^2}(2l+1)\sin^2\delta_l \\
  = \frac{4\pi}{k^2} (2l+1)\frac{\tan^2(\delta_l)}{1 + \tan^2(\delta_l)} \\
\sigma_l  = \frac{4\pi}{k^2} (2l+1) \frac{j_l^2(ka)}{j_l^2(ka) + n_l^2(ka)}
\end{align*}
What happens in the low-energy (long-wavelength) approximation? Recall $k^2 = 2mE/\hbar^2$. We make the approximation $ka << 1$, and therefore $(ka)^2 << 1$ and hence $2mEa^2/\hbar^2 << 1$, and thus \[
E << \frac{\hbar^2}{2ma^2}
\] and if we calculate the right using $a$ as the nucleus scattering site, $m$ as an electron size, we get $E \approx 10^{-8}$ Joules, or $100 GeV$... Our allowed energies are much less than that (2-3 orders of magnitude), so hundreds of MeV. Lower still if we consider our potential barrier as not being infinite. Also note that $\lambda >> a$ for $ka << 1$. 

When we take $x \rightarrow 0$,
\begin{align*}
  j_l(x) \sim \frac{x^l}{(2l+1)!!} \\
  n_l(x) \sim -\frac{(2l+1)!!}{x^{l+1}} \\
  tan \delta_l \approx \frac{x^{2l+1}}{(2l+1)!!(2l-1)!!} \\
  \delta_l \approx (ka)^{2l+1}
\end{align*}
For $l = 0, ka << 1$,
\begin{align*}
  \sigma_0 \approx \frac{4\pi}{k^2}(ka)^2 \\
  \sigma_0 \approx 4\pi a^2
\end{align*}
In the low-energy (long wavelength) approximation, the particle feels the 'entire surface' of the sphere). However, for $ka >> 1$ (similar calculations), \[
\sigma_0 \approx 2\pi a^2
\] and the particle/wave only feels ``one side'' of the scattering site.

The approximation of an infinite potential wall isn't very good for Coulomb interactions, because of only a $1/r$ dependence. However, the strong nuclear force is a good barrier for that approximation. 

\section{Finite Square Well Potential Scattering}

We have $U = -V_0$ at $r \in [0, a]$, and $U = 0$ for $r > a$. We have $\kappa^2 = \frac{2m(E+V_0)}{\hbar^2}$, since we have \[
\frac{-\hbar^2}{2m} \nabla^2 \psi + V_0 \psi = E\psi
\] and if we convert this to the Helmholtz equation, we have $\kappa^2$. We define
\begin{align*}
  \kappa^2 = \frac{2m(E+V_0)}{\hbar^2} \\
  k^2 = \frac{2mE}{\hbar^2}
\end{align*}
We also have, inside the barrier, \[
R_{kl, in} = B_l j_l(kr)
\] because the Bessel function of the second kind $n_l$ can't exist at $r = 0$, or else we would have an infinite potential. We can say for the outside,
\begin{align*}
  \label{eq:outside}
  R_{kl, out} (kr) = A_l[\cos \delta_l j_l(kr) - \sin \delta_l n_l(kr)] \\
  R_{kl, out} (kr) \sim \frac{A_l}{kr} \sin (kr - \pi l /2 + \delta_l) \tag{Through similar computations as last time}
\end{align*}

\subsection{Boundary Conditions}

We have \[
\frac{\psi'}{\psi} \text{ continuous over boundary}
\]which gives us, when we plug in our expressions for $R_{\kappa l}(\kappa r)$
\begin{align*}
  \frac{\kappa j'_l(\kappa a)}{j_l(\kappa a)} = \frac{k[j'_l(ka) \cos\delta_l - n'_l (ka)\sin \delta_l]}{j_l(ka) \cos \delta_l - n_l(ka) \sin \delta_l} 
\end{align*}
and we define $\gamma_l$ as that ratio,
\begin{equation}
  \gamma_l \equiv   \frac{\kappa j'_l(\kappa a)}{j_l(\kappa a)} 
\end{equation}
We can move terms around and solve for $\tan \delta_l$ as
\begin{equation}
  \tan \delta_l = \frac{k j_l'(ka) - \gamma_l j_l (ka)}{k n_l'(ka) - \gamma_l j_l(ka)}
\end{equation}

\subsection{Limits}

If we take $l = 0$, we have
\begin{align*}
  \gamma_0 = \frac{\kappa j_0'(\kappa a)}{j_0(\kappa a)} \\
  j_0(\kappa a) = \frac{\sin \kappa a}{\kappa a} \\
  j_0' = \frac{\cos(\kappa a)}{\kappa a} \tag{Now plug in to $\gamma_0$; multiply both sides by $a$} \\
  a\gamma_0 = \kappa a \cot(\kappa a) - 1
\end{align*}


% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
