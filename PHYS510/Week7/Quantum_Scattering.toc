\select@language {english}
\contentsline {section}{\numberline {1}Quantum Scattering}{1}{section.1}
\contentsline {section}{\numberline {2}Unitary Limit}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Optical Theorem}{4}{subsection.2.1}
\contentsline {section}{\numberline {3}Review}{5}{section.3}
\contentsline {section}{\numberline {4}Perfectly Rigid Sphere Scattering}{5}{section.4}
\contentsline {section}{\numberline {5}Finite Square Well Potential Scattering}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Boundary Conditions}{8}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Limits}{8}{subsection.5.2}
