\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Quantum Scattering Theory\\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Finite Square Well Potential Scattering}

We have $U = -V_0$ at $r \in [0, a]$, and $U = 0$ for $r > a$. We have $\kappa^2 = \frac{2m(E+V_0)}{\hbar^2}$, since we have \[
\frac{-\hbar^2}{2m} \nabla^2 \psi + V_0 \psi = E\psi
\] and if we convert this to the Helmholtz equation, we have $\kappa^2$. We define
\begin{align*}
  \kappa^2 = \frac{2m(E+V_0)}{\hbar^2} \\
  k^2 = \frac{2mE}{\hbar^2}
\end{align*}
We also have, inside the barrier, \[
R_{kl, in} = B_l j_l(kr)
\] because the Bessel function of the second kind $n_l$ can't exist at $r = 0$, or else we would have an infinite potential. We can say for the outside,
\begin{align*}
  \label{eq:outside}
  R_{kl, out} (kr) = A_l[\cos \delta_l j_l(kr) - \sin \delta_l n_l(kr)] \\
  R_{kl, out} (kr) \sim \frac{A_l}{kr} \sin (kr - \pi l /2 + \delta_l) \tag{Through similar computations as last time}
\end{align*}

\subsection{Boundary Conditions}

We have \[
\frac{\psi'}{\psi} \text{ continuous over boundary}
\]which gives us, when we plug in our expressions for $R_{\kappa l}(\kappa r)$
\begin{align*}
  \frac{\kappa j'_l(\kappa a)}{j_l(\kappa a)} = \frac{k[j'_l(ka) \cos\delta_l - n'_l (ka)\sin \delta_l]}{j_l(ka) \cos \delta_l - n_l(ka) \sin \delta_l} 
\end{align*}
and we define $\gamma_l$ as that ratio,
\begin{equation}
  \gamma_l \equiv   \frac{\kappa j'_l(\kappa a)}{j_l(\kappa a)} 
\end{equation}
We can move terms around and solve for $\tan \delta_l$ as
\begin{equation}
  \tan \delta_l = \frac{k j_l'(ka) - \gamma_l j_l (ka)}{k n_l'(ka) - \gamma_l j_l(ka)}
\end{equation}

\subsection{Zero-Order Scattering}

If we take $l = 0$, we have
\begin{align*}
  \gamma_0 = \frac{\kappa j_0'(\kappa a)}{j_0(\kappa a)} \\
  j_0(\kappa a) = \frac{\sin \kappa a}{\kappa a} \\
  j_0' = \frac{\cos(\kappa a)}{\kappa a} \tag{Now plug in to $\gamma_0$; multiply both sides by $a$} \\
  a\gamma_0 = \kappa a \cot(\kappa a) - 1
\end{align*}

\section{Partial Scattering Coefficients in Scattering}

Recall for a finite potential well,
\begin{align*}
  \tan \delta_l = \frac{k j_l'(ka) - \gamma_l j_l (ka)}{k n_l'(ka) - \gamma_l j_l(ka)} \\
  k^2 =\frac{2mE}{\hbar^2} \\
  \kappa^2 = \frac{2m(E+V_0)}{\hbar^2} \\
  \gamma_l = \frac{\kappa j_l'(\kappa a)}{j_l(\kappa a)}
\end{align*}

\subsection{Zeroth-Order Scattering Cross Section}

If we make the long wave, low energy approximation, then $ka << 1$. Let's define $x \equiv ka$. This implies high harmonics should be suppressed. We then consider
\begin{align*}
  j_0(x) = \frac{\sin x}{x} \\
  j_0'(x) = \frac{\cos x}{x} - \frac{\sin x}{x^2} \\
  n_0(x) = \frac{-\cos x}{x} \\
  n_0'(x) = \frac{\sin x}{x} + \frac{\cos x}{x^2}
\end{align*}
which lets us have for the tangent term, after multiplying top and bottom by $a$,
\begin{align}
  \tan \delta_0 = & \frac{x j_0'(x) - a\gamma_0 j_0(x)}{x n_0'(x) - a\gamma_0 n_0(x)} \nonumber \tag{Let $x \rightarrow 0$; sub in definitions}\\
  = & \frac{x (\frac{\cos x}{x} - \frac{\sin x}{x^2}) - a\gamma_0 \frac{\sin x}{x}}{x (\frac{\sin x}{x} + \frac{\cos x}{x^2}) - a\gamma_0 (-\frac{\cos x}{x})}\Big|_{x \rightarrow 0} \nonumber \\
  = & \frac{-a \gamma_0}{\frac{1}{x} + \frac{1}{x}a\gamma_0} \nonumber \\
  \tan \delta_0 = & -x\frac{a\gamma_0}{1+a\gamma_0}
\end{align}
and therefore
\begin{equation}
  \tan \delta_0 \approx \sin \delta_0 = -(ka)\frac{a\gamma_0}{1+a\gamma_0}
\end{equation}
Recall our total effective partial cross section \[
\sigma_l = \frac{4\pi}{k^2}\sin^2\delta_l (2l+1)
\]
 We have, then,
 \begin{align*}
   a\gamma_0 = & \frac{a \kappa j_0'(\kappa a)}{j_0(\kappa a)} \\
   = & \kappa a \frac{\frac{\cos (\kappa a)}{\kappa a} - \frac{\sin (\kappa a)}{(\kappa a)^2}}{\sin (\kappa a)/(\kappa a)} \\
   = & (\kappa a) \cot (\kappa a) - 1 \\
   \implies \tan \delta_0 \approx \sin \delta_0 = & -(ka) \frac{(\kappa a) \cot(\kappa a) - 1}{(\kappa a)\cot(\kappa a)} \\
   \tan \delta_0 = & (-ka)\left(1 - \frac{\tan(\kappa a)}{\kappa a}\right)
 \end{align*}
 If we only take the first harmonic of $\sigma$, $\sigma_0$, we get
 \begin{align*}
   \sigma_0 \approx & \frac{4\pi}{k^2} (ka)^2 (1 - \frac{\tan (\kappa a)}{\kappa a})^2 \\
   = & 4\pi a^2 \left(1 - \frac{\tan \kappa a}{\kappa a} \right)^2 \tag{Doesn't depend on $k$!}
 \end{align*}
For zero energy, and zero wavelength, we still have some scattering. e.g. even for
\begin{align*}
  k = 0 \\
  \kappa = \sqrt{\frac{2mV_0}{\hbar^2}}
\end{align*}

\subsection{First Order Correction to Scattering}

Here we consider $l = 1$.
\begin{align}
  j_1(x) = \frac{\sin x}{x^2} - \frac{\cos x}{x} \\
  j_1'(x) = \frac{2 \cos x}{x^2} + \frac{\sin x}{x} - \frac{2 \sin x}{x^3} \\
  n_1(x) = -\frac{\cos x}{x^2} - \frac{\sin x}{x} \\
  n_1'(x) = \frac{2\sin x}{x^2} + \frac{2 \cos x}{x^3} - \frac{\cos x}{x} 
\end{align}
Using these, we can write
\begin{align*}
  \tan \delta_1 = \frac{x (2 \frac{\cos x}{x^2} - \frac{2 \sin x}{x^3} + \frac{\sin x}{x}) - a\gamma_a(\frac{\sin x}{x^2} - \frac{\cos x}{x})}{x(\frac{2\sin x}{x^2} + \frac{2\cos x}{x^3} - \frac{\cos x}{x}) - a\gamma_1(-\frac{\cos x}{x^2}  - \frac{\sin x}{x^2})}
\end{align*}
We want to Taylor expand these to second/third order now, and keep the remaining terms, allowing $x \rightarrow 0$. The top left parentheses simplifies to $(1/3)x$, the top right parenthesis yields $(1/3)a\gamma_1 x$, bottom left yields $(2/x^2)$, bottom right yields $a\gamma_1/x^2$.

\begin{align}
  \tan \delta_l = & \frac{(1/3)x - (1/3)a\gamma_1 x}{\frac{2}{x^2} + (a\gamma_1)/x^2} \\
  = & \frac{x^3}{3} \frac{1 - a\gamma_1}{2 + a\gamma_1}
\end{align}
We thus have
\begin{align*}
  \tan d_1 \approx \delta_1 = \frac{(ka)^3}{3}\frac{1 - a\gamma_1}{2 + a\gamma_1}
\end{align*}
and
\begin{align*}
  \sigma_1 \approx & \frac{4\pi }{k^2} \cdot 3 \frac{(ka)^6}{9} \left(\frac{1 - a\gamma_1}{2 + a \gamma_1} \right)^2 \\
  = & \frac{4\pi}{ 3}a^2 (ka)^4 \left(\frac{1 - a\gamma_1}{2 + a\gamma_1} \right)^2
\end{align*}
This means there's no angular dependence of scattering for long wavelengths.
\begin{remark}
  These equations are from approximations; they don't work everywhere. Same for electrodynamics scattering; the dipole approximation for scattering doesn't work everywhere. 
\end{remark}

\section{Born Approximation}

We have the Green's function formalism of quantum mechanical scattering,
\begin{align}
  \psi(\vec r) = e^{i\kappa z} - \frac{1}{4\pi}\frac{2m}{\hbar^2} \int \frac{e^{i\kappa \abs{\vec r - \vec r'}}}{\abs{\vec r - \vec r'}}U(\vec r') \psi(\vec r') d^3 \vec r'
\end{align}
In the Schrodinger Equation, we treat $U(r) \psi$ as a perturbation. Let's write
\begin{align*}
  \abs{\vec r - \vec r'} \approx r - r' \cos \theta
\end{align*}
We get this by making the far-field approximation, $r'/r << 1$, and have expanded the absolute value as \[
\sqrt{(r - r')^2} = \sqrt{r^2 + r'^2 - 2 r r' \cos \theta} = r(1 + \frac{r'^2}{r^2} - 2\frac{r'}{r}\cos \theta)^{1/2} \approx r(1 - \frac{r'}{r} \cos \theta).
\]
This lets us write our scattered wavefunction as
\begin{align*}
  \psi(\vec r) \approx e^{i\kappa z} -\frac{m}{2\pi \hbar^2}\frac{1}{r}e^{i\kappa r} \int e^{-\kappa r' \cos \theta} U(r')\psi(r')d^3\vec r'
\end{align*}
In particular, we ignore the $\cos \theta$ term in the denominator because it's smoothly varying, but when it's in the exponent, we need to account for it, since it leads to large oscillations on the ends. In particular, for small $r$ in $e^{-ikr'\cos \theta}$, this term is of the order 1. 

Thus for our scattering, we can say
\begin{align*}
  f(\theta) = \frac{-m}{2\pi \hbar^2} \int e^{-i\vec \kappa \cdot \vec r'} U(\vec r') \psi(\vec r') d^3 \vec r'
\end{align*} where $\vec \kappa$ = $\kappa \vec n'$. 

\begin{remark}
  Recall our wavefunction form originally comes from the Green's function approach. We have an integral equation with an asymmetric kernel. 
\end{remark}
If $a$ is a typical size of our system, then if $a/r$ is small, we have the far-field approximation.
\begin{remark}
  Landau has a section on the near-field approximation. 
\end{remark}
\begin{remark}
  This is for the scattering to be on a particle of 'infinite' mass. Electron on proton, proton on uranium nucleus, etc.
\end{remark}

We have
\begin{align*}
  \psi(\ov r) = e^{ikz} - \frac{e^{ikr}}{r} \frac{2m}{4\pi \hbar^2} \int e^{-ikr' \cos \theta}U(r') \psi(\ov r') d^3 \ov r'\\
  \implies f(\theta) = -\frac{m}{2\pi \hbar^2} \int e^{-ikr' \cos \theta} U(r') \psi(\ov r') d^3 \ov r'
\end{align*}
where $f(\theta)$ is the same angular dependence seen in the scattering formulae from before.
Keep in mind that this formulation is universal; all it needs is the far-field approximation.

\subsection{Applying the Born Approximation}

The Born approximation is to approximate $\psi(\ov r')$ with $\psi_0(\ov r)$, the free particle wavefunction. We can do this rigorously by expanding $\psi = \psi_0 + \psi_1 + \cdots$, and saying the potential $U(r')$ is such that it times the higher order terms in $\psi$ are small (perturbations).
\begin{remark}
  We can then solve this equation iteratively for $\psi_1$, then plug that in to the integral and solve for $\psi_2$, etc. In fact, we can prove some convergence properties for this method such that it converges to the true solution $\psi(\ov r)$. 
\end{remark}

We treat the potential energy as a perturbation. We have
\begin{align*}
  \nabla^2 \psi + k^2 \psi = \frac{2mU}{\hbar^2} \psi \\
  \psi = \psi_0 + \psi_1 + \cdots
\end{align*}
We say that $U$ times the higher order terms of $U \psi$ are small such that only $U \psi_0$ survives, and we have
\begin{align*}
  (\nabla^2 + k^2)\psi_1 = \frac{2mU}{\hbar^2}\psi_0 \\
\end{align*} Let's solve for the Green's function of this system.
\begin{align*}
  (\nalba^2 + k^2)G(\ov r - \ov r') = \delta(\ov r - \ov r') \\
  G(\ov r - \ov r') = \frac{-1}{4\pi} \frac{e^{ik\abs{\ov r - \ov r'}}}{\abs{\ov r - \ov r'}}
\end{align*}
Using this method of solvation, we get
\begin{align*}
  \psi_1(r) = \frac{-1}{4\pi} \int \frac{e^{ik\abs{r - r'}}}{\abs{r - r'}} \frac{2mU(r')}{\hbar^2}\psi_0(r')d^3r' \\
  \abs{\psi_1(r)} \approx \frac{m}{2\pi \hbar^2}\abs{\psi_0}\abs{U}a^2
\end{align*}
where $a$ is a typical length in our system. 

This means for this to work, $\abs{\psi_1 / \psi_0} << 1$, and therefore \[
\frac{m}{2\pi \hbar^2}\abs{U}a^2 << 1
\] or
\begin{equation}
  \abs{U} << \frac{2\pi \hbar^2}{ma^2}.
\end{equation}
This is kind of like the ground state energy of the infinite potential well. Meaning our potential should be much less than that. Alternatively, consider the uncertainty principle for our system, $pa \sim \hbar$, and thus $\hbar / a \sim p$. We can write our potential requirement, then, as
\begin{equation}
  \abs{U} << \frac{(h/a)^2}{m} = p^2 / m \sim K
\end{equation}
where $K$ is the kinetic energy of our system for the characteristic length scale. In terms of our system of repulsive Coulomb scattering,
\begin{equation}
  \frac{\alpha}{r_{min}} << \frac{m v_\infty^2}{2}
\end{equation} where $\alpha$ is our impact parameter. 

You can also do the Fermi golden rule with perturbation theory to derive the Born approximation. 

With the Born approximation, we write $f(\theta)$ above as
\begin{align}
  f(\theta) = -\frac{m}{2\pi \hbar^2} \int e^{ikr'\cos\theta'}U(r')\psi_0(\ov r')d^3 \ov r'
\end{align}

We introduce a quantity $\vec q = \vec k' - \vec k$ and $\abs{q}^2 = k^2 + k'^2 - 2k k' \cos\theta $ such that we can write 

\begin{align}
  f(\theta) = -\frac{m}{2\pi \hbar^2} \int e^{-i \vec q \cdot \vec r'}U(r')\psi_0(\ov r')d^3 \ov r'
\end{align}

\subsection{Elastic Scattering}

If we are talking about elastic scattering, then $k = k'$, the magnitudes are equal, and thus $\abs{q}^2 = 2k^2(1 - \cos \theta) = 4k^2 \sin^2 (\theta/2)$, or $q = 2k\sin (\theta/2)$.


We can write the differential cross section as
\begin{align*}
  d\sigma = \left(\frac{m}{2m\hbar^2} \right)^2 \abs{\int e^{i\vec q \cdot \vec r} U(r) d^3 r}^2 d\Omega.
\end{align*}
In particular, that's the form of a Fourier transform; if we measure the angular dependence of a scattering event, we can calculate the potential using an inverse Fourier transform of the data. This also goes into finding/calculating form factors for systems.

\subsection{Central Potential}

Here, $U = U(r)$, and we can choose our direction have
\begin{align*}
  \int U(r) e^{i q r \cos \theta'} r^2 \sin \theta' dr d\theta' d\phi \\
  = 2\pi \int U(r)r^2 \int e^{-qr \cos \theta'}\sin \theta' d\theta' dr \\
  = 2\pi \int_0^\infty U(r) r^2 dr \int_{-1}^1 e^{-iqr x}dx \tag{$\cos \theta' = x$} \\
  = 2\pi \int_0^\infty U(r) r^2 \frac{1}{-iqr}(e^{-iqr} - e^{iqr})dr \\
  = 4\pi \int_0^\infty \frac{U(r)r \sin (q r)}{q} dr \\
  = \frac{4\pi}{q} \int_0^\infty U(r)r \sin (qr) dr
\end{align*}
Therefore, we can write
\begin{align}
  d\sigma = \frac{4m^2}{q^2\hbar^4} \abs{\int_0^\inftyU(r) r \sin (qr) dr}^2 d\Omega
\end{align}
And recall the angular dependence lies inside $q$.
\begin{remark}
  This differential cross section looks very much like the form factor in solid state physics, with some constants. 
\end{remark}

% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
