\select@language {english}
\contentsline {section}{\numberline {1}Finite Square Well Potential Scattering}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Boundary Conditions}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Zero-Order Scattering}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Partial Scattering Coefficients in Scattering}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Zeroth-Order Scattering Cross Section}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}First Order Correction to Scattering}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Born Approximation}{5}{section.3}
