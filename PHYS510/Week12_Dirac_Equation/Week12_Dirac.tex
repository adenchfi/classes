\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Dirac Equation \\ Relativistic Mechanics}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}

In 1930, we had a scalar particle field theory, but it didn't really describe the particles we had. For example, in 1930 we knew our particles were spinors, Pauli spinors.

Dirac took the Klein-Gordon equation,
\begin{align}
  E^2 = p^2 + m^2 \tag{c = 1} \\
  (\Box + m^2)\psi = 0
\end{align}
and we substituted the energy and momentum with their operator definitions,
\begin{align}
  E \rightarrow i\hbar \pdv{}{t}\\
  \vec p \rightarrow -i\hbar \nabla
\end{align}

If we take the energy and solve, we find
\begin{align}
  E = \sqrt{\pvec p^2 + m^2}.
\end{align}
How do we find this square root?
We know from special relativity that $p^\mu p_\mu = m^2$, and recall $p^\mu = (E, \vec p)$. 

Dirac introduced Dirac matrices, $\gamma^\mu$. Let $\hat p$ be a quantity such that $\hat p = p^\mu \gamma_\mu$ and $\hat p^2 = m^2$. Let's write
\begin{align}
  (\hat p + m)(\hat p - m) \psi = 0
\end{align}
where $\psi$ is a column vector, and writing more indices, we get
\begin{align}
  ((p^\mu \gamma_\mu)_{ab} - mI_{ab})\psi_B = 0.
\end{align}

We can substitute $p_\mu \rightarrow i\hbar \pdv{x^\mu}$ (it would be negative, but we change sign by using contra-variant $x^\mu$ rather than the co-variant). We then get
\begin{align}
  (i\gamma^\mu \pdv{}{x^\mu} - m)\psi = 0 \tag{Dirac equation; $\hbar, c = 1$}.
\end{align}

We can obtain the Klein-Gordon equation by multiplying both sides on the left by $(i\gamma^\mu \pdv{}{x^\mu} + m)$. Therefore, we can see the Dirac equation is kind of a square root of the Klein-Gordon equation.

Note that $o^\mu \gamma_\mu p^\nu \gamma_\nu = m^2 = p_\mu p^\mu$. Since the contraction yields constants times matrices, we know these quantities commute, and can write it as
\begin{align}
  p^\mu p^\nu (\gamma_\mu \gamma_\nu) = p_\lambda p^\lambda \\
  \frac{1}{2}p^\mu p^\nu (\gamma_\mu \gamma_\nu + \gamma_\nu \gamma_\mu) = \eta_{\mu \nu} p^\mu p^\nu \\
  \gamma_\mu \gamma_\nu + \gamma_\nu \gamma_\mu = 2\eta_{\mu \nu} \tag{By writing $p_\mu = \eta_{\mu \nu}p^\mu$}
\end{align}
We cannot find 2 dimensional matrices that obey this. We can, however, find four-dimensional matrices that obey these anticommutation rules. In this case, we case
\begin{align*} \psi =
  \begin{bmatrix}
    \psi_1 \\ \psi_2 \\ \psi_3 \\ \psi_4
  \end{bmatrix}
\end{align*}
and $\gamma^\mu$ are $4 \cross 4$ matrices. 

The $\psi$ are then Dirac spinors, or bi-spinors. We have a different name (bi-spinors) because they are 4 dimensional. Also, spinors do not transform the same as vectors in Lorentz transformations. 
\subsection{Algebra of Dirac Matrices}

We have
\begin{align}
  \gamma^\mu g^\nu + \gamma^\nu \gamma^\mu = 2\eta^{\mu \nu} \\
  \eta^{\mu \nu} =
  \begin{bmatrix}
    \dmat{1, -1, -1, -1}
  \end{bmatrix}
\end{align}

The standard form is written in block-matrix form,
\begin{align}
  \gamma^0 =
  \begin{bmatrix}
    \dmat{I, -I}
  \end{bmatrix} \\
  \gamma^i =
  \begin{bmatrix}
    0 & \sigma_i \\ -\sigma_i & 0
  \end{bmatrix} \\
  \gamma^5 = i\gamma^0 \gamma^1 \gamma^2 \gamma^3 =
  \begin{bmatrix}
    0 & I \\ I & 0
  \end{bmatrix}
\end{align}
where $\sigma_i$ is the $i$-th spin matrix. We could also write it in chiral form, where $\gamma^0$ and $\gamma^5$ are switched.
$\gamma^4$ is skipped due to historical reasons.

If we have $\mu = \nu = 0$, we have, for the anticommutation relation,
$2(\gamma^0)^2 = 2$ and therefore $(\gamma^0)^2 = 1$, the identity matrix. We also have
\begin{align}
  (\gamma^i)^2 = -I \\
  (\gamma^0)^\dagger = \gamma^0 \\
  (\gamma^i)^\dagger = -\gamma^i = \gamma_i \nonumber \\
  (\gamma^\mu)^\dagger = \gamma_\mu.
\end{align}

We also have $\Tr \gamma^0 = -\Tr (\gamma^i)^2 \gamma^0 = -\Tr \gamma^i \gamma^i \gamma^0$, and we can use the cyclic property of the trace, $\Tr ABC = \Tr BCA = \dotsc$. We therefore have
\begin{align}
  \Tr \gamma^0 & = -\Tr \gamma^i \gamma^0 \gamma^i \\
               & = \Tr \gamma^i \gamma^i \gamma^0 \tag{By using the anticommutation relation} \\
                 & = 0 \tag{In any representation}
\end{align}
The trace of the $\gamma$-matrices is 0 in any representation. We can also calculate
\begin{align}
  \Tr g^\mu \gamma^\nu & = \frac{1}{2}\Tr (\gamma^\mu \gamma^\nu + \gamma^\nu \gamma^\mu)\\
                       & = \frac{1}{2}\Tr (2\eta^{\mu \nu}) \\
                       & = \eta^{\mu \nu} \Tr I = 4\eta^{\mu \nu} \\
  \Tr \gamma^\mu \gamma^\nu & = 4\eta^{\mu \nu}.
\end{align}

If we do it for three, we get 0. This is because $\{\gamma^5, \gamma^5\} = 0$ and we can substitute $(\gamma^5)^2 = I$ into the trace to prove it. In fact, we can prove that the trace of any odd multiple of gamma-matrices is 0. 

We can define the Dirac adjoint spinor,
\begin{align}
  \ov \psi = \psi^\dagger \gamma^0 \\
  \partial_\mu \ov \psi + m \ov \psi = 0
\end{align}
and we can define the Lagrangian
\begin{align}
  \mathcal{L} = i \ov \psi \gamma^\mu \partial_\mu \psi - m \ov \psi \psi \\
  \pdv{\mathcal{L}}{\ov \psi} = i\gamma^\mu \partial_\mu \psi - m\psi = 0.
\end{align}
We may prefer a more symmetric representation, which he wrote down.

We can then calculate a 4-dimensional current and a probability that is positively defined.

\section{Review}

Recall we had the equation
\begin{equation}\label{eq:1}
  i \gamma^\mu \partial_\mu \psi - m\psi = 0
\end{equation}
and the Dirac equation was
\begin{equation}
  (\Box + m^2)\psi = 0.
\end{equation}

We defined $\ov \psi = \psi^\dagger \gamma^0$, and we could then also have
\begin{equation} \label{eq:2}
  i\partial_\mu \ov \psi \gamma^\mu + m\psi = 0.
\end{equation}

We can write the Lagrangian as
\begin{equation}
  \mathcal{L} = \frac{i}{2} (\ov \gamma^\mu \partial_\mu \psi - \partial_\mu \ov \psi \gamma^\mu \psi) - m\ov \psi \psi \tag{Recall $\ov \psi = \psi^\dagger \gamma^0$}.
\end{equation}
We have $U(1)$ symmetry, such that $\psi \rightarrow e^{-i\alpha} \psi$ leaves the Lagrangian unchanged. If we consider Noether's theorem, we must have a conservation law. 

We can multiply \eqref{eq:1} on the left by $\ov \psi$ and \eqref{eq:2} on the right by $\psi$ and subtract, yielding
\begin{align*}
  i[\ov \psi \gamma^\mu \partial_\mu \psi + \partial_\mu \ov \psi \gamma^\mu \psi] = 0 \\
  \implies i \partial_\mu (\ov \psi \gamma^\mu \psi) = 0.
\end{align*}
We then have $\gamma^\mu = \ov \psi \gamma^\mu \psi$ and $\partial_\mu \gamma^\mu = 0$.

\begin{remark}[502]
  Recall our discussions of the Lorentz group in PHYS 502. Under Lorentz transformations, we have objects that transformed as
  \begin{equation}
    \psi_R \rightarrow \exp{\frac{i}{2}\vec \sigma \cdot \vec \theta + \frac{\vec \sigma}{2} \cdot \vec \phi} \psi_R
  \end{equation}
  where the first term is a rotation, and the second is a Lorentz boost. We also have
  \begin{equation}
    \psi_L \rightarrow \exp{\frac{i}{2}\vec \sigma \cdot \vec \theta - \frac{\vec \sigma}{2} \cdot \vec \phi} \psi_L.
  \end{equation}
\end{remark}
  Let us write
  \begin{equation}
    \psi =
    \begin{bmatrix}
      \psi_R \\ \psi_L
    \end{bmatrix}
  \end{equation}
  and notice that $\ov \psi \psi$ is Lorentz invariant. We want to use the Chiral representation, so we write
  \begin{equation}
    \psi^\dagger \gamma^0 \psi = (\psi^\dagger_R, \psi^\dagger_L)
    \begin{bmatrix}
      0 & 1 \\ 1 & 0
    \end{bmatrix}
    \begin{bmatrix}
      \psi_R \\ \psi_L
    \end{bmatrix}
  \end{equation}
  which, after multiplying and simplifying, we can write as
  \begin{equation}
    \psi^\dagger \gamma^0 \psi = \psi^\dagger_L \psi_R + \psi^\dagger_R \psi_L.
  \end{equation}
  We now have parity invariance, such that if we switch the two components (right and left), we have the same expression. These spinors $\psi_R, \psi_L$ are irreducible representations of the Lorentz group. We defined $\psi$ in such a way that it also had parity invariance.

  We can express the two components as
  \begin{align}
    \psi_R = \frac{1 + \gamma_5}{2}\psi \\
    \psi_L = \frac{1 - \gamma_5}{2}\psi
  \end{align}

  If we have a Standard Model Lagrangian, we now have these expressions. Recall that
  \begin{equation}
    \gamma^5 =
    \begin{bmatrix}
      dmat{1, -1}
    \end{bmatrix}
  \end{equation}

  Since we had
  \begin{equation}
    \gamma^0 = \ov \psi \gamma^0 \psi,
  \end{equation}
  we can write
  \begin{align*}
    \psi^\dagger \underbrace{\gamma^0 \gamma^0}_{= I} \psi = \psi^\dagger \psi \\
    = (\psi^\dagger_R, \psi^\dagger_L)
    \begin{bmatrix}
      \psi_R \\ \psi_L
    \end{bmatrix} \\
    = \psi^\dagger_R \psi_R + \psi^\dagger_L \psi_L \\
    = \abs{\psi_R^1}^2 + \abs{\psi_R^2}^2 + \dotsc + \abs{\psi_L^1}^2 + \abs{\psi_L^2}^2 + \dotsc.
  \end{align*}

\subsection{Lorentz Boosts}

Now let us prove that the components $\psi_L, \psi_R$ transform like $\psi$. We look at Lorentz boosts. We can write
\begin{align*}
  \psi_R \rightarrow e^{\vec \sigma_2 \cdot \vec \phi} \psi_R \\
  = (\cosh(\phi/2) + \vec \sigma \cdot \vec n \sinh(\phi/2))\psi_R 
\end{align*} where $\vec n$ is the direction of $\phi$. Also recall that any even power of $\sigma$ is 1, meaning only the odd powers remain on the outside (for the $\sinh$ term). 
Recall that $\cosh(\phi) = \frac{1}{\sqrt{1 - v^2/c^2}} = \gamma$, and he proved that $\cosh(\phi/2) = \sqrt{\frac{1 + \gamma}{2}} = \sqrt{\frac{E+m}{2m}}$ and $\sinh(\phi/2) = \sqrt{\frac{\gamma - 1}{2}} = \sqrt{\frac{E - m}{2m}} $.

  We can further write
  \begin{align*}
    \sinh(\phi/2) & = \sqrt{\frac{\gamma^2 - 1}{2(\gamma + 1)}} \tag{$\gamma^2 - 1 = \frac{\beta^2}{1-\beta^2} = \beta^2 \gamma^2$}  \\
                  & = \frac{\beta \gamma}{\sqrt{2(E/m + 1)}} \\
    & = \frac{m \beta \gamma}{\sqrt{2m(E + m)}}.
  \end{align*}

  Therefore we have
  \begin{align}
    \cosh(\phi/2) = \sqrt{\frac{E+m}{2m}} \\
    \sinh(\phi/2) = \frac{p}{\sqrt{2m(E+m)}} \tag{$p$ is the relativistic momentum}
  \end{align}

  Let us then write
  \begin{align}
    \psi_R(\vec p) = \frac{E+m + \vec \sigma \cdot \vec p}{[2m(E+m)]^{1/2}} \psi_R(0) \\
    \psi_l(\vec p) = \frac{E+m - \vec \sigma \cdot \vec p}{[2m(E+m)]^{1/2}} \psi_L(0)
  \end{align}

  If $\vec p = 0$, then $\psi_L(0) = \psi_R(0)$. Recall that if $\vec p = 0$, then $E = m$.

  Simplifying, we can write
  \begin{align*}
    \psi_R(\vec p) & = \frac{E + m + \vec \sigma \cdot \vec p}{E + m - \vec \sigma \cdot \vec p} \psi_L(\vec p) \\
                   & = \frac{(E+m + \vec \sigma \cdot \vec p)^2}{(E+m)^2 - \pvec p^2} \psi_L(\vec p) \\
                   & = \frac{(E+m)^2 + p^2 + 2(E+m)\vec \sigma \cdot \vec p}{(E+m)^2 - p^2}\psi_L(\vec p) \\
                   & = \frac{(E+m)^2 + (E^2 - m^2) + 2(E+m)\vec \sigma \cdot \vec p}{(E+m)^2 - (E^2 - m^2)} \psi_L(\vec p) \\
                   & = \frac{E^2 + 2mE + E^2 + 2(E+m)\vec \sigma \cdot \vec p}{m^2 + 2Em + m^2} \psi_l(\vec p) \\
                   & = \frac{2E(E+m) + 2(E+m)\vec \sigma \cdot \vec p}{2m(E+m)} \psi_L(\vec p) \\
    \psi_R(\vec p) & = \frac{E + \vec \sigma \cdot \vec p}{m} \psi_L(\vec p) \\
    \psi_L(\vec p) & = \frac{E - \vec \sigma \cdot \vec p}{m} \psi_R(\vec p).
  \end{align*}

  We then can have two equations:
  \begin{align}
    (E+ \vec \sigma \cdot \vec p) \psi_L(\vec p) - m\psi_R(\vec p)= 0 \\
    (E - \vec \sigma \cdot \vec p) \psi_R(\vec p) - m\psi_L(\vec p) = 0.
  \end{align}
  Chiral representation allows us to write the left and right components like this.

  \begin{remark}[Solid State Physics]
  Dirac Equation is used in heavy atom spectroscopy, for example. 
\end{remark}

We can write the previous equations in the matrix form:
\begin{equation}
  \begin{bmatrix}
    -m & p_0 + \vec \sigma \cdot \vec p \\
    p_0 - \vec \sigma \cdot \vec p & -m
  \end{bmatrix}
  \begin{bmatrix}
    \psi_R \\ \psi_L
  \end{bmatrix}
  = 0.
\end{equation}
Recalling the definitions of $\gamma^0$ and $\gamma^i$, we can write this as
\begin{equation}
  (\gamma^0 p_0 + \gamma^i p_i - m)\psi = 0
\end{equation}
and this is the Dirac equation, as $p_\mu = i \pdv{}{x^\mu} = i\partial_\mu$, and thus
\begin{equation}
  \gamma^\mu i \partial_\mu \psi - m \psi = 0. 
\end{equation}

We can see that the Dirac equation is a direct consequence of the transformation properties of left and right spinors under Lorentz boosts.
\begin{remark}
  We call $\vec p \cdot \vec \sigma$ the \textbf{helicity}. For a massive particle, we can have both positive and negative helicities.

  For a massless particle, $(p_0 + \vec \sigma \cdot \vec p) \psi_L = 0$ and $(p_0 - \vec \sigma \cdot \vec p)\psi_R = 0$. This means
  \begin{align*}
    \vec \sigma \cdot \frac{\vec p}{p_0}\psi_L = -\psi_L \\
    \vec \sigma \cdot \frac{\vec p}{p_0} \psi_R = \psi_R
  \end{align*}
which means $\psi_L, \psi_R$ are eigenfunctions of the helicity operator. The helicity cannot change its sign, so massless particles don't have interaction between its left and right spinors. 
\end{remark}

% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
