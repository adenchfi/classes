\documentclass{article}

\input{math_pkgs.tex}
\usepackage{hyperref}

\begin{document}
\title{Multiparticle Systems and Second Quantization \\ Desai Ch. 37}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Second Quantization}

We start with a harmonic oscillator.
\begin{equation}
  \bar{H} = \frac{\hat{p}^2}{2m} + (1/2)m\omega^2 \hat{x}^2 
\end{equation}
with $[\hat{p}, \hat{x}] = -i\hbar$ and $E = \hbar \omega (n+1/2)$.

We can introduce operators:
\begin{equation}
  a = \left(\frac{\hbar}{2m\omega}\right)^{-1/2}x + i\left(\frac{m\hbar \omega}{2}\right)^{-1/2}p
\end{equation}
and
\begin{equation}
  a^* = \left(\frac{\hbar}{2m\omega}\right)^{-1/2}x - i\left(\frac{m\hbar \omega}{2}\right)^{-1/2}p
\end{equation}
with $[a, a^*] = 1$.

Solving for $x$ and $p$, we get
\begin{equation}
  \hat{x} = (\frac{\hbar}{2m\omega})^{1/2}(a^* + a)
\end{equation}
and
\begin{equation}
  \hat{p} = i(\frac{m\hbar \omega}{2})^{1/2} (a^* - a).
\end{equation}

The Hamiltonian can then be represented as
\begin{equation}
  H = (1/2)\hbar \omega (aa^* + a^* a) = \hbar \omega (a^* a + 1/2).
\end{equation}

Now we want to establish the occupation number representation.
Let $\hat{N} = a^\dagger a = N$, a Hermitian operator. We have a set of eigenfunctions denoted $\mathbf{n_i}$ ($n_i$ is the i-th eigenfunction). Fock space is, I believe, the space spanned by the eigenfunctions. 
\begin{align*}
  Na\ket{n} = & a^\dagger a a\ket{n} \\
  = & (aa^\dagger - 1)a \ket{n} \\
  = & a(a^\dagger a - 1) \ket{n} \\
  = & a(n-1)\ket{n} = (n-1)a\ket{n}.
\end{align*}
Similarly, $Na^\dagger \ket{n} = (n+1)a^\dagger \ket{n}$.

If we look at the norm $\norm{a\ket{n}}^2$, we see $\norm{a\ket{n}}^2 = \bra{n}{a^\dagger a}\ket{n} = n\bra{n}\ket{n} = n \geq 0$. 

A bit of algebra yields
\begin{align*}
  a\ket{n} = \sqrt{n}\ket{n - 1} \\
  a^\dagger \ket{n} = \sqrt{n+1} \ket{n + 1}.
\end{align*}

We can apply $a$ again and again, to any vector. Consecutively applying $a$ to $\ket{n}$ yields the $n = 0$ state, or the vacuum state. This means the zero state $\ket{0}$ is the lowest possible energy state. Now let us apply $a^\dagger$ consecutively.
\begin{align*}
  a^\dagger \ket{0} = \ket{1} \\
  a^\dagger \ket{1} = {a^\dagger}^2\ket{0} = \sqrt{2}\ket{2}
\end{align*}
and so on. Therefore, we can build the basis for the Fock space simply through repeated applications of the $a^\dagger$ operator. A Fock space is like a Hilbert space with some tricks. We can represent the vector $\ket{n}$ as
\begin{equation*}
  \ket{n} = \frac{{a^\dagger}^2}{\sqrt{n!}} \ket{0}. 
\end{equation*}

We can then also say $n$ is the number of particles with $\hbar \omega$ energy. Recall for $n=0$, $E = \frac{\hbar \omega}{2}$. For every particle, we add $\hbar \omega$ to the energy. 

Take a linear molecule. We can make a harmonic approximation, and each particle is coupled to each other. But we can diagonalize the Hamiltonian matrix, and then suddenly it's a system of independent harmonic oscillators.
\begin{equation*}
  \hat{H} = \sum_{k} \left(\frac{p_k^2}{2m} + \frac{1}{2}m\omega^2 q_k^2  \right)
\end{equation*}

Just like our creation/annihilation operators, we also have a commutator for the $p_k, q_s$.
\begin{equation}
  \comm{p_k}{q_s} = -i\hbar \delta_{ks}
\end{equation}
with $\comm{P_k}{P_s} = 0$  and $\comm{q_k}{q_s} = 0$. 
Similarly,
\begin{equation*}
  \comm{a_k}{a_s^\dagger} = \delta_{ks}
\end{equation*}
with $\comm{a_k}{a_s} = \comm{a_k^\dagger}{a_s^\dagger} = 0$.

Now define $N_k$ = $a_k^\dagger a_k$.
\begin{equation*}
  N_k\ket{\{n_k\}} = n_k\ket{\{n_k\}}. 
\end{equation*}
where $\{n_k\} = n_1, n_2, \dotsc, n_k, \dotsc$. Then, interestingly enough, we can represent
\begin{equation*}
  \ket{n_1, \dotsc, n_k, \dotsc} = \frac{{a_1^\dagger}^{n_1}, {a_2^\dagger}^{n_2}, \dotsc \ket{0, 0, \dotsc}}{\sqrt{n_1! n_2! \dotsc}}  
\end{equation*}

All this is developed for bosons. This is because we are letting the $n$ range to 2 and above. Fermions will have a different development. 

Say you have a Hamiltonian
\begin{equation*}
  H = \frac{p^2}{2m} + \frac{1}{2}m\omega^2 x^2 + \lambda x^3
\end{equation*}
and you want to calculate the corrections of the energy from the harmonic oscillator due to the last term. You can write this as
\begin{equation*}
  \hbar \omega (a^\dagger a + 1/2) + \lambda\left(\frac{\hbar}{2m\omega} \right)^{3/2} (a + a^\dagger)^3. 
\end{equation*}
Perturbation theory tells us the first-order correction gives us
\[
  E_1 = \lambda \bra{n}\left(\frac{\hbar}{2m\omega} \right)^{3/2} (a + a^\dagger)^3 \ket{n}.
\]
Notice that $(a+a^\dagger)^3 = a^3 + aa^\dagger a^\dagger a + \dotsc$ and then through some algebra,
\[
  \bra{n} a a^\dagger a^\dagger a \ket{n} = n(n+1). 
\]

\section{Lecture Two}

\subsection{Review of Lecture One}
\label{Review of Lecture One}

For bosons, the number of particles are allowed to be any positive number in the same state. 
The vacuum state is, again, $\ket{0, \dots, 0}$. We can write the state
\[\ket{n_1, \dotsc, n_k} = \frac{(a_1^\dagger)^{n_1} \cdots (a_1^\dagger)^{n_k} \ket{0, \dotsc, 0}}{\sqrt{n_1! \cdots n_k!}}
\]
Recall $n$ is not just the energy, but the number of particles here. Recall that for bosons:
\begin{itemize}
\item $\comm{a}{a^\dagger} = 1$. In particular,
  \begin{itemize}
  \item $\comm{a_i}{a_k^\dagger} = \delta_{ik}$
  \item $\comm{a_i, a_k} = 0$
  \end{itemize}
\item $N_k = a_k^\dagger a_k$, and
\item $N_k\ket{n_1, \dotsc, n_k, \dots} = n_k\ket{n_1, \dotsc, n_k, \dots}$
\end{itemize}

\subsection{Fermions}

For fermions, the occupation numbers $n_k = 0, 1$, unlike bosons. We have
\[
  a_k^\dagger \ket{0, \dotsc, 0, \dotsc} = \ket{0, \dotsc, 1, \dotsc}.
\]
It turns out that $N_k$ in this spot is a nilpotent operator.
\begin{align}
  N_k \ket{n_1, \dotsc, n_k, \dotsc} = \\
  n_k \ket{n_1, \dotsc, n_k, \dotsc} = \\
  N_k^2 \ket{n_1, \dotsc, n_k, \dotsc} = \\
  n_k^2 \ket{n_1, \dotsc, n_k, \dotsc} = \\
  n_k \ket{n_1, \dotsc, n_k, \dotsc} = \\
\end{align}
and therefore $N_k^2 = N_k$. Recall $(a_k^\dagger)^2 = 0$ and $(a_k)^2 = 0$.

\begin{remark}[Tangent on Mathematics]
  Mathematicians have used techniques well before physicists used them. Anticommutative algebras were also developed far before physicists used them for fermions and Poisson brackets. 
\end{remark}

%\hline
Imagine we have a two particle state,
\[
  \ket{0, \dotsc, \underbrace{1}_k, \dotsc, \underbrace{1}_j, \dotsc} = a_k^\dagger a_j^\dagger \ket{0, \dotsc, 0, \dotsc, 0, \dotsc}.
\]
We can also reverse the indices $j, k$ and show that $\{ a_k^\dagger, a_j^\dagger\} = 0$, where $\{,\}$ indicates the anticommutation operator. The square of an anticommutative number/operator is equal to zero.

\begin{remark}[Alternative Notation]
  $\{a_k^\dagger, a_j^\dagger\}$ may also be written as $\comm{a_k^\dagger}{a_j^\dagger}_+$ or $\acomm{a_k^\dagger}{a_j^\dagger}$
\end{remark}

Now we want to know if $\acomm{a_k}{a_j^\dagger} = \delta_{ik}$. But we can observe
\begin{align*}
  N_k^2 (a_k^\dagger a_k)(a_k^\dagger a_k) = & a_k^\dagger (-a_k^\dagger a_k 1)a_k \\
  = & a_k^\dagger a_k \\
  a_k^\dagger a_k + a_ka_k^\dagger = 1
\end{align*}

Since we know the properties of our ladder operators, we can write
\begin{align}
  a^\dagger \ket{n} = (1-n)\ket{n_1} \\
  a\ket{n} = n\ket{n-1}
\end{align}

and
\begin{align*}
  aa^\dagger \ket{n} = (1-n^2)\ket{n} \\
  a^\dagger a \ket{n} = n(2-n)\ket{n}.
\end{align*}
The two above equations indicate that reversal of operators yields the opposite outcome. It also indicates that
\[
  a^\dagger a + aa^\dagger = 1.
\]

We can develop the analogous operators for a multiparticle state, and $a_k, a_k^\dagger$.

\paragraph{Harmonic Oscillator}
Consider a modified harmonic oscillator
\[
  H = \frac{p_x^2 + p_y^2}{2m} + \frac{1}{2} m\omega^2 (x^2 + y^2) + \alpha xy.
\]
We will now have off-diagonal terms, but you can diagonalize it and represent the Hamiltonian in terms of its eigenvectors by replacing $x, y$ with the eigenvectors expressed in terms of $x, y$.

\begin{example}[Long chained oscillator]
  We consider a chain of atoms all with mass $m$. For small changes in the long chain, we approximate the interactions as a series of springs connected via the masses. Quantum chains are relatively new. Write the Lagrangian
  \begin{equation}
    \sum_{i=1}^{i=N} \frac{m}{2}\dot{x}_i^2 - \sum_{i=1}^{i=N+1} \frac{k}{2}(x_i - x_{i-1})^2
  \end{equation}
  We may only be considering 1D, but 2D, 3D can be considered too, with only a bit more math. Consider the boundary conditions $x_{N+1} = 0, x_0 = 0$. 
  When we consider Lagrange's equations, recall that when we differentiate $x_i$, it happens twice - once for each time that $x_i$ appears in the sum.
  The $n$-th Lagrange equation is then
  \begin{align}
    &\ddot{x}_n + \frac{k}{m}(x_n - x_{n-1} + x_n - x_{n+1}) = 0 \\
    = & \ddot{x}_n + \frac{k}{m}(2x_n - x_{n-1} - x_{n+1}) = 0
  \end{align}

  Notice that the last term looks like the discrete approximation to the 2nd derivative with error $(n^2)$. The same would be true in 2D/3D. Therefore we have a form of the wave equation that is discrete in the spatial dimension. A solution is
  \[
    x = Ae^{-i\omega t \pm iqna}
  \]
  which is an example of Bloch's theorem. $na$ is an x-distance $x_n$, and $q$ is a momentum.

  Substituting our solution into the equation, we get
  
  \begin{align*}
    -\omega^2 + \frac{k}{m}(2 - e^{-iqa} - e^{+iqa}) = 0 \\
    \omega^2 = \frac{2k}{m}(1-\cos{qa}) \\
    = & \frac{4k}{m} \sin^2{qa/2} \quad \text{This is the dispersion law} \\
    x_n(+) = (c_1 e^{iqna} + c_2 e^{-iqna})e^{-iwt}
  \end{align*}
  Applying some boundary conditions. $x_0 = 0$  yields $c_1 + c_2 = 0$. Therefore we get
  \[
    x_n(+) = Ce^{-i\omega t} \sin{qna}
  \]
  Applying $x_{n+1} = 0$, we get $qa(N+1) = \pi s$. Therefore
  \[ q = \frac{\pi s}{a(N+1)} \rightarrow q = \frac{\pi s}{a(N+1)}\]
  and thus
  \begin{equation}
    \omega^2 = \frac{4k}{m} \sin^2{\frac{\pi s}{2a (N+1)}}
  \end{equation}
  If we look at $N \rightarrow \infty$, then $q_N \rightarrow \pi/a$. Then the frequency $\omega \rightarrow 2\sqrt{\frac{k}{m}}$, and that defines the first Brillouin zone.

  \textbf{Next time} we will get phonons for one dimension. 
  
\end{example}
% Note to self: Make a shortcut for writing ^\dagger (C-c d maybe)
% OR you can just write ^+ and search/replace later
\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
