\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Relativistic Mechanics\\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Introduction}

Recall we go from 3D Euclidean space to 4D Minkowski space, $(\vec r, t) = (ct, x, y, z)$. We have $x^\mu = (ct, x, y, z)$ for $\mu = 0, 1, 2, 3$. Our invariant is $ds^2 = c^2 dt^2 - dr^2 = (dx^0)^2 - (dx^1)^2 - (dx^2)^2 - (dx^3)^2$ . We can either introduce a complex time to have the same sign, which is known as a Wick rotation in Quantum Field Theory.

We can have contra-variant variables, $x^\mu = (ct, x, y, z)$, or co-variant vectors $x_\mu = (ct, -x, -y, -z)$. We convert between the two with a metric
\begin{equation}
  x_\mu = \eta_{\mu \nu}x^\nu
\end{equation}
where \[
\eta_{\mu \nu} =
\begin{bmatrix}
\dmat{1, -1, -1, -1}
\end{bmatrix}
\]
We can now write $ds^2 = dx^\nu dx)\nu = \eta_{\mu \nu}dx^\mu dx^\nu$. 
\subsection{Relativistic Particle}

What should an action be for a relativistic particle? The simplest Lorentz-invariant object is
\begin{align}
  S_p = \alpha \int ds = \alpha c\int \sqrt{1 - (v/c)^2} dt \tag{$\alpha$ is a constant}
\end{align}
\begin{remark}
  Fermi in 1956-1957 proposed the V-A interaction, the first quantitative model for the weak interaction. Frolich integral. Works for low energies; at higher energies we must consider the W-bosons and Z-boson, but at low energies we can integrate over those degrees of freedom 
\end{remark}
\begin{align*}
  L = \alpha c\sqrt{1 - (v/c)^2} \approx \alpha c - \alpha \frac{v^2}{2c^2} \tag{Binomial expansion for low velocity}
\end{align*} and note this means  $\alpha = -mc$. We then have the Lagrangian
\begin{align*}
  L = -mcs^2 \sqrt{1 - (v/c)^2} - U \\
  p_i = \pdv{L}{\dot q_i} \\
  \vec p = \frac{m \vec v}{\sqrt{1 - (v/c)^2}}
\end{align*}
We want to note the energy is
\begin{align*}
  E = \sum \dot q_i \pdv{L}{\dot q_i} - L \\
  \vec p \cdot \vec v - L = \frac{mc^2}{\sqrt{1 - (v/c)^2}}.
\end{align*}
We can write
\begin{align*}
  u^\mu = \dv{x^\mu}{s} \\
%  u^\mu = \dv{x^\mu}{\tau} = \frac{1}{\sqrt{1 - (v/c)^2}} \dv{x^\mu}{t} \tag{cd\tau = ds}
\end{align*}
We can introduce an energy-momentum vector
\begin{align*}
  P^\mu = mu^\mu = (E/c, \vec p) \\
  P^\mu P_\mu = (E/c)^2 - p^2 = m^2 c^2
\end{align*}

We can see that we now have an expression for the energy,
\begin{align}
  E^2 = m^2 c^4 + p^2 c^2 \\
  E = \pm \sqrt{m^2 c^4 + p^2 c^2}.
\end{align}
This means that our Hamiltonian is
\begin{equation}
  H = \sqrt{m^2 c^4 + p^2 c^2}
\end{equation}
and we can use our quantization rule,
\begin{align*}
  E = i \hbar \pdv{}{t} \\
  \vec p = -i \hbar \nabla. 
\end{align*}
We have $E^2 - p^2 c^2 - m^2 c^4 = 0$, and let's apply this to some scalar function:
\begin{align*}
  \left[(i \hbar \pdv{}{t})^2 - c^2(i\hbar \nabla)^2 - m^2 c^4 \right] \psi = 0 \\
  (\frac{1}{c^2} \pdv[2]{}{t} - \nabla^2) \psi + \frac{m^2 c^2}{\hbar^2}\psi = 0
\end{align*}
For a massless particle, we have the wave equation. Note that
\begin{equation}
  \frac{1}{c^2}\pdv[]{}{t} - \nabla^2 = \frac{\partial^2}{\partial x_\mu \partial x^\mu} = \Box 
\end{equation}
which is the d'Ambertian operator. We have the Klein-Gordon-Fock equation:
\begin{equation}
  \frac{1}{c^2} \pdv[2]{\psi}{t} - \nabla^2 \psi + \frac{m^2 c^2}{\hbar ^2} \psi = 0
\end{equation}
We should check if the solutions to this are fine with our probabilistic interpretation. 
\subsection{A Glance Back at Non-relativistic QM}

We should look back at Non-relativistic quantum mechanics first. The Schrodinger equation and its complex conjugate are
\begin{align}
  i\hbar \pdv{\psi}{t} = H\psi \frac{-\hbar^2}{2m}\nabla^2 \psi + U\psi \\
  -i\hbar \pdv{\psi^*}{t} = -\frac{\hbar^2}{2m} \nabla^2 \psi^* + U\psi^*
\end{align}
Let's multiply the top by $\psi^*$ and the bottom by $\psi$, and subtract them. We get
\begin{equation}
  i\hbar (\psi^* \pdv{\psi}{t} + \psi \pdv{\psi^*}{t}) = -\frac{\hbar^2}{2m} \left(\psi^* \nabla^2 \psi - \psi \nabla^2 \psi^* \right)
\end{equation}

Recall we can write
\begin{equation}
  u \nabla^2 v - v\nabla^2 u = \nabla \cdot (u \nabla v - v \nabla u).
\end{equation}

Therefore we the above is
\begin{align*}
  = \frac{\hbar^2}{2m} \nabla \cdot (\psi^* \nabla \psi - \psi \nabla \psi^*).
\end{align*}
Recall the probability conservation law, $\pdv{\rho}{t} + \nabla \cdot \vec j = 0$. If we write $\rho = \abs{\psi^2}$, which is positively defined, we can compare with our expression and identify
\begin{equation}
  \vec j = -i \frac{\hbar^2}{2m} (\psi^* \nabla \psi - \psi \nabla \psi^*).
\end{equation}
This means the probabilistic interpretation is valid for Non-relativistic quantum mechanics. 
\subsection{Relativistic Mechanics}

Now let's try applying the above method to the Klein-Gordon-Fock equation. Same approach, multiply top and bottom by their complex conjugate versions of $\psi$, subtract, things cancel, you get
\begin{align*}
  \frac{1}{c^2} \left(\psi^* \pdv[2]{\psi}{t} - \psi \pdv[2]{\psi^*}{t} \right) - \left(\psi^* \nabla^2 \psi - \psi \nabla^2 \psi^* \right) = 0
\end{align*}
The second expression can be factored similarly to the Non-relativistic case, but the first expression is harder.
\begin{remark}
  Can we treat the first term as a second time derivative of $\abs{\psi^* \psi}$ somehow? 
\end{remark}

We can find that $\psi^* \pdv[2]{\psi}{t} - \psi \pdv[2]{\psi^*}{t} = \pdv{}{t} \left(\psi^* \pdv{\psi}{t} - \psi \pdv{\psi^*}{t} \right)$. Now we cannot simply write $\rho = \abs{\psi}^2$. We must write
\begin{align}
  \vec j = i(\psi^* \nabla \psi - \psi \nabla \psi^*) \\
  \rho = i(\psi^* \pdv{\psi}{t} - \psi \pdv{\psi^*}{t}).
\end{align}
But if we have a complex wavefunction, $\psi = \psi_1 + i \psi_2$, we no longer have a positive-definite probability. To see this, we expand out $\rho$ and see it equals $2(\psi_1 \dot \psi_2 - \psi_2 \dot \psi_1)$. The probabilistic interpretation doesn't work for relativistic quantum mechanics.
\begin{remark}
  Can this connect to solid state? If we have $s + is$ wavefunctions, doesn't this mean the wavefunction is incorrect if we used relativistic quantum mechanics? We don't have a probability density, at least. Consider that Fermi electrons move at like 0.5\% the speed of light. 
\end{remark}
\begin{remark}
  Noether's theorem can play in here. 
\end{remark}

\subsection{Multiparticle probabilistic interpretation}

Our next step is to consider relativistic quantum mechanics as a multiparticle probabilistic interpretation. Since phonons look like photons, can we do second quantization on photons and relativistic particles?

We have the Klein-Gordon-Fock equation. Let's get a Lagrangian. For simplicity, let $\hbar = 1, c = 1$. We have an action,
\begin{align*}
  S = \int d^4 x \mathcal L(\psi, d_\mu \psi, x^\mu) \tag{$d^4x$ is an invariant volume; Lorentz transformations have a Jacobian of $1$}
\end{align*}
We have a Lagrangian
\begin{align*}
  \mathcal{L} = \frac{1}{2} \partial_\mu \psi^* \partial^\mu \psi - \frac{1}{2}m^2 \psi^* \psi \tag{$\partial_\mu = \pdv{}{x^\mu}$} 
\end{align*}
which gives us Euler-Lagrange equations
\begin{align*}
  \partial_\mu \pdv{\mathcal{L}}{\partial_\mu \psi_a} - \pdv{\mathcal{L}}{\psi_a} = 0 \tag{$a$ is a subscript indicating which dimension/degree of freedom}.
\end{align*}

Recall $\pdv{\mathcal{L}}{\partial_\mu \psi^*} = \frac{1}{2}\partial^\mu \psi$ and $\pdv{\mathcal{L}}{\psi^*} = -\frac{1}{2}m\psi$ and now we get
\begin{align}
  \partial_\mu \partial^\mu \psi + m^2 \psi = 0 \\
  \Box \psi + m^2 \psi = 0.
\end{align}
We have phase invariance for transformations $e^{-i\alpha}$ where $\alpha$ is constant. Then we can apply Noether's theorem to this invariance, there should be a conservation law coming with this symmetry. This shift is a rotation in the abstract complex field space, and this invariant brings the conservation law mentioned before, of probability? 

\section{Least Action Principle}

\subsection{Stationary (slow) system}

Schrodinger Action:
\begin{align}
  S = \int (\psi^* H \psi - E \psi^* \psi)dt \\
  \pdv{S}{\psi^*} = H\psi - E\psi = 0
\end{align}

\subsection{Non-stationary (relativistic) system}

\begin{align}
  S = \int \left[i\hbar(\dot \psi^* \psi - \psi^* \dot \psi) + \frac{\hbar^2}{2m} \nabla \psi^*
 \nabla \psi \right] dt - \frac{\hbar^2}{2m} \psi^* \nabla^2 \psi
\end{align}

We can have the KGF equation, or derive it.
\begin{align}
  S = \int (\frac{1}{2} \partial_\mu \psi^* \partial^\mu \psi - (1/2)m^2 \psi^* \psi)
\end{align}
We can have a global gauge transformation, $\psi \rightarrow e^{-i\alpha}\psi$, where $\alpha$ is constant.

We can write $e^{-i\alpha} = \cos\alpha - i\sin\alpha$, and can write $\psi = \psi_1 + i\psi_2$. Applying the $e^{-i\alpha}$ to $\psi$, we can show it's just a rotation of $\psi$, and it is evidence of $U(1)$ symmetry.

\subsection{Real Scalar Field}

If we have
\begin{align}
  \mathcal{L} & = (1/2)\partial_\mu \psi \partial^\mu \psi - (1/2)m^2 \psi^2 \\
  & = \frac{1}{2}\dot \psi^2 - \frac{1}{2}(\nabla \psi)^2 - (1/2)m^2 \psi^2.
\end{align}
We can now define the energy density, same as in classical mechanics:
\begin{align}
  E = \pdv{\mathcal{L}}{\dot \psi} \dot \psi - \mathcal{L} = \dot \psi \cdot \dot \psi - (1/2)\dot \psi^2 + (1/2)(\nabla \psi)^2 + (1/2)m^2 \psi^2  \\
  E = \frac{1}{2}\dot \psi^2 + \frac{1}{2}(\nabla \psi)^2 + \frac{1}{2}m^2 \psi^2 .
\end{align}

Recall we have the Klein-Gordon-Fock equation,
\begin{align}
  \Box \psi + m^2 \psi = 0.
\end{align}
We can Fourier transform (express $\psi$ in terms of its Fourier transform),
\begin{align}
  \psi(x) = \frac{1}{(2\pi)^4}\int e^{-ik_\mu x^\mu} \tilde \psi(k) d^4 k \tag{Sometimes just written as $e^{ikx}$, sadly}
\end{align}

and we can substitute this expression into the KGF equation, defining $k^2 = k_0^2 - \vec k^2$, and we have
\begin{align}
  (-k^2 + m^2)\tilde \psi(k) = 0
\end{align}
and therefore $k^2 = m^2$. This means the mass and space are not independent; this is similar to the energy-momentum relations in classical special relativity.
If our particle has the above relationship with $k$ and $m$, we say it is on the mass shell, or mass surface. Imagine k-space. Since we have a k-space we can define for particles, now we know that for any particle that obeys the KGF equations (which should be all of them?), we now can define ``mass space'', with allowed mass values of particles similar to the k-sphere or k-surface or Fermi surface.

In order to make sure our solutions obey the KGF equation, we write
\begin{align}
  \tilde \psi (k) = (2\pi) \delta(k^2 - m^2) \psi(k)
\end{align}
and we have
\begin{align}
  \psi(x) & = \int \frac{1}{(2\pi)^3} e^{ik_\mu x^\mu} \delta(k^2 - m^2) \psi(k)tag{How? He says it just comes from a substitution} \nonumber \\
          & = \frac{1}{(2\pi)^3} \int e^{ik_0 x^0 - i\vec k \cdot \vec x} \delta(k_0^2 - \vec k^2 - m^2) \psi(k) \tag{$k_0 = \pm \sqrt{\vec k^2 + m^2}$}
\end{align}

Recall
\begin{align} \label{Identity}
  \int f(x) \delta(g(x))dx & = \int f(x) \delta(g) \dv{g}{g'(x)} \\
                           & = \sum_i \frac{f(x_i)}{\abs{g'(x_i)}} \\
  g(x_i) & = 0
\end{align}
which gives us $k_0 = \pm \sqrt{\vec k^2 + m^2}$ above. 
We get equations for a paraboloid in 3D (both positive and negative, since $k^2 = m^2$) and they are Lorentz-invariant. Let us consider the two solutions to the KGF equation, with the two different values of $k_0$.
\begin{align}
  \psi^+ = \frac{1}{(2\pi)^3} \int e^{ik_\mu x^\mu} \psi^+ \delta(k^2 - m^2) \tag{$k^0 = \sqrt{\vec k^2 + m^2}$} d^4 k \\
  \psi^- (x) = \frac{1}{(2\pi)^3}\int e^{-ik_\mu x^\mu} \delta(k^2 - m^2) \psi^- (k) d^4 k
\end{align}
Since $d^4k$ is an invariant (equals $dk_0 dk_1 dk_2 dk_3$). We can invert $k^\mu \rightarrow -k^\mu$ to switch from one to the other $psi$.

Now, recall \eqref{Identity}. If we rewrite this and take the derivative, we get a $2k_0$, and we have
\begin{align}
  \psi^+ (x) = \int \frac{1}{(2\pi)^3 2k_0} e^{ikx} \psi^+ d^3 \vec k \\
  \psi^- (x) = \int \frac{1}{(2\pi)^3 2k_0} e^{-ikx} \psi^- d^3 \vec k \\ \tag{Recall $kx \equiv k_\mu x^\mu$}.
\end{align}
Now, we want to write the energy density in a way such that we can recognize the harmonic oscillator, and can perhaps rewrite it in the second quantization scheme.
\begin{align*}
  E & = \frac{1}{2} \int \left[(\dot \psi^+ + \dot \psi^-)^2 + (\nabla \psi^+ + \nabla \psi^-)^2 + m^2(\psi^+ \psi^-)^2  \right] d^3 \vec x \\
    & = \frac{1}{2} \int \Big[ 2\left( \dot \psi^+ \psi^- + \nabla \psi^+ \cdot \psi^- + m^2 \psi^+ \psi^-\right) \\
  & \quad (\dot \psi^+)^2 + (\nabla \psi^+)^2 + m^2 (\psi^+)^2 + (\dot \psi^-)^2 + (\nabla \psi^-)^2 + m^2 (\psi^-)^2 Big]
\end{align*}
We can take the first term in parentheses(the other terms end up canceling), and take the Fourier transform of that,
\begin{align*}
  & \dot \psi^+ \psi^- + \nabla \psi^+ \cdot \psi^- + m^2 \psi^+ \psi^- \\
  & = \int \frac{1}{(2\pi)^6 \sqrt{2k_0} \sqrt{2k_0'}} \left((ik_0)(ik_0') + (-i\vec k) \cdot (i\pvec k') + m^2 \right) e^{ikx - ik'x} \psi^+ (\ov k) \psi^-(\ov k') .
\end{align*}
Recall $e^{ikx - ik'x} = e^{i(k_0 - k_0')x_0} e^{-i(\vec k - \pvec k')\cdot \vec x}$. We can recall as well that
\begin{align}
  \int e^{-i(\vec k - \pvec k')\cdot \vec x} d^3 \vec x = (2\pi)^3 \delta(\vec k - \pvec k'),
\end{align}
and thus it sets $\vec k = \pvec k'$. If we use this in our big integral for the first line, and recall the relationship between $k_0$ and $\vec k$, we also know $k_0 = k_0'$. We then get
\begin{align}
  E & = \int \frac{1}{(2\pi)^3} \frac{2k_0^2}{2k^0} \psi^+ (\vec k) \psi^- (\vec k) d^3 \vec k \tag{Co- and contra- variant are equal} \\
  E & = \int \frac{1}{(2\pi)^3} k_0 \psi^+ (\vec k) \psi^-(\vec k) d^3 \vec k.
\end{align}
We can interpret $k_0$ as the energy of a particle, and the two $\psi^\pm$ as creation and annihilation operators, with $k_0 = \sqrt{\vec k^2 + m^2}$. It's similar to the form $E = \int \epsilon a_k a_k^\dagger$. This means $\psi^\pm (\vec k) = \frac{\psi^\pm (k)}{\sqrt{2k_0}}$.
\begin{remark}
  We won't deal with quantum field theory beyond this, with the commutators, etc. Next we will consider interactions between particles of the form of electromagnetic interactions. 
\end{remark}

% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
