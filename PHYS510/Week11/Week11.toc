\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Relativistic Particle}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}A Glance Back at Non-relativistic QM}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Relativistic Mechanics}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Multiparticle probabilistic interpretation}{5}{subsection.1.4}
\contentsline {section}{\numberline {2}Least Action Principle}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Stationary (slow) system}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Non-stationary (relativistic) system}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Real Scalar Field}{6}{subsection.2.3}
