\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Quantum Scattering Theory\\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Finishing Quantum Scattering - Born Approximation}

Recall from last week that we have 
\begin{align}
  d\sigma = \frac{4m^2}{q^2\hbar^4} \abs{\int_0^\infty U(r) r \sin (qr) dr}^2 d\Omega
\end{align}
And recall the angular dependence lies inside $q$.
We have
\begin{align}
  \vec q = \vec k' - \vec k \\
  q^2 = k'^2 + k^2 - 2k k' \cos \theta
\end{align}
For elastic scattering, $k'^2 = k^2$ and
\begin{align*}
  q^2 = 2k^2(1-\cos \theta) \\
  q^2 = 4k^2 \sin^2(\theta/2) \\
  q = 2k\sin (\theta/2)
\end{align*}

If $U(r)$ is a central potential, we can, like last time, simplify to get
\begin{align*}
  d\sigma = \frac{4m^2}{q^2 \hbar^4} \abs{\int_0^\infty U(r) r \sin(qr) dr}^2 d\Omega
\end{align*}

If we have a spherical well (finite well in $r$) with spacing $a$ and potential $-V_0$.
We then get
\begin{align*}
  \sqrt{d\sigma} \propto & \int_0^a (-V_0) r\sin(qr) dr \tag{let $x = qr$} \\
  = & \frac{-V_0}{q^2}\int_0^{qa} x \sin(x) dx \tag{Integrate by parts} \\
  = & -\frac{V_0}{q^2} \left[\sin(qa) - (qa)\cos(qa) \right] \\
  d\sigma = & \frac{4m^2}{q^2 \hbar^2} \frac{V_0^2}{q^4} \left[\sin(qa) - (qa)\cos(qa) \right]^2 \\
  d\sigma = & \frac{4m^2 V_0^2}{q^6 \hbar^4} [\sin(qa) - (qa)\cos(qa)] \tag{Let's factor out an area; mult. top, bot. by $a^6$} \\
  = & a^2 (\frac{2mV_0}{\hbar^2}a^2)^2 \left[\frac{\sin(qa) - (qa)\cos(qa)}{(qa)^3} \right]^2
\end{align*}
\begin{remark}
  Can we say we could model tunneling of nuclei out of the nucleus with this formulation? e.g. potential well and then Coulomb force after that.
  
  \textit{Answer: } No. Tunneling is not scattering, according to Shylnov. 
\end{remark}

\section{Yukawa Potential}

Let
\begin{equation}
  U(r) = \frac{\alpha}{r} e^{-r/a}.
\end{equation}
Recall that photons mediate the electromagnetic force, but people used to think pi-mesons (pions) mediated the strong nuclear force. We have $a \propto \frac{1}{m}$, where $a$ is kind of a distance over which the potential decays. As $m \rightarrow 0, a \rightarrow \infty$, we get the Coulomb potential. We can model this potential in scattering as
\begin{align*}
  \sqrt{d\sigma} \propto & \int_0^\infty U(r) \sin(qr) r dr \\
  = & \int_0^\infty \alpha e^{-r/a} \sin(qr) dr \tag{Let $x \equiv qr$, $\rho = qa$} \\
  = & \frac{\alpha}{q} \int_0^\infty e^{-x/(qa)} \sin(x) dx \tag{Either int. by parts twice, or rep. sin as complex} \\
  = & \frac{\alpha}{2iq} \int_0^\infty e^{-x/\rho} (e^{ix - e^{-ix}}) dx \\
  = & \frac{\alpha}{2iq} \int_0^\infty [e^{-x(1/\rho - i) - e^{-x(1/\rho + i)}}]dx \\
  = & \frac{\alpha}{2iq} \left[\frac{1}{1/(\rho) - i} - \frac{1}{(1/\rho) + i} \right] \\
  = & \frac{\alpha}{2iq} \frac{2i}{(1/\rho)^2 + 1} \\
  = & \frac{\alpha}{q}\frac{\rho^2}{1 + \rho^2} \\
\end{align*}
Recall what $\rho$ is defined as. We have
\begin{align}
  d\sigma = & \frac{4m^2}{q^2 \hbar^4} \frac{\alpha^2}{q^2} \left(\frac{(qa)^2}{1 + (qa)^2} \right)^2 d\Omega \\
  d\sigma = & a^2 \underbrace{\left(\frac{2m\alpha a}{\hbar^2} \right)^2}_{\text{Dimensionless}} \frac{d\Omega}{(1+(qa)^2)^2}
\end{align}
Letting $a \rightarrow \infty$, we ignore the 1 on the denominator, and get
\begin{align}
  d\sigma =& \left(\frac{2m\alpha}{\hbar^2 q^2}\right)^2 d\Omega \tag{Recall  defn of $q$}\\
  d\sigma = & \frac{4m^2 \alpha^2}{\hbar^4 k^4 2^4 \sin^4(\theta/2)} d\Omega \tag{Recall $mv = \hbar k = p$, go to classical limit}\\
  d\sigma = & \frac{4 m^2 \alpha^2 }{16(\frac{(\hbar k)}{m})^4 m^4} \frac{d\Omega}{\sin^4(\theta/2)} \\
  d\sigma = & \left(\frac{\alpha}{2mv^2} \right)^2 \frac{d\Omega}{\sin^4(\theta/2)} 
\end{align}
This is exactly the Rutherford scattering formula!

\section{Scattering by a Coulomb Field}

Landau solves the Coulomb scattering problem in fine detail. We won't go in depth here, but
\begin{equation}
  U(r) = \frac{Z_1 Z_2 e^2}{r}
\end{equation}
We use a parabolic system of coordinates $(\xi, \eta, \phi)$. In this way,
\begin{align}
  x = \sqrt{\xi \eta} \cos \phi \\
  y = \sqrt{\xi \eta} \sin \phi \\
  z = \frac{1}{2}(\xi - \eta)
\end{align}
For $z < 0$, we have $\psi = e^{ikz}$. We need to use confluent hypergeometric functions, apply boundary conditions, and you eventually get the asymptotic behavior of our system as
\begin{equation}
  \psi \sim [1 + \frac{1}{k^3 r (1 - \cos a)}] e^{ikz + \frac{i}{k} \ln(kr(1 - \cos \theta))} + f(\theta) \frac{e^{ikr - \frac{i}{k}\ln(kr(1 - \cos \theta))}}{r}
\end{equation}
where
\begin{equation}
  f(\theta) = \frac{-1}{2k^2\sin^2(\theta/2)} \frac{\Gamma(1+i/k)}{\Gamma(1 - i/k)}e^{2i/k} \ln(\sin(\theta/2))
\end{equation}
These terms destroy normal aymptotic behavior. We no longer have plane waves like before. Why? It's because the Coulomb law goes to zero too slowly - $\lim_{r \rightarrow \infty} U(r) \cdot r \not = 0$

% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
