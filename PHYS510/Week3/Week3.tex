\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Bogolyubov \\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Bogolyubov Transformations}
To eliminate off-diagonal terms from our quantized Hamiltonian, we can use this class of transformations. Like how originally we could write $px$ as linear combinations of ladder operators, we can also write $x_n x_{n-1}$ as much.

Introduce
\begin{align}
  b_k = u_{ki} a_k + v_{ki}a_i^\dagger \\
  b_k^\dagger = v_{ki}^* a_i + u_{ki}^* a_i^\dagger\\
\end{align}
where $u$ and $v$ are matrices in general.

\subsection{Bosons}

Recall that for 1-D, we have
\begin{align*}
  \comm{a}{a^\dagger} = 1 \\
  [a, a] = 0.
\end{align*}
We will also want $b, b^\dagger$ to be annihilation and creation operators.
\begin{align*}
  \comm{b}{b^\dagger} = & [ua+va^\dagger, u^* a^\dagger + v^* a] \\
  = & (u^*a + v^*a^\dagger)(ua + va^\dagger)\\
  = & \abs{u}^2 (a a^\dagger - a^\dagger a) + \abs{v}^2 (a^\dagger a - a a^\dagger) + uv^* (a a - aa) + u^* v(a^\dagger a^\dagger - a^\dagger a^\dagger) \tag{Use comm rel. on $v^2$ coef}\\
  = & (\abs{u}^2 - \abs{v}^2)[a, a^\dagger] + uv^* [a, a] + u^*v [a^\dagger, a^\dagger] = 1 \\
  \implies & \abs{u}^2 - \abs{v}^2 = 1.
\end{align*}
$u$ and $v$ may be complex numbers. However, for simple cases this need not be the case. We can write
\begin{align}
  u = \cosh \lambda \\
  v = \sinh \lambda \\
  b = a \cosh \lambda + a^\dagger \sinh \lambda\\
  b^\dagger = a^\dagger \cosh \lambda + a \sinh \lambda
\end{align}

\subsection{Fermions}

1-D case is trivial - almost equivalent to a phase shift. Because $a^2 = 0, (a^\dagger)^2 = 0$, any Hamiltonian will be quadratic at best. We now introduce
\begin{align}
  b = a^\dagger \\
  b^\dagger = a \tag{Turns electrons into positrons, perhaps}
\end{align}

\subsubsection{2D}

\begin{align}
  b_1 = a_1 \cos \theta - a_2^\dagger \sin \theta\\
  b_2^\dagger = a_1 \sin\theta + a_2^\dagger \cos \theta \\
  b_1^\dagger = a_1^\dagger \cos \theta - a_2 \sin \theta \\
  b_2 = a_1^\dagger \sin \theta + a_2 \cos \theta.
\end{align}

Recall that
\begin{align}
  \acomm{a_i}{a_k^\dagger} = \delta_{ik} \\
  \acomm{a_i}{a_k} = 0 \\
  \acomm{a_i^\dagger}{a_k^\dagger} = 0
\end{align}
for fermions.
\begin{remark}
  If you don't know what transformations to use, try the most general transformations and see what the coefficients have to be (like bosons case before). 
\end{remark}

Given that we want to replicate the above properties, let us see if our $b_i$ fulfill them.
\begin{align*}
  \acomm{b_1}{b_1^\dagger} = & \acomm{a_1 \cos \theta - a_2^\dagger \sin \theta}{a_1^\dagger \cos \theta - a_2 \sin \theta} \\
  = & (a_1 \cos \theta - a_2^\dagger \sin \theta)(a_1^\dagger \cos \theta - a_2 \sin \theta) + (a_1^\dagger \cos \theta - a_2 \sin \theta)(a_1 \cos \theta - a_2^\dagger \sin \theta) \\
  = & (a_1 a_1^\dagger + a_1^\dagger a_1) \cos^2 \theta + (a_2^\dagger a_2 + a_2 a_2^\dagger) \sin^2 \theta \\
  + & (a_1 a_2 + a_2 a_1)(-\sin \theta \cos \theta) + (a_2^\dagger a_1^\dagger + a_1^\dagger a_2^\dagger)(-\sin \theta \cos \theta)\\
  = &\ 1. \tag{After applying our fermion anticommutator properties} 
\end{align*}

Therefore, in 2-D, we have $\abs{u}^2 + \abs{v}^2 = 1$, and the transformations being sin and cos is obvious.
\begin{remark}
  We are not looking for the \textit{complete set} of Bogolyubov transformations, but \textit{any} one. We could use a similar Bogolyubov transformation for fermions as for bosons, but it would be non-linear. 
\end{remark}

\begin{example}
  Let us play with a toy model for superconductivity. 
  $H = \epsilon (a_1^\dagger a_1 + a_2^\dagger a_2) + \lambda (a_1^\dagger a_2^\dagger + a_2 a_1)$ is our Hamiltonian. We can rewrite it as
  \begin{equation}
    \frac{1}{2}
    \begin{bmatrix}
      a_1^\dagger & a_2 &  a_2^\dagger & a_1
    \end{bmatrix}
    \begin{bmatrix}
      \epsilon & \lambda & 0 & 0 \\
      \lambda & -\epsilon & 0 & 0 \\
      0 & 0 & -\epsilon & -\lambda \\
      0 & 0 & -\lambda & \epsilon
    \end{bmatrix}
    \begin{bmatrix}
      a_1 \\ a_2^\dagger \\ a_2 \\ a_1^\dagger
    \end{bmatrix} + \epsilon.
  \end{equation}
  This works if you use the anticommutator properties. Since it's in block-matrix form, diagonalizing it is a lot easier.

  We are looking for eigenvalues $\tilde{\epsilon}$ of $
  \begin{bmatrix}
    \epsilon & \lambda \\ \lambda & -\epsilon
  \end{bmatrix}$. We obtain the equation
  \begin{equation*}
    \det
    \begin{vmatrix}
      \epsilon - \tilde{\epsilon} & \lambda \\
      \lambda & -\epsilon - \tilde{\epsilon} 
    \end{vmatrix}
    = 0
  \end{equation*}
  and therefore
  \begin{align}
    \tilde{\epsilon}^2 - \epsilon^2 - \lambda^2 = 0 \\
    \tilde{\epsilon} = \pm \sqrt{\epsilon^2 + \lambda^2}
  \end{align}.
  \begin{remark}
    We see the eigenvalues (and therefore the eigenenergies) are no longer zero; we contain the so-called ``vacuum energy''. 
  \end{remark}
  We can find a unitary transformation to diagonalize our matrix, $A \rightarrow U^{-1} A' U$.
  The form of $U$ is \[
    \begin{bmatrix}
      \cos \theta & \sin \theta \\
      -\sin \theta & \cos \theta
    \end{bmatrix}
  \]

  Applying the unitary transformation, we obtain
  \begin{equation*}
    \begin{bmatrix}
    \epsilon \cos 2\theta - \lambda \sin 2\theta & \epsilon \sin 2\theta + \lambda \cos 2\theta \\
    \epsilon \sin 2\theta + \lambda \cos 2\theta & - \epsilon \cos 2\theta + \lambda \sin 2\theta
  \end{bmatrix}
  \end{equation*}
  We want to find $\theta$ such that it results in a diagonalized matrix. We find that $\epsilon \sin 2\theta + \lambda \cos 2\theta = 0 \implies \tan{2\theta} = -\frac{\lambda}{\epsilon}$.
  \begin{align*}
    \cos 2\theta = &\frac{1}{\sqrt{1 + \tan^2 2\theta}} \\
    \sin 2\theta = & \frac{\tan 2\theta}{\sqrt{1 + \tan^2 2\theta}} \\
    \implies & \epsilon \cos 2\theta - \lambda \sin 2\theta \\
    = & \frac{\epsilon}{\sqrt{1 + (\lambda/\epsilon)^2}} + \frac{\lambda/\epsilon}{\sqrt{1 + (\lambda / \epsilon)^2}} \\
    = & \frac{\epsilon^2 + \lambda^2}{\sqrt{\epsilon^2 + \lambda^2}} \\
    =& \sqrt{\epsilon^2 + \lambda^2}.
  \end{align*}
  These are the correct eigenvalues, $\tilde{\epsilon}$.

  Therefore our Hamiltonian is
  \begin{equation}
    H = \frac{1}{2}
    \begin{bmatrix}
      b_1^\dagger & b_2 & b_2^\dagger & b_1 
    \end{bmatrix}
    \begin{bmatrix}
      \tilde{\epsilon} & 0 & 0 & 0\\
      0 & \tilde{\epsilon} & 0 & 0 \\
      0 & 0 & \tilde{\epsilon} & 0 \\
      0 & 0 & 0 & -\tilde{\epsilon}
    \end{bmatrix}
    \begin{bmatrix}
      b_1 \\ b_2^\dagger \\ b_2 \\ b_1^\dagger
    \end{bmatrix} + \epsilon
  \end{equation}
  And writing out the product, we get
  \begin{equation}
    H = \tilde{\epsilon} (b_1^\dagger b_1 + b_2^\dagger b_2) + \underbrace{(\tilde{\epsilon} - \epsilon)}_\text{non-negative}
  \end{equation}
  Note the energy gap. 
\end{example}
\begin{remark}
  For bosons, we will be able to do the same, just playing with anticommutators instead of commutators. For bosons, the sign of the energy gap changes from + to - for the $(\tilde{\epsilon} - \epsilon)$ term. 
\end{remark}
\begin{remark}
  We detect particles for diagonal Hamiltonians. When we diagonalize hamiltonians, we get particles we actually detect - what we get for superfluidity, superconductivity, etc. Non-diagonal Hamiltonians have annihilation/creation operators for particles we cannot detect (and perhaps don't exist?). 
\end{remark}
\begin{remark}
  Note that for fermions in this case, we get proper rotations (the classical case), but for bosons we get stuff like Lorentz transformations. 
\end{remark}

Simplest case: 
\begin{align*}
  b_1^\dagger = ua_1^\dagger + va_2 \\
  b_2^\dagger = ua_2^\dagger + va_1
\end{align*}

\section{2nd Lecture - Phonons}
\begin{remark}
  We get a discrete low, take a continuous limit, switch to the continuous case, we get a Lagrange function that leads to the wave equation. We will use second quantization, we a Hamiltonian with off-diagonal terms, and do a Bogolyubov transformation to get a diagonal Hamiltonian.
\end{remark}
\begin{align}
  L = \sum_{n=1}^{N+1} \left(\frac{m}{2} \dot u_n^2 - \frac{k}{2}(u_n - u_{n-1})^2 \right) \tag*{$u_0 = u_{N+1} = 0$}
\end{align}
Continuous limit: $a \rightarrow 0, N \rightarrow \infty$. Introduce $l = aN, x = na$. Is this allowed? Do we lose information by taking the continuous limit?

\begin{align*}
  u_n - u_{n-1} = & \frac{u(x + \Delta x, t) - u(x, t)}{\Delta x} \Delta x \\
  & = \frac{\partial u}{\partial x} a \tag{What is the error introduced by this?}
\end{align*}

\begin{align*}
  \rho = \frac{m}{a} \tag{Linear density}\\
  \sum f(x_n, t) \\
  = & \frac{1}{a} \sum f(x_n, t) a \tag{This is our Riemann integral; recall defn of a} \\
  = & \frac{1}{a} \int_0^\rho f(x, t)dx \\
  \implies & L = \int_0^\rho [\frac{1}{2} \rho (\frac{\partial u}{\partial t})^2 - \frac{k}{2}(\frac{\partial u}{\partial x})^2] dx \\
\end{align*}
The above is integrating our Lagrangian over all space. This means our classic Euler-Lagrange Equations result in
\begin{equation}
  \rho \pdv[2]{u}{t} = k \pdv[2]{u}{x}
\end{equation}
In Quantum, we have
\begin{align*}
  \hat{\mathcal{H}} = & \hat{\pi} \dot{\hat{u}} - \mathcal{L} \\
  \pi = & \pdv{\mathcal{L}}{\dot u} = \rho \pdv{u}{t} \\
  \hat{\mathcal{H}} = & \frac{1}{2\rho} \hat{\pi}^2 + \frac{ka}{2} (\pdv{\hat{u}}{x})^2 \tag{Definition of quantum Hamiltonian} \\
  \comm{\hat{u}(x')}{\pi(x)} = & i\hbar \delta(x - x') \tag{Then do a Fourier transform} \\
  \hat{u}(x, t) = & \frac{1}{\sqrt{\rho}} \sum_q u_q(t) e^{iqx}  \tag{Can turn into integral}\\
  \hat{\pi}(x, t) = & \frac{1}{\sqrt{\rho}} \sum_q \pi_q (t) e^{-iqx} \tag{Needs to be of this form to satisfy comm. relation} \\
  \comm{\hat u_q}{\hat \pi_{q'}} = & i\hbar \delta_{q, q'} \\
  H = & \sum_{q, q'} \frac{1}{\rho} \int_0^\rho [p\frac{1}{2\rho} \pi_q \pi_{q'} e^{-i(q + q')x} + \frac{ka}{2} \hat u_q \hat u_{q'} (-q q') e^{i(q + q')x}]dx \\
  \text{recall} & \frac{1}{\rho} \int_0^\rho e^{i(k-k')x}dx = \delta(x-x') \\
  H = & \sum_{q, q'} (\frac{1}{2\rho} \hat \pi_q \hat \pi_{q'} - \frac{ka}{2} \hat u_q \hat u_{q'} qq')\delta(q + q') \tag{Apply delta function}\\
  = & \sum_q (\frac{1}{2\rho}\hat \pi_q \hat \pi_{-q} + \frac{ka q^2}{2} \hat u_q \hat u_{-q}) \tag{Since delta means $q' = -q$}.
\end{align*}
Recall that for $qa << 1$,
\[
\omega^2_q \approx \frac{k}{m}q^2 a^2
\]
We can also write
\[
\frac{kaq^2}{2} = (1/2) (k/m) m aq^2 = (1/2)(k/m) pa aq^2 = (1/2)\rho (k/m) (qa)^2 = \frac{1}{2} \rho w_q^2
\]
and therefore
\begin{align}
  H = & \sum_q \left[\frac{1}{2\rho} \hat \pi_q \hat \pi_{-q} + \frac{1}{2} \rho \omega_q^2 \hat u_q \hat u_{-q} \right] \\
  \text{define }a_q = & \sqrt{\frac{m\omega_q}{2t}} (\hat u_q + \frac{i}{m\omega_q} \hat \pi_{-q}) \\
  \text{define }a_q^\dagger = & \sqrt{\frac{m\omega_q}{2t}} (\hat u_{-q} - \frac{i}{m\omega_q} \hat \pi_{q}) {These obey bosonic comm. relation}\\
  \comm{\hat u_q}{\hat u_{q'}} = & 0, \comm{\hat \pi_{q}}{\hat \pi_{q'}} = 0 \\
  \comm{\hat u_q}{\hat \pi_{q'}} = i\hbar \delta(q - q')
\end{align}
Small momentum $q$, we get a quadratic approximation for $\omega$. Solving for $H$ in terms of the $a_q$, we get
\begin{equation}
  H = \sum_q \hbar \omega_q (a_q^\dagger a_q + \frac{1}{2})
\end{equation}
\begin{remark}
  Why do we call this \textit{second quantization}?
  With these new operators, we have a wavefunction that is quantized by itself. We introduce operators $a_i, a_k, a_j^\dagger, a_k^\dagger$ such that
  \begin{align}
    \hat \psi (x) = \sum_i a_i \psi_i (x) \tag{Latter WFs are the Schrodinger ones}\\
    \hat \psi^\dagger (x) = \sum_i a_i^\dagger \psi^*_i (x)  \tag{Choose these such that -> } \\
    \comm{\hat \psi (x)}{\psi^\dagger (x')} = \delta(x - x').
  \end{align}
  \begin{remark}
    But crystals aren't continuous. So what's wrong with our assumption of the continuous limit?
    
\subsection{Classical Case}


  \end{remark}
  In the classical case, $H = \sum_{i=-\infty}^{i=\infty} p^2/2m + (K/2)(x_i - x_{i+1})^2$. We introduce creation/annihilation operators
  \begin{align*}
    \hat x_i =& sqrt{\frac{\hbar}{2m_i \omega}} (a_i + a_i^\dagger) \\
    P_i =& sqrt{\frac{\hbar m_i \omega}{2}} \frac{1}{i}(a_i - a_i^\dagger) \tag{Plug this into classical Hamiltonian} \\
    H = & \sum \left[-\frac{\hbar \omega}{4} (a_i - a_i^\dagger)^2 + \frac{\hbar K}{4m\omega} (a_i + a_i^\dagger - a_{i+1} - a_{i+1}^\dagger)^2    \right] \\
    \text{recall } (a_i - a_i^\dagger)^2 = & a_i^2 + (a_i^\dagger)^2 - a_i a_i^\dagger - a_i^\dagger a_i \\
    = & -(a_i\dagger a_i - a_i^2) + \text{ its hermitian conjugate H.C.} \\
  \end{align*}
  \begin{align*}
    \implies H = & \sum \left[ \frac{\hbar \omega}{4} (a_i^\dagger a+i - a_i^2) + \frac{\hbar k}{4m\omega} \left( (a_i - a_{i+1})(a_i^\dagger - a_{i+1}^\dagger) (a_i - a_{i+1})^2 \right) + H.C.    \right] \\
              & \text{Now Fourier Transform this thing} \\
    a_m = & \frac{1}{2\pi}\int_{-\pi}^\pi a_k e^{ikm} dk \approx \sum_k a_k e^{ikm} \\
  \end{align*}
  \begin{align*}
    \implies a_k = & \sum e^{-ikm}a_m \\
    \implies \FT (a_n - a_{n+1}) \rightarrow & (1 - e^{ik}) a_k \\
    \implies \FT (a_n^\dagger - a_{n+1}^\dagger) \rightarrow & (1 - e^{-ik})a_k \\
  \end{align*}
  (Missing a line in the .tex file - debug later)
  \begin{align*}
%    \implies H = & \sum_n \sum_{k, k'} \left[ \frac{k\omega}{4} \underbrace{(a_k^\dagger e_^{-ikn} a_{k'} e^{ik'n} - a_k a_{k'} e^{ikn} e^{ik'n} + H.C.  )}_{from $(a_i^\dagger a_i - a_i^2) term$} + \frac{\hbar k}{4m\omega} \left((1-e^{ik}) a_k(1-e^{-ik})a_{k'}^\dagger e^{ikn} e^{-ikn} + (1-e^{ik}) (1-e^{ik'}) a_k a_{k}e^{ik} + H.C. \right) \right] \\
    & \text{The sum over the k, k' will produce delta functions at the exponentials }
  \end{align*}
  Terms like $a_k a_{-k}$ means we created a particle with momenta $k, -k$ and so conserve momenta. The other terms also make sense. In the end, we get a Hamiltonian that is similar to what we got earlier.
  \begin{remark}[Homework]
    Homework due in two weeks. 
  \end{remark}
\end{remark}
% Note to self: Make a shortcut for writing ^\dagger (C-c d maybe)
% OR you can just write ^+ and search/replace later
\end{document} % END


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
