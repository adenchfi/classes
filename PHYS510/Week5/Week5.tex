\documentclass{article}

\input{/home/adenchfi/useful_scripts_and_templates/LaTeX_Templates/math_pkgs.tex}

\begin{document}
\title{Algebra of ANgular Momenta\\ Desai}
\author{Adam Denchfield}
\date{\today{}}
\maketitle

\tableofcontents

\section{Algebra of Angular Momentum - Cont'd}

\subsection{Review}

Recall that last time, we found \[
[J_i, J_j] = i\epsilon_{ijk}J_k
\]
They form the SO(3) group and the so(3) Lie Algebra. We can define \[
J_\pm = J_x \pm iJ_y
\]
where \[
J_\pm \ket{j, m} \sim \ket{j, m\pm 1}
\].
If we have a Hamiltonian with a central force, \[
\hat H \sim U(r)
\]
then we also have \[
[J_z, J^2] = 0
\]
Also recall
\begin{align*}
  J_z \ket{j, m} = \hbar m\ket{j,m} \\
  J^2 \ket{j, m} = \hbar^2j(j+1)\ket{j, m}
\end{align*}
$m_max$ is $j$.
\begin{align*}
  J_z \ket{j, m_{max}} = \hbar j\ket{j, m_{max}} \\
  J_z \ket{j, j} = j\ket{j, j}
  J^2\ket{j, j} = [(1/2)(J_+ J_- + J_- J_+) + J_z^2]\ket{j, j} \\
  = J_x^2 + J_y^2
\end{align*}
We can put together some of the above statements and recognize that the raising operator cannot go above $j_{max}$.
\begin{align*}
  J_+ \ket{j, j} = 0 \\
  \implies   J^2\ket{j, j} = [(1/2)(J_+ J_- + J_- J_+) + J_z^2]\ket{j, j} = 0 \tag{Can then transform this however we want} \\
  \left[(1/2)[J_+, J_-] + J_z^2] \ket{j, j} \right] \\
  = (\hbar J_z + J_z^2)\ket{j, j} \\
  = \hbar^2 j(j+1)\ket{j, j}
\end{align*}
Since $J^2$ only 'feels' the left $j$, we can say $J^2\ket{j, m} = \hbar^2 j(j+1)\ket{j, m} $ which lets us see $j$ is the max possible value of $m$. This makes sense; the max of the projection.

Now what is the \textit{minimum} possible value of $m$?
\begin{align*}
  J^2 \ket{j, m_{min}} = &J^2\ket{j, j} = [(1/2)(J_+ J_- + J_- J_+) + J_z^2]\ket{j, j} \\
  = & [-(1/2)[J_+, J_-] + J_z^2]\ket{j, m_{min}} \\
  = & (-\hbar J_z + J_z^2)\ket{j, m_{min}} = \hbar^2 (m_{min}^2 - m_{min}) \ket{j, m_{min}}
\end{align*}
Consolidating our expressions for $J^2$'s action on states, we can find \[
m_{min} (m_{min}-1) = j(j+1)
\] and therefore \[
m_{min} = -j.
\]

We have, then, $m_{max} = +j$, and we can see \[
J_+\ket{j, m} = \xi \ket{j, m+1}
\]
where $\xi$ is a constant dependent on $m$. Furthermore, \[
J_+ \ket{j, -j} \sim \ket{j, -j + 1}.
\]
To get from $\ket{j, -j}$ to $\ket{j, j}$, it only takes an integer number of steps. We have $j - (-j) = n$, and for $n$ even, we have integer $j$. If $n$ is odd, then $2j = n$, $j = n/2$ are integers.
\begin{remark}
  This means that spin comes from classical mathematics, group theory, the full rotation group SO(3) and the Lorentz group. Do \textit{not} say spin is like electrons spinning.. 
\end{remark}

We proved before that
\begin{align*}
  [J_z, J_+] = \hbar J_+ \\
  [J_z, J_-] = - \hbar J_i
\end{align*} which means
\begin{align*}
  J_z J_+\ket{j, m} = & J_+(J_z + \hbar) \ket{j, m} \tag{Using the commutation relation} \\
  = & \hbar (m+1) J_+ \ket{j, m+1}\\
  J_z J_+\ket{j, m} = & \hbar J_+ (m+1)\ket{j, m+1} \\
  J_z J_- \ket{j, m} =& \hbar (m-1)J_- \ket{j, m-1} \\
  J_+ \ket{j, m}= &a_{jm} \ket{j, m+1} \\
  J_i \ket{j, m} = & b_{jm} \ket{j, m-1}
\end{align*}
Let's try and find these constants. We calculate
\begin{align*}
  \norm{J_+ \ket{j, m}}^2 = & \bra{j, m} J_+^\dagger J_+ \ket{j, m} \\
  = & \bra{j, m} J_- J_+ \ket{j, m}
\end{align*}
where we used the fact that
\begin{align*}
  J_+ = J_z + iJ_y \\
  J_+^\dagger = J_x^\dagger - iJ_y^\dagger = Y_x - iJ_y = J_-
\end{align*}
and therefore, using the commutation relations, 
\begin{align*}
  \norm{J_+ \ket{j, m}}^2 = & \bra{j, m} (J^2 - J_z^2 - h\hbar J_z)\ket{j, m} \\
  = & \hbar^2 [j(j+1) - m^2 - m] \\
  = & \hbar^2 [j(j+1) - m(m+1)] \\
  = & \abs{a_{jm}^2} \\
  \implies J_+ \ket{j, m} = & \hbar \sqrt{j(j+1) - m(m+1)} \ket{j, m+1} \\
  = & \hbar \sqrt{(j + m + 1)(j - m)}.
\end{align*} Similarly,
\begin{align*}
  J_-\ket{j, m} = & \hbar \sqrt{j(j+1)-m(m-1)}\ket{j, m-1} \\
  = & \hbar \sqrt{(j-m+1)(j+m)}\ket{j, m-1}
\end{align*}
We can define the matrix elements
\begin{align*}
  \bra{j', m'} J_+ \ket{j, m} = \hbar \delta_{j j'} \delta_{m+1, m'} \sqrt{(j-m)(j+m+1)}
\end{align*} which yields off-diagonal elements. Recall that the different states are orthogonal.
Similarly,
\begin{align*}
  \bra{j', m'} J_- \ket{j, m} = \hbar \delta_{j, j'} \delta_{m+1, m'} \sqrt{(j+m)(j-m+1)}.
\end{align*}
Also note \[
J_x = (J_+ + J_-)/2, \quad J_y = (J_+ - J_-)/2i
\]
\begin{example}
  Let's consider $j = 1/2, m = \pm 1/2$. We calculate
  \begin{align*}
    \bra{1/2, \underbrace{-1/2}_{m'}} J_x \ket{1/2, \underbrace{1/2}_{m}} = \hbar/2 \\
    \bra{1/2, 1/2} J_x \ket{1/2, -1/2} = \hbar 
  \end{align*}
  which follows if you consider $J_+ = 0$ for $m = 1/2$, and $J_- = 1/2$ for $m = 1/2$ (therefore $m' \equiv -1/2$ is required); and the reverse when $m = -1/2.$
  Therefore
  \begin{equation}
    J_x = \frac{\hbar}{2}
    \begin{bmatrix}
      \pmat{x}
    \end{bmatrix} = \frac{\hbar}{2} \sigma_x
  \end{equation}
  Similar,y
  \begin{align*}
    J_y = \frac{\hbar}{2}
    \begin{bmatrix}
      \pmat{y}
    \end{bmatrix} = \frac{\hbar}{2} \sigma_y \\
    J_z = \frac{\hbar}{2} \sigma_z = \frac{\hbar}{2}
    \begin{bmatrix}
      \pmat{z}
    \end{bmatrix}
  \end{align*}
  
\end{example}
\begin{example}
  Let $j = 1, m = -1, 0, 1$.
  \begin{enumerate}
  \item $J = 1, m' = 0$. This requires $m$ to take on certain values. Going through the calculations yields
    \begin{align*}
      J_x = \frac{\hbar}{\sqrt{2}}
      \begin{bmatrix}
        0 & 1 & 0 \\
        1 & 0 & 1 \\
        0 & 1 & 0
      \end{bmatrix} \\
      J_y = \frac{\hbar}{\sqrt{2}}
      \begin{bmatrix}
        0 & -i & 0 \\
        i & 0 & -i \\
        0 & i & 0
      \end{bmatrix} \\
      J_z = \hbar
      \begin{bmatrix}
        1 & 0 & 0 \\
        0 & 0 & 0 \\
        0 & 0 & -1
      \end{bmatrix}
    \end{align*}
    This isn't the usual representation of the SO(3) group generators, but we can show that they are equivalent (similar, up to a unitary transformation). 
  \end{enumerate}
\end{example}

\section{Systems of Identical Particles}

We will talk about ferromagnetism and spin-spin interactions. Our wavefunction has two parameters: position $\vec r$, and spin $\vec s$.

\subsection{Two Particles}

System of indistinguishable particles, with total wavefunction \[
\psi(\xi_1, \xi_2).
\]
When we permute the particles, the observable must be preserved, but nevertheless can introduce a phase factor \[
e^{i\alpha} \psi(\xi_1, \xi_2).
\]
When we permute it twice, we should get back the original system, meaning \[
e^{2i\alpha} = 1
\] which gives $e^{i\alpha} = \pm 1$. Therefore, \[
\psi(\xi_1, \xi_2) = \pm \psi(\xi_2, \xi_1)
\]
\begin{remark}
  There is a field of parastatistics for when the $\alpha$ is general. 
\end{remark}
We say that + corresponds to bosons, and - corresponds to fermions. This gives us the ability to prove the Pauli principle.
\begin{equation*}
  \psi(\xi_1, \xi_1) = -\psi(\xi_1, \xi_1) 
\end{equation*} which means $\psi_f(\xi_1, \xi_1) = 0$ for fermions.
\begin{example}[Two Fermions]
  With $s = 1/2$, we can say
  \begin{align*}
    \psi(\xi_1, \xi_2) = \phi(\vec r_1, \vec r_2) \chi(\vec s_1, \vec s_2)
  \end{align*}
  If $\chi$ is symmetric, then $\phi$ has to be antisymmetric for it to be a fermion. Similarly, if $\chi$ is antisymmetric, $\phi$ must be symmetric for fermions. 
  What does it mean for $\chi$ to be symmetric? We have three situations/states (triplet state):
  \begin{enumerate}[label=(\roman*)]
  \item $\ket{\uparrow, \uparrow}$ with spin 1.
  \item $\ket{\downarrow, \downarrow}$ with spin -1.
  \item $\frac{1}{\sqrt{2} \ket{\uparrow \downarrow} + \ket{\downarrow \uparrow}}$ with spin 0. 
  \end{enumerate}

  However, if $\chi$ is antisymmetric, we only have a singlet state: \[
\frac{1}{\sqrt{2}} (\ket{\uparrow \downarrow} - \ket{downarrow \uparrow}) 
\] with spin 0.
\begin{remark}
  In BCS superconductivity, we talk about pairs of electrons with opposite spins. Superfluid He-3 has boson states (singlet and triplet), Discovered in 1970-something. Spin-spin interaction models such as the Ising, Hubbard models serve as microscopic theory. 
\end{remark}
When we find the expectation value, we'll find non-classical effects. Exchange interaction is a pure quantum effect. 

For relatively weak coupling, we can write 
\begin{equation}
  \phi(\vec r_1, \vec r_2) = \frac{1}{\sqrt{2}}(\phi(\vec r_1) \phi(\vec r_2) \pm \phi(\vec r_2) \phi(\vec r_1)).
\end{equation}

We say we have a central force $U(\abs{\vec r_1 - \vec r_2})$, though the general case is relatively similar qualitatively.

\begin{align*}
  \exp{U} = \int \phi^* (\vec r_1, \vec r_2) U(\abs{\vec r_1 - \vec r_2}) \phi(\vec r_1, \vec r_2) d^3 r_1 d^3 r_2 \\
  = \frac{1}{2} \int [\phi_1^*(\vec r_1) \phi^*_2(\vec r_2) \pm \phi_1^*(\vec r_2) \phi^*(\vec r_2)] U(\abs{\vec r_1 - \vec r_2})[\phi_1(\vec r_1) \phi_2(\vec r_2) \pm \phi_1(\vec r_2) \phi_2(\vec r_1)] d^3 r_1 d^3 r_2 \\
  = \frac{1}{2}\int [\abs{\phi_1(\vec r_1)}^2 \abs{\phi_2(\vec r_2)}^2 + \abs{\phi_1(\vec r_2)}^2 \abs{\phi_2(\vec r_1)}^2 U(\abs{\vec r_1 - \vec r_2})] d^3 r_1 d^3 r_2\\
  \pm \frac{1}{2} \int U(\abs{\vec r_1 - \vec r_2}) [\phi_1^* (\vec r_1) \phi^*_2 (\vec r_2) \phi_1(\vec r_2) \phi_2(\vec r_1) + \phi^*_1(\vec r_2)\phi_2^*(\vec r_1) \phi_1(\vec r_1) \phi_2(\vec r_2)] d^3 r_1 d^3 r_2 \\
  = \underbrace{\int \abs{\phi_1(\vec r_1)}^2 \abs{\phi_2(\vec r_2)}^2 U(\abs{\vec r_1 - \vec r_2}) d^3 r_1 d^3 r_2}_{= E \text{ exp. val. of en. w.r.t. joint dist.}} \\
  \pm \underbrace{\int \phi_1^* (\vec r_1) \phi_1(\vec r_2) \phi_1^*(\vec r_2) \phi_2(\vec r_1) U(\abs{r_1 - r_2}) d^3 r_1 d^3 r_3}_{J}
\end{align*}
$E$ corresponds to the classical energy, whereas the second term $J$ corresponds to the exchange energy/integral, depending on the symmetrization/antisymmetrization of the spatial part of the wavefunction.
\begin{remark}
  We can see this splitting in experiments. If spin is singlet state, then spatial wavefunction is symmetric, and we get $+J$. If spin is in triplet states, it's symmetric, and we get $-J$. 
  We haven't even considered the spin-spin interactions yet, 
\end{remark}
Let us consider the exchange interaction more. Let's model the spin-spin interaction: \[
V_{exchange} = -\frac{1}{2} J(1 + 4 \vec s_1 \cdot \vec s_2)
\]
which is like the Ising model, Heisenberg model.
Let total spin $\vec s = \vec s_1 + \vec s_2$. Then
\begin{align*}
  S^2 = (\vec S_1 + \vec S_2)^2 \\
  = S_1^2 + S_2^2 + 2 \vec S_1 \cdot \vec S_2 \tag{They commute b.c. diff. particles} \\
  = \vec S_1 \cdot \vec S_2 = \frac{1}{2} [S^2 - S_1^2 - S_2^2] 
\end{align*}
Since we know the eigenvalues of those operators, we can write
\begin{align*}
  4 \vec S_1 \cdot \vec S_2 = 2 \frac{1}{2} [s(s+1) - s_1 (s_1 + 1) - s_2(s_2 + 1)] 
\end{align*}
We have a few cases. For $s_1 = 1/2, s_2 = 1/2$, we can have $s = 0, s = 1$. Plugging in, we get for $s = 0, 4 s_1 \cdot s_2 = +J$, and for $s = 1, 4 s_1 \cdot s_2 = -J$. 
\end{example}

\begin{example}[Ising Model]
  We write the Hamiltonian \[
H = J \sum \vec S_i \cdot \vec S_{i+1}
\]
\begin{remark}[Heisenberg Model]
  This model has all spins in a chain interact with each other; \[
\sum_{i \neq k} \vec s_i \cdot \vec s_k J_{ik}
\], although this isn't always necessary because the integrals drop off quickly. 
\end{remark}
For weak-couplings, we can assume the Ising model instead of the Heisenberg model.
\begin{remark}[2D]
  For 2D Ising model, we do have a phase transition depending on temperature - paramagnetic vs ferromagnetic. 
\end{remark}

\end{example}

\subsection{Multiparticle}

Determinants must be antisymmetrized?


% Note to self: Make a shortcut for writing ^\dagger (C-c d maybe)
% OR you can just write ^+ and search/replace later
\end{document} % END

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
